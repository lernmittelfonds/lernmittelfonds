ALTER TABLE `klasse` 
    ADD COLUMN `MetaKey` VARCHAR(45) NULL DEFAULT NULL AFTER `EndKlassenStufe`;

ALTER TABLE `schueler` 
ADD COLUMN `eisStatus` TINYINT(1) NOT NULL DEFAULT 0 AFTER `istElternvertreter`;

ALTER TABLE `schueler` 
CHANGE COLUMN `Anmerkungen` `Anmerkungen` VARCHAR(1000) NULL DEFAULT '' ;

ALTER TABLE `eltern` 
ADD COLUMN `Freigabe_lmf` TINYINT(1) NULL DEFAULT NULL AFTER `Freigabe_info`;

update eltern
set Freigabe_lmf=1 where FamilienId in
( SELECT FamilienId FROM view_schueler_dieses_jahr where bezahlt=1);

ALTER TABLE `eltern` 
    ADD COLUMN `initCode` VARCHAR(14) NULL DEFAULT NULL ;