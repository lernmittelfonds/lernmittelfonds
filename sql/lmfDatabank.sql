CREATE DATABASE  IF NOT EXISTS `lmfDatabaseName` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `lmfDatabaseName`;

-- phpMyAdmin SQL Dump
-- version 4.0.0
-- http://www.phpmyadmin.net
--
-- Erstellungszeit: 26. Mai 2015 um 22:35
-- Server Version: 5.1.73-log
-- PHP-Version: 5.4.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `anmeldung`
--

CREATE TABLE IF NOT EXISTS `anmeldung` (
  `anmeldungsId` int(11) NOT NULL AUTO_INCREMENT,
  `SchuelerId` int(10) NOT NULL,
  `Schuljahr` int(4) NOT NULL,
  `bezahlt` int(1) DEFAULT NULL,
  `KlassenId` int(11) NOT NULL,
  PRIMARY KEY (`anmeldungsId`),
  UNIQUE KEY `Secondary` (`SchuelerId`,`Schuljahr`),
  KEY `klassenId` (`KlassenId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ausgeliehen`
--

CREATE TABLE IF NOT EXISTS `ausgeliehen` (
  `schuelerId` int(11) NOT NULL,
  `buchId` int(11) NOT NULL,
  `eingesammelt` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`schuelerId`,`buchId`),
  KEY `fk_schueler_ausgeliehen` (`schuelerId`),
  KEY `fk_ausgeliehen_buecher` (`buchId`),
  KEY `fk_ausgeliehen_schueler` (`schuelerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `buecher`
--

CREATE TABLE IF NOT EXISTS `buecher` (
  `BuchId` int(11) NOT NULL AUTO_INCREMENT,
  `Isbn` varchar(45) NOT NULL DEFAULT 'ISBN',
  `Titel` varchar(45) NOT NULL DEFAULT 'Titel',
  `von` int(2) NOT NULL DEFAULT '7',
  `bis` int(2) NOT NULL DEFAULT '0',
  `Neupreis` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`BuchId`),
  UNIQUE KEY `buchId_UNIQUE` (`BuchId`),
  UNIQUE KEY `isbn_UNIQUE` (`Isbn`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `buecherinlisten`
--

CREATE TABLE IF NOT EXISTS `buecherinlisten` (
  `buchId` int(11) NOT NULL,
  `listenId` int(11) NOT NULL,
  `preisImJahr` decimal(5,2) DEFAULT '0.00',
  PRIMARY KEY (`buchId`,`listenId`),
  KEY `fkBuchId` (`buchId`),
  KEY `fklistenid` (`listenId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `buecherlisten`
--

CREATE TABLE IF NOT EXISTS `buecherlisten` (
  `listenId` int(11) NOT NULL AUTO_INCREMENT,
  `jahr` int(4) NOT NULL DEFAULT '2013',
  `klassenStufe` int(11) DEFAULT NULL,
  `sprache` varchar(7) COLLATE latin1_german2_ci DEFAULT NULL,
  `beitrag` decimal(5,2) DEFAULT '0.00',
  `teilnehmerBezahlt` int(3) DEFAULT '0',
  `teilnehmerZahlungsBefreit` int(3) DEFAULT '0',
  PRIMARY KEY (`listenId`),
  UNIQUE KEY `beitragId_UNIQUE` (`listenId`),
  UNIQUE KEY `unique2` (`jahr`,`klassenStufe`,`sprache`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `check_tmp`
--
CREATE TABLE IF NOT EXISTS `check_tmp` (
`prio` varchar(1)
,`einzahlungsId` int(11)
,`Buchungstag` varchar(45)
,`Valutatag` varchar(45)
,`Buchungstext` varchar(45)
,`Verwendungszweck` varchar(90)
,`Absender` varchar(45)
,`Betrag` varchar(10)
,`Konto` varchar(10)
,`BLZ` varchar(8)
,`anmeldungsId` int(11)
,`anMeldId` int(11)
,`KlassenId` int(11)
,`ZahlungsKey` varchar(62)
,`schuelerName` varchar(61)
,`elternName` varchar(61)
);
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `constants`
--

CREATE TABLE IF NOT EXISTS `constants` (
  `key` varchar(45) NOT NULL,
  `intValue` int(11) DEFAULT NULL,
  `strValue` varchar(45) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `einzahlungen`
--

CREATE TABLE IF NOT EXISTS `einzahlungen` (
  `einzahlungsId` int(11) NOT NULL AUTO_INCREMENT,
  `Buchungstag` varchar(45) DEFAULT NULL,
  `Valutatag` varchar(45) DEFAULT NULL,
  `Buchungstext` varchar(45) DEFAULT NULL,
  `Verwendungszweck` varchar(90) DEFAULT NULL,
  `Absender` varchar(45) DEFAULT NULL,
  `Betrag` varchar(10) DEFAULT NULL,
  `Konto` varchar(10) DEFAULT NULL,
  `BLZ` varchar(8) DEFAULT NULL,
  `anmeldungsId` int(11) DEFAULT NULL,
  `verworfen` int(1) DEFAULT NULL,
  PRIMARY KEY (`einzahlungsId`),
  UNIQUE KEY `anmeldunsId_UNIQUE` (`anmeldungsId`),
  UNIQUE KEY `unique_Checker` (`Buchungstag`,`Valutatag`,`Buchungstext`,`Verwendungszweck`,`Absender`,`Betrag`,`Konto`,`BLZ`),
  KEY `FKanmeldung` (`anmeldungsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Trigger `einzahlungen`
--
DROP TRIGGER IF EXISTS `tr_einzahlungen`;
DELIMITER //
CREATE TRIGGER `tr_einzahlungen` AFTER UPDATE ON `einzahlungen`
 FOR EACH ROW BEGIN
   IF OLD.anmeldungsId IS NOT NULL THEN
       UPDATE anmeldung SET bezahlt=0 WHERE anmeldung.anmeldungsId=OLD.anmeldungsId;
   END IF  ;         
   IF NEW.anmeldungsId IS NOT NULL THEN
       UPDATE anmeldung SET bezahlt=1 WHERE anmeldung.anmeldungsId=NEW.anmeldungsId ;
   END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `eltern`
--

CREATE TABLE IF NOT EXISTS `eltern` (
  `FamilienId` int(10) NOT NULL AUTO_INCREMENT,
  `Nachname` varchar(30) NOT NULL,
  `Vorname` varchar(30) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Telefon` varchar(20) NOT NULL,
  `Anschrift` varchar(75) DEFAULT NULL,
  `Passwort` varchar(40) NOT NULL,
  `Activation` varchar(40) DEFAULT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `zuzahlungsBefreit` tinyint(1) DEFAULT '0',
  `registriertSeit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stopMail` tinyint(1) DEFAULT '0',
  `Freigabe_alle` tinyint(1) DEFAULT NULL,
  `Freigabe_ev` tinyint(1) DEFAULT NULL,
  `Freigabe_info` tinyint(1) DEFAULT NULL,
  `Freigabe_lmf` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`FamilienId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `klasse`
--

CREATE TABLE IF NOT EXISTS `klasse` (
  `KlassenId` int(11) NOT NULL AUTO_INCREMENT,
  `StartJahr` int(4) NOT NULL,
  `StartKlassenStufe` int(2) NOT NULL,
  `SubKlasse` varchar(5) DEFAULT NULL,
  `Sprache` varchar(7) DEFAULT NULL,
  `EndKlassenStufe` int(2) NOT NULL DEFAULT '1',
  `MetaKey` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`KlassenId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Trigger `klasse`
--
DROP TRIGGER IF EXISTS `tr_klasse_delete`;
DELIMITER //
CREATE TRIGGER `tr_klasse_delete` AFTER DELETE ON `klasse`
 FOR EACH ROW BEGIN
   declare kl_index int default 0;
   SET kl_index := 0;
   WHILE OLD.StartKlassenStufe+kl_index <= OLD.EndKlassenStufe do
        IF (SELECT COUNT(*) FROM `klasse` k
            WHERE OLD.StartJahr-k.StartJahr = OLD.StartKlassenStufe- k.StartKlassenStufe 
            AND OLD.StartKlassenStufe + kl_index <= k.EndKlassenStufe
            AND OLD.StartKlassenStufe + kl_index >= k.StartKlassenStufe
            AND OLD.Sprache=k.Sprache) = 0
        THEN
            DELETE FROM buecherlisten
            WHERE buecherlisten.sprache=OLD.Sprache
            AND buecherlisten.klassenStufe = OLD.StartKlassenStufe + kl_index
            AND buecherlisten.jahr = OLD.StartJahr+ kl_index;
         END IF;   
         set kl_index := kl_index + 1;
    END WHILE; 
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `tr_klasse_insert`;
DELIMITER //
CREATE TRIGGER `tr_klasse_insert` AFTER INSERT ON `klasse`
 FOR EACH ROW BEGIN
   declare kl_index int default 0;
   SET kl_index := 0;
   WHILE NEW.StartKlassenSTUFE+kl_index <= NEW.EndKlassenSTUFE do
        INSERT IGNORE INTO `buecherlisten` 
        (jahr, klassenStufe, sprache) 
        VALUES (NEW.StartJahr+kl_index,NEW.StartKlassenSTUFE+kl_index,new.SPRACHE);
        set kl_index := kl_index + 1;
    END while; 
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `tr_klasse_update`;
DELIMITER //
CREATE TRIGGER `tr_klasse_update` AFTER UPDATE ON `klasse`
 FOR EACH ROW BEGIN
   declare kl_index int default 0;

   IF OLD.Sprache != NEW.Sprache 
   	OR OLD.StartJahr-OLD.StartKlassenStufe != NEW.StartJahr-NEW.StartKlassenStufe
   THEN #komplett alles weg und neu
	   SET kl_index := 0;
   	WHILE OLD.StartKlassenStufe+kl_index <= OLD.EndKlassenStufe do
	        IF (SELECT COUNT(*) FROM `klasse` k
	            WHERE OLD.StartJahr-k.StartJahr = OLD.StartKlassenStufe- k.StartKlassenStufe 
	            AND OLD.StartKlassenStufe + kl_index <= k.EndKlassenStufe
	            AND OLD.StartKlassenStufe + kl_index >= k.StartKlassenStufe
	            AND OLD.Sprache=k.Sprache) = 0
	        THEN
	            DELETE FROM buecherlisten
	            WHERE buecherlisten.sprache=OLD.Sprache
	            AND buecherlisten.klassenStufe = OLD.StartKlassenSTUFE + kl_index
	            AND buecherlisten.jahr = OLD.StartJahr+ kl_index;
	         END IF;   
	         set kl_index := kl_index + 1;
	    END WHILE; 
	    SET kl_index := 0;
	    WHILE NEW.StartKlassenStufe+kl_index <= NEW.EndKlassenStufe do
	        INSERT IGNORE INTO `buecherlisten` 
	        (jahr, klassenStufe, sprache) 
	        VALUES (NEW.StartJahr+kl_index,NEW.StartKlassenStufe+kl_index,new.SPRACHE);
	        set kl_index := kl_index + 1;
	    END while; 
    ELSE
       SET kl_index := 0; # loesche von alt Start bis neuen Start falls der alte vor dem neuen liegt
       WHILE OLD.StartKlassenStufe+kl_index < NEW.StartKlassenStufe do
	        IF (SELECT COUNT(*) FROM `klasse` k
	            WHERE OLD.StartJahr-k.StartJahr = OLD.StartKlassenStufe- k.StartKlassenStufe 
	            AND OLD.StartKlassenStufe + kl_index <= k.EndKlassenStufe
	            AND OLD.StartKlassenStufe + kl_index >= k.StartKlassenStufe
	            AND OLD.Sprache=k.Sprache) = 0
	        THEN
	            DELETE FROM buecherlisten
	            WHERE buecherlisten.sprache=OLD.Sprache
	            AND buecherlisten.klassenStufe = OLD.StartKlassenSTUFE + kl_index
	            AND buecherlisten.jahr = OLD.StartJahr+ kl_index;
	         END IF;   
	         set kl_index := kl_index + 1;
	    END WHILE; 
       SET kl_index := 1; # loesche vom neuen Ende bis zum alten Ende falls das neue vor dem alten liegt
       WHILE NEW.EndKlassenStufe+kl_index <= OLD.EndKlassenStufe do
	        IF (SELECT COUNT(*) FROM `klasse` k
	            WHERE OLD.StartJahr-k.StartJahr = OLD.StartKlassenStufe- k.StartKlassenStufe 
	            AND OLD.StartKlassenStufe + kl_index <= k.EndKlassenStufe
	            AND OLD.StartKlassenStufe + kl_index >= k.StartKlassenStufe
	            AND OLD.Sprache=k.Sprache) = 0
	        THEN
	            DELETE FROM buecherlisten
	            WHERE buecherlisten.sprache=OLD.Sprache
	            AND buecherlisten.klassenStufe = OLD.StartKlassenSTUFE + kl_index
	            AND buecherlisten.jahr = OLD.StartJahr+ kl_index;
	         END IF;   
	         set kl_index := kl_index + 1;
	    END WHILE; 
	    SET kl_index := 0; # fuege neue ein, falls es sie noch nicht gibt
	    WHILE NEW.StartKlassenStufe+kl_index <= NEW.EndKlassenStufe do
	        INSERT IGNORE INTO `buecherlisten` 
	        (jahr, klassenStufe, sprache) 
	        VALUES (NEW.StartJahr+kl_index,NEW.StartKlassenStufe+kl_index,new.SPRACHE);
	        set kl_index := kl_index + 1;
	    END while; 
    END IF; 
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `schueler`
--

CREATE TABLE IF NOT EXISTS `schueler` (
  `SchuelerId` int(8) NOT NULL AUTO_INCREMENT,
  `FamilienId` int(10) NOT NULL,
  `Nachname` varchar(30) NOT NULL,
  `Vorname` varchar(30) NOT NULL,
  `Anmerkungen` varchar(1000) DEFAULT '',
  `istElternvertreter` tinyint(1) DEFAULT NULL,
  `eisStatus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SchuelerId`),
  KEY `FamilienId` (`FamilienId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `texte`
--

CREATE TABLE IF NOT EXISTS `texte` (
  `code` varchar(20) NOT NULL,
  `text` varchar(5000) NOT NULL DEFAULT '',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_analyse_buch_ausgabe`
--
CREATE TABLE IF NOT EXISTS `view_analyse_buch_ausgabe` (
`anmeldungsId` int(11)
,`jahr` int(4)
,`KlassenId` int(11)
,`bezahlt` int(1)
,`beitrag` decimal(5,2)
,`klassenStufe` int(11)
,`sprache` varchar(7)
,`SchuelerId` int(8)
,`sVorname` varchar(30)
,`sNachname` varchar(30)
,`FamilienId` int(10)
,`eVorname` varchar(30)
,`eNachname` varchar(30)
,`BuchId` int(11)
,`Isbn` varchar(45)
,`Titel` varchar(45)
,`von` int(2)
,`bis` int(2)
,`Neupreis` float
,`auszugeben` int(1)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_analyse_buch_rueckgabe`
--
CREATE TABLE IF NOT EXISTS `view_analyse_buch_rueckgabe` (
`anmeldungsId` int(11)
,`SchuelerId` int(10)
,`Schuljahr` int(4)
,`bezahlt` int(1)
,`KlassenId` int(11)
,`bezahltNeu` int(1)
,`KlassenIdneu` int(11)
,`Stufe` bigint(13)
,`SubKlasse` varchar(5)
,`Sprache` varchar(7)
,`StufeNeu` bigint(14)
,`eingesammelt` tinyint(1)
,`BuchId` int(11)
,`Isbn` varchar(45)
,`Titel` varchar(45)
,`von` int(2)
,`bis` int(2)
,`Neupreis` float
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_anmeldungen_altes_jahr`
--
CREATE TABLE IF NOT EXISTS `view_anmeldungen_altes_jahr` (
`anmeldungsId` int(11)
,`jahr` int(4)
,`KlassenId` int(11)
,`bezahlt` int(1)
,`beitrag` decimal(5,2)
,`klassenStufe` int(11)
,`sprache` varchar(7)
,`SchuelerId` int(8)
,`sVorname` varchar(30)
,`sNachname` varchar(30)
,`FamilienId` int(10)
,`eVorname` varchar(30)
,`eNachname` varchar(30)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_anmeldungen_alt_und_neu`
--
CREATE TABLE IF NOT EXISTS `view_anmeldungen_alt_und_neu` (
`anmeldungsId` int(11)
,`SchuelerId` int(10)
,`Schuljahr` int(4)
,`bezahlt` int(1)
,`KlassenId` int(11)
,`bezahltNeu` int(1)
,`KlassenIdneu` int(11)
,`Stufe` bigint(13)
,`SubKlasse` varchar(5)
,`Sprache` varchar(7)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_anmeldungen_neues_jahr`
--
CREATE TABLE IF NOT EXISTS `view_anmeldungen_neues_jahr` (
`anmeldungsId` int(11)
,`jahr` int(4)
,`KlassenId` int(11)
,`bezahlt` int(1)
,`beitrag` decimal(5,2)
,`klassenStufe` int(11)
,`sprache` varchar(7)
,`SchuelerId` int(8)
,`sVorname` varchar(30)
,`sNachname` varchar(30)
,`FamilienId` int(10)
,`eVorname` varchar(30)
,`eNachname` varchar(30)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_bezahlt`
--
CREATE TABLE IF NOT EXISTS `view_bezahlt` (
`Klassenid` int(11)
,`bezahlt` bigint(21)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_buecherlisten`
--
CREATE TABLE IF NOT EXISTS `view_buecherlisten` (
`listenId` int(11)
,`jahr` int(4)
,`klassenStufe` int(11)
,`sprache` varchar(7)
,`beitrag` decimal(5,2)
,`teilnehmerBezahlt` int(3)
,`teilnehmerZahlungsBefreit` int(3)
,`buchId` int(11)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_buecher_in_klassen`
--
CREATE TABLE IF NOT EXISTS `view_buecher_in_klassen` (
`buchid` int(11)
,`klassenid` int(11)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_check_einzahlungen`
--
CREATE TABLE IF NOT EXISTS `view_check_einzahlungen` (
`prio` varchar(1)
,`einzahlungsId` int(11)
,`Buchungstag` varchar(45)
,`Valutatag` varchar(45)
,`Buchungstext` varchar(45)
,`Verwendungszweck` varchar(90)
,`Absender` varchar(45)
,`Betrag` varchar(10)
,`Konto` varchar(10)
,`BLZ` varchar(8)
,`anmeldungsId` int(11)
,`anMeldId` int(11)
,`KlassenId` int(11)
,`ZahlungsKey` varbinary(28)
,`schuelerName` varchar(61)
,`elternName` varchar(61)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_klassen_altes_jahr`
--
CREATE TABLE IF NOT EXISTS `view_klassen_altes_jahr` (
`KlassenId` int(11)
,`Stufe` bigint(13)
,`SubKlasse` varchar(5)
,`Sprache` varchar(7)
,`MetaKey` varchar(45)
,`Jahr` int(11)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_klassen_kurz`
--
CREATE TABLE IF NOT EXISTS `view_klassen_kurz` (
`Klassenstufe` bigint(13)
,`KlassenId` int(11)
,`StartJahr` int(4)
,`StartKlassenStufe` int(2)
,`SubKlasse` varchar(5)
,`Sprache` varchar(7)
,`EndKlassenStufe` int(2)
,`MetaKey` varchar(45)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_klassen_lang`
--
CREATE TABLE IF NOT EXISTS `view_klassen_lang` (
`KlassenStufeAlt` varchar(34)
,`KlassenStufeNeu` varchar(34)
,`OrderAlt` varchar(18)
,`OrderNeu` varchar(18)
,`Klassenstufe` bigint(13)
,`KlassenId` int(11)
,`StartJahr` int(4)
,`StartKlassenStufe` int(2)
,`SubKlasse` varchar(5)
,`Sprache` varchar(7)
,`EndKlassenStufe` int(2)
,`MetaKey` varchar(45)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_klassen_neues_jahr`
--
CREATE TABLE IF NOT EXISTS `view_klassen_neues_jahr` (
`KlassenId` int(11)
,`Stufe` bigint(14)
,`SubKlasse` varchar(5)
,`Sprache` varchar(7)
,`Jahr` bigint(12)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_klassen_uebersicht`
--
CREATE TABLE IF NOT EXISTS `view_klassen_uebersicht` (
`schuelerName` varchar(62)
,`elternName` varchar(62)
,`FamilienId` int(10)
,`Telefon` varchar(20)
,`email` varchar(50)
,`bezahlt` int(1)
,`klassenId` int(11)
,`schuelerId` int(8)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_klassen_uebersicht2`
--
CREATE TABLE IF NOT EXISTS `view_klassen_uebersicht2` (
`schuelerId` int(8)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_klassen_uebersicht3`
--
CREATE TABLE IF NOT EXISTS `view_klassen_uebersicht3` (
`schuelerName` varchar(62)
,`elternName` varchar(62)
,`FamilienId` int(10)
,`Telefon` varchar(20)
,`email` varchar(50)
,`bezahlt` int(1)
,`klassenId` int(11)
,`schuelerId` int(8)
,`altAngemeldet` int(8)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_schueler_dieses_jahr`
--
CREATE TABLE IF NOT EXISTS `view_schueler_dieses_jahr` (
`SchuelerId` int(8)
,`FamilienId` int(10)
,`Nachname` varchar(30)
,`Vorname` varchar(30)
,`Anmerkungen` varchar(1000)
,`istElternvertreter` tinyint(1)
,`EisStatus` tinyint(1)
,`KlassenId` int(11)
,`Stufe` bigint(13)
,`SubKlasse` varchar(5)
,`Sprache` varchar(7)
,`Jahr` int(11)
,`MetaKey` varchar(45)
,`anmeldungsId` int(11)
,`bezahlt` int(1)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_schueler_naechstes_jahr`
--
CREATE TABLE IF NOT EXISTS `view_schueler_naechstes_jahr` (
`SchuelerId` int(8)
,`FamilienId` int(10)
,`Nachname` varchar(30)
,`Vorname` varchar(30)
,`Anmerkungen` varchar(1000)
,`istElternvertreter` tinyint(1)
,`KlassenId` int(11)
,`Stufe` bigint(14)
,`SubKlasse` varchar(5)
,`Sprache` varchar(7)
,`Jahr` bigint(12)
,`anmeldungsId` int(11)
,`bezahlt` int(1)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_uebersicht`
--
CREATE TABLE IF NOT EXISTS `view_uebersicht` (
`KlassenId` int(11)
,`Stufe` bigint(14)
,`SubKlasse` varchar(5)
,`Sprache` varchar(7)
,`bezahlt` bigint(20)
,`unbezahlt` bigint(20)
,`gesamt` bigint(21)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_unbezahlt`
--
CREATE TABLE IF NOT EXISTS `view_unbezahlt` (
`Klassenid` int(11)
,`unbezahlt` bigint(21)
);
-- --------------------------------------------------------

--
-- Struktur des Views `check_tmp`
--
DROP TABLE IF EXISTS `check_tmp`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `check_tmp` AS select '1' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`c`.`strValue`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`s`.`Vorname`,' ',`s`.`Nachname`) AS `schuelerName`,concat(`el`.`Vorname`,' ',`el`.`Nachname`) AS `elternName` from ((((`anmeldung` `a` join `einzahlungen` `e`) join `schueler` `s`) join `eltern` `el`) join `constants` `c`) where (isnull(`e`.`anmeldungsId`) and (`c`.`key` = 'thisYear') and (`el`.`FamilienId` = `s`.`FamilienId`) and (`s`.`SchuelerId` = `a`.`SchuelerId`) and (`a`.`Schuljahr` = (`c`.`intValue` + 1)) and (`a`.`bezahlt` = 0) and (`e`.`Verwendungszweck` like concat('%LMF-',`c`.`strValue`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A%'))) union select '2' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`c`.`strValue`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`s`.`Vorname`,' ',`s`.`Nachname`) AS `schuelerName`,concat(`el`.`Vorname`,' ',`el`.`Nachname`) AS `elternName` from ((((`anmeldung` `a` join `einzahlungen` `e`) join `schueler` `s`) join `eltern` `el`) join `constants` `c`) where (isnull(`e`.`anmeldungsId`) and (`c`.`key` = 'thisYear') and (`el`.`FamilienId` = `s`.`FamilienId`) and (`s`.`SchuelerId` = `a`.`SchuelerId`) and (`a`.`Schuljahr` = (`c`.`intValue` + 1)) and (`a`.`bezahlt` = 0) and (`e`.`Verwendungszweck` like concat('%',`s`.`Nachname`,'%')) and (`e`.`Verwendungszweck` like concat('%',`s`.`Vorname`,'%'))) union select '3' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`c`.`strValue`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`s`.`Vorname`,' ',`s`.`Nachname`) AS `schuelerName`,concat(`el`.`Vorname`,' ',`el`.`Nachname`) AS `elternName` from ((((`anmeldung` `a` join `einzahlungen` `e`) join `schueler` `s`) join `eltern` `el`) join `constants` `c`) where (isnull(`e`.`anmeldungsId`) and (`c`.`key` = 'thisYear') and (`el`.`FamilienId` = `s`.`FamilienId`) and (`s`.`SchuelerId` = `a`.`SchuelerId`) and (`a`.`Schuljahr` = (`c`.`intValue` + 1)) and (`a`.`bezahlt` = 0) and (`e`.`Verwendungszweck` like concat('%',`s`.`Nachname`,'%'))) union select '4' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`c`.`strValue`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`s`.`Vorname`,' ',`s`.`Nachname`) AS `schuelerName`,concat(`el`.`Vorname`,' ',`el`.`Nachname`) AS `elternName` from ((((`anmeldung` `a` join `einzahlungen` `e`) join `schueler` `s`) join `eltern` `el`) join `constants` `c`) where (isnull(`e`.`anmeldungsId`) and (`c`.`key` = 'thisYear') and (`el`.`FamilienId` = `s`.`FamilienId`) and (`s`.`SchuelerId` = `a`.`SchuelerId`) and (`a`.`Schuljahr` = (`c`.`intValue` + 1)) and (`a`.`bezahlt` = 0) and (`e`.`Absender` like concat('%',`el`.`Nachname`,'%')) and (`e`.`Absender` like concat('%',`el`.`Vorname`,'%'))) union select '5' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`c`.`strValue`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`s`.`Vorname`,' ',`s`.`Nachname`) AS `schuelerName`,concat(`el`.`Vorname`,' ',`el`.`Nachname`) AS `elternName` from ((((`anmeldung` `a` join `einzahlungen` `e`) join `schueler` `s`) join `eltern` `el`) join `constants` `c`) where (isnull(`e`.`anmeldungsId`) and (`c`.`key` = 'thisYear') and (`el`.`FamilienId` = `s`.`FamilienId`) and (`s`.`SchuelerId` = `a`.`SchuelerId`) and (`a`.`Schuljahr` = (`c`.`intValue` + 1)) and (`a`.`bezahlt` = 0) and (`e`.`Absender` like concat('%',`el`.`Nachname`,'%'))) union select '6' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`c`.`strValue`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`s`.`Vorname`,' ',`s`.`Nachname`) AS `schuelerName`,concat(`el`.`Vorname`,' ',`el`.`Nachname`) AS `elternName` from ((((`anmeldung` `a` join `einzahlungen` `e`) join `schueler` `s`) join `eltern` `el`) join `constants` `c`) where (isnull(`e`.`anmeldungsId`) and (`c`.`key` = 'thisYear') and (`el`.`FamilienId` = `s`.`FamilienId`) and (`s`.`SchuelerId` = `a`.`SchuelerId`) and (`a`.`Schuljahr` = (`c`.`intValue` + 1)) and (`a`.`bezahlt` = 0) and (`e`.`Absender` like concat('%',`s`.`Nachname`,'%'))) union select '7' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`c`.`strValue`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`s`.`Vorname`,' ',`s`.`Nachname`) AS `schuelerName`,concat(`el`.`Vorname`,' ',`el`.`Nachname`) AS `elternName` from ((((`anmeldung` `a` join `einzahlungen` `e`) join `schueler` `s`) join `eltern` `el`) join `constants` `c`) where (isnull(`e`.`anmeldungsId`) and (`c`.`key` = 'thisYear') and (`el`.`FamilienId` = `s`.`FamilienId`) and (`s`.`SchuelerId` = `a`.`SchuelerId`) and (`a`.`Schuljahr` = (`c`.`intValue` + 1)) and (`a`.`bezahlt` = 0) and (`e`.`Verwendungszweck` like concat('%',`el`.`Nachname`,'%')));

-- --------------------------------------------------------

--
-- Struktur des Views `view_analyse_buch_ausgabe`
--
DROP TABLE IF EXISTS `view_analyse_buch_ausgabe`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_analyse_buch_ausgabe` AS select `a`.`anmeldungsId` AS `anmeldungsId`,`a`.`jahr` AS `jahr`,`a`.`KlassenId` AS `KlassenId`,`a`.`bezahlt` AS `bezahlt`,`a`.`beitrag` AS `beitrag`,`a`.`klassenStufe` AS `klassenStufe`,`a`.`sprache` AS `sprache`,`a`.`SchuelerId` AS `SchuelerId`,`a`.`sVorname` AS `sVorname`,`a`.`sNachname` AS `sNachname`,`a`.`FamilienId` AS `FamilienId`,`a`.`eVorname` AS `eVorname`,`a`.`eNachname` AS `eNachname`,`b`.`BuchId` AS `BuchId`,`b`.`Isbn` AS `Isbn`,`b`.`Titel` AS `Titel`,`b`.`von` AS `von`,`b`.`bis` AS `bis`,`b`.`Neupreis` AS `Neupreis`,if((isnull(`ag`.`buchId`) or (`ag`.`eingesammelt` = 1)),1,0) AS `auszugeben` from (((`view_anmeldungen_neues_jahr` `a` join `view_buecherlisten` `bl` on(((`a`.`jahr` = `bl`.`jahr`) and (`a`.`sprache` = `bl`.`sprache`) and (`a`.`klassenStufe` = `bl`.`klassenStufe`)))) join `buecher` `b` on((`b`.`BuchId` = `bl`.`buchId`))) left join `ausgeliehen` `ag` on(((`bl`.`buchId` = `ag`.`buchId`) and (`a`.`SchuelerId` = `ag`.`schuelerId`)))) where (`a`.`bezahlt` >= 0);

-- --------------------------------------------------------

--
-- Struktur des Views `view_analyse_buch_rueckgabe`
--
DROP TABLE IF EXISTS `view_analyse_buch_rueckgabe`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_analyse_buch_rueckgabe` AS select `a`.`anmeldungsId` AS `anmeldungsId`,`a`.`SchuelerId` AS `SchuelerId`,`a`.`Schuljahr` AS `Schuljahr`,`a`.`bezahlt` AS `bezahlt`,`a`.`KlassenId` AS `KlassenId`,`a`.`bezahltNeu` AS `bezahltNeu`,`a`.`KlassenIdneu` AS `KlassenIdneu`,`a`.`Stufe` AS `Stufe`,`a`.`SubKlasse` AS `SubKlasse`,`a`.`Sprache` AS `Sprache`,`k`.`Stufe` AS `StufeNeu`,`al`.`eingesammelt` AS `eingesammelt`,`b`.`BuchId` AS `BuchId`,`b`.`Isbn` AS `Isbn`,`b`.`Titel` AS `Titel`,`b`.`von` AS `von`,`b`.`bis` AS `bis`,`b`.`Neupreis` AS `Neupreis` from (((`view_anmeldungen_alt_und_neu` `a` left join `view_klassen_neues_jahr` `k` on((`a`.`KlassenIdneu` = `k`.`KlassenId`))) join `ausgeliehen` `al` on((`a`.`SchuelerId` = `al`.`schuelerId`))) join `buecher` `b` on((`b`.`BuchId` = `al`.`buchId`))) where (((`al`.`eingesammelt` <> 1) or isnull(`al`.`eingesammelt`)) and (((`a`.`bezahltNeu` < 0) and (`a`.`bezahlt` < 2)) or ((`b`.`bis` > 0) and (`b`.`bis` < `k`.`Stufe`))));

-- --------------------------------------------------------

--
-- Struktur des Views `view_anmeldungen_altes_jahr`
--
DROP TABLE IF EXISTS `view_anmeldungen_altes_jahr`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_anmeldungen_altes_jahr` AS select `a`.`anmeldungsId` AS `anmeldungsId`,`bl`.`jahr` AS `jahr`,`a`.`KlassenId` AS `KlassenId`,`a`.`bezahlt` AS `bezahlt`,`bl`.`beitrag` AS `beitrag`,`bl`.`klassenStufe` AS `klassenStufe`,`bl`.`sprache` AS `sprache`,`s`.`SchuelerId` AS `SchuelerId`,`s`.`Vorname` AS `sVorname`,`s`.`Nachname` AS `sNachname`,`s`.`FamilienId` AS `FamilienId`,`e`.`Vorname` AS `eVorname`,`e`.`Nachname` AS `eNachname` from (((((`anmeldung` `a` join `klasse` `k`) join `buecherlisten` `bl`) join `schueler` `s`) join `constants` `c`) join `eltern` `e`) where ((`a`.`SchuelerId` = `s`.`SchuelerId`) and (`a`.`Schuljahr` = `c`.`intValue`) and (`c`.`key` = 'thisYear') and (`e`.`FamilienId` = `s`.`FamilienId`) and (`k`.`KlassenId` = `a`.`KlassenId`) and (`a`.`Schuljahr` = `bl`.`jahr`) and (((`k`.`StartKlassenStufe` + `a`.`Schuljahr`) - `k`.`StartJahr`) = `bl`.`klassenStufe`) and (`k`.`Sprache` = convert(`bl`.`sprache` using utf8)));

-- --------------------------------------------------------

--
-- Struktur des Views `view_anmeldungen_alt_und_neu`
--
DROP TABLE IF EXISTS `view_anmeldungen_alt_und_neu`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_anmeldungen_alt_und_neu` AS select `alt`.`anmeldungsId` AS `anmeldungsId`,`alt`.`SchuelerId` AS `SchuelerId`,`alt`.`Schuljahr` AS `Schuljahr`,`alt`.`bezahlt` AS `bezahlt`,`alt`.`KlassenId` AS `KlassenId`,`neu`.`bezahlt` AS `bezahltNeu`,`neu`.`KlassenId` AS `KlassenIdneu`,`kl`.`Stufe` AS `Stufe`,`kl`.`SubKlasse` AS `SubKlasse`,`kl`.`Sprache` AS `Sprache` from (((`anmeldung` `alt` join `anmeldung` `neu`) join `constants` `c`) join `view_klassen_altes_jahr` `kl`) where ((`alt`.`Schuljahr` = `c`.`intValue`) and (`c`.`key` = 'thisYear') and (`alt`.`SchuelerId` = `neu`.`SchuelerId`) and ((`alt`.`Schuljahr` + 1) = `neu`.`Schuljahr`) and (`alt`.`bezahlt` > 0) and (`kl`.`KlassenId` = `alt`.`KlassenId`));

-- --------------------------------------------------------

--
-- Struktur des Views `view_anmeldungen_neues_jahr`
--
DROP TABLE IF EXISTS `view_anmeldungen_neues_jahr`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_anmeldungen_neues_jahr` AS select `a`.`anmeldungsId` AS `anmeldungsId`,`bl`.`jahr` AS `jahr`,`a`.`KlassenId` AS `KlassenId`,`a`.`bezahlt` AS `bezahlt`,`bl`.`beitrag` AS `beitrag`,`bl`.`klassenStufe` AS `klassenStufe`,`bl`.`sprache` AS `sprache`,`s`.`SchuelerId` AS `SchuelerId`,`s`.`Vorname` AS `sVorname`,`s`.`Nachname` AS `sNachname`,`s`.`FamilienId` AS `FamilienId`,`e`.`Vorname` AS `eVorname`,`e`.`Nachname` AS `eNachname` from (((((`anmeldung` `a` join `klasse` `k`) join `buecherlisten` `bl`) join `schueler` `s`) join `constants` `c`) join `eltern` `e`) where ((`a`.`SchuelerId` = `s`.`SchuelerId`) and (`a`.`Schuljahr` = (`c`.`intValue` + 1)) and (`c`.`key` = 'thisYear') and (`e`.`FamilienId` = `s`.`FamilienId`) and (`k`.`KlassenId` = `a`.`KlassenId`) and (`a`.`Schuljahr` = `bl`.`jahr`) and (((`k`.`StartKlassenStufe` + `a`.`Schuljahr`) - `k`.`StartJahr`) = `bl`.`klassenStufe`) and (`k`.`Sprache` = convert(`bl`.`sprache` using utf8)));

-- --------------------------------------------------------

--
-- Struktur des Views `view_bezahlt`
--
DROP TABLE IF EXISTS `view_bezahlt`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_bezahlt` AS select `a`.`KlassenId` AS `Klassenid`,count(0) AS `bezahlt` from (`anmeldung` `a` join `constants` `c`) where ((`c`.`key` = 'thisYear') and (`a`.`Schuljahr` = (`c`.`intValue` + 1)) and (`a`.`bezahlt` = 1)) group by `a`.`KlassenId`;

-- --------------------------------------------------------

--
-- Struktur des Views `view_buecherlisten`
--
DROP TABLE IF EXISTS `view_buecherlisten`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_buecherlisten` AS select `bl`.`listenId` AS `listenId`,`bl`.`jahr` AS `jahr`,`bl`.`klassenStufe` AS `klassenStufe`,`bl`.`sprache` AS `sprache`,`bl`.`beitrag` AS `beitrag`,`bl`.`teilnehmerBezahlt` AS `teilnehmerBezahlt`,`bl`.`teilnehmerZahlungsBefreit` AS `teilnehmerZahlungsBefreit`,`bil`.`buchId` AS `buchId` from (`buecherlisten` `bl` join `buecherinlisten` `bil`) where (`bl`.`listenId` = `bil`.`listenId`);

-- --------------------------------------------------------

--
-- Struktur des Views `view_buecher_in_klassen`
--
DROP TABLE IF EXISTS `view_buecher_in_klassen`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_buecher_in_klassen` AS select distinct `a`.`buchId` AS `buchid`,`s`.`KlassenId` AS `klassenid` from (`ausgeliehen` `a` join `view_schueler_dieses_jahr` `s`) where (`a`.`schuelerId` = `s`.`SchuelerId`) order by 2;

-- --------------------------------------------------------

--
-- Struktur des Views `view_check_einzahlungen`
--
DROP TABLE IF EXISTS `view_check_einzahlungen`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_check_einzahlungen` AS select '1' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_neues_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Verwendungszweck`,' ','') like concat('%LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A%'))) union select '2' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_neues_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Verwendungszweck`,' ','') like concat('%',`a`.`sNachname`,'%')) and (replace(`e`.`Verwendungszweck`,' ','') like concat('%',`a`.`sVorname`,'%'))) union select '3' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_neues_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Verwendungszweck`,' ','') like concat('%',`a`.`sNachname`,'%'))) union select '4' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_neues_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Absender`,' ','') like concat('%',`a`.`eNachname`,'%')) and (replace(`e`.`Absender`,' ','') like concat('%',`a`.`eVorname`,'%'))) union select '5' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_neues_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Absender`,' ','') like concat('%',`a`.`eNachname`,'%'))) union select '6' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_neues_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Absender`,' ','') like concat('%',`a`.`sNachname`,'%'))) union select '7' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_neues_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Verwendungszweck`,' ','') like concat('%',`a`.`eNachname`,'%')));

-- --------------------------------------------------------

--
-- Struktur des Views `view_klassen_altes_jahr`
--
DROP TABLE IF EXISTS `view_klassen_altes_jahr`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_klassen_altes_jahr` AS select `kl`.`KlassenId` AS `KlassenId`,((`c`.`intValue` - `kl`.`StartJahr`) + `kl`.`StartKlassenStufe`) AS `Stufe`,`kl`.`SubKlasse` AS `SubKlasse`,`kl`.`Sprache` AS `Sprache`,`kl`.`MetaKey` AS `MetaKey`,`c`.`intValue` AS `Jahr` from (`klasse` `kl` join `constants` `c`) where ((`c`.`key` = 'thisYear') and (`c`.`intValue` <= ((`kl`.`StartJahr` + `kl`.`EndKlassenStufe`) - `kl`.`StartKlassenStufe`)) and (`c`.`intValue` >= `kl`.`StartJahr`)) order by (`kl`.`StartKlassenStufe` - `kl`.`StartJahr`),`kl`.`SubKlasse`,`kl`.`Sprache`;

-- --------------------------------------------------------

--
-- Struktur des Views `view_klassen_kurz`
--
DROP TABLE IF EXISTS `view_klassen_kurz`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_klassen_kurz` AS select ((`c`.`intValue` - `k`.`StartJahr`) + `k`.`StartKlassenStufe`) AS `Klassenstufe`,`k`.`KlassenId` AS `KlassenId`,`k`.`StartJahr` AS `StartJahr`,`k`.`StartKlassenStufe` AS `StartKlassenStufe`,`k`.`SubKlasse` AS `SubKlasse`,`k`.`Sprache` AS `Sprache`,`k`.`EndKlassenStufe` AS `EndKlassenStufe`,`k`.`MetaKey` AS `MetaKey` from (`klasse` `k` join `constants` `c`) where (`c`.`key` = 'thisYear');

-- --------------------------------------------------------

--
-- Struktur des Views `view_klassen_lang`
--
DROP TABLE IF EXISTS `view_klassen_lang`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_klassen_lang` AS select if(((`k`.`Klassenstufe` + 0) < `k`.`StartKlassenStufe`),'- - - - -',if(((`k`.`Klassenstufe` + 0) > `k`.`EndKlassenStufe`),'+++++',concat((0 + `k`.`Klassenstufe`),ifnull(`k`.`SubKlasse`,''),'-',`k`.`Sprache`))) AS `KlassenStufeAlt`,if(((`k`.`Klassenstufe` + 1) < `k`.`StartKlassenStufe`),'- - - - -',if(((`k`.`Klassenstufe` + 1) > `k`.`EndKlassenStufe`),'+++++',concat((1 + `k`.`Klassenstufe`),ifnull(`k`.`SubKlasse`,''),'-',`k`.`Sprache`))) AS `KlassenStufeNeu`,concat(if(((`k`.`Klassenstufe` + 0) < `k`.`StartKlassenStufe`),char((60 + `k`.`Klassenstufe`)),if(((`k`.`Klassenstufe` + 0) > `k`.`EndKlassenStufe`),concat('__',char((60 + `k`.`Klassenstufe`))),concat('_',char((60 + `k`.`Klassenstufe`))))),ifnull(`k`.`SubKlasse`,''),`k`.`Sprache`) AS `OrderAlt`,concat(if(((`k`.`Klassenstufe` + 1) < `k`.`StartKlassenStufe`),char((60 + `k`.`Klassenstufe`)),if(((`k`.`Klassenstufe` + 1) > `k`.`EndKlassenStufe`),concat('__',char((60 + `k`.`Klassenstufe`))),concat('_',char((60 + `k`.`Klassenstufe`))))),ifnull(`k`.`SubKlasse`,''),`k`.`Sprache`) AS `OrderNeu`,`k`.`Klassenstufe` AS `Klassenstufe`,`k`.`KlassenId` AS `KlassenId`,`k`.`StartJahr` AS `StartJahr`,`k`.`StartKlassenStufe` AS `StartKlassenStufe`,`k`.`SubKlasse` AS `SubKlasse`,`k`.`Sprache` AS `Sprache`,`k`.`EndKlassenStufe` AS `EndKlassenStufe`,`k`.`MetaKey` AS `MetaKey` from `view_klassen_kurz` `k`;

-- --------------------------------------------------------

--
-- Struktur des Views `view_klassen_neues_jahr`
--
DROP TABLE IF EXISTS `view_klassen_neues_jahr`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_klassen_neues_jahr` AS select `kl`.`KlassenId` AS `KlassenId`,(((`c`.`intValue` - `kl`.`StartJahr`) + `kl`.`StartKlassenStufe`) + 1) AS `Stufe`,`kl`.`SubKlasse` AS `SubKlasse`,`kl`.`Sprache` AS `Sprache`,(`c`.`intValue` + 1) AS `Jahr` from (`klasse` `kl` join `constants` `c`) where ((`c`.`key` = 'thisYear') and ((`c`.`intValue` + 1) <= ((`kl`.`StartJahr` + `kl`.`EndKlassenStufe`) - `kl`.`StartKlassenStufe`)) and ((`c`.`intValue` + 1) >= `kl`.`StartJahr`)) order by (`kl`.`StartKlassenStufe` - `kl`.`StartJahr`),`kl`.`SubKlasse`,`kl`.`Sprache`;

-- --------------------------------------------------------

--
-- Struktur des Views `view_klassen_uebersicht`
--
DROP TABLE IF EXISTS `view_klassen_uebersicht`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_klassen_uebersicht` AS select concat(`s`.`Nachname`,', ',`s`.`Vorname`) AS `schuelerName`,concat(`e`.`Nachname`,', ',`e`.`Vorname`) AS `elternName`,`e`.`FamilienId` AS `FamilienId`,`e`.`Telefon` AS `Telefon`,`e`.`Email` AS `email`,`a`.`bezahlt` AS `bezahlt`,`a`.`KlassenId` AS `klassenId`,`s`.`SchuelerId` AS `schuelerId` from (((`anmeldung` `a` join `schueler` `s`) join `eltern` `e`) join `constants` `c`) where ((`c`.`key` = 'thisYear') and (`a`.`bezahlt` <> 2) and (`a`.`bezahlt` <> -(1)) and (`a`.`Schuljahr` = (`c`.`intValue` + 1)) and (`a`.`SchuelerId` = `s`.`SchuelerId`) and (`s`.`FamilienId` = `e`.`FamilienId`));

-- --------------------------------------------------------

--
-- Struktur des Views `view_klassen_uebersicht2`
--
DROP TABLE IF EXISTS `view_klassen_uebersicht2`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_klassen_uebersicht2` AS select `s`.`SchuelerId` AS `schuelerId` from (((`anmeldung` `a` join `schueler` `s`) join `eltern` `e`) join `constants` `c`) where ((`c`.`key` = 'thisYear') and (`a`.`Schuljahr` = `c`.`intValue`) and (`a`.`SchuelerId` = `s`.`SchuelerId`) and (`s`.`FamilienId` = `e`.`FamilienId`) and (`a`.`bezahlt` > 0));

-- --------------------------------------------------------

--
-- Struktur des Views `view_klassen_uebersicht3`
--
DROP TABLE IF EXISTS `view_klassen_uebersicht3`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_klassen_uebersicht3` AS select `vk1`.`schuelerName` AS `schuelerName`,`vk1`.`elternName` AS `elternName`,`vk1`.`FamilienId` AS `FamilienId`,`vk1`.`Telefon` AS `Telefon`,`vk1`.`email` AS `email`,`vk1`.`bezahlt` AS `bezahlt`,`vk1`.`klassenId` AS `klassenId`,`vk1`.`schuelerId` AS `schuelerId`,`vk2`.`schuelerId` AS `altAngemeldet` from (`view_klassen_uebersicht` `vk1` left join `view_klassen_uebersicht2` `vk2` on((`vk1`.`schuelerId` = `vk2`.`schuelerId`)));

-- --------------------------------------------------------

--
-- Struktur des Views `view_schueler_dieses_jahr`
--
DROP TABLE IF EXISTS `view_schueler_dieses_jahr`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_schueler_dieses_jahr` AS select `s`.`SchuelerId` AS `SchuelerId`,`s`.`FamilienId` AS `FamilienId`,`s`.`Nachname` AS `Nachname`,`s`.`Vorname` AS `Vorname`,`s`.`Anmerkungen` AS `Anmerkungen`,`s`.`istElternvertreter` AS `istElternvertreter`,`s`.`eisStatus` AS `EisStatus`,`kl`.`KlassenId` AS `KlassenId`,`kl`.`Stufe` AS `Stufe`,`kl`.`SubKlasse` AS `SubKlasse`,`kl`.`Sprache` AS `Sprache`,`kl`.`Jahr` AS `Jahr`,`kl`.`MetaKey` AS `MetaKey`,`a`.`anmeldungsId` AS `anmeldungsId`,`a`.`bezahlt` AS `bezahlt` from ((`schueler` `s` join `anmeldung` `a`) join `view_klassen_altes_jahr` `kl`) where ((`a`.`SchuelerId` = `s`.`SchuelerId`) and (`a`.`KlassenId` = `kl`.`KlassenId`) and (`kl`.`Jahr` = `a`.`Schuljahr`));

-- --------------------------------------------------------

--
-- Struktur des Views `view_schueler_naechstes_jahr`
--
DROP TABLE IF EXISTS `view_schueler_naechstes_jahr`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_schueler_naechstes_jahr` AS select `s`.`SchuelerId` AS `SchuelerId`,`s`.`FamilienId` AS `FamilienId`,`s`.`Nachname` AS `Nachname`,`s`.`Vorname` AS `Vorname`,`s`.`Anmerkungen` AS `Anmerkungen`,`s`.`istElternvertreter` AS `istElternvertreter`,`kl`.`KlassenId` AS `KlassenId`,`kl`.`Stufe` AS `Stufe`,`kl`.`SubKlasse` AS `SubKlasse`,`kl`.`Sprache` AS `Sprache`,`kl`.`Jahr` AS `Jahr`,`a`.`anmeldungsId` AS `anmeldungsId`,`a`.`bezahlt` AS `bezahlt` from ((`schueler` `s` join `anmeldung` `a`) join `view_klassen_neues_jahr` `kl`) where ((`a`.`SchuelerId` = `s`.`SchuelerId`) and (`a`.`KlassenId` = `kl`.`KlassenId`) and (`kl`.`Jahr` = `a`.`Schuljahr`));

-- --------------------------------------------------------

--
-- Struktur des Views `view_uebersicht`
--
DROP TABLE IF EXISTS `view_uebersicht`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_uebersicht` AS select `kl`.`KlassenId` AS `KlassenId`,`kl`.`Stufe` AS `Stufe`,`kl`.`SubKlasse` AS `SubKlasse`,`kl`.`Sprache` AS `Sprache`,ifnull(`bez`.`bezahlt`,0) AS `bezahlt`,ifnull(`unbez`.`unbezahlt`,0) AS `unbezahlt`,(ifnull(`bez`.`bezahlt`,0) + ifnull(`unbez`.`unbezahlt`,0)) AS `gesamt` from ((`view_klassen_neues_jahr` `kl` left join `view_bezahlt` `bez` on((`kl`.`KlassenId` = `bez`.`Klassenid`))) left join `view_unbezahlt` `unbez` on((`kl`.`KlassenId` = `unbez`.`Klassenid`)));

-- --------------------------------------------------------

--
-- Struktur des Views `view_unbezahlt`
--
DROP TABLE IF EXISTS `view_unbezahlt`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_unbezahlt` AS select `a`.`KlassenId` AS `Klassenid`,count(0) AS `unbezahlt` from (`anmeldung` `a` join `constants` `c`) where ((`c`.`key` = 'thisYear') and (`a`.`Schuljahr` = (`c`.`intValue` + 1)) and (`a`.`bezahlt` = 0)) group by `a`.`KlassenId`;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `anmeldung`
--
ALTER TABLE `anmeldung`
  ADD CONSTRAINT `anmeldung_ibfk_1` FOREIGN KEY (`SchuelerId`) REFERENCES `schueler` (`SchuelerId`) ON DELETE CASCADE;

--
-- Constraints der Tabelle `ausgeliehen`
--
ALTER TABLE `ausgeliehen`
  ADD CONSTRAINT `fk_ausgeliehen_buecher` FOREIGN KEY (`buchId`) REFERENCES `buecher` (`BuchId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ausgeliehen_schueler` FOREIGN KEY (`schuelerId`) REFERENCES `schueler` (`SchuelerId`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `buecherinlisten`
--
ALTER TABLE `buecherinlisten`
  ADD CONSTRAINT `fkBuchId` FOREIGN KEY (`buchId`) REFERENCES `buecher` (`BuchId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fklistenid` FOREIGN KEY (`listenId`) REFERENCES `buecherlisten` (`listenId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `einzahlungen`
--
ALTER TABLE `einzahlungen`
  ADD CONSTRAINT `FKanmeldung` FOREIGN KEY (`anmeldungsId`) REFERENCES `anmeldung` (`anmeldungsId`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
