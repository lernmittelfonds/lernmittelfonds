--
-- Struktur des Views `view_schueler_naechstes_jahr`
--

CREATE OR REPLACE  VIEW `view_schueler_naechstes_jahr` 
AS select `s`.`SchuelerId` AS `SchuelerId`,`s`.`FamilienId` AS `FamilienId`,`s`.`Nachname` AS `Nachname`,`s`.`Vorname` AS `Vorname`,
`s`.`Anmerkungen` AS `Anmerkungen`, `s`.`istElternvertreter` AS `istElternvertreter`,
`kl`.`KlassenId` AS `KlassenId`,`kl`.`Stufe` AS `Stufe`,`kl`.`SubKlasse` AS `SubKlasse`,`kl`.`Sprache` AS `Sprache`,`kl`.`Jahr` AS `Jahr`,
`a`.`anmeldungsId` AS `anmeldungsId`,`a`.`bezahlt` AS `bezahlt` 
from ((`schueler` `s` join `anmeldung` `a`) 
join `view_klassen_neues_jahr` `kl`) 
where ((`a`.`SchuelerId` = `s`.`SchuelerId`) 
and (`a`.`KlassenId` = `kl`.`KlassenId`) 
and (`kl`.`Jahr` = `a`.`Schuljahr`));

CREATE OR REPLACE VIEW `view_schueler_dieses_jahr` AS
    select 
        `s`.`SchuelerId` AS `SchuelerId`,
        `s`.`FamilienId` AS `FamilienId`,
        `s`.`Nachname` AS `Nachname`,
        `s`.`Vorname` AS `Vorname`,
        `s`.`Anmerkungen` AS `Anmerkungen`,
        `s`.`istElternvertreter` AS `istElternvertreter`,
        `kl`.`KlassenId` AS `KlassenId`,
        `kl`.`Stufe` AS `Stufe`,
        `kl`.`SubKlasse` AS `SubKlasse`,
        `kl`.`Sprache` AS `Sprache`,
        `kl`.`Jahr` AS `Jahr`,
        `a`.`anmeldungsId` AS `anmeldungsId`,
        `a`.`bezahlt` AS `bezahlt`
    from
        ((`schueler` `s`
        join `anmeldung` `a`)
        join `view_klassen_altes_jahr` `kl`)
    where
        ((`a`.`SchuelerId` = `s`.`SchuelerId`)
            and (`a`.`KlassenId` = `kl`.`KlassenId`)
            and (`kl`.`Jahr` = `a`.`Schuljahr`))

CREATE OR REPLACE  VIEW `view_klassen_kurz` AS
    select 
        ((`c`.`intValue` - `k`.`StartJahr`) + `k`.`StartKlassenStufe`) AS `Klassenstufe`,
        `k`.`KlassenId` AS `KlassenId`,
        `k`.`StartJahr` AS `StartJahr`,
        `k`.`StartKlassenStufe` AS `StartKlassenStufe`,
        `k`.`SubKlasse` AS `SubKlasse`,
        `k`.`Sprache` AS `Sprache`,
        `k`.`EndKlassenStufe` AS `EndKlassenStufe`,
        `k`.`MetaKey` AS `MetaKey`
    from
        (`klasse` `k`
        join `constants` `c`)
    where
        (`c`.`key` = 'thisYear')


CREATE OR REPLACE  VIEW `view_klassen_lang` AS
    select 
        if(((`k`.`Klassenstufe` + 0) < `k`.`StartKlassenStufe`),
            '- - - - -',
            if(((`k`.`Klassenstufe` + 0) > `k`.`EndKlassenStufe`),
                '+++++',
                concat((0 + `k`.`Klassenstufe`),
                        ifnull(`k`.`SubKlasse`, ''),
                        '-',
                        `k`.`Sprache`))) AS `KlassenStufeAlt`,
        if(((`k`.`Klassenstufe` + 1) < `k`.`StartKlassenStufe`),
            '- - - - -',
            if(((`k`.`Klassenstufe` + 1) > `k`.`EndKlassenStufe`),
                '+++++',
                concat((1 + `k`.`Klassenstufe`),
                        ifnull(`k`.`SubKlasse`, ''),
                        '-',
                        `k`.`Sprache`))) AS `KlassenStufeNeu`,
        concat(if(((`k`.`Klassenstufe` + 0) < `k`.`StartKlassenStufe`),
                    char((60 + `k`.`Klassenstufe`)),
                    if(((`k`.`Klassenstufe` + 0) > `k`.`EndKlassenStufe`),
                        concat('__', char((60 + `k`.`Klassenstufe`))),
                        concat('_', char((60 + `k`.`Klassenstufe`))))),
                ifnull(`k`.`SubKlasse`, ''),
                `k`.`Sprache`) AS `OrderAlt`,
        concat(if(((`k`.`Klassenstufe` + 1) < `k`.`StartKlassenStufe`),
                    char((60 + `k`.`Klassenstufe`)),
                    if(((`k`.`Klassenstufe` + 1) > `k`.`EndKlassenStufe`),
                        concat('__', char((60 + `k`.`Klassenstufe`))),
                        concat('_', char((60 + `k`.`Klassenstufe`))))),
                ifnull(`k`.`SubKlasse`, ''),
                `k`.`Sprache`) AS `OrderNeu`,
        `k`.`Klassenstufe` AS `Klassenstufe`,
        `k`.`KlassenId` AS `KlassenId`,
        `k`.`StartJahr` AS `StartJahr`,
        `k`.`StartKlassenStufe` AS `StartKlassenStufe`,
        `k`.`SubKlasse` AS `SubKlasse`,
        `k`.`Sprache` AS `Sprache`,
        `k`.`EndKlassenStufe` AS `EndKlassenStufe`,
        `k`.`MetaKey` AS `MetaKey`
    from
        `view_klassen_kurz` `k`

CREATE OR REPLACE  VIEW `view_klassen_altes_jahr` AS
    select 
        `kl`.`KlassenId` AS `KlassenId`,
        ((`c`.`intValue` - `kl`.`StartJahr`) + `kl`.`StartKlassenStufe`) AS `Stufe`,
        `kl`.`SubKlasse` AS `SubKlasse`,
        `kl`.`Sprache` AS `Sprache`,
        `kl`.`MetaKey` AS `MetaKey`,
        `c`.`intValue` AS `Jahr`
    from
        (`klasse` `kl`
        join `constants` `c`)
    where
        ((`c`.`key` = 'thisYear')
            and (`c`.`intValue` <= ((`kl`.`StartJahr` + `kl`.`EndKlassenStufe`) - `kl`.`StartKlassenStufe`))
            and (`c`.`intValue` >= `kl`.`StartJahr`))
    order by (`kl`.`StartKlassenStufe` - `kl`.`StartJahr`) , `kl`.`SubKlasse` , `kl`.`Sprache`


CREATE OR REPLACE  VIEW `view_schueler_dieses_jahr` AS
    select 
        `s`.`SchuelerId` AS `SchuelerId`,
        `s`.`FamilienId` AS `FamilienId`,
        `s`.`Nachname` AS `Nachname`,
        `s`.`Vorname` AS `Vorname`,
        `s`.`Anmerkungen` AS `Anmerkungen`,
        `s`.`istElternvertreter` AS `istElternvertreter`,
        `s`.`   eisStatus` AS `EisStatus`,
        `kl`.`KlassenId` AS `KlassenId`,
        `kl`.`Stufe` AS `Stufe`,
        `kl`.`SubKlasse` AS `SubKlasse`,
        `kl`.`Sprache` AS `Sprache`,
        `kl`.`Jahr` AS `Jahr`,
        `kl`.`MetaKey` AS `MetaKey`,
        `a`.`anmeldungsId` AS `anmeldungsId`,
        `a`.`bezahlt` AS `bezahlt`
    from
        ((`schueler` `s`
        join `anmeldung` `a`)
        join `view_klassen_altes_jahr` `kl`)
    where
        ((`a`.`SchuelerId` = `s`.`SchuelerId`)
            and (`a`.`KlassenId` = `kl`.`KlassenId`)
            and (`kl`.`Jahr` = `a`.`Schuljahr`))

CREATE OR REPLACE VIEW `view_klassen_uebersicht` AS
    select 
        concat(`s`.`Nachname`, ', ', `s`.`Vorname`) AS `schuelerName`,
        concat(`e`.`Nachname`, ', ', `e`.`Vorname`) AS `elternName`,
        `e`.`FamilienId` AS `FamilienId`,
        `e`.`Telefon` AS `Telefon`,
        `e`.`Email` AS `email`,
        `a`.`bezahlt` AS `bezahlt`,
        `a`.`KlassenId` AS `klassenId`,
        `s`.`SchuelerId` AS `schuelerId`
    from
        (((`anmeldung` `a`
        join `schueler` `s`)
        join `eltern` `e`)
        join `constants` `c`)
    where
        ((`c`.`key` = 'thisYear')
            and (`a`.`bezahlt` <> 2)
            and (`a`.`bezahlt` <> -(1))
            and (`a`.`Schuljahr` = (`c`.`intValue` + 1))
            and (`a`.`SchuelerId` = `s`.`SchuelerId`)
            and (`s`.`FamilienId` = `e`.`FamilienId`))


CREATE OR REPLACE VIEW `view_klassen_uebersicht3` AS
    select 
        `vk1`.`schuelerName` AS `schuelerName`,
        `vk1`.`elternName` AS `elternName`,
        `vk1`.`FamilienId` AS `FamilienId`,
        `vk1`.`Telefon` AS `Telefon`,
        `vk1`.`email` AS `email`,
        `vk1`.`bezahlt` AS `bezahlt`,
        `vk1`.`klassenId` AS `klassenId`,
        `vk1`.`schuelerId` AS `schuelerId`,
        `vk2`.`schuelerId` AS `altAngemeldet`
    from
        (`view_klassen_uebersicht` `vk1`
        left join `view_klassen_uebersicht2` `vk2` ON ((`vk1`.`schuelerId` = `vk2`.`schuelerId`)))


CREATE OR REPLACE VIEW `view_buecherlisten` AS
    select 
        `bl`.`listenId` AS `listenId`,
        `bl`.`jahr` AS `jahr`,
        `bl`.`klassenStufe` AS `klassenStufe`,
        `bl`.`sprache` AS `sprache`,
        `bl`.`beitrag` AS `beitrag`,
        `bl`.`teilnehmerBezahlt` AS `teilnehmerBezahlt`,
        `bl`.`teilnehmerZahlungsBefreit` AS `teilnehmerZahlungsBefreit`,
        `bil`.`buchId` AS `buchId`,
		`bil`.`preisImJahr` AS `preisImJahr`
    from
        (`buecherlisten` `bl`
        join `buecherinlisten` `bil`)
    where
        (`bl`.`listenId` = `bil`.`listenId`)




UPDATE buecherlisten bl JOIN 
(SELECT a.Schuljahr, k.stufe, k.Sprache, sum( if( ifnull( a.bezahlt , -2 ) =1, 1, 0 ) ) AS teilnahme, sum( if( ifnull( a.bezahlt , -2 ) =2, 1, 0 ) ) AS befreit
FROM `view_klassen_altes_jahr` k, anmeldung a
WHERE k.klassenid = a.klassenid
AND k.`Jahr` = a.Schuljahr
AND a.bezahlt >0
GROUP BY k.stufe, k.Sprache) t 
ON t.Schuljahr = bl.jahr
AND t.stufe=bl.klassenStufe
AND t.Sprache=bl.sprache
set bl.teilnehmerBezahlt=t.teilnahme,
bl.teilnehmerZahlungsBefreit=t.befreit


oder

SELECT SUM(IF(bezahlt=1,1,0)) a ,SUM(IF(bezahlt=2,1,0)) v,  StartKlassenStufe+2017-StartJahr, sprache, StartKlassenStufe,StartJahr
FROM anmeldung a, klasse k
WHERE a.Schuljahr=2017
AND k.klassenId=a.KlassenId
AND bezahlt>=0
GROUP by StartKlassenStufe, sprache, StartJahr
order by 3,4

Statistik 

SELECT Jahr, sum(`teilnehmerZahlungsBefreit`), sum(teilnehmerBezahlt)  FROM `buecherlisten` 
Group by Jahr
Order by 1


SELECT `jahr` , sum( `teilnehmerBezahlt` ) , sum( `teilnehmerZahlungsBefreit` )
FROM `buecherlisten`
GROUP BY `jahr` 

SELECT `jahr` , sum( `teilnehmerBezahlt` ) , sum( `teilnehmerZahlungsBefreit` )
FROM `buecherlisten`
GROUP BY `jahr`
LIMIT 0 , 30

// Löschen von Schülern ohne Klassen vorbereiten - query sucht die noch bücher haben
select * from schueler s , anmeldung a, ausgeliehen aus
where s.schuelerid not in (select v.schuelerid from view_schueler_dieses_jahr v)
AND s.schuelerid not in (select v.schuelerid from view_schueler_naechstes_jahr v)
AND a.schuelerid=s.schuelerid
AND aus.schuelerId = s.SchuelerId

CREATE OR REPLACE VIEW `view_anmeldungen_alt_und_neu2` AS select `alt`.`anmeldungsId` AS `anmeldungsId`,`alt`.`SchuelerId` AS `SchuelerId`,`alt`.`Schuljahr` AS `Schuljahr`,`alt`.`bezahlt` AS `bezahlt`,`alt`.`KlassenId` AS `KlassenId`,`neu`.`bezahlt` AS `bezahltNeu`,`neu`.`KlassenId` AS `KlassenIdneu`,`kl`.`Stufe` AS `Stufe`,`kl`.`SubKlasse` AS `SubKlasse`,`kl`.`Sprache` AS `Sprache` from (((`anmeldung` `alt` join `anmeldung` `neu`) join `constants` `c`) join `view_klassen_altes_jahr` `kl`) where ((`alt`.`Schuljahr` = `c`.`intValue`) and (`c`.`key` = 'thisYear') and (`alt`.`SchuelerId` = `neu`.`SchuelerId`) and ((`alt`.`Schuljahr` + 1) = `neu`.`Schuljahr`) and (`kl`.`KlassenId` = `alt`.`KlassenId`));

CREATE OR REPLACE VIEW  `view_analyse_buch_rueckgabe` AS select `a`.`anmeldungsId` AS `anmeldungsId`,`a`.`SchuelerId` AS `SchuelerId`,`a`.`Schuljahr` AS `Schuljahr`,`a`.`bezahlt` AS `bezahlt`,`a`.`KlassenId` AS `KlassenId`,`a`.`bezahltNeu` AS `bezahltNeu`,`a`.`KlassenIdneu` AS `KlassenIdneu`,`a`.`Stufe` AS `Stufe`,`a`.`SubKlasse` AS `SubKlasse`,`a`.`Sprache` AS `Sprache`,`k`.`Stufe` AS `StufeNeu`,`al`.`eingesammelt` AS `eingesammelt`,`b`.`BuchId` AS `BuchId`,`b`.`Isbn` AS `Isbn`,`b`.`Titel` AS `Titel`,`b`.`von` AS `von`,`b`.`bis` AS `bis`,`b`.`Neupreis` AS `Neupreis` from (((`view_anmeldungen_alt_und_neu2` `a` left join `view_klassen_neues_jahr` `k` on((`a`.`KlassenIdneu` = `k`.`KlassenId`))) join `ausgeliehen` `al` on((`a`.`SchuelerId` = `al`.`schuelerId`))) join `buecher` `b` on((`b`.`BuchId` = `al`.`buchId`))) where (((`al`.`eingesammelt` <> 1) or isnull(`al`.`eingesammelt`)) and (((`a`.`bezahltNeu` < 0) and (`a`.`bezahlt` < 2)) or ((`b`.`bis` > 0) and (`b`.`bis` < `k`.`Stufe`))));


SELECT e.email, e.Freigabe_alle, e.Freigabe_ev, e.Freigabe_info, e.Freigabe_lmf, s.`Vorname` , s.`Nachname` , s.`KlassenId` , s.`Stufe` , s.`SubKlasse` , s.`bezahlt`
FROM `view_schueler_naechstes_jahr` s, eltern e
WHERE e.`FamilienId` = s.`FamilienId` #AND s.`KlassenId`=54

ORDER BY s.`Stufe` , s.`SubKlasse` , s.`Nachname` , s.`Vorname`
LIMIT 0 , 30

DELETE FROM `anmeldung` a WHERE a.`SchuelerId` NOT IN (SELECT s.`SchuelerId` from schueler s)

SELECT s.SchuelerId, e.Email, e.Telefon, concat( e.Vorname, ' ', e.Nachname ) AS Eltern, concat( s.Vorname, ' ', s.Nachname ) AS Schueler, s.istElternvertreter, concat( s.Stufe, s.SubKlasse, ' ', s.Sprache ) AS Klasse, e.Anschrift
FROM view_schueler_naechstes_jahr s, eltern e
WHERE s.FamilienId = e.FamilienId
AND e.zuzahlungsBefreit !=1
AND s.Stufe ="7"
AND s.SubKlasse ="-4"


//elternvertreter
SELECT e.*, s.schuelerid, stufe, subklasse FROM 
eltern e join schueler s on (e.familienid=s.familienid)
left join `view_schueler_dieses_jahr` `vsj` ON (`s`.`schuelerId` = `vsj`.`schuelerId`)
where s.istElternvertreter=1
order by stufe, subklasse

select 
        s.*
    from
        (`schueler` `s`
        left join `view_schueler_dieses_jahr` `alt` ON ((`s`.`schuelerId` = `alt`.`schuelerId`))
        left join `view_schueler_naechstes_jahr` `neu` ON ((`s`.`schuelerId` = `neu`.`schuelerId`))
        )
    WHERE isnull(alt.schuelerId)
AND isnull(neu.schuelerId)

mailjobs: jobId, nutzerid, startzeit, anzahl
Files: jobId,filename
mails: mailid, jobId, absendername,absenderadresse, empfaenger, betreff, text, fehler

CREATE TABLE `db462321821`.`mailjobs` (
  `jobId` INT NOT NULL AUTO_INCREMENT,
  `familienId` INT(10) NOT NULL,
  `zeitpunkt` DATETIME NOT NULL,
  `anzahl` INT(6) NOT NULL,
  `kommentar` VARCHAR(45) NULL,
  PRIMARY KEY (`jobId`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `db462321821`.`mailanlagen` (
  `jobId` INT NOT NULL,
  `filename` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`jobId`, `filename`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `db462321821`.`mails` (
  `mailId` INT NOT NULL AUTO_INCREMENT,
  `jobId` INT NOT NULL,
  `absenderName` VARCHAR(45) NULL,
  `absenderAdresse` VARCHAR(100) NOT NULL,
  `empfaengerAdresse` VARCHAR(100) NOT NULL,
  `betreff` VARCHAR(200) NULL,
  `text` TEXT NULL,
  `fehler` INT(1) NULL DEFAULT 0,
  PRIMARY KEY (`mailid`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

//Bestellungen nächstes Jahr
SELECT Stufe, sprache, count( schuelerid )
FROM view_schueler_naechstes_jahr
WHERE bezahlt >0
Group by Stufe, sprache


SELECT sum(leihpreis),  teilnehmer, beitrag, klassenstufe, sprache
FROM view_abrechnung_altes_jahr_grundlage
Group by listenId
Order by  klassenstufe, sprache

Select sum(beitrag*teilnehmer) as Einnahmen, 
	sum(leihpreis*teilnehmer) as Ausgaben,  
	sum((beitrag-leihpreis)*teilnehmer) as Differenz, 
	sum(beitrag*teilnehmer)/sum(leihpreis*teilnehmer)*100 as Deckung
from (
	SELECT sum(leihpreis) as leihpreis,  teilnehmer, beitrag, klassenstufe, sprache 
	FROM view_abrechnung_altes_jahr_grundlage
	Group by listenId
	Order by  klassenstufe, sprache
) as listen_kalkulation
