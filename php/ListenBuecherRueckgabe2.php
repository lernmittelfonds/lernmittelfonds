<?php
require_once ('./lmf-session.php');
include ('database_connection.php');
require_once ('constants.php');
require_once ('./lmf-logging.php');
if(!isset($_SESSION)){ 
    session_start(); 
} 

$error = array(); //this array will store all error messages
$loggedIn = true; //is logged in???
$request = json_decode(file_get_contents('php://input'), true);

if (!isset($_SESSION['FamilienId'])) {
    $error[] = 'Loginerror';
    echo '{"loggedIn":false, "errors":["Nicht korrekt angemeldet"], "success":false}';
    exit;
}


$abfrage_klassen = "select * from  `view_klassen_altes_jahr`";
$result_klassen = mysqli_query($dbc, $abfrage_klassen);
while ($klasse = mysqli_fetch_array($result_klassen)) {
    $abfrage_buecher = "SELECT BuchId, count(BuchId) , Isbn, Titel,Neupreis
        FROM view_analyse_buch_rueckgabe
        Where KlassenId=" . $klasse['KlassenId'] . " 
        GROUP BY BuchId;";
    $result_buecher = mysqli_query($dbc, $abfrage_buecher);
    $anzahlBuecher = 0;
    $selectAbfrage = "SELECT e.email, s.*";
    $summe =""; 
    $buecher=array();
    while ($buch = mysqli_fetch_array($result_buecher)) {
        $buecher[$anzahlBuecher]=$buch;
        $anzahlBuecher++;
        $selectAbfrage .= ",\n sum(if(v.BuchId=" . $buch['BuchId'] . ",1 , 0)) as buch_" . $anzahlBuecher;
        $summe .= "v.BuchId=" . $buch['BuchId'] . " OR ";
    }
    if ($summe!=""){
        $selectAbfrage .= ",\n sum(if(" . $summe . "1=2,1 , 0)) as gesamt";
    }
    $abfrage_schueler = $selectAbfrage . "
        FROM   view_schueler_dieses_jahr s 
        LEFT JOIN view_analyse_buch_rueckgabe v 
        ON s.schuelerid=v.schuelerid
        LEFT JOIN eltern e 
        ON s.familienId=e.familienId
        WHERE s.klassenId = " . $klasse['KlassenId'] . " 
        GROUP BY s.schuelerid
        ORDER BY s.nachname, s.vorname";
    $result_schueler = mysqli_query($dbc, $abfrage_schueler);
    $lfdNr = 1;
    while ($schueler = mysqli_fetch_array($result_schueler)) {
        if ($schueler['gesamt']!=0){
            
            print '<tr><td>' . $lfdNr . '</td><td> ' . $schueler['Nachname'] . ', ' . $schueler['Vorname'] . '('.$schueler['email'] .')</td>';
            for ($i = 1; $i <= $anzahlBuecher; $i++) {
                $rueckgabe= (intval ($schueler['buch_' . $i]) > 0)?$buecher[$i]['Titel'].'\n' :'';
            }
            $lfdNr++;
        }
    }
}
print '</body></html>';

mysqli_close($dbc); //Close the DB Connection;
exit;
?>
