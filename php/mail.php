<?php

require_once ('constants.php');
require_once('lmf-logging.php');
require_once('lmf-sendMail.php');
require_once ('mailToDb.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once 'PHPMailer/src/Exception.php';
require_once 'PHPMailer/src/PHPMailer.php';
require_once 'PHPMailer/src/SMTP.php';

function doMailIntern($mailTo, $subject, $message, $smtpType){
    $mailer = new PHPMailer;
    $mailer->CharSet = 'UTF-8';
    $mailer->isSMTP();                 // SMTP aktivieren
    $mailer->Host = constant($smtpType . '_SMTP_HOST');       //SMTP-Server
    $mailer->SMTPAuth = constant($smtpType . '_SMTP_AUTH');   // SMTP Authentifizierung aktivieren
    $mailer->Username = constant($smtpType . '_SMTP_USER');   // SMTP Benutzer
    $mailer->Password = constant($smtpType . '_SMTP_PW');     // SMTP Benutzer Passwort
    $mailer->SMTPSecure = constant($smtpType . '_SMTP_SEC');  // Verbindungssicherheit setzen ( SSL und TLS möglich )
    $mailer->Port = constant($smtpType . '_SMTP_PORT');       // Verbindungsport festlegen

    $mailer->IsHTML(false);
    if ($smtpType == "LMF") {
        $mailer->SetFrom(constant($smtpType . '_SMTP_USER'), 'Lernmittelfonds');
    } else {
        $mailer->SetFrom(constant($smtpType . '_SMTP_USER'), 'ElternInfoSystem');
    }
    $mailer->AddAddress($mailTo);
    $mailer->Subject = $subject;
    $mailer->Body = $message;
    
        if (!$mailer->Send()) {
            lmf_trace('Message ('.$subject.') could not be sent to : ' . $mailTo);
            lmf_trace('Mailer Error: ' . $mailer->ErrorInfo);
            return false;
        }
        lmf_trace('Nachricht ('.$subject.') wurde versendet an: ' . $mailTo);
        return true;
}

function getEmailDataBySchuelerId($id, $dbc) {
    $query_get_child = "SELECT eltern.*, schueler.SchuelerId, schueler.Vorname as SVN, schueler.Nachname as SNN, bl.beitrag 
                FROM view_schueler_naechstes_jahr schueler, eltern, buecherlisten bl "
            . " WHERE schueler.FamilienId=eltern.FamilienId "
            . " AND schueler.Stufe=bl.klassenStufe "
            . " AND schueler.Jahr=bl.jahr "
            . " AND schueler.Sprache = bl.sprache "
            . " AND `SchuelerId`=" . $id . ";";
    $result_get_child = mysqli_query($dbc, $query_get_child);
    lmf_trace($query_get_child);
    if ($row = mysqli_fetch_array($result_get_child)) {
        return $row;
    } else {
        lmf_queryTrace($query_get_child, false, $dbc);
        return NULL;
    }
}

function getDataByEinzahlungsId($id, $dbc) {
    $query_get_data = "SELECT  e.*, el.email, 
            Concat(s.vorname,' ', s.nachname) as schuelerName , 
            Concat(el.vorname,' ', el.nachname) as elternName
            FROM anmeldung a, einzahlungen e, schueler s, eltern el
            WHERE e.anmeldungsId = a.anmeldungsId
            AND el.FamilienId=s.FamilienId
            AND s.schuelerId=a.schuelerId
            AND einzahlungsId= " . $id . ";";
    $result_get_data = mysqli_query($dbc, $query_get_data);
    if ($row = mysqli_fetch_array($result_get_data)) {
        return $row;
    } else {
        lmf_queryTrace($query_get_data, false, $dbc);
        return NULL;
    }
}

// Einzelmail - sofort senden
function sendMailEinzahlungErhalten($EinzahlungsId, $dbc) {
    $dataRow = getDataByEinzahlungsId($EinzahlungsId, $dbc);
    if ($dataRow != NULL) {
        $subj = 'Lernmittelfonds Einzahlungsbestätigung';
        $message = "Sehr geehrte(r) " . $dataRow['elternName'] . "!\n\n";
        $message .= "Sie haben erfolgreich Ihr Kind " . $dataRow['schuelerName'] . " für den Lernmittelfonds für das nächste Schuljahr angemeldet. \n";
        $message .= "Wir haben mittlerweile folgende Einzahlung über " . $dataRow['Betrag'] . "€ Ihrer Anmeldung zugeordnet:\n\n";
        $message .= "Konto: ******************" . substr($dataRow['Konto'], -4) . "\n";
        $message .= "BLZ: " . substr($dataRow['BLZ'], 0, 5) . "******\n";
        $message .= "Inhaber: " . $dataRow['Absender'] . "\n";
        $message .= "Verwendungszweck: " . $dataRow['Verwendungszweck'] . "\n\n";
        $message .= "Damit ist Ihre Anmeldung gültig und verbindlich. Ihr Kind wird zu Beginn des neuen Schuljahres die Bücher über den Lernmittelfonds erhalten.\n";
        $message .= "Wir danken Ihnen für Ihr Vertrauen.\n\n";
        $message .= "mit freundlichen Grüßen - Ihr Lernmittelfonds";
        $mailResult = doMailIntern($dataRow['email'], $subj, $message, "LMF");
        return $mailResult;
    } else {
        return false;
    }
}

// Einzelmail - sofort senden
function sendMailAnmeldung($SchuelerId, $beitrag, $jahr, $dbc) {
    $dataRow = getEmailDataBySchuelerId($SchuelerId, $dbc);
    if ($dataRow != NULL) {
        $subj = 'Lernmittelfonds Anmeldebestätigung';
        $message = "Sehr geehrte(r) " . $dataRow['Vorname'] . " " . $dataRow['Nachname'] . "!\n\n";
        $message .= "Sie haben erfolgreich Ihr Kind " . $dataRow['SVN'] . " " . $dataRow['SNN'] . " für den Lernmittelfonds für das nächste Schuljahr angemeldet. \n";
        if ($beitrag > 0) {
            $message .= "Um die Anmeldung verbindlich zu machen, zahlen Sie bitte bis zum " . $_SESSION['lmfZahlungsTermin_str'] . " den Jahresbeitrag von " . $dataRow['beitrag'] . " Euro auf folgendes Konto:\n\n";
            $message .= "Inhaber: Lernmittelfonds MDG\n";
            $message .= "Institut: " . LMF_BANK . "\n";
            $message .= "BIC: " . LMF_BIC . "\n";
            $message .= "IBAN: " . LMF_IBAN . "\n";
            $message .= "alt:( BLZ: " . LMF_BLZ . "; Konto: " . LMF_KONTO . ")\n";
            $message .= "Verwendungszweck: LMF-" . $jahr . "-" . $dataRow['SchuelerId'] . "A\n";
            $message .= "Verwendungszweck2: " . $dataRow['SVN'] . " " . $dataRow['SNN'] . "\n\n";
            $message .= "Sollten Sie innerhalb von 14 Tagen nach Einzahlung kein Bestätigungsmail erhalten, wenden Sie sich bitte an unsere Mailadresse, damit wir das Problem klären können.\n\n";
        } else {
            $message .= "Die Teilnahme ist für Sie ohne weitere Zahlungen möglich und hiermit verbindlich.\n\n";
        }
        $message .= "mit freundlichen Grüßen - Ihr Lernmittelfonds";
        lmf_trace("sendMailAnmeldung: From:" . EMAIL_LMF . "to " . $dataRow['Email']);
        lmf_trace("sendMailAnmeldung: $message . ");
        $mailResult = doMailIntern($dataRow['Email'], $subj, $message, "LMF");
        return '{"loggedIn":true, "mailResult":"' . $mailResult . '","success":true, "beitrag": ' . $beitrag . ', "schuelerId": ' . $SchuelerId . ', "mail": "' . $dataRow['Email'] . '", "content":"' . str_replace("\n", "<br>", $message) . '"  }';
    } else {
        return '{"loggedIn":true, "mailResult":"false","success":true, "errors":["Keine Schuelerdaten zum Mailversand gefunden"]}';
    }
}

// Massenmail - in die DB
function sendMailElternDatenGeloescht($dbc, $jobId, $Eltern) {
    $message = "Sehr geehrte(r) " . $Eltern['Vorname'] . " " . $Eltern['Nachname'] . "!\n\n";
    $message .= "Im Zuge von Datenschutz und Datenbereinigung haben wir festgestellt, "
            . "dass Ihrem Datensatz im Elterninfosysten / Lernmittelfonds keine Kinder (mehr) zugeordnet sind."
            . "Deshalb haben wir alle dieser Email zugeordneten Daten jetzt vollständig gelöscht - worüber wir Sie hiermit informieren möchten. \n "
            . "Mit freundlichen Grüßen - Ihr Lernmittelfonds / Elterninfosystem";
    $mail = new sendmail();
    $mail->from("ElternInfoSystem", EMAIL);
    $mail->to($Eltern['Email']);
    $mail->subject('Lernmittelfonds Daten gelöscht');
    $mail->text($message);
    return simpleTextMailToDb($dbc, $jobId, $mail);
}
// Massenmail - in die DB
function sendMailDatenGeloescht($dbc, $jobId, $Schueler, $Eltern) {
    $message = "Sehr geehrte(r) " . $Eltern['Vorname'] . " " . $Eltern['Nachname'] . "!\n\n";
    $message .= "Im Zuge von Datenschutz und Datenbereinigung haben wir festgestellt, "
            . "dass Ihr  Kind, " . $Schueler['Vorname'] . " " . $Schueler['Nachname']
            . " weder im vergangenen, noch im neuen Schuljahr mit einer aktuellen Klasse in unserem System gemeldet ist. \n"
            . "Deshalb haben wir diese Daten vollständig gelöscht - worüber wir Sie hiermit informieren möchten. \n "
            . "Sollte dies der einzige/letzte Datensatz mit einem Kind von Ihnen sein, werden in einem zweiten Prozess auch Ihre Daten als Eltern löschen.\n\n"
            . "Mit freundlichen Grüßen - Ihr Lernmittelfonds / Elterninfosystem";
    $mail = new sendmail();
    $mail->from("ElternInfoSystem", EMAIL);
    $mail->to($Eltern['Email']);
    $mail->subject('Lernmittelfonds Daten gelöscht');
    $mail->text($message);
    return simpleTextMailToDb($dbc, $jobId, $mail);
}

// Einzelmail - sofort senden
function sendMailRegister($mail, $id, $activation) {
    lmf_trace("sendMailAktivierungsCode start: " . $id);
    $subj = 'ElternInfoSystem Registrierungsbestätigung';
    $message = "Vielen Dank für ihre Registrierung beim Elterninfosystem.  Bei Ihrer nächsten Anmeldung werden Sie nach einem initialen Code gefragt. Bitte geben sie dann folgende Zeichenkette ein:\n\n";
    $message .= "     " . $activation;
    lmf_trace("sendMailRegister senden: " . $mail);
    return doMailIntern($mail, $subj, $message, "EIS");
}

// Einzelmail - sofort senden
function sendMailNeuesPasswort($passwort, $mailAdresse, $Nachname) {
    // ALLES ok MAIL SENDEN	
    $subj = 'Lernmittelfonds Datenänderung';
    $message = "Sehr geehrte Familie " . $Nachname . "!\n\n";
    $message .= " Für Ihre Mailadresse wurde ein neues Passwort für den Lernmittelfonds angefordert! \n";
    $message .= " Das neue Passwort lautet: " . $passwort . "\n\n";
    $message .= " Bitte melden sie sich auf unserer Webseite an und ändern das Passwort möglichst bald.\n\n";
    $message .= " Mit freundlichen Grüßen - Ihr Team vom Lernmittelfonds!";
    return doMailIntern($mailAdresse, $subj, $message, "EIS");
}

// Massenmail - in die DB
function sendMailAnmeldungFreigegeben($dbc) {
    $mailBetreff = "Anmeldung zum Lernmittelfonds freigegeben";
    $message = getTextAusDB("anmeldungNeu", $dbc);
    $count = -1;
    $query_familie = "SELECT DISTINCT e.*"
            . " FROM eltern e, view_schueler_dieses_jahr s, constants c 
        Where c.key='letzteKlasse'
        AND c.intValue>s.Stufe
        AND  s.FamilienId=e.FamilienId  AND e.stopMail!=1 AND s.bezahlt > 0;";

    $result_familie = mysqli_query($dbc, $query_familie);
    if ($result_familie) {
        lmf_trace("sendMailAnmeldungFreigegeben start");
        $jobId = createMailJob($dbc,"AnmeldungFreigegeben");
        $count = 0;
        $empfaenger = "";
        if ($jobId>0){
            while ($row = mysqli_fetch_array($result_familie)) {
                $mail = new sendmail();
                $mail->from("Lernmittelfonds", EMAIL_LMF);
                $mail->to($row['Email']);
                $mail->subject($mailBetreff);
                $mail->text(" Liebe Familie " . $row['Nachname'] . "!\n" . $message);
                if (!simpleTextMailToDb($dbc, $jobId, $mail)){
                    lmf_trace("Mail 'Anmeldung Freigegeben' an: " . $row['Email'] . " konnte nicht in DB übertragen werden");
                }
                $count++;
            }
        }else{
            lmf_trace("sendMailAnmeldungFreigegeben: Es konnt kein Mail-Job in der Datenbank angelegt werden");
            return $count;
        }
        lmf_trace("sendMailAnmeldungFreigegeben: " . $count . " Mails in DB");
        return $count;
    } else {
        lmf_queryTrace($query_familie, false, $dbc);
        return -1;
    }
}

// Massenmail - in die DB
function sendMailErinnern($dbc) {
    $mailBetreff = "Erinnerung: Anmeldezeitraum Lernmittelfonds endet in wenigen Tagen!!!";
    $message = getTextAusDB("anmeldungErinnern", $dbc);
    $count = -1;
    $query_familie = "SELECT DISTINCT e.Nachname, e.Email
        FROM view_anmeldungen_alt_und_neu a, schueler s, eltern e, constants c 
        Where c.key='letzteKlasse'
        AND c.intValue>a.Stufe
        AND a.Schuelerid = s.Schuelerid
        AND e.FamilienId = s.FamilienId
        AND e.stopMail=0
        AND a.bezahlt>0
        AND a.bezahltNeu=-1";
    $result_familie = mysqli_query($dbc, $query_familie);
    if ($result_familie) {
        lmf_trace("sendMailErinnern start");
        $jobId = createMailJob($dbc,"AnmeldungFreigegeben");
        $count = 0;
        if ($jobId>0){
            while ($row = mysqli_fetch_array($result_familie)) {
                $mail = new sendmail();
                $mail->from("Lernmittelfonds", EMAIL_LMF);
                $mail->to($row['Email']);
                $mail->subject($mailBetreff);
                $mail->text(" Liebe Familie " . $row['Nachname'] . "!\n" . $message);
                if (!simpleTextMailToDb($dbc, $jobId, $mail)){
                    lmf_trace("Mail 'Anmeldung Erinnern' an: " . $row['Email'] . " konnte nicht in DB übertragen werden");
                }
                $count++;
            }
        }else{
            lmf_trace("sendMailErinnern: Es konnt kein Mail-Job in der Datenbank angelegt werden");
            return $count;
        }
        lmf_trace("sendMailErinnerung: " . $count . " Mails vorbereitet");
        return $count;
    } else {
        lmf_queryTrace($query_familie, false, $dbc);
        return -1;
    }
}

// Massenmail - in die DB
function sendMailZahlungErinnern($dbc) {
    $mailBetreff = "Erinnerung: noch keine Einzahlung zum Lernmittelfonds verbucht!!!";
    $message = getTextAusDB("zahlungErinnern", $dbc);
    $count = -1;
    $query_familie = "SELECT DISTINCT e.Nachname, e.Email
        FROM view_schueler_naechstes_jahr s, eltern e 
        Where e.FamilienId = s.FamilienId
        AND e.stopMail=0
        AND s.bezahlt=0";

    $result_familie = mysqli_query($dbc, $query_familie);
    if ($result_familie) {
        lmf_trace("sendMailZahlungErinnern start");
        $count = 0;
        $jobId = createMailJob($dbc,"Zahlung Erinnern");
        if ($jobId>0){
            while ($row = mysqli_fetch_array($result_familie)) {
                $mail = new sendmail();
                $mail->from("Lernmittelfonds", EMAIL_LMF);
                $mail->to($row['Email']);
                $mail->subject($mailBetreff);
                $mail->text(" Liebe Familie " . $row['Nachname'] . "!\n" . $message);
                if (!simpleTextMailToDb($dbc, $jobId, $mail)){
                    lmf_trace("Mail 'Zahlung Erinnern' an: " . $row['Email'] . " konnte nicht in DB übertragen werden");
                }
                $count++;
            }
        }else{
            lmf_trace("sendMailZahlungErinnern: Es konnt kein Mail-Job in der Datenbank angelegt werden");
            return $count;
        }
        lmf_trace("sendMailZahlungErinnern: " . $count . " Mails vorbereitet");
        return $count;
    } else {
        lmf_queryTrace($query_familie, false, $dbc);
        return -1;
    }
}
// Massenmail - in die DB
function sendMailBuchabgabeErinnern($dbc) {
    $mailBetreff = "Lernmittelfonds: Rückgabe der alten Bücher";
    $message = getTextAusDB("buecherRückgabeMahn", $dbc);
    $count = -1;
    $abfrage_schueler = "SELECT DISTINCT e.*, s.Vorname as sVorname, s.Nachname as sNachname, s.SchuelerId
        FROM view_analyse_buch_rueckgabe v, schueler s, eltern e
        WHERE s.SchuelerId=v.SchuelerId
        AND s.FamilienId=e.FamilienId
        AND e.zuzahlungsBefreit=0";
    $ergebnis_schueler = mysqli_query($dbc, $abfrage_schueler);
    if ($ergebnis_schueler) {
        lmf_trace("sendMailZahlungErinnern start");
        $count = 0;
        $jobId = createMailJob($dbc,"Buchabgabe Erinnern");
        if ($jobId>0){
            while ($row = mysqli_fetch_array($ergebnis_schueler)) {
                $abfrage_buecher = "SELECT *
                FROM view_analyse_buch_rueckgabe v
                WHERE v.schuelerId = " . $row['SchuelerId'];
                $ergebnis_buecher = mysqli_query($dbc, $abfrage_buecher);
                $buecher = "";
                while ($row2 = mysqli_fetch_array($ergebnis_buecher)) {
                    $buecher .= $row2['Titel'] . ' (' . $row2['Isbn'] . ') - Neupreis:' . $row2['Neupreis'] . " Euro \n";
                }
                $message2 = str_replace("#KIND#", $row['sVorname'] . ' ' . $row['sNachname'], $message);
                $message2 = str_replace("#BUECHER#", $buecher, $message2);
                $mail = new sendmail();
                $mail->from("Lernmittelfonds", EMAIL_LMF);
                $mail->to($row['Email']);
                $mail->subject($mailBetreff);
                $mail->text(" Liebe Familie " . $row['Nachname'] . "!\n" . $message2);
                if (!simpleTextMailToDb($dbc, $jobId, $mail)){
                    lmf_trace("Mail 'Buchabgabe Erinnern' an: " . $row['Email'] . " konnte nicht in DB übertragen werden");
                }
                $count++;
            }
        }else{
            lmf_trace("sendMailBuchabgabeErinnern: Es konnt kein Mail-Job in der Datenbank angelegt werden");
            return $count;
        }
        lmf_trace("sendMailBuchabgabeErinnern: " . $count . " Mails vorbereitet");
        return $count;
    } else {
        lmf_queryTrace($abfrage_schueler, false, $dbc);
        return -1;
    }
}

function sendMailBuchabgabeInfo($dbc) {
    $mailBetreff = "Lernmittelfonds: Rückgabe der alten Bücher";
    $message = getTextAusDB("buecherRückgabeInfo", $dbc);
    $empfaenger = "";
    lmf_trace("sendMailBuchabgabeInfo start");
    $count = 0;
    $abfrage_klassen = "select * from  `view_klassen_altes_jahr`";
    $result_klassen = mysqli_query($dbc, $abfrage_klassen);
    $jobId = createMailJob($dbc,"Buchabgabe Info");
    if ($jobId>0){
        while ($klasse = mysqli_fetch_array($result_klassen)) {
            $abfrage_buecher = "SELECT BuchId, count(BuchId) , Isbn, Titel,Neupreis
            FROM view_analyse_buch_rueckgabe
            Where KlassenId=" . $klasse['KlassenId'] . " 
            GROUP BY BuchId;";
            $result_buecher = mysqli_query($dbc, $abfrage_buecher);
            $anzahlBuecher = 0;
            $selectAbfrage = "SELECT e.Nachname as familienName, e.email, s.*";
            $summe = "";
            $buecher = array();
            while ($buch = mysqli_fetch_array($result_buecher)) {
                $buecher[$anzahlBuecher] = $buch;
                $anzahlBuecher++;
                $selectAbfrage .= ",\n sum(if(v.BuchId=" . $buch['BuchId'] . ",1 , 0)) as buch_" . $anzahlBuecher;
                $summe .= "v.BuchId=" . $buch['BuchId'] . " OR ";
            }
            if ($summe != "") {
                $selectAbfrage .= ",\n sum(if(" . $summe . "1=2,1 , 0)) as gesamt";
            }
            $abfrage_schueler = $selectAbfrage . "
            FROM   view_schueler_dieses_jahr s 
            LEFT JOIN view_analyse_buch_rueckgabe v 
            ON s.schuelerid=v.schuelerid
            LEFT JOIN eltern e 
            ON s.familienId=e.familienId
            WHERE s.klassenId = " . $klasse['KlassenId'] . " 
            AND e.zuzahlungsBefreit = 0     
            GROUP BY s.schuelerid
            ORDER BY s.nachname, s.vorname";
            lmf_trace("*******************************");
            lmf_trace($buecher[0], true);
            lmf_trace("*******************************");
            $result_schueler = mysqli_query($dbc, $abfrage_schueler);
            while ($schueler = mysqli_fetch_array($result_schueler)) {
                if ($schueler['gesamt'] != 0) {
                    $empfaenger .= $count . " " . $schueler['Vorname'] . ' ' . $schueler['Nachname'] . ": " . $schueler['email'] . "\n";
                    $buecherListe = "";
                    for ($i = 1; $i <= $anzahlBuecher; $i++) {
                        if (intval($schueler['buch_' . $i]) > 0) {
                            $buecherListe .= $buecher[$i - 1]['Titel'] . ' (' . $buecher[$i - 1]['Isbn'] . ') - Neupreis:' . $buecher[$i - 1]['Neupreis'] . " Euro \n";
                        }
                    }
                    $message2 = str_replace("#KIND#", $schueler['Vorname'] . ' ' . $schueler['Nachname'], $message);
                    $message2 = str_replace("#BUECHER#", $buecherListe, $message2);
                    $mail = new sendmail();
                    $mail->from("Lernmittelfonds", EMAIL_LMF);
                    $mail->to($schueler['email']);
                    $mail->subject($mailBetreff);
                    $mail->text(" Liebe Familie " . $schueler['familienName'] . "!\n" . $message2);
                    if (!simpleTextMailToDb($dbc, $jobId, $mail)){
                        lmf_trace("Mail 'Mail BuchabgabeInfo' an: " . $row['Email'] . " konnte nicht in DB übertragen werden");
                    }
                    $count++;
                }
            }
        }
    }else{
        lmf_trace("sendMailBuchabgabeInfo: Es konnt kein Mail-Job in der Datenbank angelegt werden");
        return $count;
    }
    lmf_trace("sendMailBuchabgabeInfo: " . $count . " Mails vorbereitet:" . $check . "=OK?");
    return $count;
}

function getTextAusDB($textCode, $dbc) {
    $abfrage_textLaden = "SELECT * FROM texte WHERE code='" . $textCode . "';";
    $result_textLaden = mysqli_query($dbc, $abfrage_textLaden);
    if (!$result_textLaden) {
        return '';
    } else {
        if (mysqli_num_rows($result_textLaden) == 1) {
            $row = mysqli_fetch_array($result_textLaden);
            return $row['text'];
        } else {
            return "";
        }
    }
}

?>