<?php

/* CAT:Stacked chart */

/* pChart library inclusions */
include("../pChart/class/pData.class.php");
include("../pChart/class/pDraw.class.php");
include("../pChart/class/pImage.class.php");
require_once ('./lmf-session.php');
require_once ('database_connection.php');
require_once ('constants.php');
if (!isset($_SESSION)) {
    session_start();
}
$abfrage = "SELECT `jahr` , `klassenStufe` , 
            SUM( IF( klassenStufe =7, `teilnehmerBezahlt`+`teilnehmerZahlungsBefreit` , 0 ) ) sum7, 
            SUM( IF( klassenStufe =8, `teilnehmerBezahlt`+`teilnehmerZahlungsBefreit` , 0 ) ) sum8, 
            SUM( IF( klassenStufe =9, `teilnehmerBezahlt`+`teilnehmerZahlungsBefreit` , 0 ) ) sum9, 
            SUM( IF( klassenStufe =10, `teilnehmerBezahlt`+`teilnehmerZahlungsBefreit` , 0 ) ) sum10, 
            SUM( IF( klassenStufe =11, `teilnehmerBezahlt`+`teilnehmerZahlungsBefreit` , 0 ) ) sum11, 
            SUM( IF( klassenStufe =12, `teilnehmerBezahlt`+`teilnehmerZahlungsBefreit` , 0 ) ) sum12
            FROM `buecherlisten`
            GROUP BY `jahr`
            HAVING sum( `teilnehmerBezahlt` ) >0";
$ergebnis = mysqli_query($dbc, $abfrage);
if (!$ergebnis) {
    lmf_queryTrace($abfrage, false, $dbc);
    echo '';
} else {
    $jahr = array();
    $sum7 = array();
    $sum8 = array();
    $sum9 = array();
    $sum10 = array();
    $sum11 = array();
    $sum12 = array();

    while ($r = mysqli_fetch_assoc($ergebnis)) {
        $jahr[] = $r['jahr'];
        $sum7[] = $r['sum7'];
        $sum8[] = $r['sum8'];
        $sum9[] = $r['sum9'];
        $sum10[] = $r['sum10'];
        $sum11[] = $r['sum11'];
        $sum12[] = $r['sum12'];
    }
    /* Create and populate the pData object */
    $MyData = new pData();
    $MyData->addPoints($sum7, "Klasse 7");
    $MyData->addPoints($sum8, "Klasse 8");
    $MyData->addPoints($sum9, "Klasse 9");
    $MyData->addPoints($sum10, "Klasse 10");
    $MyData->addPoints($sum11, "Klasse 11");
    $MyData->addPoints($sum12, "Klasse 12");
    $MyData->addPoints($jahr, "Labels");
    $MyData->setSerieDescription("Labels", "Jahr");
    $MyData->setAbscissa("Labels");


    /* Create the pChart object */
    $myPicture = new pImage(1000, 380, $MyData);
    $myPicture->drawGradientArea(0, 0, 1000, 380, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
    $myPicture->drawGradientArea(0, 0, 1000, 380, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));

    /* Set the default font properties */
    $myPicture->setFontProperties(array("FontName" => "../fonts/verdana.ttf", "FontSize" => 10));

    /* Draw the scale and the chart */
    $myPicture->setGraphArea(60, 10, 950, 335);
    $myPicture->drawScale(array("DrawSubTicks" => TRUE, "Mode" => SCALE_MODE_ADDALL_START0));
    $myPicture->setShadow(FALSE);
    $myPicture->drawStackedBarChart(array("Surrounding" => -15, "DisplayValues"=>TRUE, "InnerSurrounding" => 15));

    /* Write a label */
//    for ($i = 0; $i < count($jahr); $i++) {
//        $myPicture->writeLabel(array("Klasse 7", "Klasse 8","Klasse 9", "Klasse 10","Klasse 11", "Klasse 12"), $i, array("DrawVerticalLine" => False));
//    }
    /* Write the chart legend */
    $myPicture->drawLegend(480, 365, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL));

    /* Render the picture (choose the best way) */
    $myPicture->autoOutput("pictures/example.drawStackedBarChart.shaded.png");
}
mysqli_close($dbc); //Close the DB Connection;
exit;
?>