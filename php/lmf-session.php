<?php
if(!isset($_SESSION)){ 
    session_start(); 
} 
// set time-out period (in seconds)
$inactive = 3600;
 
// check to see if $_SESSION["timeout"] is set
if (isset($_SESSION["timeout"])) {
    // calculate the session's "time to live"
    $sessionTTL = time() - $_SESSION["timeout"];
    if ($sessionTTL > $inactive) {
        session_destroy();
    }
}
$_SESSION["timeout"] = time();

?>
