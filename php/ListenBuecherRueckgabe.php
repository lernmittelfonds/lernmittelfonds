<?php
require_once ('./lmf-session.php');
include ('database_connection.php');
require_once ('constants.php');
require_once ('./lmf-logging.php');
if(!isset($_SESSION)){ 
    session_start(); 
} 

$error = array(); //this array will store all error messages
$loggedIn = true; //is logged in???
$request = json_decode(file_get_contents('php://input'), true);

if (!isset($_SESSION['FamilienId'])) {
    $error[] = 'Loginerror';
    echo '{"loggedIn":false, "errors":["Nicht korrekt angemeldet"], "success":false}';
    exit;
}

print '<html slick-uniqueid="3" xmlns="http://www.w3.org/1999/xhtml" xml:lang="de-de" lang="de-de">
    <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <style type="text/css">
    .lmf_pb { 
        page-break-after:always ; 
         margin-top: 20px;
         font-size: 14pt;
         border-bottom: 1px black solid;
         }
    @media print { 
    body, table { 
        font-size: 12pt; 
    }
}
   </style>
  </head>
<body>';

$abfrage_klassen = "select * from  `view_klassen_altes_jahr`";
$result_klassen = mysqli_query($dbc, $abfrage_klassen);
while ($klasse = mysqli_fetch_array($result_klassen)) {
    $abfrage_buecher = "SELECT BuchId, count(BuchId) , Isbn, Titel,Neupreis
    FROM view_analyse_buch_rueckgabe
    Where KlassenId=" . $klasse['KlassenId'] . " 
    GROUP BY BuchId;";
    $result_buecher = mysqli_query($dbc, $abfrage_buecher);
    $anzahlBuecher = 0;
    $selectAbfrage = "SELECT s.*";
    $summe =""; 
    $initial=true;
    while ($buch = mysqli_fetch_array($result_buecher)) {
        if ($initial){
            print '<h2 class="lmf_listenTitel"> Lernmittelfonds ' . $klasse['Jahr'] . '/' . (1 + $klasse['Jahr']) . ' - Buchrückgabe Klasse ' . $klasse['Stufe'] . $klasse['SubKlasse'] . '-' . $klasse['Sprache'] . '</h2>';
            print '<div>Folgende Lernmittel werden eingesammelt:</div><ol style="margin:3px">';
            $initial=false;
        }
        $anzahlBuecher++;
        print '<li> ' . $buch['Isbn'] . ': ' . $buch['Titel'] . ' (' . $buch['Neupreis'] . ' €)</li>';
        $selectAbfrage .= ",\n sum(if(v.BuchId=" . $buch['BuchId'] . ",1 , 0)) as buch_" . $anzahlBuecher;
        $summe .= "v.BuchId=" . $buch['BuchId'] . " OR ";
    }
    if ($summe!=""){
        $selectAbfrage .= ",\n sum(if(" . $summe . "1=2,1 , 0)) as gesamt";
    
    $abfrage_schueler = $selectAbfrage . "
        FROM   view_schueler_dieses_jahr s 
        LEFT JOIN view_analyse_buch_rueckgabe v 
        ON s.schuelerid=v.schuelerid
        WHERE s.klassenId = " . $klasse['KlassenId'] . " 
        GROUP BY s.schuelerid
        ORDER BY s.nachname, s.vorname";
    //lmf_trace($abfrage_schueler);
    $result_schueler = mysqli_query($dbc, $abfrage_schueler);
    $lfdNr = 1;
    print '</ol>';
    print '<div>'.str_replace("\n", "<br>",$_POST['kommentar']).'</div>';
    print '<table border=1 cellspacing="0" cellpadding="3" style="width:100%"><thead><tr><th style="width:30px">Nr</th><th >Schüler</th>';
    for ($i = 1; $i <= $anzahlBuecher; $i++) {
        print '<th style="width:50px">Buch ' . $i . '</th>';
    }
    if (isset($_REQUEST["type"]) && $_REQUEST["type"] == "alle") {
        echo '<th>Bemerkungen</th></tr></thead>';
    }else{
        echo '<th>Rückgabe</th></tr></thead>';
    }    
    while ($schueler = mysqli_fetch_array($result_schueler)) {
        if ($schueler['gesamt']!=0){
            print '<tr><td>' . $lfdNr . '</td><td> ' . $schueler['Nachname'] . ', ' . $schueler['Vorname'] . '</td>';
            for ($i = 1; $i <= $anzahlBuecher; $i++) {
                $rueckgabe= (intval ($schueler['buch_' . $i]) > 0)?'X':'&nbsp';
                echo '<td align="center"> ' . $rueckgabe . '</td>';
            }
            echo '<td> &nbsp </td></tr>';
            $lfdNr++;
        }
    }
    echo '</table>';
    print '<div class = "lmf_pb">einsammelnde(r) Lehrer(in): </div>';
    }
    
}
print '</body></html>';

mysqli_close($dbc); //Close the DB Connection;
exit;
?>
