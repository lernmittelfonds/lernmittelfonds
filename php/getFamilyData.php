<?php

require_once ('./lmf-session.php');
if (!isset($_SESSION)) {
    session_start();
}
header('P3P: CP="CAO PSA OUR"');
header('Cache-Control: no-cache');
header('Pragma: no-cache');
?>
<?php

include ('database_connection.php');
require_once ('./constants.php');
require_once ('./lmf-logging.php');

$error = array(); 
$loggedIn = true; 

if (!isset($_SESSION['FamilienId'])) {
    echo '{"loggedIn":false, "errors":["Nicht korrekt angemeldet:' .  $_GET['_FamilienId'] . '"], "success":false}';
} else if ($_GET["type"] == "stammdaten") {
    if ($_GET["FamilienId"] == "") {
        $FamilienId = $_SESSION['FamilienId'];
    } else {
        $FamilienId = $_GET["FamilienId"];
    }
    $abfrage_eltern = "SELECT * FROM eltern WHERE FamilienId=" . $FamilienId;

    $result_eltern = mysqli_query($dbc, $abfrage_eltern);
    if ($result_eltern && @mysqli_num_rows($result_eltern) == 1) {
        $eltern = mysqli_fetch_array($result_eltern, MYSQLI_ASSOC); //Assign the result of this query to Variable
        lmf_trace('getFamilydate eltern ' . json_encode($eltern));
    } else {
        lmf_trace('getFamilydate mist ');
        lmf_queryTrace($abfrage_eltern, false, $dbc);
    }

    $abfrage_kinder = " SELECT anm_1.bezahlt as thisYear, anm_2.bezahlt as nextYear, MAX(anm_3.bezahlt)as fixed, schueler.*, k1.KlassenId as alteKlassenId, k2.KlassenId"
            . " FROM schueler"
            . " LEFT JOIN (select * from anmeldung, constants c where Schuljahr=c.intValue AND c.key = 'thisYear' )anm_1 ON schueler.SchuelerId=anm_1.SchuelerId "
            . " LEFT JOIN (select * from anmeldung, constants c where Schuljahr=(c.intValue +1) AND c.key = 'thisYear' ) anm_2 ON schueler.SchuelerId=anm_2.SchuelerId "
            . " LEFT JOIN klasse k1 on k1.KlassenId=anm_1.KlassenId"
            . " LEFT JOIN klasse k2 on k2.KlassenId=anm_2.KlassenId"
            . " LEFT JOIN anmeldung anm_3 ON schueler.SchuelerId=anm_3.SchuelerId "
            . " WHERE FamilienId=" . $FamilienId
            . " GROUP BY schueler.SchuelerId;";
    $result_kinder = mysqli_query($dbc, $abfrage_kinder);

    $abfrage_klassenAlt = "SELECT *
                FROM view_klassen_lang 
                Where (KlassenStufeAlt != '+++++' OR KlassenStufeNeu != '+++++')
                ORDER BY OrderAlt";
    $result_klassenAlt = mysqli_query($dbc, $abfrage_klassenAlt);
    $abfrage_klassenNeu = "SELECT *
                FROM view_klassen_lang 
                Where (KlassenStufeAlt != '+++++' OR KlassenStufeNeu != '+++++')
                ORDER BY OrderNeu";
    $result_klassenNeu = mysqli_query($dbc, $abfrage_klassenNeu);


    echo '{ "thisYear" :  ' . THIS_YEAR . ",\n";
    echo '"eltern" : {' . "\n";
    echo '"loggedIn": "' . $loggedIn . "\",\n";
    echo '"FamilienId" :  "' . $eltern['FamilienId'] . '",' . "\n";
    echo '"Vorname" :  "' . $eltern['Vorname'] . '",' . "\n";
    echo '"Nachname" :  "' . $eltern['Nachname'] . '",' . "\n";
    echo '"Anschrift" :  "' . $eltern['Anschrift'] . '",' . "\n";
    echo '"Freigabe_ev" :  "' . $eltern['Freigabe_ev'] . '",' . "\n";
    echo '"Freigabe_alle" :  "' . $eltern['Freigabe_alle'] . '",' . "\n";
    echo '"Freigabe_info" :  "' . $eltern['Freigabe_info'] . '",' . "\n";
    echo '"Freigabe_lmf" :  "' . $eltern['Freigabe_lmf'] . '",' . "\n";
    echo '"isAdmin" :  "' . $eltern['isAdmin'] . '",' . "\n";
    echo '"zuzahlungsBefreit" :  "' . $eltern['zuzahlungsBefreit'] . '",' . "\n";
    echo '"stopMail" :  "' . $eltern['stopMail'] . '",' . "\n";
    echo '"Telefon" :  "' . $eltern['Telefon'] . '",' . "\n";
    echo '"Email" :  "' . $eltern['Email'] . '"},' . "\n";
    echo '"Schueler" : [' . "\n";

    $firstLine = true;
    while ($row = mysqli_fetch_array($result_kinder)) {
        if (!$firstLine) {
            echo ",\n";
        } else {
            $firstLine = false;
        }
        echo "{\n\"SchuelerId\": \"" . $row['SchuelerId'] . "\",\n";
        echo "\"FamilienId\": \"" . $row['FamilienId'] . "\",\n";
        echo "\"fixed\": \"" . $row['fixed'] . "\",\n";
        echo "\"Vorname\": \"" . $row['Vorname'] . "\",\n";
        echo "\"Nachname\": \"" . $row['Nachname'] . "\",\n";
        echo "\"istEV\": " . (1 == $row['istElternvertreter'] ? "true" : "false") . ",\n";
        echo "\"EisStatus\": " . (1 == $row['eisStatus'] ? "true" : "false") . ",\n";
        echo "\"Kommentar\": \"" . str_replace("\n", "<br>", $row['Anmerkungen']) . "\",\n";
        echo "\"alteKlassenId\": \"" . $row['alteKlassenId'] . "\",\n";
        echo "\"KlassenId\": \"" . $row['KlassenId'] . "\",\n";
        echo "\"thisYear\": \"" . $row['thisYear'] . "\",\n";
        echo "\"nextYear\": \"" . $row['nextYear'] . "\"}\n";
    }
    mysqli_free_result($result_kinder);

    echo "],\n";
    $klassenKeysAlt = "KlassenKeysAlt: [\n";
    $klassenStringsAlt = "KlassenStringsAlt: [\n";
    $klassenKeysNeu = "KlassenKeys: [\n";
    $klassenStringsNeu = "KlassenStrings: [\n";

    $firstLine = true;
    while ($row = mysqli_fetch_assoc($result_klassenAlt)) {
        if (!$firstLine) {
            $klassenKeysAlt .= ",\n";
            $klassenStringsAlt .= ",\n";
        } else {
            $firstLine = false;
        }
        $klassenKeysAlt .= $row['KlassenId'];
        $klassenStringsAlt .= '"' . $row['KlassenStufeAlt'] . '"';
    }
    $firstLine = true;
    while ($row = mysqli_fetch_assoc($result_klassenNeu)) {
        if (!$firstLine) {
            $klassenKeysNeu .= ",\n";
            $klassenStringsNeu .= ",\n";
        } else {
            $firstLine = false;
        }
        $klassenKeysNeu .= $row['KlassenId'];
        $klassenStringsNeu .= '"' . $row['KlassenStufeNeu'] . '"';
    }

    mysqli_free_result($result_klassenAlt);
    mysqli_free_result($result_klassenNeu);
    $klassenKeysAlt .= "],";
    $klassenStringsAlt .= "],";
    $klassenKeysNeu .= "],";
    $klassenStringsNeu .= "]";
    echo $klassenKeysAlt;
    echo $klassenStringsAlt;
    echo $klassenKeysNeu;
    echo $klassenStringsNeu;
    echo "}";
} else if ($_GET["type"] == "buecher") {
    if ($_GET["FamilienId"] == "") {
        $FamilienId = $_SESSION['FamilienId'];
    } else {
        $FamilienId = $_GET["FamilienId"];
    }
    $abfrage_buecher = "select a.schuelerId,a.eingesammelt, b.*
                        from schueler s, buecher b, ausgeliehen a
                        where s.schuelerid=a.schuelerid
                        and b.buchId = a.buchId
                        and s.familienid=" . $FamilienId
            . " Order BY a.schuelerId;";
    $result_buecher = mysqli_query($dbc, $abfrage_buecher);
    echo '{"ausGelieheneBuecher" : { ' . "\n";
    $letztesKind = "";
    $erstesKind = true;
    $erstesBuch = true;
    if ($result_buecher){
        while ($row = mysqli_fetch_array($result_buecher)) {
            //echo "/*---".$letztesKind."---".$row['SchuelerId']."---*/";
            if ($letztesKind != $row['schuelerId']) {
                if (!$erstesKind) {
                    echo '],' . "\n";
                }
                $letztesKind = $row['schuelerId'];
                $erstesKind = false;
                echo '"schueler_' . $row['schuelerId'] . "\":[\n";
            } else {
                echo ',' . "\n";
            }
            echo '{ "BuchId":' . $row['BuchId'] . ",\n";
            echo '  "eingesammelt":' . ($row['eingesammelt'] == 1 ? 'true' : 'false') . ",\n";
            echo '  "Isbn":"' . $row['Isbn'] . "\",\n";
            echo '  "Titel":"' . $row['Titel'] . "\",\n";
            echo '  "Neupreis":' . $row['Neupreis'] . "}";
        }
    }else{
         lmf_queryTrace($abfrage_buecher, false, $dbc);
    }
    if (!$erstesKind) {
        echo ']' . "\n";
    }
    echo '}}';
}
mysqli_close($dbc); //Close the DB Connection;
exit;
?>