<?php

require_once ('./lmf-session.php');
include ('database_connection.php');
require_once ('constants.php');
require_once ('./lmf-logging.php');
if (!isset($_SESSION)) {
    session_start();
}

$error = array(); //this array will store all error messages
$loggedIn = true; //is logged in???

if (!isset($_SESSION['FamilienId'])) {
    $error[] = 'Loginerror';
    echo '{"loggedIn":false, "errors":["Nicht korrekt angemeldet"], "success":false}';
    exit;
}
setlocale(LC_MONETARY, 'de_DE');

echo '<html slick-uniqueid="3" xmlns="http://www.w3.org/1999/xhtml" xml:lang="de-de" lang="de-de">
    <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <style type="text/css">
    .lmf_pb { 
        page-break-after:always ; 
         margin-top: 20px;
         font-size: 14pt;
         border-bottom: 1px black solid;
         }
    @media print { 
    body, table { 
        font-size: 12pt; 
    }
}
   </style>
  </head>
<body><table border=1 cellspacing="0" cellpadding="3" style="padding:5px, spacing:15px;">';
echo "<h1>Abrechnung Lernmittelfonds (LMF) aktuelles Schuljahr</h1>";
echo "<div>Wie funktioniert es?</div>";
echo "<div>Die Bücher des Lernmittelfonds müssen laut Schulgesetz durch die Schule gekauft werden und gehören auch der Schule!</div>";
echo "<div>Aus unterschiedlichsten Gründen kommt es immer wieder vor, dass der LMF Bücher verteilt, die die Schule ursprünglich für sich bestellt hat und umgekehrt.</div>";
echo "<div>Wenn also die Bücher regelmäßig ihren (eh nur virtuellen) Besitzer wechseln, wie soll man dann kalkulieren?</div>";
echo "<div>Unsere Lösung und Kalkulationsgrundlage ist ein gedachtes Leihmodell:<div>";
echo "<div>  ==>> der LMF leiht die Bücher von der Schule gegen eine Gebühr und die Einnahmen des Lernmittelfonds müssen diese gedachte Gebühr decken.</div>";
echo "<p></p>";
echo "<div><b>Beispiel 1: </b> Ein Buch (z.B Ethik 7+8)kostet 20€ und wird für 2Jahre pro Schüler ausgeliehen.</div>";
echo "<div>Das Schulamt verlangt eine Mindestnutzungsdauer von 4 Jahren. Das heisst - es kann 2 x ausgeliehen werden - somit kostet jede Leihe 10€.</div>";
echo "<div>Die Schule bekommt aber beim kauf von Büchern 12% Rabatt, so dass eine Leihe dieses Buches mit 8,80 Euro kalkuliert wird:</div>";
echo '<div style="color:blue;">Kurz: 20€ / 4 Jahre Nutzungsdauer x 2 Jahre Ausleihe - 12% = 8,80 €</div>';
echo "<div><b>Beispiel 2: </b> Ein zweites Buch (z.B. Deutsch) welches auch 20€ kostet, aber wird immer nur für ein Jahr ausgeliehen. Hier sieht die Rechnung so aus:</div>";
echo '<div style="color:blue;">Kurz: 20€ / 4 Jahre Nutzungsdauer x 1 Jahre Ausleihe - 12% = 4,40 €</div>';
echo "<div><b>Beispiel 3: </b> Ein Arbeitsheft (z.B. Englisch) welches 10€ kostet, aber dauerhaft ausgegeben wird. Hier sieht die Rechnung so aus:</div>";
echo '<div style="color:blue;">Heft: 10€ / 1 Jahre Nutzungsdauer x 1 Jahre Ausleihe - 12% = 8,80 €</div>';
echo "<p></p>";
echo "<div>Nehmen wir jetzt an, wir Haben im LMF für eine Klasse ein Buch aus Beispiel 1, zwei Bücher aus Beispiel 2 und 2 Arbeitshefte. Dann müssen wir mit:</div>";
echo '<div style="color:blue;">Heft: 1 * 8,80€ (Buch 1) + 2 * 4,40€ (Buch2) und 2*8,80€ (Arbeitshefte) = 35,20€ Leihgebühr kalkulieren</div>';
echo "<div>Bei 36,-Euro Teilnahmebeitrag haben wir eine Kostendeckung von 36,00/35,20 = 102%</div>";
echo "<p></p>";
echo "<div>Das wäre aber zu wenig, da die Klassenstärken und Teilnehmerzahlen schwanken, Bücher verloren gehen oder auch mal ein paar zuviel gekauft werden.</div>";
echo "<div>Wir haben uns darauf verständigt, das der Beitrag angemessen ist, wenn die Kostendeckung bei 105%-115% liegt.</div>";
echo "<p></p>";
echo "<div>Es folgt unsere Kalkulation. Im Gegensatz zu den Beispielen kalkulieren wir aber mit einer durchschnittlichen Nutzungsdauer von 5 Jahren.</div>";
echo "<div>Das entspricht unseren Erfahrungen und führt zu günstigeren Beiträgen</div>";
echo "<p></p>";

$standardNutzDauer = 5;
$abfrage_kalkulation = "SELECT * FROM view_abrechnung_altes_jahr_grundlage Order by  klassenstufe, sprache";
$result_kalkulation = mysqli_query($dbc, $abfrage_kalkulation);
$sprache = "";
$klasse = "";
$teilnehmer = 0;
$beitrag = 0;
$summe = 0;
$ausgaben = 0;
$einnahmen = 0;
while ($buch = mysqli_fetch_array($result_kalkulation)) {
    if ($sprache . $klasse != $buch['sprache'] . $buch['klassenStufe']) {
        if ($sprache . $klasse != "") {
            // nicht die erste Zeile - die alte Klasse ist zu Ende - Summe schreiben
            $ausgaben+=$summe * $teilnehmer;
            $einnahmen+=$beitrag * $teilnehmer;
            echo '<tr class="Summe"><td colspan=4>Summe Ausgaben: </td><td style="text-align: right; color:red;">' . number_format($summe, 2, ',', '.') . ' €</td>'
            . '<td>x ' . $teilnehmer . ' Teilnehmer =</td><td style="text-align: right; color:red";>- ' . number_format($summe * $teilnehmer, 2, ',', '.') . ' €</td></tr>' . PHP_EOL;
            echo '<tr class="Summe"><td colspan=4>Summe Einnahmen: </td><td style="text-align: right; color:green;">' . number_format($beitrag, 2, ',', '.') . ' €</td>'
            . '<td>x ' . $teilnehmer . ' Teilnehmer =</td><td style="text-align: right; color:green;"> ' . number_format($beitrag * $teilnehmer, 2, ',', '.') . ' €</td></tr>' . PHP_EOL;
            echo '<tr class="Summe"><td colspan=5></td></tr>' . PHP_EOL;
        }
        $summe = 0;
        $sprache = $buch['sprache'];
        $klasse = $buch['klassenStufe'];
        $beitrag = $buch['beitrag'];
        $teilnehmer = $buch['teilnehmer'];

        echo '<tr style="width:30px" class="ZwischenTitel"><td colspan=5><b>Klasse/Kurs: ' . $klasse . ' - ' . $sprache . '</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Teilnehmer: ' . $teilnehmer . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Beitrag: ' . $beitrag . '</td></tr>' . PHP_EOL;
        echo '<tr><td>Buch</td><td>Leihdauer</td><td>Nutzdauer</td><td>Neupreis</td><td>Leihpreis</td></tr>' . PHP_EOL;
    }
    $summe+=$buch['leihpreis'];
    $leihdauer = $buch['bis'] == 0 ? 1 : 1 + $buch['bis'] - $buch['von'];
    $nutzdauer = $buch['bis'] == 0 ? 1 : max($standardNutzDauer, 1 + $buch['bis'] - $buch['von']);
    echo '<tr><td>' . $buch['Titel'] . '</td>'
    . '<td>' . $leihdauer . '</td>'
    . '<td>' . $nutzdauer . '</td>'
    . '<td style="text-align: right;">' . number_format($buch['Neupreis'], 2, ',', '.') . ' €</td>'
    . '<td style="text-align: right;">' . number_format($buch['leihpreis'], 2, ',', '.') . ' €</td></tr>' . PHP_EOL;
}
echo '<tr class="Summe"><td colspan=4>Summe Ausgaben: </td><td style="text-align: right; color:red;">' . number_format($summe, 2, ',', '.') . ' €</td>'
 . '<td>x ' . $teilnehmer . ' Teilnehmer =</td><td style="text-align: right; color:red";>- ' . number_format($summe * $teilnehmer, 2, ',', '.') . ' €</td></tr>' . PHP_EOL;
echo '<tr class="Summe"><td colspan=4>Summe Einnahmen: </td><td style="text-align: right; color:green;">' . number_format($beitrag, 2, ',', '.') . ' €</td>'
 . '<td>x ' . $teilnehmer . ' Teilnehmer =</td><td style="text-align: right; color:green;"> ' . number_format($beitrag * $teilnehmer, 2, ',', '.') . ' €</td></tr>' . PHP_EOL;
            echo '<tr class="Summe"><td colspan=5></td></tr>' . PHP_EOL;
            echo '<tr class="Summe"><td colspan=5></td></tr>' . PHP_EOL;
echo '<tr class="Summe"><td colspan=6><b>Gesamtsumme Ausgaben:</b> </td><td style="text-align: right; color:red";><b>- ' . number_format($ausgaben, 2, ',', '.') . ' €</b></td></tr>' . PHP_EOL;
echo '<tr class="Summe"><td colspan=6><b>Gesamtsumme Einnahmen:</b> </td><td style="text-align: right; color:green;"><b> ' . number_format($einnahmen, 2, ',', '.') . ' €</b></td></tr>' . PHP_EOL;
echo '<tr class="Summe"><td colspan=6><b>Kostendeckung gesamt:</b> </td><td style="text-align: right; "><b> ' . number_format($einnahmen/$ausgaben*100, 2, ',', '.') . ' %</b></td></tr>' . PHP_EOL;

echo '</table></body></html>';

mysqli_close($dbc); //Close the DB Connection;
exit;
?>
