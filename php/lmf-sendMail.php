<?php
class sendmail {

// Variable deklarieren
    var $fromName="";
    var $fromMail="";
    var $emailheader = "";
    var $textheader = "";
    var $textbody = "";
    var $textboundary = "";
    var $emailboundary = "";
    var $betreff = "";
    var $empfaenger = "";
    var $attachment = array();

    function __construct() {
        $this->textboundary = uniqid(time());
        $this->emailboundary = uniqid(time());
    }

// Von wem die Email kommt in den Header setzen
    function from($name, $email) {
        $this->fromName = $name;
        $this->fromMail = $email;
        $this->emailheader .= "From: $name<$email>\n";
        $this->emailheader .= "MIME-Version: 1.0\n";
    }

// Funktion um den Adressaten anzugeben
    function to($to) {
        $this->empfaenger = $to;
    }

// Funktionn für den Betreff anzugeben
    function subject($subject) {
        $this->betreff = $subject;
    }

// Textdaten in Email Header packen
    function text($text) {
        $this->textheader .= "Content-Type: multipart/alternative; boundary=\"$this->textboundary\"\n\n";
        $this->textheader .= "--$this->textboundary\n";
        $this->textheader .= "Content-Type: text/plain; charset=\"UTF-8\"\n";
        $this->textheader .= "Content-Transfer-Enconding: quoted-printable\n\n";
        $this->textheader .= strip_tags($text) . "\n\n";
        $this->textheader .= "--$this->textboundary\n";
        $this->textheader .= "Content-Type: text/html; charset=\"UTF-8\"\n";
        $this->textheader .= "Content-Transfer-Enconding: quoted-printable\n\n";
        $this->textheader .= "<html><body>$text</body></html>\n\n";
        $this->textheader .= "--$this->textboundary--\n\n";
        $this->text = $text;
    }

    /*
// Funktion zum anhängen für Attachments in der Email
    function attachment($datei, $filename) {
// Überprüfen ob File Existiert
        if (is_file($datei)) {
// Header für Attachment erzeugen
            $attachment_header = "--" . $this->emailboundary . "\n";
            $attachment_header = "----=_NextPart_BIG_UUID_1\r\n";
            $attachment_header .= "Content-Type: application/octet-stream;\n name=\"$filename\"\n";
            $attachment_header .= "Content-Transfer-Encoding: base64\n";
            $attachment_header .= "Content-Disposition: attachment;\n filename=\"$filename\"\n\n";

// Daten der Datei einlesen, in das BASE64 Format formatieren und auf max 72 Zeichen pro Zeile
// aufteilen
            $file['inhalt'] = fread(fopen($datei, "rb"), filesize($datei));
            $file['inhalt'] = base64_encode($file['inhalt']);
            $file['inhalt'] = chunk_split($file['inhalt'], 72);

// Attachment mit Header in der Klassenvariable speichern
            $this->attachment[] = $attachment_header . $file['inhalt'] . "\n";
        } else {
            lmf_trace("Die Datei $datei existiert nicht...\n");
        }
    }
     * 
     */

    /* wird nicht mehr benutzt
// Funktion zum erstellen des Kompletten Headers der Email 
// Senden der Email
    function send() {
    $header = $this->emailheader;

// Überprüfen ob Attachments angehängt wurden 
            $header .= "Content-Type: multipart/mixed; boundary=\"$this->emailboundary\"\n\n";
            $header .= "--$this->emailboundary\n";
            
            $headers  =  $this->emailheader;
            $headers .= "Content-Type: multipart/mixed; boundary=\"--=_NextPart_BIG_UUID_1\"\r\n";

            $body  = "----=_NextPart_BIG_UUID_1\r\n";
            $body .= "content-type: multipart/alternative; boundary=\"--=_NextAltPart_BIG_UUID_2\"\r\n";
            $body .= "\r\n";
            
            $body .= "----=_NextAltPart_BIG_UUID_2\r\n";
            $body .= "content-type: text/plain; charset=UTF-8\r\n";
            $body .= "content-transfer-encoding: quoted-printable\r\n";
            $body .= "\r\n";
//            $body .= strip_tags( $this->text)."\n";
            $body .=  $this->text."\n";

            $body .= "\r\n----=_NextAltPart_BIG_UUID_2";
            $body .= "\r\n";
            $body .= "content-type: multipart/related; boundary=\"--=_NextAltRelPart_BIG_UUID_3\"\r\n";
            $body .= "\r\n";

            $body .= "----=_NextAltRelPart_BIG_UUID_3\r\n";
            $body .= "content-type: text/html; charset=UTF8\r\n";
            $body .= "content-transfer-encoding: quoted-printable\r\n";
            $body .= "\r\n";
            $body .= "<html><body>".$this->text."</body></html>\n";


            $body .= "\r\n----=_NextAltRelPart_BIG_UUID_3--\r\n";
            $body .= "\r\n----=_NextAltPart_BIG_UUID_2--\r\n";

            for ($i = 0; $i < count($this->attachment); $i++) {
               $body .= $this->attachment[$i];
            }

            $body .= "\r\n----=_NextPart_BIG_UUID_1--";

           // Versenden der Mail   
           return mail($this->empfaenger, $this->betreff, $body, $headers);
    } */
}
?> 