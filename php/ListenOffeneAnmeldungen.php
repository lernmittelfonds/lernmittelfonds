<?php
require_once ('./lmf-session.php');
include ('database_connection.php');
require_once ('constants.php');
require_once ('./lmf-logging.php');
if(!isset($_SESSION)){ 
    session_start(); 
} 

$error = array(); //this array will store all error messages
$loggedIn = true; //is logged in???
$request = json_decode(file_get_contents('php://input'), true);

if (!isset($_SESSION['FamilienId'])) {
    $error[] = 'Loginerror';
    echo '{"loggedIn":false, "errors":["Nicht korrekt angemeldet"], "success":false}';
    exit;
}

print '<html slick-uniqueid="3" xmlns="http://www.w3.org/1999/xhtml" xml:lang="de-de" lang="de-de">
    <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <style type="text/css">
    .lmf_pb { 
        page-break-after:always ; 
         margin-top: 20px;
         font-size: 14pt;
         border-bottom: 1px black solid;
         }
    @media print { 
    body, table { 
        font-size: 12pt; 
    }
}
   </style>
  </head>
<body>';

$abfrageOffeneAnmeldungen = "SELECT a.*,  concat(s.Nachname, ', ', s.Vorname) AS schuelerName,  
                    e.Nachname AS elternName, 
                    s.FamilienId, e.email, e.Telefon, s.Anmerkungen
                    FROM view_anmeldungen_alt_und_neu a, schueler s, eltern e 
                    Where a.Schuelerid = s.Schuelerid
                    AND e.FamilienId = s.FamilienId
                    AND e.zuZahlungsBefreit=0
                    AND bezahltNeu=-1 AND Stufe<12 Limit 100;";
$result = mysqli_query($dbc, $abfrageOffeneAnmeldungen);
while ($schueler = mysqli_fetch_array($result)) {
    print '<h3>Absender: Lernmittelfonds  Max-Delbrück-Gymnasium</h3><p></p>';
    print '<h2 class="lmf_listenTitel">' . $schueler['schuelerName'] . ' / Klasse: ' . $schueler['Stufe'] .$schueler['SubKlasse'] . '</h2><p><p/>';
    print 'Sehr geehrte Familie ' . $schueler['elternName'] . '!<p><p/>';
    $abfrage_buecher = "SELECT * FROM ausgeliehen a, buecher b
                    where a.buchId=b.BuchId
                    AND ifNull(a.eingesammelt,0) = 0
                    AND a.schuelerId= ".$schueler['SchuelerId'].";";
    $result_buecher = mysqli_query($dbc, $abfrage_buecher);
    print '<div>Die reguläre Anmeldefrist für den Lernmittelfonds 2015/2016 ist abgelaufen. Leider haben wir von Ihnen keine neue Anmeldung noch eine andere Information erhalten.
        Wir müssen also davon ausgehen, dass Ihr Kind '. $schueler['schuelerName'] .' die Schule verlässt oder Sie andere trifftige Gründe für Ihre Entscheidung haben.
        Sollte Ihr Kind weiterhin das Max-Delbrück-Gymnasium besuchen, weisen wir Sie noch einmal darauf hin, dass es alle vom Lernmittelfonds geliehenen Bücher zurückgeben muss.
        Das betrifft insbesondere auch folgende Lernmittel  :</div><ol>';
    while ($buch = mysqli_fetch_array($result_buecher)) {
        if ( (int)$buch['bis']>(int)$schueler['Stufe'] ){
            print '<li> ' . $buch['Isbn'] . ': ' . $buch['Titel'] . ' (' . $buch['Neupreis'] . ' €)</li>';
        }
    }
    print '</ol><p></p><div>Falls das alles so von Ihnen gewünscht ist, wären wir über eine kurze Info froh, dass wir Sie nicht weiter zu behelligen brauchen.
        Schicken Sie uns eine kurze Mail oder melden sich an usnerer webapplikation an und klicken am Ende der zeile mit dem namen Ihres Kindes auf "abmelden."';
    print '<p></p><div>Falls es nicht von Ihnen gewünscht ist, Ihre Teilnahme am Lernmittelfonds zu beenden, so geben wir Ihnen noch weitere 4 Tage Zeit um ihre Anmeldung vorzunehmen.
        Besuchen Sie bitte möglichst bald, bis spätestens Donnerstag Abend unsere Webseite und melden sich unter dem Link "Lernmittelfonds" mit Ihrer email-Adresse an.
        Wenn Sie Ihr Passwort vergessen haben, können sie ein neues Passwort anfordern. Wenn Sie Probleme haben, senden Sie uns eine Mail mit einer Festnetznummer - wir rufen sie zurück.
        <br>
        Bitte überweisen Sie den Teilnahmebetrag (außer Schüler der 12.Klassen, deren Anmeldung kostenfrei ist) bis spätestens Freitag auf unser Konto. 
        Die Kontoverbindung erhalten Sie, sobald Sie an unserer Anwendung angemeldet sind und am Ende der Zeile Ihres Kindes "anmelden" gedrückt haben.
        </div><p></p><p></p><div>Besonderheiten:</div>
        <div>zukünftige 11.Klassen: Wählen Sie als Klasse "11-LK-Ma" aus, falls Ihr Kind sich für einen Mathematik-Leistungkurs entschieden hat, im anderen Fall "11-GK-Ma".</div><br>
        <div>zukünftige 12.Klassen:Ihre Teilnahme ist zwar kostenfrei, aber eine Anmeldung ist trotzdem dringend erforderlich!.</div><br><br>
        </div><p></p><div>mit freundlichen Grüßen - Ihr Team vom Lernmittelfonds   
        <p></p><p></p>(mail: lernmittelfonds@max-delbrueck-gymnasium.de)</div>';
    
     print '<div class = "lmf_pb"></div>';
}
print '</body></html>';

mysqli_close($dbc); //Close the DB Connection;
exit;
?>
