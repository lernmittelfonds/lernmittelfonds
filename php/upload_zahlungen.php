<?php

require ('./lmf-session.php');
require_once ('database_connection.php');
require_once ('mail.php');
require_once ('constants.php');

if (!isset($_SESSION)) {
    session_start();
}
echo '<html><body><textarea>{';
$lastId = 0;
$count = 0;
if ($_FILES["csv"]["size"] > 0) {
    //get the csv file
    $file = $_FILES["csv"][tmp_name];
    utf8_encode(fgets($file));
    $handle = fopen($file, "r");

    $error = array(); //this array will store all error messages
    $successLines = 0;
    $successMatch = 0;
    $errorLines = 0;
    $notInsertedLines = 0;
    //loop through the csv file and insert into database
    do {
        if ($data[0]) {
            if ($count > 0) {
                $values = "'" . addslashes(str_replace('"', '', $data[EINZAHLUNG_BUCHUNGS_TAG])) . "', '"
                        . addslashes(str_replace('"', '', $data[EINZAHLUNG_VALUTA_TAG])) . "', '"
                        . addslashes(str_replace('"', '', $data[EINZAHLUNG_BUCHUNGS_TEXT])) . "', '"
                        . utf8_encode(addslashes(str_replace('"', '', $data[EINZAHLUNG_VERWENDUNGSZWECK]))) . "', '"
                        . utf8_encode(addslashes(str_replace('"', '', $data[EINZAHLUNG_ABSENDER]))) . "', '"
                        . addslashes(str_replace('"', '', $data[EINZAHLUNG_IBAN])) . "', '"
                        . addslashes(str_replace('"', '', $data[EINZAHLUNG_BIC])) . "', '"
                        . addslashes(str_replace('"', '', $data[EINZAHLUNG_BETRAG])) . "'";
                $query_insert_einzahlung = "INSERT INTO einzahlungen (`Buchungstag`, `Valutatag`, `Buchungstext`, `Verwendungszweck`, `Absender`, `Konto`, `BLZ`, `Betrag`) VALUES ( " . $values . ")";
                echo '"insert' . $count . '": "' . $query_insert_einzahlung . '",';
                $result_insert_einzahlung = mysqli_query($dbc, $query_insert_einzahlung);
                $einzahlungsId = mysqli_insert_id($dbc);
                echo '"id' . $count . '": "' . $einzahlungsId . '",';

                if ($einzahlungsId == 0) {
                    $notInsertedLines;
                    $error[] = utf8_encode($values);
                    $lastId = $einzahlungsId;
                } else if ($lastId == $einzahlungsId) {
                    $error[] = utf8_encode($values);
                    $errorLines++;
                } else {
                    $lastId = $einzahlungsId;
                    $successLines++;
                    $toReplace = array('"', " ", "-");
                    $replacedVwz = str_replace($toReplace, '', $data[EINZAHLUNG_VERWENDUNGSZWECK]);
                    $query_check_einzahlung = "SELECT anmeldungsId  FROM view_anmeldungen_neues_jahr"
                            . " WHERE bezahlt=0"
                            . " AND beitrag = " . floatval(str_replace('"', '', $data[EINZAHLUNG_BETRAG]))
                            . " AND upper('" . $replacedVwz . "') like upper(CONCAT('%LMF',jahr,CAST(schuelerId AS CHAR),'A%'))"
                            . " AND upper('" . $replacedVwz . "') like upper(CONCAT('%',REPLACE(REPLACE(sVORNAME,' ',''),'-',''),'%'))"
                            . " AND upper('" . $replacedVwz . "') like upper(CONCAT('%',REPLACE(REPLACE(sNACHNAME,' ',''),'-',''),'%'));";

                    echo '"check' . $count . '": "' . $query_check_einzahlung . '",';
                    $result_check_einzahlung = mysqli_query($dbc, $query_check_einzahlung);
                    echo '"match' . $count . '": "' . mysqli_num_rows($result_check_einzahlung) . '",';

                    if (mysqli_num_rows($result_check_einzahlung) == 1) {
                        $row = mysqli_fetch_array($result_check_einzahlung);
                        $anmeldungsId = $row['anmeldungsId'];
                        echo '"anmeldungsId' . $count . '": ' . $anmeldungsId . ",";
                        $query_match_einzahlung = "UPDATE einzahlungen SET anmeldungsId=" . $anmeldungsId . " WHERE einzahlungsId=" . $einzahlungsId . ";";
                        echo '"updateqUERY' . $count . '": "' . $query_match_einzahlung . '",';
                        $result_match_einzahlung = mysqli_query($dbc, $query_match_einzahlung);
                        echo '"updateresult' . $count . '": "' . $result_match_einzahlung . '",';
                        if (mysqli_affected_rows($dbc) > 0) {
                            $successMatch++;
                            sendMailEinzahlungErhalten($einzahlungsId, $dbc);
                        } else {
                            // Zuordnung hat nicht geklappt -spaeter manuell
                        }
                    }
                }
            }
            $count++;
        }
    } while ($data = fgetcsv($handle, 1000, ";", "'"));

    echo '"errors": ' . json_encode($error);
    echo ', "successLines": ' . $successLines;
    echo ', "successMatch": ' . $successMatch;
    echo ', "notInsertedLines": ' . $notInsertedLines;
    echo ', "errorLines": ' . $errorLines;
    echo ', "count": ' . ($count - 1);
    mysqli_close($dbc); //Close the DB Connection;
}
echo '}</textarea></body></html>';
?>