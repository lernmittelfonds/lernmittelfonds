<?php

require_once ('mail.php');

function createMailJob($dbc, $kommentar) {
    $sql_neuer_job = "Insert Into `mailjobs`"
            . " (`FamilienId`, `Zeitpunkt`, `Anzahl`, `Kommentar`)"
            . "  VALUES (" . $_SESSION['FamilienId'] . ", Now(),0,\"" . $kommentar . "\");";
    $result_neuer_job = mysqli_query($dbc, $sql_neuer_job);

    if (!$result_neuer_job) {
        lmf_queryTrace($sql_neuer_job, false, $dbc);
        return -1;
    } else {
        lmf_queryTrace($sql_neuer_job, true, $dbc);
        return mysqli_insert_id($dbc);
    }
}

function simpleTextMailToDb($dbc, $jobId, $mail) {
    $mail->text(nl2br($mail->text));
    mailToDb($dbc, $jobId, $mail);
}

function mailToDb($dbc, $jobId, $mail) {
    $sql_neue_mail = "Insert Into `mails`"
            . " (`jobId`, `absendername`, `absenderadresse`, `empfaengeradresse`, `betreff`, `text`, `fehler`)"
            . "  VALUES (" . $jobId . ", '" . $mail->fromName . "', '" . $mail->fromMail . "', '" . $mail->empfaenger . "', '" . $mail->betreff . "', '" . $mail->text . "',0);";
    $result_neue_mail = mysqli_query($dbc, $sql_neue_mail);
    if (!$result_neue_mail) {
        lmf_queryTrace(substr($sql_neue_mail, 0, 500), false, $dbc);
        return false;
    } else {
        lmf_queryTrace(substr($sql_neue_mail, 0, 500), true, $dbc);
        $sql_update_job = "Update `mailjobs` Set `Anzahl` = Anzahl+1 WHERE jobId=" . $jobId . ";";
        mysqli_query($dbc, $sql_update_job);
        return true;
    }
}

function anlagenToDb($dbc, $jobId, $attachments) {
    lmf_trace("mailtoDb.php: anlagenToDb()");
    mkdir(realpath(dirname(__FILE__)) . "/../attachments/" . $jobId, 0777, true);
    $cnt = 0;
    // Angeben eines Attachment´s (sind auch mehrere möglich)
    while (isset($attachments['uploadedfile' . $cnt])) {
        if (!copy($attachments['uploadedfile' . $cnt]['tmp_name'], realpath(dirname(__FILE__)) . "/../attachments/" . $jobId . "/" . $attachments['uploadedfile' . $cnt]['name'])) {
            lmf_trace("failed to copy " . $attachments['uploadedfile' . $cnt]['tmp_name']);
        } else {
            $sql_neue_anlage = "Insert Into `mailanlagen`"
                    . " (`jobId`, `filename`)"
                    . "  VALUES (" . $jobId . ", '" . $attachments['uploadedfile' . $cnt]['name'] . "');";
            if (!mysqli_query($dbc, $sql_neue_anlage)) {
                lmf_queryTrace($sql_neue_anlage, false, $dbc);
            } else {
                lmf_trace("copied succesfull: " . $attachments['uploadedfile' . $cnt]['tmp_name']);
            }
            $cnt++;
        }
    }
}

?>