<?php
error_reporting(E_ALL);
require_once ('./lmf-session.php');
include ('database_connection.php');
include ('constants.php');
lmf_trace("start");
if (!isset($_SESSION)) {
    session_start();
}

echo("1: ".!isset($_SESSION['FamilienId'])."<br>\n");
echo("2: ".!isset($_SESSION['isAdmin'])."<br>\n");
echo("3: ".(!isset($_SESSION['FamilienId']) || !isset($_SESSION['isAdmin']))."<br>\n");
$error = array(); //this array will store all error messages

if (!isset($_SESSION['FamilienId']) || !isset($_SESSION['isAdmin'])) {
    $error[] = 'Loginerror';
    echo '{"loggedIn":false, "errors":["Nicht korrekt angemeldet' . $_SESSION['FamilienId'] . ' oder keine Adminrechte!"], "success":false}';
    exit;
}
$updateAblauf = array();
$updateAblaufOk = array();
$update = array();

///////////////////  Start Update Definition ///////////////////////
$update['referenzId'] = "0.0.1";
$update['kommentar'] = "Initialisierung des Updatemechanismus";
$update['sqlStatement'] = "CREATE TABLE IF NOT EXISTS `updates` (
  `updateId` INT NOT NULL AUTO_INCREMENT,
  `updateTime` TIMESTAMP NULL DEFAULT NOW(),
  `referenzId` VARCHAR(45) NULL,
  `sqlStatement` TEXT NULL,
  `kommentar` VARCHAR(255) NULL,
  `erfolgreich` TINYINT NULL,
  PRIMARY KEY (`updateId`),
  UNIQUE INDEX `referenzId_UNIQUE` (`referenzId` ASC));";
array_push($updateAblauf, $update);
////////////////////////////////////////////////////////////////////
$update = array();
$update['referenzId'] = "0.0.2";
$update['kommentar'] = "Test-1 update Mechanismus";
$update['sqlStatement'] = "INSERT INTO `constants` (`key`, `intValue`, `strValue`) 
VALUES ('updateInitialisiert',1,'1');";
array_push($updateAblauf, $update);
////////////////////////////////////////////////////////////////////
$update = array();
$update['referenzId'] = "0.0.3";
$update['kommentar'] = "Test-1 update Mechanismus";
$update['sqlStatement'] = "CREATE OR REPLACE VIEW `view_testUpdate` AS
    select * from constants;";
array_push($updateAblauf, $update);
////////////////////////////////////////////////////////////////////
$update = array();
$update['referenzId'] = "0.1.0";
$update['kommentar'] = "Behandeln von Neu und Altanmeldungen unterschiedlich";
$update['sqlStatement'] = "INSERT INTO `constants` (`key`, `intValue`, `strValue`) 
VALUES ('lmfNeuAnmeldungenStoppen',0,'14');";
array_push($updateAblauf, $update);
////////////////////////////////////////////////////////////////////
$update = array();
$update['referenzId'] = "0.1.1";
$update['kommentar'] = "Bücherückgabetexte für Mail-1";
$update['sqlStatement'] = "Update `texte` SET `code` = 'buecherRückgabeMahn' where  `texte`.`code`='buecherRückgabe';";
array_push($updateAblauf, $update);
////////////////////////////////////////////////////////////////////
$update = array();
$update['referenzId'] = "0.1.2";
$update['kommentar'] = "Bücherückgabetexte für Mail-1";
$update['sqlStatement'] = "INSERT INTO `texte` (`code`, `text`) VALUES ('buecherRückgabeInfo','');";
array_push($updateAblauf, $update);
////////////////////////////////////////////////////////////////////
$update = array();
$update['referenzId'] = "0.1.3";
$update['kommentar'] = "Mail als Info zur Buchrückgabe";
$update['sqlStatement'] = "INSERT INTO `constants` (`key`, `intValue`, `strValue`) 
VALUES ('lmfMailBuchRueckgabeInfo',0,'15');";
array_push($updateAblauf, $update);

$update = array();
$update['referenzId'] = "0.2.0";
$update['kommentar'] = "View Anmeldungen logisch nach Freigabe von Anmeldungen";
$update['sqlStatement'] = "CREATE OR REPLACE VIEW `view_anmeldungen` AS select `a`.`anmeldungsId` AS `anmeldungsId`,`bl`.`jahr` AS `jahr`,`a`.`KlassenId` AS `KlassenId`,`a`.`bezahlt` AS `bezahlt`,
`bl`.`beitrag` AS `beitrag`,`bl`.`klassenStufe` AS `klassenStufe`,`bl`.`sprache` AS `sprache`,
`s`.`SchuelerId` AS `SchuelerId`,`s`.`Vorname` AS `sVorname`,`s`.`Nachname` AS `sNachname`,`s`.`FamilienId` AS `FamilienId`,
`e`.`Vorname` AS `eVorname`,`e`.`Nachname` AS `eNachname` 
from ((((((`anmeldung` `a` 
join `klasse` `k`) 
join `buecherlisten` `bl`) 
join `schueler` `s`) 
join `constants` `c`) 
join `constants` `c1`) 
join `eltern` `e`)
 where ((`a`.`SchuelerId` = `s`.`SchuelerId`) 
and (`a`.`Schuljahr` = `c`.`intValue`+`c1`.`intValue`) 
and (`c`.`key` = 'thisYear') 
and (`c1`.`key` = 'lmfAnmeldungenFreigeben') 
and (`e`.`FamilienId` = `s`.`FamilienId`) 
and (`k`.`KlassenId` = `a`.`KlassenId`)
 and (`a`.`Schuljahr` = `bl`.`jahr`) 
and (((`k`.`StartKlassenStufe` + `a`.`Schuljahr`) - `k`.`StartJahr`) = `bl`.`klassenStufe`)
and (`k`.`Sprache` = convert(`bl`.`sprache` using utf8)))";
array_push($updateAblauf, $update);

$update = array();
$update['referenzId'] = "0.2.1";
$update['kommentar'] = "View Anmeldungen logisch nach Freigabe von Anmeldungen";
$update['sqlStatement'] = "CREATE OR REPLACE VIEW `view_analyse_buch_ausgabe` AS select `a`.`anmeldungsId` AS `anmeldungsId`,`a`.`jahr` AS `jahr`,`a`.`KlassenId` AS `KlassenId`,`a`.`bezahlt` AS `bezahlt`,`a`.`beitrag` AS `beitrag`,`a`.`klassenStufe` AS `klassenStufe`,`a`.`sprache` AS `sprache`,`a`.`SchuelerId` AS `SchuelerId`,`a`.`sVorname` AS `sVorname`,`a`.`sNachname` AS `sNachname`,`a`.`FamilienId` AS `FamilienId`,`a`.`eVorname` AS `eVorname`,`a`.`eNachname` AS `eNachname`,`b`.`BuchId` AS `BuchId`,`b`.`Isbn` AS `Isbn`,`b`.`Titel` AS `Titel`,`b`.`von` AS `von`,`b`.`bis` AS `bis`,`b`.`Neupreis` AS `Neupreis`,if((isnull(`ag`.`buchId`) or (`ag`.`eingesammelt` = 1)),1,0) AS `auszugeben` from (((`view_anmeldungen` `a` join `view_buecherlisten` `bl` on(((`a`.`jahr` = `bl`.`jahr`) and (`a`.`sprache` = `bl`.`sprache`) and (`a`.`klassenStufe` = `bl`.`klassenStufe`)))) join `buecher` `b` on((`b`.`BuchId` = `bl`.`buchId`))) left join `ausgeliehen` `ag` on(((`bl`.`buchId` = `ag`.`buchId`) and (`a`.`SchuelerId` = `ag`.`schuelerId`)))) where (`a`.`bezahlt` >= 0)";
array_push($updateAblauf, $update);


$update = array();
$update['referenzId'] = "0.2.2";
$update['kommentar'] = "View Anmeldungen logisch nach Freigabe von Anmeldungen";
$update['sqlStatement'] = "CREATE OR REPLACE VIEW view_check_einzahlungen_alt as select '1' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_altes_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Verwendungszweck`,' ','') like concat('%LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A%'))) union select '2' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_altes_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Verwendungszweck`,' ','') like concat('%',`a`.`sNachname`,'%')) and (replace(`e`.`Verwendungszweck`,' ','') like concat('%',`a`.`sVorname`,'%'))) union select '3' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_altes_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Verwendungszweck`,' ','') like concat('%',`a`.`sNachname`,'%'))) union select '4' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_altes_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Absender`,' ','') like concat('%',`a`.`eNachname`,'%')) and (replace(`e`.`Absender`,' ','') like concat('%',`a`.`eVorname`,'%'))) union select '5' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_altes_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Absender`,' ','') like concat('%',`a`.`eNachname`,'%'))) union select '6' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_altes_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Absender`,' ','') like concat('%',`a`.`sNachname`,'%'))) union select '7' AS `prio`,`e`.`einzahlungsId` AS `einzahlungsId`,`e`.`Buchungstag` AS `Buchungstag`,`e`.`Valutatag` AS `Valutatag`,`e`.`Buchungstext` AS `Buchungstext`,`e`.`Verwendungszweck` AS `Verwendungszweck`,`e`.`Absender` AS `Absender`,`e`.`Betrag` AS `Betrag`,`e`.`Konto` AS `Konto`,`e`.`BLZ` AS `BLZ`,`e`.`anmeldungsId` AS `anmeldungsId`,`a`.`anmeldungsId` AS `anMeldId`,`a`.`KlassenId` AS `KlassenId`,concat('LMF-',`a`.`jahr`,'-',cast(`a`.`SchuelerId` as char charset utf8),'A') AS `ZahlungsKey`,concat(`a`.`sVorname`,' ',`a`.`sNachname`) AS `schuelerName`,concat(`a`.`eVorname`,' ',`a`.`eNachname`) AS `elternName` from (`view_anmeldungen_altes_jahr` `a` join `einzahlungen` `e`) where (isnull(`e`.`anmeldungsId`) and isnull(`e`.`verworfen`) and (`a`.`bezahlt` = 0) and (replace(`e`.`Verwendungszweck`,' ','') like concat('%',`a`.`eNachname`,'%')))";
array_push($updateAblauf, $update);

$update = array();
$update['referenzId'] = "1.0.0";
$update['kommentar'] = "Erzeuge Tabelle mailjobs";
$update['sqlStatement'] = "CREATE TABLE `mailjobs` (
  `jobId` INT NOT NULL AUTO_INCREMENT,
  `familienId` INT(10) NOT NULL,
  `zeitpunkt` DATETIME NOT NULL,
  `anzahl` INT(6) NOT NULL,
  `kommentar` VARCHAR(254) NULL,
  PRIMARY KEY (`jobId`));";
array_push($updateAblauf, $update);

$update = array();
$update['referenzId'] = "1.0.1";
$update['kommentar'] = "Erzeuge Tabelle mailanlagen";
$update['sqlStatement'] = "CREATE TABLE `mailanlagen` (
  `jobId` INT NOT NULL,
  `filename` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`jobId`, `filename`));";
array_push($updateAblauf, $update);

$update = array();
$update['referenzId'] = "1.0.2";
$update['kommentar'] = "Erzeuge Tabelle mails";
$update['sqlStatement'] = "CREATE TABLE `mails` (
  `mailId` INT NOT NULL AUTO_INCREMENT,
  `jobId` INT NOT NULL,
  `absenderName` VARCHAR(45) NULL,
  `absenderAdresse` VARCHAR(100) NOT NULL,
  `empfaengerAdresse` VARCHAR(100) NOT NULL,
  `betreff` VARCHAR(200) NULL,
  `text` TEXT NULL,
  `fehler` INT(1) NULL DEFAULT 0,
  PRIMARY KEY (`mailid`));";
array_push($updateAblauf, $update);

$update = array();
$update['referenzId'] = "1.1.0";
$update['kommentar'] = "View Bücherlisten altes Jahr - für Kalkulation";
$update['sqlStatement'] = "CREATE OR Replace VIEW `view_buecherlisten_altes_jahr` AS
	select 
		`bl`.`listenId` AS `listenId`,
        `bl`.`klassenStufe` AS `klassenStufe`,
        `bl`.`sprache` AS `sprache`,
 		count(`a`.`anmeldungsId`) As `teilnehmer`,
        `bl`.`beitrag` AS `beitrag`
    from
        ((((`anmeldung` `a`
        join `klasse` `k`)
        join `buecherlisten` `bl`)
        join `constants` `c`)
        join `constants` `c1`)
    where
        ( (`a`.`Schuljahr` = (`c`.`intValue` + `c1`.`intValue`))
			and (`a`.`bezahlt`=1)
            and (`c`.`key` = 'thisYear')
            and (`c1`.`key` = 'lmfAnmeldungenFreigeben')
            and (`k`.`KlassenId` = `a`.`KlassenId`)
            and (`a`.`Schuljahr` = `bl`.`jahr`)
            and (((`k`.`StartKlassenStufe` + `a`.`Schuljahr`) - `k`.`StartJahr`) = `bl`.`klassenStufe`)
            and (`k`.`Sprache` = convert( `bl`.`sprache` using utf8)))
	group by  `bl`.`klassenStufe`, `bl`.`sprache`";
array_push($updateAblauf, $update);

$update = array();
$update['referenzId'] = "1.1.1";
$update['kommentar'] = "View Abrechnung laufendes Jahr - Grundlage für Kalkulation";
$update['sqlStatement'] = "CREATE OR REPLACE VIEW view_abrechnung_altes_jahr_grundlage As
SELECT bl.*, b.*, bil.preisImJahr, 
	bil.preisImJahr*(100-12)/100*(if(b.bis=0,1,if(b.bis-b.von>3,1,(b.bis-b.von+1)/5))) as leihpreis
FROM 
    view_buecherlisten_altes_jahr bl,
    buecherinlisten bil,
    buecher b
Where bl.listenId=bil.listenId	
    And bil.buchId=b.BuchId;";
array_push($updateAblauf, $update);

$update['referenzId'] = "1.2.0";
$update['kommentar'] = "Mailjobverwaltung durch Nutzer";
$update['sqlStatement'] = "ALTER TABLE `db462321821`.`mailjobs` 
ADD COLUMN `fehler` INT(6) NULL DEFAULT -1 COMMENT 'Die Fehlerzahl ist -1 und wird erst mit beenden gesetzt' AFTER `kommentar`,
ADD COLUMN `beendet` TINYINT(1) NULL DEFAULT 0 AFTER `fehler`;";
array_push($updateAblauf, $update);

////////////////////////////////////////////////////////////////////

///////////////////  Ende Update Definition ///////////////////////

///////////////////  Start Ablauf Steuerung ///////////////////////
$sonderzeichen = array("'", '"', "\n");
$ersatz = array("\\'", "\\\"", " ");
for ($x = 0; $x < count($updateAblauf); $x++) {
    $update = $updateAblauf[$x];
    $weiter = false;
    $initial = true;
    
    if ($update['referenzId'] == "0.0.1") {
        echo("5a: ".$update['referenzId']." <br>\n");
        
        $weiter = true;
    } else {
        $initial = false;
        echo("5b: ".$update['referenzId']." <br>\n");
        $sucheRefId = "SELECT * FROM updates WHERE referenzId='" . $update['referenzId'] . "';";
        $ergebnis = mysqli_query($dbc, $sucheRefId);
        if (mysqli_num_rows($ergebnis) == 0) {
            // nichts gefunden -> update schritt ausführen;
            echo("6a: // nichts gefunden -> update schritt ausführen");
            $weiter = true;
        } else {
            $row = mysqli_fetch_assoc($ergebnis);
            echo("6b: // was gefunden ");
            echo($row);
            if ($row['erfolgreich'] == 1) {
                // dieser Schritt wurde bereits erfolgreich ausgeführt
                echo("7a: war erfolgreich ");
                $weiter = false;
            } else {
                echo("7a: war nicht erfolgreich ");
                // dieser Schritt wurde zuletzt mit Fehler ausgeführt
                $weiter = true;
            }
        }
    }
    if ($weiter) {
        $resultUpdate = mysqli_query($dbc, $update['sqlStatement']);
        if (!$resultUpdate) {
            lmf_queryTrace($update['sqlStatement'], false, $dbc);
            echo"<br>\n";
            echo"<br>\n";
            echo $update['sqlStatement']."<br>\n";
            echo"<br>\n";
            echo ('{"loggedIn":true, 
                "errors":["Datenbankfehler"], 
                "success":false}, 
                "query":["' . str_replace($sonderzeichen, $ersatz, $update['sqlStatement']) . '"], 
                "referenzId":["' . $update['referenzId'] . '"], 
                "kommentar":["' . $update['kommentar'] . '"]');
            break;
        } else {
            echo"<br>\n";
            echo "OK: ".$update['sqlStatement']."<br>\n";
            echo"<br>\n";
            if (    !$initial){
                lmf_queryTrace(mysqli_real_escape_string( $dbc, $update['sqlStatement']), true, $dbc);
                $updateProtokoll = "INSERT INTO `updates` (`sqlStatement`, `referenzId`, `kommentar`, `erfolgreich`) 
                                            VALUES ('" . mysqli_real_escape_string( $dbc, $update['sqlStatement']) . "','" . $update['referenzId'] . "','" . $update['kommentar'] . "', 1);";

                $resultUpdate = mysqli_query($dbc,  $updateProtokoll);
                if (!$resultUpdate) {
                    lmf_queryTrace($updateProtokoll, false, $dbc);
                    echo ('{"loggedIn":true, 
                    "errors":["Datenbankfehler"], 
                    "success":false}, 
                    "query":["' . str_replace($sonderzeichen, $ersatz, $updateProtokoll) . '"], 
                    "kommentar":["Fehler beim protokollieren des Updates. Update abgebrochen"]');
                    break;
                } else {
                    lmf_queryTrace($updateProtokoll, true, $dbc);
                    array_push($updateAblaufOk, $update);
                }   
            }
        }
    }
}

echo '{"loggedIn":true, "success":true, "updates":' . json_encode($updateAblaufOk) . '}';
mysqli_close($dbc); //Close the DB Connection;
exit;
?>