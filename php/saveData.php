<?php

require_once ('./lmf-session.php');
require_once ('./constants.php');
require_once ('database_connection.php');
require_once ('mail.php');
if (!isset($_SESSION)) {
    session_start();
}
error_reporting(E_ALL);
$error = array(); //this array will store all error messages
$request = json_decode(file_get_contents('php://input'), true);

if (!isset($_SESSION['FamilienId'])) {
    $error[] = 'Loginerror';
    echo '{"loggedIn":false, "errors":["Nicht korrekt angemeldet"], "success":false}';
    exit;
}

if ($request["type"] == "eltern_save") {
    if (empty($request['Nachname'])) {//kein Nachname eingetragen
        $error[] = 'Bitte geben sie einen Nachnamen an '; //add to array "error"
    }
    if (empty($request['Vorname'])) {//kein Vorname eingetragen
        $error[] = 'Bitte geben Sie einen Vornamen an '; //add to array "error"
    }
    if (empty($request['Telefon'])) {
        $error[] = 'Bitte geben Sie eine gültige Telefonnummer an '; //add to array "error"
    }
    if (empty($request['Email'])) {
        $error[] = 'Bitte geben Sie eine gültige eMail-Adresse an ';
    } else {
        if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $request['Email'])) {
            //regular expression for email validation
            $mail = $request['Email'];
        } else {
            $error[] = 'Ihre eMail-Adresse ist ungültig  ';
        }
    }
    $zuzahlungsQuery = (isset($request['zuzahlungsBefreit'])) ? " `zuzahlungsBefreit`=" . $request['zuzahlungsBefreit'] . "," : "";
    $freigabe_ev_Query = (isset($request['Freigabe_ev'])) ? " `Freigabe_ev`=" . $request['Freigabe_ev'] . "," : "";
    $freigabe_alle_Query = (isset($request['Freigabe_alle'])) ? " `Freigabe_alle`=" . $request['Freigabe_alle'] . "," : "";
    $freigabe_info_Query = (isset($request['Freigabe_info'])) ? " `Freigabe_info`=" . $request['Freigabe_info'] . "," : "";
    $freigabe_lmf_Query = (isset($request['Freigabe_lmf'])) ? " `Freigabe_lmf`=" . $request['Freigabe_lmf'] . "," : "";
    $adminQuery = (isset($request['istAdmin'])) ? " `isAdmin`=" . $request['istAdmin'] . "," : "";
    $stopMailQuery = (isset($request['stopMail'])) ? " `stopMail`=" . $request['stopMail'] . "," : "";
    $sql_verify_email = "SELECT * FROM eltern  WHERE Email ='" . $mail . "' AND `FamilienId`!=" . $request['FamilienId'] . ";";
    $result_verify_email = mysqli_query($dbc, $sql_verify_email);
    if (!$result_verify_email) {
        $error[] = 'Fehler bei der Datenbankabfrage';
    }
    if (count($error) == 0) {
        if (mysqli_num_rows($result_verify_email) == 0) { // noch niemand anderes nutzt diese adresse.
            $sql_eltern_save = "Update `eltern`"
                    . " SET `Vorname`='" . $request['Vorname'] . "',"
                    . " `Nachname`='" . $request['Nachname'] . "',"
                    . " `Telefon`='" . $request['Telefon'] . "',"
                    . " `Anschrift`='" . $request['Anschrift'] . "',"
                    . $freigabe_ev_Query
                    . $freigabe_alle_Query
                    . $freigabe_info_Query
                    . $freigabe_lmf_Query
                    . $zuzahlungsQuery
                    . $stopMailQuery
                    . $adminQuery
                    . " `Email`='" . $request['Email'] . "'"
                    . " WHERE `FamilienId`=" . $request['FamilienId'] . ";";
            $result_eltern_save = mysqli_query($dbc, $sql_eltern_save);
            if (!$result_eltern_save) {
                lmf_queryTrace($sql_eltern_save, false, $dbc);
                echo ('{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}');
            } else {
                lmf_queryTrace($sql_eltern_save, true, $dbc);
                echo ('{"loggedIn":true, "success":true}');
            }
        } else { // email address ist nicht frei.
            echo ('{"loggedIn":true, "errors":["Die email-Adresse, die Sie angegeben haben ist bereits registriert"]}');
        }
    } else {
        echo ('{"loggedIn":true, "hier":' . count($error) . ', "errors": ' . json_encode($error) . '}');
    }
}

if (isset ($request["type"]) && $request["type"] == "schuelerSpeichern") {
    $offset = -1;
    $setzeEisStatus = false;
    if (isset ($request["subType"]) && $request["subType"] == "anmeldungNaechstesJahr") {
        $offset = 1;
    }
    if (isset ($request["subType"]) && $request["subType"] == "anmeldungDiesesJahr") {
        $offset = 0;
        if (!$_SESSION['isAdmin']) {
            lmf_trace("setzeEisStatus = true");
            $setzeEisStatus = true;
        }
    }
    if ($offset > -1) {
        $sql_upd_anmeldung = "Update `anmeldung`"
                . " SET `KlassenId`=" . $request['KlassenId']
                . " WHERE `SchuelerId`=" . $request['SchuelerId']
                . " AND `Schuljahr`=" . (THIS_YEAR + $offset) . ";";
        $result_upd_anmeldung = mysqli_query($dbc, $sql_upd_anmeldung);
        if (!$result_upd_anmeldung) {
            lmf_queryTrace($sql_upd_anmeldung, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        } else {
            lmf_queryTrace($sql_upd_anmeldung, true, $dbc);
            $sql_upd_anmeldung = "Update `anmeldung`"
                    . " SET `KlassenId`=" . $request['KlassenId']
                    . " WHERE `SchuelerId`=" . $request['SchuelerId']
                    . " AND `Schuljahr`=" . (THIS_YEAR + 1 - $offset)   //offset=1: +0 diesesjahr; offset=0: +1 naechstesJahr
                    . " AND `KlassenId`=0;"; // das andere Jahr mitsetzen, falls noch nicht gesetzt
            $result_upd_anmeldung = mysqli_query($dbc, $sql_upd_anmeldung);
            if (!$result_upd_anmeldung) {
                lmf_queryTrace($sql_upd_anmeldung, false, $dbc);
                echo '{"loggedIn":true, "errors":["Datenbankfehler2"], "success":false}';
            } else {
                lmf_queryTrace($sql_upd_anmeldung, true, $dbc);
                if ($setzeEisStatus) {
                    $updateSchueler = "Update `schueler` SET eisStatus=0, istElternvertreter=0 WHERE `SchuelerId`=" . $request['SchuelerId'];
                    $resultUpdateSchueler = mysqli_query($dbc, $updateSchueler);
                    if (!$resultUpdateSchueler) {
                        lmf_queryTrace($updateSchueler, false, $dbc);
                        echo '{"loggedIn":true, "errors":["Datenbankfehler3"], "success":false}';
                    } else {
                        lmf_queryTrace($updateSchueler, true, $dbc);
                        echo '{"loggedIn":true, "success":true, "add":"mit Klasse"}';
                    }
                } else {
                    echo '{"loggedIn":true, "success":true, "add":"mit Klasse"}';
                }
            }
        }
    } else {
        $umlaute = Array("/ä/", "/ö/", "/ü/", "/Ä/", "/Ö/", "/Ü/", "/ß/");
        $replace = Array("ae", "oe", "ue", "Ae", "Oe", "Ue", "ss");
        $sql_schuelerSpeichern = "Update `schueler`"
                . " SET `Vorname`='" . preg_replace($umlaute, $replace, $request['Vorname']) . "',"
                . " `Nachname`='" . preg_replace($umlaute, $replace, $request['Nachname']) . "',"
                . " `istElternvertreter`='" . 1*$request['istEV'] . "'"
                . " WHERE `SchuelerId`=" . $request['SchuelerId'] . ";";
        $result_schuelerSpeichern = mysqli_query($dbc, $sql_schuelerSpeichern);

        if (!$result_schuelerSpeichern) {
            lmf_queryTrace($sql_schuelerSpeichern, false, $dbc);
            lmf_trace("request2: " .json_encode($request) );
            lmf_trace($request,true);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        } else {
            lmf_queryTrace($sql_schuelerSpeichern, true, $dbc);
            echo '{"loggedIn":true, "success":true}';
        }
    }
}


if ($request["type"] == "neuerSchueler") {
    $sql_neues_kind = "Insert Into `schueler`"
            . " (`FamilienId`, `Nachname`, `Vorname`)"
            . "  VALUES (" . $request['FamilienId'] . ", '" . $request['Nachname'] . "', '" . $request['Vorname'] . "');";
    $result_neues_kind = mysqli_query($dbc, $sql_neues_kind);

    if (!$result_neues_kind) {
        lmf_queryTrace($sql_neues_kind, false, $dbc);
        echo '{"loggedIn":true, "errors":["' . mysqli_error($dbc) . '(' . mysqli_errno($dbc) . ')"], "success":false}';
    } else {
        lmf_queryTrace($sql_neues_kind, true, $dbc);
        $schuelerId = mysqli_insert_id($dbc);
        $sql_anmeldung_einfuegen = "INSERT INTO `anmeldung`"
                . " (`SchuelerId`, `Schuljahr`, `KlassenId`, `bezahlt` )"
                . "  VALUES (" . $schuelerId . ", '" . (THIS_YEAR + 1) . "', 0, -1), "
                . "  (" . $schuelerId . ", '" . THIS_YEAR . "', 0, -1);";
        $result_anmeldung_einfuegen = mysqli_query($dbc, $sql_anmeldung_einfuegen);
        if (!$result_anmeldung_einfuegen) {
            lmf_queryTrace($sql_anmeldung_einfuegen, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler)"], "success":false}';
        } else {
            lmf_queryTrace($sql_anmeldung_einfuegen, true, $dbc);
            echo '{"loggedIn":true, "success":true, "result":"' . mysqli_insert_id($dbc) . '", "newId":' . $schuelerId . '}';
        }
    }
}
if ($request["type"] == "schuelerLoeschen") {
    $sql_schuelerLoeschen = "Delete from `schueler`"
            . " WHERE `SchuelerId`=" . $request['SchuelerId'] . ";";
    $result_schuelerLoeschen = mysqli_query($dbc, $sql_schuelerLoeschen);
    if (!$result_schuelerLoeschen) {
        lmf_queryTrace($sql_schuelerLoeschen, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($sql_schuelerLoeschen, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}

if ($request["type"] == "anmelden") {
    $anmeldenMoeglich = true;
    $unbezahlteTeilnahme = false;
    $Fehlermeldung = "";
    $abfrage_naechstesJahr = "SELECT a.anmeldungsId, a.bezahlt, bl.beitrag, bl.klassenStufe , bl.sprache, e.zuzahlungsBefreit"
            . " FROM anmeldung a, klasse k, buecherlisten bl, schueler s, eltern e"
            . " WHERE a.Schuelerid= " . $request['SchuelerId']
            . " AND a.Schuelerid=s.Schuelerid"
            . " AND s.FamilienId=e.FamilienId"
            . " AND a.Schuljahr= " . (THIS_YEAR + 1)
            . " AND k.klassenId = a.KlassenId"
            . " AND a.Schuljahr=bl.jahr"
            . " AND k.StartKlassenstufe+a.Schuljahr-k.StartJahr=bl.klassenStufe"
            . " AND k.Sprache=bl.sprache";

    $abfrage_laufendesJahr = "SELECT a.anmeldungsId, a.bezahlt, bl.beitrag, bl.klassenStufe , bl.sprache, e.zuzahlungsBefreit"
            . " FROM anmeldung a, klasse k, buecherlisten bl, schueler s, eltern e"
            . " WHERE a.Schuelerid= " . $request['SchuelerId']
            . " AND a.Schuelerid=s.Schuelerid"
            . " AND s.FamilienId=e.FamilienId"
            . " AND a.Schuljahr= " . THIS_YEAR
            . " AND k.klassenId = a.KlassenId"
            . " AND a.Schuljahr=bl.jahr"
            . " AND k.StartKlassenstufe+a.Schuljahr-k.StartJahr=bl.klassenStufe"
            . " AND k.Sprache=bl.sprache";

    if ($request['jahr'] == THIS_YEAR) {
        $result_laufendesJahr = mysqli_query($dbc, $abfrage_laufendesJahr);
        if (@mysqli_num_rows($result_laufendesJahr) == 1) {
            $anmeldungLaufendesJahr = mysqli_fetch_array($result_laufendesJahr, MYSQLI_ASSOC);

            // anmelden fuer das laufende Jahr - ohne weitere checks - kann nur ein admin
            $sql_anmelden = "Update `anmeldung` "
                    . " SET bezahlt = " . 2 * $anmeldungLaufendesJahr['zuzahlungsBefreit']
                    . " WHERE SchuelerId = " . $request['SchuelerId']
                    . " AND bezahlt < 1 "  // keine bezahlten überschreiben !!!
                    . " AND Schuljahr = " . THIS_YEAR . ";";
            $result_anmelden = mysqli_query($dbc, $sql_anmelden);
            if (!$result_anmelden) {
                lmf_queryTrace($sql_anmelden, false, $dbc);
                echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
            } else {
                lmf_queryTrace($sql_anmelden, true, $dbc);
                if ($anmeldungLaufendesJahr['zuzahlungsBefreit']) {
                    echo '{"loggedIn":true, "success":true, "zzb":true, "schuelerId": ' . $request['SchuelerId'] . ', "content":"Anmeldung erfolgreich!"}';
                } else {
                    lmf_trace("Schueler " . $request['SchuelerId'] . " erfolgreich angemeldet");
                    echo sendMailAnmeldung($request['SchuelerId'], $anmeldungLaufendesJahr['beitrag'], THIS_YEAR, $dbc);
                }
            }
        } else {
            lmf_queryTrace($abfrage_laufendesJahr, false, $dbc);
            lmf_trace('ERROR: keine Anmeldung fuer Schueler ' . $request['SchuelerId'] . ' fuer das laufende Jahr (' . (THIS_YEAR) . ') gefunden.');
            $Fehlermeldung = 'ERROR: Keine Anmeldung fuer Schueler ' . $request['SchuelerId'] . ' fuer das laufende Jahr (' . (THIS_YEAR) . ') gefunden.'
                    . ' Bitte informieren sie einen Verantwortlichen und einen Entwickler der Software.';
            echo '{"loggedIn":true, "errors":["' . $Fehlermeldung . '"], "success":false}';
        }
    } else { // naechstes Schuljahr anmelden
        $result_naechstesJahr = mysqli_query($dbc, $abfrage_naechstesJahr);
        if (@mysqli_num_rows($result_naechstesJahr) != 1) {
            lmf_queryTrace($abfrage_naechstesJahr, false, $dbc);
            lmf_trace('ERROR: keine Anmeldung fuer Schueler ' . $request['SchuelerId'] . ' und Schuljahr= ' . (THIS_YEAR + 1) . 'gefunden.');
            $Fehlermeldung = 'ERROR: keine Anmeldung fuer Schueler ' . $request['SchuelerId'] . ' und Schuljahr= ' . (THIS_YEAR + 1) . 'gefunden. '
                    . 'Bitte informieren sie einen Verantwortlichen und einen Entwickler der Software.';
            echo '{"loggedIn":true, "errors":["' . $Fehlermeldung . '"], "success":false}';
        } else {
            $anmeldungNaechstesJahr = mysqli_fetch_array($result_naechstesJahr, MYSQLI_ASSOC);
            if ($anmeldungNaechstesJahr['bezahlt'] >= 0) {
                lmf_trace('ERROR: doppelte Anmeldung fuer Schueler '
                        . $request['SchuelerId'] . ' und Schuljahr= '
                        . (THIS_YEAR + 1) . '(Status bezahlt: '
                        . $anmeldungNaechstesJahr['bezahlt'] . ')versucht.');
                $Fehlermeldung = 'ERROR: Der Schueler ist bereits für das nächste Schuljahr angemeldet!';
                $anmeldenMoeglich = false;
            } elseif ($anmeldungNaechstesJahr['beitrag'] == 0) {
                // in diesem Fall muss geprueft werden, ob der Schueler dieses Jahr schon teilnimmt
                // Regel1: liegt im Jahr x eine bezahlte Teilnahme vor, und ist im Jahr x+1 der Beitrag == 0, 
                //         so gilt für das Jahr x+1 die Teilnahme als bezahlt
                // Regel2: liegt im Jahr x keine bezahlte Teilnahme vor, und ist im Jahr x+1 der Beitrag == 0, 
                //         so ist für das Jahr x+1 keine Teilnahme möglich
                // ausnahme zu beiden Regeln: zuzahlungsbefreite können immer teilnehmen
                $result_laufendesJahr = mysqli_query($dbc, $abfrage_laufendesJahr);
                if (@mysqli_num_rows($result_laufendesJahr) == 1) {
                    $anmeldungLaufendesJahr = mysqli_fetch_array($result_laufendesJahr, MYSQLI_ASSOC);
                    if ($anmeldungLaufendesJahr['bezahlt'] < 1) {
                        // -1 ... keine Teilnahme
                        // 0 ... Teilnahme angemeldet 
                        // 1 ... Teilnahme bezahlt
                        // 2 ... zuzahlungsbefreit und angemeldet
                        lmf_trace('ERROR: keine bezahlte Anmeldung fuer Schueler ' . $request['SchuelerId'] . ' und verpflichtendes Vorjahr= ' . (THIS_YEAR) . 'gefunden.');
                        $Fehlermeldung = 'ERROR: Sie können für diese Klassestufe im nächsten Jahr nur am Lernmittelfonds teilnehmen, wenn sie bereits dieses Jahr teilgenommen haben. '
                                . 'Das ist leider nicht der Fall.';
                        $anmeldenMoeglich = false;
                    } else {
                        $unbezahlteTeilnahme = true;
                    }
                } else {
                    lmf_trace('ERROR: keine Anmeldung fuer Schueler ' . $request['SchuelerId'] . ' und verpflichtendes Vorjahr= ' . (THIS_YEAR) . 'gefunden.');
                    $Fehlermeldung = 'ERROR: Sie können für dieses Schuljahr im nächsten Jahr nur am Lernmittelfonds teilnehmen, wenn sie bereits letztes Jahr teilgenommen haben. '
                            . 'Das ist leider nicht der Fall.';
                    $anmeldenMoeglich = false;
                }
            } // alles in Ordnung

            if ($anmeldenMoeglich) {

                $sql_anmelden = "Update `anmeldung` "
                        . " SET bezahlt = " . ($unbezahlteTeilnahme ? 1 : 2 * $anmeldungNaechstesJahr['zuzahlungsBefreit'])
                        . " WHERE SchuelerId = " . $request['SchuelerId']
                        . " AND Schuljahr = " . (THIS_YEAR + 1) . ";";
                $result_anmelden = mysqli_query($dbc, $sql_anmelden);
                if (!$result_anmelden) {
                    lmf_queryTrace($sql_anmelden, false, $dbc);
                    echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
                } else {
                    lmf_queryTrace($sql_anmelden, true, $dbc);
                    if ($anmeldungNaechstesJahr['zuzahlungsBefreit']) {
                        echo '{"loggedIn":true, "success":true, "zzb":true, "schuelerId": ' . $request['SchuelerId'] . ', "content":"Anmeldung erfolgreich!"}';
                    } else {
                        lmf_trace("Schueler " . $request['SchuelerId'] . " erfolgreich angemeldet");
                        echo sendMailAnmeldung($request['SchuelerId'], $anmeldungNaechstesJahr['beitrag'], (THIS_YEAR + 1), $dbc);
                    }
                }
            } else {
                echo '{"loggedIn":true, "errors":["' . $Fehlermeldung . '"], "success":false}';
            }
        }
    }
}

if ($request["type"] == "abmelden") {
    $offset=$request['offset']===null?1:$request['offset'];
    lmf_trace("offset: ".$offset."  /  ".$request['offset']);
    $sql_abmelden = "Update `anmeldung` "
            . " SET bezahlt = -2"
            . " WHERE SchuelerId = " . $request['SchuelerId']
            . " AND Schuljahr = " . (THIS_YEAR + $offset)
            . " AND (`bezahlt`=0 OR `bezahlt`=2 OR `bezahlt`=-1);";
    $result_abmelden = mysqli_query($dbc, $sql_abmelden);
    if (!$result_abmelden) {
        lmf_queryTrace($sql_abmelden, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($sql_abmelden, true, $dbc);
        echo '{"loggedIn":true, "success":true, "zzb":' . $_SESSION['zuzahlungsBefreit'] . '}';
    }
}
// eine Anmeldung die bereits bezahlt ist soll verändert werden
// mit dieser Funktion wird die Zahlung gelöst und der Status auf angemeldet zurückgesetzt
if ($request["type"] == "zahlungLoesen") {
    lmf_trace('Query: zahlungLoesen');
    $sql_abfrage = "Select einzahlungsid from `einzahlungen` e, anmeldung a "
            . " WHERE a.SchuelerId = " . $request['SchuelerId']
            . " AND a.Schuljahr = " . (THIS_YEAR + 1)
            . " AND a.anmeldungsid = e.anmeldungsid";
    if ($result = $result = mysqli_query($dbc, $sql_abfrage)) {
        if (mysqli_num_rows($result) > 0) {
            // mind. eine passende Einzahlung gefunden (mehr sollten es auch nicht sein ..) 
            while ($einzahlung = mysqli_fetch_array($result)) {
                $sql_query2 = "update einzahlungen set anmeldungsid = null"
                        . " WHERE einzahlungsid = " . $einzahlung['einzahlungsid'];
                $result2 = mysqli_query($dbc, $sql_query2);
                if (!$result2) {
                    lmf_queryTrace($sql_query2, false, $dbc);
                    echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
                } else {
                    lmf_queryTrace($sql_query2, true, $dbc);
                    echo '{"loggedIn":true, "success":true}';
                }
                mysqli_free_result($result2);
            }
        } else {
            // es gab keine Einzahlung ... das heisst dann wohl kostenlose Teilnahme
            $sql_query2 = "update anmeldung set bezahlt = 0"
                    . "  WHERE SchuelerId = " . $request['SchuelerId']
                    . " AND Schuljahr = " . (THIS_YEAR + 1);
            $result2 = mysqli_query($dbc, $sql_query2);
            if (!$result2) {
                lmf_queryTrace($sql_query2, false, $dbc);
                echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
            } else {
                lmf_queryTrace($sql_query2, true, $dbc);
                echo '{"loggedIn":true, "success":true}';
            }
            mysqli_free_result($result2);
        }
        mysqli_free_result($result);
    } else {
        lmf_queryTrace($sql_abfrage, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    }
}

if ($request["type"] == "passwort_aendern") {
    $sql_PW_neu = "Update `eltern`"
            . " SET `Passwort`='" . md5($request['newPasswort']) . "'"
            . " WHERE `FamilienId`=" . $request['FamilienId']
            . " AND `Passwort`='" . md5($request['oldPasswort']) . "';";
    $result_PW_neu = mysqli_query($dbc, $sql_PW_neu);
    $rows = mysqli_affected_rows($dbc);

    if (!$result_PW_neu) {
        lmf_queryTrace($sql_PW_neu, false, $dbc);
        echo ('{,"loggedIn":true, "errors": ["Fehler beim Ändern des Passworts!"]}');
    } else {
        lmf_queryTrace($sql_PW_neu, true, $dbc);
        if ($rows == 0) {
            echo ('{"loggedIn":true, "rows": "' . $rows . '", "errors": ["Ihr altes Passwort war nicht richtig"]}');
        } else {
            echo '{"loggedIn":true, "rows": "' . $rows . '", "success":true}';
        }
    }
}

if ($request["type"] == "einsammelnUndAusgeben") {
    $sql_einsammelnUndAusgeben = "Update `ausgeliehen`"
            . " SET `eingesammelt`=" . $request['eingesammelt']
            . " WHERE `buchId`=" . $request['buchId']
            . " AND `schuelerId`=" . $request['schuelerId'] . ";";
    $result_einsammelnUndAusgeben = mysqli_query($dbc, $sql_einsammelnUndAusgeben);
    $rows = mysqli_affected_rows($dbc);

    if (!$result_einsammelnUndAusgeben) {
        lmf_queryTrace($sql_einsammelnUndAusgeben, false, $dbc);
        echo ('{,"loggedIn":true, "errors": ["Fehler beim Ändern des Ausleihstatus!"]}');
    } else {
        lmf_queryTrace($sql_einsammelnUndAusgeben, true, $dbc);
        echo '{"loggedIn":true, "rows": "' . $rows . '", "success":true}';
    }
}
if ($request["type"] == "einsammelnUndAusgebenKlasse") {
    $sql_einsammelnUndAusgeben = "Update `ausgeliehen`"
            . " SET `eingesammelt`=" . $request['eingesammelt']
            . " WHERE `buchId`=" . $request['buchId']
            . " AND `schuelerId` IN (SELECT  a.schuelerId"
            . " FROM anmeldung a, constants c "
            . " Where a.Schuljahr=c.intValue "
            . " AND c.key = 'thisYear' "
            . " AND a.klassenId=" . $request["klassenId"] . ");";
    $result_einsammelnUndAusgeben = mysqli_query($dbc, $sql_einsammelnUndAusgeben);
    $rows = mysqli_affected_rows($dbc);
    if (!$result_einsammelnUndAusgeben) {
        lmf_queryTrace($sql_einsammelnUndAusgeben, false, $dbc);
        echo ('{,"loggedIn":true, "errors": ["Fehler beim Ändern des Ausleihstatus!"]}');
    } else {
        lmf_queryTrace($sql_einsammelnUndAusgeben, true, $dbc);
        echo '{"loggedIn":true, "rows": "' . $rows . '", "success":true}';
    }
}

mysqli_close($dbc); //Close the DB Connection;
exit;
?>
