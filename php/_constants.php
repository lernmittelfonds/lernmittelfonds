<?php

DEFINE('LMF_IBAN', "DE62100500000123456789");
DEFINE('LMF_BIC', "BELADEBEXXX");
DEFINE('LMF_KONTO', "123456789");
DEFINE('LMF_BLZ', "10050000");
DEFINE('LMF_BANK', "Berliner Sparkasse");
DEFINE('LMF_URL', "http://www.meine-schule.de/lmf");
DEFINE('EMAIL', 'eis@meine-schule.de');
DEFINE('EMAIL_LMF', 'lmf@meine-schule.de');

DEFINE('EINZAHLUNG_BUCHUNGS_TAG', 1);
DEFINE('EINZAHLUNG_VALUTA_TAG', 2);
DEFINE('EINZAHLUNG_BUCHUNGS_TEXT', 3);
DEFINE('EINZAHLUNG_VERWENDUNGSZWECK', 4);
DEFINE('EINZAHLUNG_ABSENDER', 11);
DEFINE('EINZAHLUNG_IBAN', 12);
DEFINE('EINZAHLUNG_BIC', 13);
DEFINE('EINZAHLUNG_BETRAG', 14);


DEFINE('MAIL_SIMULATION_TEST_MODUS', false); // true .. es werden keine Mails versandt - man kann das System testen

DEFINE('LMF_SMTP_HOST', "smtp.1und1.de");   //SMTP-Server
DEFINE('LMF_SMTP_AUTH', true);   // SMTP Authentifizierung aktivieren
DEFINE('LMF_SMTP_USER', "lernmittelfonds@meine-schule.de");   // SMTP Benutzer
DEFINE('LMF_SMTP_PW', "meinPasswort1");   // SMTP Benutzer Passwort
DEFINE('LMF_SMTP_SEC', "tls");   // Verbindungssicherheit setzen ( SSL und TLS möglich )
DEFINE('LMF_SMTP_PORT', 587);   // SMTP Authentifizierung aktivieren

DEFINE('EIS_SMTP_HOST', "smtp.1und1.de");   //SMTP-Server
DEFINE('EIS_SMTP_AUTH', true);   // SMTP Authentifizierung aktivieren
DEFINE('EIS_SMTP_USER', "elterninfo@meine-schule.de");   // SMTP Benutzer
DEFINE('EIS_SMTP_PW', "meinPasswort2");   // SMTP Benutzer Passwort
DEFINE('EIS_SMTP_SEC', "tls");   // Verbindungssicherheit setzen ( SSL und TLS möglich )
DEFINE('EIS_SMTP_PORT', 587);   // SMTP Authentifizierung aktivieren

DEFINE('EV_MAIL_FROM_EV', false); // true: sendet die EV-Adresse als Absender (Spamgefahr, aber besser für Nutzer lesbar???) 
DEFINE('MAILS_PER_CALL', 50); // maximale Anzahl an Mails, die pro Aufruf verschickt werden
DEFINE('CALL_INTERVAL_ON_CRON', 180); // Sekunden zwischen zwei Cron aufrufen
// Berechnung: Mail pro Stunde: 3600/CALL_INTERVAL_ON_CRON*MAILS_PER_CALL = 1000
// Berechnung: Mail pro Minute: 60/CALL_INTERVAL_ON_CRON*MAILS_PER_CALL = 16,6

DEFINE('MAIL_TRIGGER_CODE','irgendEinCode'); // frei wählbarer Code_String
DEFINE('MAIL_TRIGGER_EXTERN',true); // man benötigt einen cronjob, der http://..../lmf/php/mailHandler.php?irgendEinCode=true aufruft (richtigen MAIL_TRIGGER_CODE benutzen!)
?>
