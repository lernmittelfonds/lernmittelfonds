<?php

require_once ('./lmf-session.php');
require_once ('database_connection.php');
require_once ('constants.php');
require_once ('mail.php');
if (!isset($_SESSION)) {
    session_start();
}


$error = array(); //this array will store all error messages
$loggedIn = true; //is logged in???
$request = json_decode(file_get_contents('php://input'), true);
if (!isset($_SESSION['FamilienId'])) {
    $error[] = 'Loginerror';
    $login = false;
    echo '{"loggedIn":false, "errors":["Nicht korrekt angemeldet' . $_SESSION['FamilienId'] . '|' . $request['_FamilienId'] . '"], "success":false}';
    exit;
}

if ($request["type"] == "kommentarBearbeiten") {
    $sql_schueler_kommentar = "Update `schueler`"
            . " SET `Anmerkungen`='" . $request['kommentar'] . "'"
            . " WHERE `SchuelerId`=" . $request['schuelerId'] . ";";
    $result_schueler_kommentar = mysqli_query($dbc, $sql_schueler_kommentar);
    if (!$result_schueler_kommentar) {
        lmf_queryTrace($sql_schueler_kommentar, false, $dbc);
        echo ('{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}');
    } else {
        lmf_queryTrace($sql_schueler_kommentar, true, $dbc);
        echo ('{"loggedIn":true, "success":true}');
    }
}
if ($request["type"] == "einzelnesBuchAusleihen") {
    if ($request['schuelerId'] != null && $request['buchId'] != null && $request['buchId'] != -1) {
        $sql_buchausleihen = "Insert into  `ausgeliehen` (schuelerId, buchId) values ("
                . $request["schuelerId"] . ", " . $request["buchId"] . "); ";
        $result_buchausleihen = mysqli_query($dbc, $sql_buchausleihen);
        if (!$result_buchausleihen) {
            lmf_queryTrace($sql_buchausleihen, false, $dbc);
            echo ('{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}');
        } else {
            lmf_queryTrace($sql_buchausleihen, true, $dbc);
            echo ('{"loggedIn":true, "success":true}');
        }
    } else {
        echo ('{"loggedIn":true, "errors":["Parameterfehler"]');
    }
}



if ($request["type"] == "offeneEinzahlungen") {
    $abfrage = "SELECT * FROM einzahlungen WHERE anmeldungsId IS NULL AND (verworfen IS NULL OR  verworfen=0)";
    $ergebnis = mysqli_query($dbc, $abfrage);
    if (!$ergebnis) {
        lmf_queryTrace($abfrage, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($ergebnis)) {
            $resultArray[] = $r;
        }
        echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
    }
}

if ($request["type"] == "einzahlungVerwerfen") {
    $abfrage = "UPDATE einzahlungen SET verworfen=1 WHERE einzahlungsId=" . $request['einzahlungsId'];
    $ergebnis = mysqli_query($dbc, $abfrage);
    if (!$ergebnis) {
        lmf_queryTrace($abfrage, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($abfrage, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}


if ($request["type"] == "CheckEinzahlungen") {
    if ($_SESSION['lmfAnmeldungenFreigeben_int'] == 1) {
        $abfrage_CheckEinzahlungen = "SELECT Min(prio), q.* FROM view_check_einzahlungen q Group by einzahlungsId, anMeldId Order by einzahlungsId, prio;";
    } else {
        $abfrage_CheckEinzahlungen = "SELECT Min(prio), q.* FROM view_check_einzahlungen_alt q Group by einzahlungsId, anMeldId Order by einzahlungsId, prio;";
    }
    $result_CheckEinzahlungen = mysqli_query($dbc, $abfrage_CheckEinzahlungen);

    if (!$result_CheckEinzahlungen) {
        lmf_queryTrace($abfrage_CheckEinzahlungen, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($result_CheckEinzahlungen)) {
            $resultArray[] = $r;
        }
        echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
    }
}
if ($request["type"] == "EinzahlungKlonen") {
    $abfrage_Klonen = "INSERT INTO  einzahlungen 
                    (Buchungstag, Valutatag, Buchungstext, Verwendungszweck, Absender, Betrag, Konto, BLZ)
                    SELECT 
                    Buchungstag, Valutatag, Buchungstext, Verwendungszweck, Absender, 0, Konto, BLZ
                    FROM einzahlungen
                    WHERE einzahlungsId=" . $request['einzahlungsId'];
    $result_Klonen = mysqli_query($dbc, $abfrage_Klonen);
    if (!$result_Klonen) {
        lmf_queryTrace($abfrage_Klonen, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($abfrage_Klonen, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}

if ($request["type"] == "db") {
    $abfrage_generic = "SELECT * FROM " . $request['table'] . ";";
    $result_generic = mysqli_query($dbc, $abfrage_generic);
    if (!$result_generic) {
        lmf_queryTrace($abfrage_generic, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($result_generic)) {
            $resultArray[] = $r;
        }
        echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
    }
}

if ($request["type"] == "EinzahlungZuordnen") {
    $query_matchEinzahlung = "UPDATE einzahlungen SET anmeldungsId=" . $request['anmeldungsId'] . " WHERE einzahlungsId=" . $request['einzahlungsId'] . ";";
    $result_matchEinzahlung = mysqli_query($dbc, $query_matchEinzahlung);
    if (!$result_matchEinzahlung) {
        lmf_queryTrace($query_matchEinzahlung, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($query_matchEinzahlung, true, $dbc);
        if (sendMailEinzahlungErhalten($request['einzahlungsId'], $dbc)) {
            echo '{"loggedIn":true, "success":true}';
        } else {
            lmf_trace("MailErrorAfter: " . $query_matchEinzahlung);
            echo '{"loggedIn":true, "success":true, "errors":["Leider konnte das System keine Mail an den Absender versenden!"]}';
        }
    }
}
if ($request["type"] == "Uebersicht") {
    $abfrage_uebersicht = "SELECT sum(bezahlt) as bezahlt, sum(unbezahlt) as unbezahlt, sum(gesamt) as gesamt FROM view_uebersicht;";
    $result_Uebersicht = mysqli_query($dbc, $abfrage_uebersicht);
    if (!$result_Uebersicht) {
        lmf_queryTrace($abfrage_uebersicht, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultSumme = array();
        while ($r = mysqli_fetch_assoc($result_Uebersicht)) {
            $resultSumme[] = $r;
        }
        $abfrage_uebersicht = "SELECT * FROM view_uebersicht;";
        $result_Uebersicht = mysqli_query($dbc, $abfrage_uebersicht);

        if (!$result_Uebersicht) {
            lmf_queryTrace($abfrage_uebersicht, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        } else {
            $resultArray = array();
            while ($r = mysqli_fetch_assoc($result_Uebersicht)) {
                $resultArray[] = $r;
            }
            echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . ', "summe":' . json_encode($resultSumme) . '}';
        }
    }
}
if ($request["type"] == "KlassenUebersicht") {
    $abfrage_klassen_uebersicht = "SELECT * FROM view_klassen_uebersicht3 where klassenId=" . $request["_klassenId"] . ";";
    $result_klassen_Uebersicht = mysqli_query($dbc, $abfrage_klassen_uebersicht);
    if (!$result_klassen_Uebersicht) {
        lmf_queryTrace($abfrage_klassen_uebersicht, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($result_klassen_Uebersicht)) {
            $resultArray[] = $r;
        }
        echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
    }
}
if ($request["type"] == "NeuAnmeldungen") {
    $abfrage_neuAnmeldungen = "SELECT u.*, concat(k.stufe,k.SubKlasse,'-',k.Sprache) as Klasse
                FROM `view_klassen_uebersicht3` u, view_klassen_neues_jahr k, constants c
                WHERE c.key='ersteKlasse'
                AND k.Stufe>c.intValue
                AND u.klassenId=k.klassenId
                AND isNull(u.altAngemeldet)
                AND u.bezahlt>=0";
    $result_neuAnmeldungen = mysqli_query($dbc, $abfrage_neuAnmeldungen);
    if (!$result_neuAnmeldungen) {
        lmf_queryTrace($abfrage_neuAnmeldungen, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($result_neuAnmeldungen)) {
            $resultArray[] = $r;
        }
        echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
    }
}

if ($request["type"] == "AnmeldungenAltUndNeu") {
    $abfrage_AnmeldungenAltUndNeu = "SELECT count(a.SchuelerId) as alt_gesamt, 
            SUM(CASE WHEN a.bezahltneu >=0 THEN 1 ELSE 0 END) AS neu_angemeldet ,
            SUM(CASE WHEN a.bezahltneu =-2 THEN 1 ELSE 0 END) AS neu_abgemeldet ,
            SUM(CASE WHEN a.bezahltneu =-1 THEN 1 ELSE 0 END) AS offen 
            FROM view_anmeldungen_alt_und_neu a, schueler s, eltern e 
            WHERE bezahlt =1 
            AND a.SchuelerId=s.SchuelerId
            AND e.FamilienID=s.FamilienID
            AND a.Stufe <" . $_SESSION['letzteKlasse_int'] . ";";
    //  count(a.SchuelerId) - SUM(CASE WHEN a.bezahltneu >=0 THEN 1 ELSE 0 END) AS offen 
    $result_AnmeldungenAltUndNeu = mysqli_query($dbc, $abfrage_AnmeldungenAltUndNeu);
    if (!$result_AnmeldungenAltUndNeu) {
        lmf_queryTrace($abfrage_AnmeldungenAltUndNeu, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultSumme = array();
        while ($r = mysqli_fetch_assoc($result_AnmeldungenAltUndNeu)) {
            $resultSumme[] = $r;
        }
        $abfrage_AnmeldungenAltUndNeu = "SELECT a.KlassenId, a.Stufe, a.SubKlasse, a.sprache,
            count(a.SchuelerId) as alt_gesamt, 
            SUM(CASE WHEN a.bezahltneu >=0 THEN 1 ELSE 0 END) AS neu_angemeldet ,
            SUM(CASE WHEN a.bezahltneu =-2 THEN 1 ELSE 0 END) AS neu_abgemeldet ,
            SUM(CASE WHEN a.bezahltneu =-1 THEN 1 ELSE 0 END) AS offen 
            FROM view_anmeldungen_alt_und_neu a, schueler s, eltern e 
            WHERE bezahlt =1 
            AND a.SchuelerId=s.SchuelerId
            AND e.FamilienID=s.FamilienID
            AND a.Stufe <" . $_SESSION['letzteKlasse_int'] . "
            Group by KlassenId;";
        $result_AnmeldungenAltUndNeu = mysqli_query($dbc, $abfrage_AnmeldungenAltUndNeu);
        if (!$result_AnmeldungenAltUndNeu) {
            lmf_queryTrace($abfrage_AnmeldungenAltUndNeu, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        } else {
            $resultArray = array();
            while ($r = mysqli_fetch_assoc($result_AnmeldungenAltUndNeu)) {
                $resultArray[] = $r;
            }
            echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . ', "summe":' . json_encode($resultSumme) . '}';
        }
    }
}
if ($request["type"] == "AnmeldungenAltUndNeuKlasse") {
    $abfrage_AnmeldungenAltUndNeu = "SELECT a.*,  concat(s.Nachname, ', ', s.Vorname) AS schuelerName,  
                    concat(e.Nachname, ', ', e.Vorname) AS elternName, 
                    s.FamilienId, e.email, e.Telefon, s.Anmerkungen
                    FROM view_anmeldungen_alt_und_neu a, schueler s, eltern e 
                    Where a.Schuelerid = s.Schuelerid
                    AND e.FamilienId = s.FamilienId
                    AND e.zuZahlungsBefreit=0 ";
    if ("0" != $request["_klassenId"]) {
        $abfrage_AnmeldungenAltUndNeu .= " AND KlassenId=" . $request["_klassenId"];
    } else {
        $abfrage_AnmeldungenAltUndNeu .= " AND bezahltNeu=-1 AND Stufe<12 Limit 100;";
    }
    $result_AnmeldungenAltUndNeu = mysqli_query($dbc, $abfrage_AnmeldungenAltUndNeu);
    if (!$result_AnmeldungenAltUndNeu) {
        lmf_queryTrace($abfrage_AnmeldungenAltUndNeu, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($result_AnmeldungenAltUndNeu)) {
            $resultArray[] = $r;
        }
        echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
    }
}


if ($request["type"] == "KlassenListe") {
    $abfrage_klassen = "SELECT *  
                FROM view_klassen_lang 
                Where (KlassenStufeAlt != '+++++' OR KlassenStufeNeu != '+++++')
                ORDER BY OrderAlt";
    $result_klassen = mysqli_query($dbc, $abfrage_klassen);
    if (!$result_klassen) {
        lmf_queryTrace($abfrage_klassen, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($result_klassen)) {
            $resultArray[] = $r;
        }
        echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
    }
}

if ($request["type"] == "KlassenLoeschen") {
    $abfrage_klasseLoeschen = "DELETE FROM klasse WHERE KlassenId=" . $request["KlassenId"] . ";";
    $result_klasseLoeschen = mysqli_query($dbc, $abfrage_klasseLoeschen);
    if (!$result_klasseLoeschen) {
        lmf_queryTrace($abfrage_klasseLoeschen, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($abfrage_klasseLoeschen, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}

if ($request["type"] == "KlassenAktualisieren") {
    if ($request["KlassenId"] == 0) {
        $abfrage_KlassenAktualisieren = "INSERT INTO klasse (StartJahr, StartKlassenStufe, SubKlasse, Sprache, EndKlassenStufe, Metakey) values ("
                . $request["StartJahr"] . ", "
                . $request["StartKlassenStufe"] . ",' "
                . $request["SubKlasse"] . "', '"
                . $request["Sprache"] . "', "
                . $request["EndKlassenStufe"] . ", '"
                . $request["MetaKey"] . "');";
    } else {
        $abfrage_KlassenAktualisieren = "UPDATE klasse SET   
            StartJahr = " . $request["StartJahr"] . ",  
            StartKlassenStufe= " . $request["StartKlassenStufe"] . ", 
            SubKlasse = '" . $request["SubKlasse"] . "', 
            Sprache = '" . $request["Sprache"] . "',  
            EndKlassenStufe = " . $request["EndKlassenStufe"] . ",  
            MetaKey = '" . $request["MetaKey"] . "'" .
                " WHERE KlassenId = " . $request["KlassenId"] . ";";
    }
    $result_KlassenAktualisieren = mysqli_query($dbc, $abfrage_KlassenAktualisieren);
    $idNeu = mysqli_insert_id($dbc);
    if (!$result_KlassenAktualisieren) {
        lmf_queryTrace($abfrage_KlassenAktualisieren, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($abfrage_KlassenAktualisieren, true, $dbc);
        if ($request["KlassenId"] == "0") {
            echo '{"loggedIn":true, "success":true, id:' . $idNeu . '}';
        } else {
            echo '{"loggedIn":true, "success":true}';
        }
    }
}

if ($request["type"] == "buecher") {
    $abfrage_buecher = "SELECT *  
                FROM buecher 
                WHERE (von <= " . $request["klassenStufe"] . "
                AND   bis >= " . $request["klassenStufe"] . ")
                OR    (von = " . $request["klassenStufe"] . "
                AND   bis = 0)
                ORDER BY Titel";
    $result_buecher = mysqli_query($dbc, $abfrage_buecher);

    if (!$result_buecher) {
        lmf_queryTrace($abfrage_buecher, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($result_buecher)) {
            $resultArray[] = $r;
        }
        echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
    }
}

if ($request["type"] == "BuecherListen") {
    $abfrage_buecherListen = "SELECT * FROM `buecherlisten` WHERE jahr=" . $request["_jahr"] . ";";
    $result_buecherListen = mysqli_query($dbc, $abfrage_buecherListen);

    if (!$abfrage_buecherListen) {
        lmf_queryTrace($abfrage_buecherListen, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray1 = array();
        while ($r = mysqli_fetch_assoc($result_buecherListen)) {
            $resultArray1[] = $r;
        }
        $abfrage_buecherInListen = "SELECT '' as checked, bl.*, b.Titel, b.Isbn,  bl.preisImJahr as Neupreis FROM view_buecherlisten bl, buecher b WHERE bl.buchId=b.buchId AND jahr=" . $request["_jahr"] . ";";
        $result_buecherInListen = mysqli_query($dbc, $abfrage_buecherInListen);
        if (!$result_buecherInListen) {
            lmf_queryTrace($abfrage_buecherInListen, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        } else {
            $resultArray2 = array();
            while ($r = mysqli_fetch_assoc($result_buecherInListen)) {
                $resultArray2[] = $r;
            }
            echo '{"loggedIn":true, "success":true, "data":{"buecher":'
            . json_encode($resultArray2) . ',"buecherListen":' . json_encode($resultArray1) . ' }}';
        }
    }
}

if ($request["type"] == "BeitragAendern") {
    $abfrage_beitragAendern = "UPDATE buecherlisten 
                                    SET beitrag = " . $request["_neuerBeitrag"] . "
                                    WHERE listenId = " . $request["_listenId"];
    $result_beitragAendern = mysqli_query($dbc, $abfrage_beitragAendern);
    if (!$result_beitragAendern) {
        lmf_queryTrace($abfrage_beitragAendern, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($abfrage_beitragAendern, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}

if ($request["type"] == "buchInListeEinfuegen") {
    $abfrage_buchInListeEinfuegen = "INSERT INTO buecherinlisten (listenId, buchId, preisImJahr) values ("
            . $request["listenId"] . ", " . $request["buchId"] . ", " . $request["preisImJahr"] . ");";
    $result_buchInListeEinfuegen = mysqli_query($dbc, $abfrage_buchInListeEinfuegen);
    if (!$result_buchInListeEinfuegen) {
        lmf_queryTrace($abfrage_buchInListeEinfuegen, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($abfrage_buchInListeEinfuegen, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}

if ($request["type"] == "BuchAusListeLoeschen") {
    $abfrage_buecherListeLoeschen = "DELETE FROM buecherinlisten WHERE listenId=" . $request["_listenId"]
            . " AND buchId=" . $request["_buchId"] . ";";
    $result_buecherListeLoeschen = mysqli_query($dbc, $abfrage_buecherListeLoeschen);
    if (!$result_buecherListeLoeschen) {
        lmf_queryTrace($abfrage_buecherListeLoeschen, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($abfrage_buecherListeLoeschen, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}
if ($request["type"] == "buchSpeichern") {
    if ($request["BuchId"] == "-1") {
        $letzteId = mysqli_insert_id($dbc);
        $abfrage_neuesBuch = "INSERT INTO buecher (Isbn, Titel, von, bis, Neupreis) VALUES ('"
                . $request["Isbn"] . "','" . $request["Titel"] . "'," . $request["von"] . "," . $request["bis"] . "," . $request["Neupreis"] . ");";
        mysqli_query($dbc, $abfrage_neuesBuch);
        $idNeu = mysqli_insert_id($dbc);
        if (null == $idNeu || $letzteId == $idNeu) {
            lmf_queryTrace($abfrage_neuesBuch, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "alteId":' . $letzteId . ',"neueId":' . $idNeu . ',"success":false}';
        } else {
            lmf_queryTrace($abfrage_neuesBuch, true, $dbc);
            echo '{"loggedIn":true, "success":true, id:' . $idNeu . '}';
        }
    } else {
        $abfrage_BuchAendern = "UPDATE buecher SET Isbn='" . $request["Isbn"] .
                "', Titel= '" . $request["Titel"] .
                "', von= " . $request["von"] .
                ", bis= " . $request["bis"] .
                ", Neupreis= " . $request["Neupreis"] .
                " WHERE BuchId=" . $request["BuchId"] . ";";
        $result_buchAendern = mysqli_query($dbc, $abfrage_BuchAendern);
        if (!$result_buchAendern) {
            lmf_queryTrace($abfrage_BuchAendern, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        } else {
            lmf_queryTrace($abfrage_BuchAendern, true, $dbc);
            echo '{"loggedIn":true, "success":true, id:' . $request["BuchId"] . '}';
        }
    }
}

// meldet einen Schueler nachtraeglich für das aktuelle Jahr an - eigentlich nur wichtig im Startjahr
if ($request["type"] == "altAnmelden") {
    $query_anmelden = "Insert into  `anmeldung`( `SchuelerId`, `Schuljahr`, `bezahlt`, `KlassenId`) "
            . " VALUES ( " . $request['SchuelerId'] . ", " . (THIS_YEAR) . ", 1,'" . $request['KlassenId'] . "');";
    $result_anmelden = mysqli_query($dbc, $query_anmelden);
    if (!$result_anmelden) {
        lmf_queryTrace($query_anmelden, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($query_anmelden, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}
// meldet einen Schueler nachtraeglich für das aktuelle Jahr ab - eigentlich nur wichtig im Startjahr
if ($request["type"] == "altAbmelden") {
    $query_abmelden = "Delete From  `anmeldung` 
            WHERE `SchuelerId` = " . $request['SchuelerId'] . " 
            AND   `Schuljahr`  = " . (THIS_YEAR) . ";";
    $result_abmelden = mysqli_query($dbc, $query_abmelden);
    if (!$result_abmelden) {
        lmf_queryTrace($query_abmelden, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($query_abmelden, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}

if ($request["type"] == "suchen") {
    if (strlen($request["suchZiel"]) > 0) {
        $query_suchen = "SELECT e.FamilienId, 
                concat (e.Nachname, ', ',e.Vorname) as Eltern, 
                concat (s.Nachname, ', ',s.Vorname) as Schueler, 
                s1.KlassenId  as KlassenIdAlt,
		concat (s1.Stufe,s1.SubKlasse,'-',s1.Sprache) as KlasseAlt,
                s2.KlassenId as KlassenIdNeu,
		concat (s2.Stufe,s2.SubKlasse,'-',s2.Sprache) as KlasseNeu,
                s.SchuelerId,
                IFNULL(concat (e.FamilienId, ' ',s.SchuelerId),concat (e.FamilienId, ' ')) as ID 
            FROM  eltern e 
            left join schueler s ON  s.FamilienId=e.FamilienId
            left join view_schueler_dieses_jahr s1 ON  s.SchuelerId=s1.SchuelerId
			left join view_schueler_naechstes_jahr s2 ON  s.SchuelerId=s2.SchuelerId
            Where LOWER(CONCAT (IFNULL(s.Nachname,''), ' ', IFNULL(s.Vorname,''), ' ', e.Nachname, ' ', e.Vorname, ' ', e.email)) 
            LIKE LOWER('%" . $request["suchZiel"] . "%');";
        $result_suchen = mysqli_query($dbc, $query_suchen);
        if (!$result_suchen) {
            lmf_queryTrace($query_suchen, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        } else {
            lmf_queryTrace($query_suchen, true, $dbc);
            $resultArray = array();
            while ($r = mysqli_fetch_assoc($result_suchen)) {
                $resultArray[] = $r;
            }
            echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
        }
    } else {
        echo '{"loggedIn":true, "errors":["kein Suchbegriff übertragen"], "success":false"}';
    }
}

if ($request["type"] == "buecherZuSchuelernZuordnen") {
    $query_zuordnen = "INSERT IGNORE INTO ausgeliehen (schuelerId, buchId)
        SELECT schuelerid, b.buchid
        FROM anmeldung a, constants c, klasse k, view_buecherlisten bl, buecher b
        WHERE c.key='thisYear'
        AND c.intValue=a.schuljahr
        AND a.bezahlt > 0
        AND k.klassenid=a.klassenid
        AND bl.klassenstufe=a.schuljahr-k.Startjahr+StartKlassenStufe
        AND bl.Sprache=k.Sprache
        AND bl.jahr=a.schuljahr
        AND b.bis>0
        AND b.buchid=bl.buchid";
    $result_zuordnen = mysqli_query($dbc, $query_zuordnen);
    if (!$result_zuordnen) {
        lmf_queryTrace($query_zuordnen, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($query_zuordnen, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}

if ($request["type"] == "buchListenKopieren") {
    $query_buecherKopieren = "INSERT IGNORE INTO buecherinlisten (listenId, buchId, preisImJahr)
                SELECT bl2.listenId, buchId, preisImJahr
                FROM buecherlisten bl, buecherinlisten bil, buecherlisten bl2, constants c
                where c.key='thisYear'
                AND bl.jahr = c.intValue
                AND bl.listenId=bil.listenId
                AND bl.sprache=bl2.sprache
                AND bl.klassenStufe=bl2.klassenStufe
                AND bl2.jahr=c.intValue+1;";
    $result_buecherKopieren = mysqli_query($dbc, $query_buecherKopieren);
    if (!$result_buecherKopieren) {
        lmf_queryTrace($query_buecherKopieren, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($query_buecherKopieren, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}
if ($request["type"] == "buchListenLoeschen") {
    $query_buecherListenLoeschen = "DELETE FROM buecherinlisten 
        WHERE listenId IN (
            SELECT listenId from buecherlisten bl, constants c
            WHERE c.key='thisYear'
            AND bl.jahr = c.intValue+1)";
    $result_buecherListenLoeschen = mysqli_query($dbc, $query_buecherListenLoeschen);
    if (!$result_buecherListenLoeschen) {
        lmf_queryTrace($query_buecherListenLoeschen, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($query_buecherListenLoeschen, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}

if ($request["type"] == "ladePlanungsStand" || $request["type"] == "setPlanungsStand") {
    if ($request["type"] == "setPlanungsStand") {

        $fehlerText = "";

        if ($request["planungsTyp"] == "lmfNeuesJahr") {
            // Löschen von Schülern ohne gültige Klasse im alten Jahr und im neuen Jahr
            $abfrage = "SELECT s.* from
            (`schueler` `s`
                left join `view_schueler_dieses_jahr` `alt` ON ((`s`.`schuelerId` = `alt`.`schuelerId`))
                left join `view_schueler_naechstes_jahr` `neu` ON ((`s`.`schuelerId` = `neu`.`schuelerId`)))
            WHERE isnull(alt.schuelerId) AND isnull(neu.schuelerId)";

            $result = mysqli_query($dbc, $abfrage);
            $jobIdSchueler = createMailJob($dbc, "Info Schülerdaten gelöscht");
            lmf_trace("$jobIdSchueler: " . $jobIdSchueler);
            $jobIdEltern = createMailJob($dbc, "Info Elterndaten glöscht");
            lmf_trace("$jobIdEltern: " . $jobIdEltern);
            if (!$result) {
                lmf_queryTrace($abfrage, false, $dbc);
            } else {
                while ($schueler = mysqli_fetch_array($result)) {

                    $abfrage2 = "SELECT * from `anmeldung` where SchuelerId = " . $schueler['SchuelerId'];
                    $result2 = mysqli_query($dbc, $abfrage2);
                    if (!$result2) {
                        lmf_queryTrace($abfrage2, false, $dbc);
                    } else {
                        while ($anmeldung = mysqli_fetch_array($result2)) {
                            $abfrage3 = "DELETE from `einzahlungen` where anmeldungsId = " . $anmeldung['anmeldungsId'];
                            $result3 = mysqli_query($dbc, $abfrage3);
                            if (!$result3) {
                                lmf_queryTrace($abfrage3, false, $dbc);
                            } else {
                                lmf_queryTrace($abfrage3, true, $dbc);
                            }
                        }
                        $abfrage4 = "DELETE from `anmeldung` where SchuelerId = " . $schueler['SchuelerId'];
                        $result4 = mysqli_query($dbc, $abfrage4);
                        if (!$result4) {
                            lmf_queryTrace($abfrage4, false, $dbc);
                        } else {
                            lmf_queryTrace($abfrage4, true, $dbc);
                        }
                    }
                    $abfrage5 = "DELETE from `schueler` where SchuelerId = " . $schueler['SchuelerId'];
                    $result5 = mysqli_query($dbc, $abfrage5);
                    if (!$result5) {
                        lmf_queryTrace($abfrage5, false, $dbc);
                    } else {
                        lmf_queryTrace($abfrage5, true, $dbc);
                        $abfrage6 = "SELECT * from eltern WHERE FamilienId = " . $schueler['FamilienId'];
                        $result6 = mysqli_query($dbc, $abfrage6);
                        if ($eltern = mysqli_fetch_array($result6)) {
                            lmf_queryTrace($abfrage6, true, $dbc);
                        } else {
                            lmf_queryTrace($abfrage6, false, $dbc);
                        }
                        if ("Vorname" != $schueler["Vorname"]) {
                            try {
                                if (!sendMailElternDatenGeloescht($dbc, $jobIdSchueler, $schueler, $eltern)) {
                                    lmf_trace("ERROR: EmailToDB 'Schüler Daten loeschen' nicht erfolgreich: " . $eltern["Email"]);
                                }
                            } catch (Exception $ex) {
                                lmf_trace("ERROR:  EmailToDB 'Schüler Daten loeschen' nicht erfolgreich: " . $eltern["Email"]);
                            }
                        }
                    }
                }
            }

            // Löschen von Eltern ohne Schüler die länger als 150 Tage registriert sind und kein Admin
            $abfrage = "SELECT  e.*
                FROM `eltern` e
                LEFT JOIN schueler s ON ( e.FamilienId = s.FamilienId )
                WHERE isnull( s.schuelerId ) 
                AND e.isAdmin!=1 
                AND DATEDIFF(NOW(),e.registriertSeit )>150";

            $result = mysqli_query($dbc, $abfrage);
            if (!$result) {
                lmf_queryTrace($abfrage, false, $dbc);
            } else {
                while ($eltern = mysqli_fetch_array($result)) {
                    $abfrage2 = "DELETE from `eltern` where FamilienId = " . $eltern['FamilienId'];
                    $result2 = mysqli_query($dbc, $abfrage2);
                    if (!$result2) {
                        lmf_queryTrace($abfrage2, false, $dbc);
                    } else {
                        lmf_queryTrace($abfrage2, true, $dbc);
                        try {
                            if (!sendMailElternDatenGeloescht($dbc, $jobIdEltern, $eltern)) {
                                lmf_trace("ERROR: EmailToDB 'Eltern Daten loeschen' nicht erfolgreich: " . $eltern["Email"]);
                            }
                        } catch (Exception $ex) {
                            lmf_trace("ERROR:  EmailToDB 'Eltern Daten loeschen' nicht erfolgreich: " . $eltern["Email"]);
                        }
                    }
                }
            }

            // Klassenwechsel -> EisStatus auf 0 zuruechsetzen
            $query_setPlanung = "update schueler set eisStatus=0 where SchuelerId in (
                                select `aAlt`.`SchuelerId` 
                                from `anmeldung` `aAlt`, `anmeldung` `aNeu`,`constants` `c`
                                where `aAlt`.`SchuelerId` = `aNeu`.`SchuelerId`
                                AND `c`.`key` = 'thisYear'
                		AND  `c`.`intValue` = aAlt.Schuljahr
                		AND  `c`.`intValue`+1 = aNeu.Schuljahr
                		And  aAlt.KlassenId != aNeu.KlassenId)";
            $result_setPlanung = mysqli_query($dbc, $query_setPlanung);
            if ($result_setPlanung) {
                lmf_queryTrace($query_setPlanung, true, $dbc);
                $query_setPlanung = "UPDATE `constants` c SET  c.intValue = (c.intValue+1), c.strValue=c.intValue WHERE c.key = 'thisYear';";
                $result_setPlanung = mysqli_query($dbc, $query_setPlanung);
                if ($result_setPlanung) {
                    lmf_queryTrace($query_setPlanung, true, $dbc);
                    // bezahlte Lernmittelfondsler bekommen EisStatus 1
                    $query_setPlanung = "update schueler set eisStatus=1
                                where SchuelerId in ( 
                                 select  `a`.`SchuelerId`
                                 from `anmeldung` `a`, `constants` `c`
                                 where `c`.`key` = 'thisYear'
                                 AND  `c`.`intValue` = a.Schuljahr
                                 AND a.bezahlt=1)";
                    $result_setPlanung = mysqli_query($dbc, $query_setPlanung);
                    if ($result_setPlanung) {
                        lmf_queryTrace($query_setPlanung, true, $dbc);
                        // zuruecksetzen der Jahresplanungsdaten
                        $query_setPlanung = "UPDATE `constants` c SET  c.intValue = 0 WHERE c.key LIKE 'lmf%';";
                        $result_setPlanung = mysqli_query($dbc, $query_setPlanung);
                        if ($result_setPlanung) {
                            lmf_queryTrace($query_setPlanung, true, $dbc);
                            // Zurueckgegebene Bücher löschen
                            $query_setPlanung = "DELETE FROM ausgeliehen where eingesammelt=1;";
                            $result_setPlanung = mysqli_query($dbc, $query_setPlanung);
                            if ($result_setPlanung) {
                                lmf_queryTrace($query_setPlanung, true, $dbc);
                                // Anmeldung für das neue naechste Schuljahr generieren
                                $query_setPlanung = "INSERT IGNORE INTO anmeldung (SchuelerId,Klassenid,Schuljahr,bezahlt)
                                        SELECT a.SchuelerId,a.Klassenid,a.Schuljahr+1, -1
                                        FROM anmeldung a, constants c
                                        Where a.Schuljahr=c.intValue
                                        AND c.key = 'thisYear'";
                                $result_setPlanung = mysqli_query($dbc, $query_setPlanung);
                                if (!$result_setPlanung) {
                                    lmf_queryTrace($query_setPlanung, false, $dbc);
                                    $fehlerText = "Anlegen neuer Schüler-Klasse-Jahr-Zuordnung in anmeldung fehlgeschlagen";
                                } else {
                                    lmf_queryTrace($query_setPlanung, true, $dbc);
                                }
                            } else {
                                lmf_queryTrace($query_setPlanung, false, $dbc);
                                $fehlerText = "Löschen zurückgegebener Bücher fehlgeschlagen";
                            }
                        } else {
                            lmf_queryTrace($query_setPlanung, false, $dbc);
                            $fehlerText = "Änderung Planungsstati fehlgeschlagen";
                        }
                    } else {
                        lmf_queryTrace($query_setPlanung, false, $dbc);
                        $fehlerText = "Änderung Eis-Aktivierung von Zahlern im LMF fehlgeschlagen";
                    }
                } else {
                    lmf_queryTrace($query_setPlanung, false, $dbc);
                    $fehlerText = "Änderung aktuelles Jahr in constants fehlgeschlagen";
                }
            } else {
                lmf_queryTrace($query_setPlanung, false, $dbc);
                $fehlerText = "Korrektur Status im EIS bei Klassenwechsel fehlgeschlagen";
            }
            if ($fehlerText != "") {
                echo '{"loggedIn":true, "errors":["' . $fehlerText . ' "], "success":false}';
            }
        } else if ($request["planungsTyp"] == "lmfAltesJahr") {
            $query_setPlanung = "UPDATE `constants` c SET  c.intValue = (c.intValue-1), c.strValue=c.intValue WHERE c.key = 'thisYear';";
            $result_setPlanung = mysqli_query($dbc, $query_setPlanung);
            if ($result_setPlanung) {
                lmf_queryTrace($query_setPlanung, true, $dbc);
                $query_setPlanung = "UPDATE `constants` c SET  c.intValue = 1 WHERE c.key LIKE 'lmf%';";
                $result_setPlanung = mysqli_query($dbc, $query_setPlanung);
                if ($result_setPlanung) {
                    lmf_queryTrace($query_setPlanung, true, $dbc);
                } else {
                    lmf_queryTrace($query_setPlanung, false, $dbc);
                }
            }
        } else {
            if ($request["value"] == null) {
                $query_setPlanung = "UPDATE `constants` c SET  c.intValue = (c.intValue-1)*(c.intValue-1) WHERE c.key = '" . $request["planungsTyp"] . "';";
            } else {
                $query_setPlanung = "UPDATE `constants` c SET  c.STRValue = '" . $request["value"] . "' WHERE c.key = '" . $request["planungsTyp"] . "';";
            }
            $result_setPlanung = mysqli_query($dbc, $query_setPlanung);
            if ($result_setPlanung) {
                lmf_queryTrace($query_setPlanung, true, $dbc);
                $alterWert = $_SESSION[$request["planungsTyp"] . "_int"];
                $_SESSION[$request["planungsTyp"] . "_int"] = ($alterWert - 1 ) * ($alterWert - 1);
            } else {
                lmf_queryTrace($query_setPlanung, false, $dbc);
            }
        }
    }
    if ($request["type"] == "ladePlanungsStand" || $result_setPlanung) {
        $query_planung = "Select * FROM `constants` c WHERE c.key LIKE 'lmf%' order by strValue;";
        lmf_trace('Query: ' . $query_planung);
        $result_planung = mysqli_query($dbc, $query_planung);
        lmf_trace('Result: ' . json_encode($result_planung));
        if (!$result_planung) {
            lmf_queryTrace($query_planung, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler2"], "success":false}';
        } else {
            $resultArray = array();
            while ($r = mysqli_fetch_assoc($result_planung)) {
                lmf_trace('Zeile: ' . json_encode($r));
                $resultArray[] = $r;
            }
            echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
        }
    } else {
        echo '{"loggedIn":true, "errors":["Datenbankfehler1"], "success":false}';
    }
}

if (isset($_REQUEST["type"]) && $_REQUEST["type"] == "ListenVerlieheneBuecher") {
    echo '{';
    $abfrage_buecher = "SELECT DISTINCT b.*
        FROM anmeldung a,  ausgeliehen al, buecher b, constants c, klasse k
        Where a.Schuljahr=c.intValue
        AND c.key = 'thisYear'
        AND a.schuelerId=al.schuelerId
        AND a.klassenId=" . $_REQUEST["klassenId"] . " 
        AND a.klassenId=k.klassenId
        AND b.buchId=al.buchId
        AND b.bis >0
        AND b.bis <= k.StartKlassenStufe + c.intValue - k.StartJahr
        ORDER BY b.buchId";
    $result_buecher = mysqli_query($dbc, $abfrage_buecher);
    if (!$result_buecher) {
        lmf_queryTrace($abfrage_buecher, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler2"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($result_buecher)) {
            $resultArray[] = $r;
        }
        echo '"loggedIn":true,  "data":{"buecher":' . json_encode($resultArray);
        $abfrage_ausleihe = "SELECT s.*, b.*, al.eingesammelt
        FROM anmeldung a,  ausgeliehen al, buecher b, constants c, schueler s
        Where a.Schuljahr=c.intValue
        AND c.key = 'thisYear'
        AND a.schuelerId=al.schuelerId
        AND a.klassenId=" . $_REQUEST["klassenId"] . " 
        AND b.buchId=al.buchId
        AND al.schuelerId = s.schuelerId
        ORDER by s.Nachname, s.Vorname, s.schuelerId, b.buchId ";
        $result_ausleihe = mysqli_query($dbc, $abfrage_ausleihe);
        if (!$result_ausleihe) {
            lmf_queryTrace($abfrage_ausleihe, false, $dbc);
            echo '}, "errors":["Datenbankfehler2"], "success":false}';
        } else {
            $schuelerId = 0;
            echo ', "schueler":[';
            $keinSchueler = true;
            while ($ausleihe = mysqli_fetch_array($result_ausleihe)) {
                $keinSchueler = false;
                if ($ausleihe['SchuelerId'] != $schuelerId) {
                    if ($schuelerId != 0) {
                        echo '},{'; // alten schueler beenden 
                    } else {
                        echo '{';
                    }
                    echo '"schuelerId":' . $ausleihe['SchuelerId'] . ', "name": "' . $ausleihe['Nachname'] . ', ' . $ausleihe['Vorname'] . '",';
                } else {
                    echo ', ';
                }
                echo '"buch_' . $ausleihe['BuchId'] . '":"' . $ausleihe['eingesammelt'] . '"';
                $schuelerId = $ausleihe['SchuelerId'];
            }
            if (!$keinSchueler) {
                echo '}';
            }
            echo ']}, "success":true}';
        }
    }
}

if ($request["type"] == "texte") {
    $sonderzeichen = array("'", '"', "\n");
    $ersatz = array("\\'", "\\\"", "\\n");
    if ($request["text"] != null && $request["text"] != "") {
        $query_textSpeichern = "INSERT INTO texte (code, text) VALUES ('" . $request["code"] . "','" . str_replace($sonderzeichen, $ersatz, $request["text"]) . "')"
                . " ON DUPLICATE KEY UPDATE text='" . str_replace($sonderzeichen, $ersatz, $request["text"]) . "';";
        $result_textSpeichern = mysqli_query($dbc, $query_textSpeichern);
        if (!$result_textSpeichern) {
            lmf_queryTrace($query_textSpeichern, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        } else {
            lmf_queryTrace($query_textSpeichern, true, $dbc);
            echo '{"loggedIn":true, "success":true}';
        }
    } else {
        $abfrage_textLaden = "SELECT * FROM texte WHERE code='" . $request['code'] . "';";
        $result_textLaden = mysqli_query($dbc, $abfrage_textLaden);
        if (!$result_textLaden) {
            lmf_queryTrace($abfrage_textLaden, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        } else {
            if ($row = mysqli_fetch_assoc($result_textLaden)) {
                echo '{"loggedIn":true, "success":true, "data":"' . str_replace($sonderzeichen, $ersatz, $row['text']) . '"}';
            } else {
                echo '{"loggedIn":true, "success":true, "data":""}';
            }
        }
    }
}
$ret["MAILS_PER_CALL"] = MAILS_PER_CALL;
$ret["CALL_INTERVAL_ON_CRON"] = CALL_INTERVAL_ON_CRON;
$ret["MAIL_TRIGGER_EXTERN"] = MAIL_TRIGGER_EXTERN;
$mailMetaInfo = ', "MAILS_PER_CALL":' . MAILS_PER_CALL
        . ', "CALL_INTERVAL_ON_CRON":' . CALL_INTERVAL_ON_CRON
        . ', "MAIL_TRIGGER_EXTERN":' . MAIL_TRIGGER_EXTERN;
if ($request["type"] == "anmeldeMail") {
    $count = sendMailAnmeldungFreigegeben($dbc);
    echo '{"loggedIn":true, "success":true, "data":' . $count . $mailMetaInfo . '}';
}
if ($request["type"] == "erinnerungsMail") {
    $count = sendMailErinnern($dbc);
    echo '{"loggedIn":true, "success":true, "data":' . $count . $mailMetaInfo . '}';
}
if ($request["type"] == "zahlungErinnerungsMail") {
    $count = sendMailZahlungErinnern($dbc);
    echo '{"loggedIn":true, "success":true, "data":' . $count . $mailMetaInfo . '}';
}
if ($request["type"] == "buchRueckgabeMail") {
    $count = sendMailBuchabgabeErinnern($dbc);
    lmf_trace("sendMailInfo2: " . $count . " Mails versandt");
    echo '{"loggedIn":true, "success":true, "bal":1, data":' . $count . $mailMetaInfo . '}';
}
if ($request["type"] == "buchRueckgabeInfoMail") {
    $count = sendMailBuchabgabeInfo($dbc);
    echo '{"loggedIn":true, "success":true, "data":' . $count . $mailMetaInfo . '}';
}


if ($request["type"] == "elternOhneSchueler") {
    $abfrage = "SELECT e.*
        FROM `eltern` e
        LEFT JOIN schueler s ON ( e.FamilienId = s.FamilienId )
        WHERE isnull( s.schuelerId ) ";
    $result = mysqli_query($dbc, $abfrage);
    if (!$result) {
        lmf_queryTrace($abfrage, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($result)) {
            $resultArray[] = $r;
        }
        echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
    }
}

if ($request["type"] == "elternOhneAnmeldungen") {
    $abfrage = "select g.* from (
        SELECT e.*, sum(if(ifnull(a.bezahlt,-2)<0, 0 , 1)) as anzahlAnmeldungen 
            FROM (eltern e
            JOIN schueler s ON e.FamilienId=s.FamilienId)
            LEFT JOIN anmeldung a ON s.SchuelerId=a.SchuelerId
                Group BY e.FamilienId) g
        WHERE g.anzahlAnmeldungen=0
        ORDER by g.registriertSeit desc;";
    $result = mysqli_query($dbc, $abfrage);
    if (!$result) {
        lmf_queryTrace($abfrage, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($result)) {
            $resultArray[] = $r;
        }
        echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
    }
}

if ($request["type"] == "elternLoeschen") {
    $abfrage = "DELETE FROM `eltern` WHERE FamilienId=" . $request["FamilienId"];
    $result = mysqli_query($dbc, $abfrage);
    if (!$result) {
        lmf_queryTrace($abfrage, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($abfrage, true, $dbc);
        echo '{"loggedIn":true, "success":true}';
    }
}


if ($request["type"] == "zusammenfuehrenVorbereiten") {
    $s1 = $request["schuelerId1"];
    $s2 = $request["schuelerId2"];
    // zuerst Eltern und Schuelerdaten
    $abfrage = "SELECT e.*, s.SchuelerId, s.Vorname as sVorname, s.Nachname as sNachname, s.Anmerkungen
            FROM eltern e, schueler s
            WHERE (s.SchuelerId=" . $s1 . " OR s.SchuelerId=" . $s2 . ")
            AND s.FamilienId=e.FamilienId 
            ORDER by  s.SchuelerId";
    $result = mysqli_query($dbc, $abfrage);
    if (!$result) {
        lmf_queryTrace($abfrage, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($result)) {
            $resultArray[] = $r;
        }
        $jsonSchueler = '"schueler":' . json_encode($resultArray);

        // jetzt die Anmeldedaten
        $abfrage = "SELECT a.anmeldungsId, a.SchuelerId, a.bezahlt, a.Schuljahr, k.* FROM anmeldung a, klasse k
            WHERE (a.SchuelerId=" . $s1 . " OR a.SchuelerId=" . $s2 . ")
            AND k.KlassenId=a.KlassenId 
            ORDER by a.Schuljahr, a.SchuelerId";
        $result = mysqli_query($dbc, $abfrage);
        if (!$result) {
            lmf_queryTrace($abfrage, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        } else {
            $resultArray = array();
            while ($r = mysqli_fetch_assoc($result)) {
                $resultArray[] = $r;
            }
            $jsonAnmeldungen = '"anmeldungen":' . json_encode($resultArray);

            // jetzt noch die Buecher
            $abfrage = "SELECT b.*, a.SchuelerId, a.eingesammelt FROM ausgeliehen a, buecher b
            WHERE (a.SchuelerId=" . $s1 . " OR a.SchuelerId=" . $s2 . ")
            AND a.buchId=b.BuchId
            ORDER by a.SchuelerId";
            $result = mysqli_query($dbc, $abfrage);
            if (!$result) {
                lmf_queryTrace($abfrage, false, $dbc);
                echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
            } else {
                $resultArray = array();
                while ($r = mysqli_fetch_assoc($result)) {
                    $resultArray[] = $r;
                }
                $jsonBuecher = '"buecher":' . json_encode($resultArray);
                echo '{"loggedIn":true, "success":true,"data":{' . $jsonSchueler . ',' . $jsonAnmeldungen . ',' . $jsonBuecher . '}}';
            }
        }
    }
}

if ($request["type"] == "zusammenfuehrenAusfuehren") {
    mysqli_autocommit($dbc, FALSE);
    $complete = $request["nurBuecher"] ? false : true;
    if ($complete && $request["elternZiel"] != $request["schuelerZiel"]) {
        // schueler wechselt erziehungsberechtigten
        $abfrage = "Update schueler s1 Set FamilienId 
            = ( SELECT id FROM   (
                    SELECT MAX(FamilienId) as id 
                    FROM schueler s2 
                    WHERE s2.SchuelerId=" . $request["elternZiel"] .
                " ) AS temp )
                WHERE s1.SchuelerId=" . $request["schuelerZiel"] . ";";
        $result = mysqli_query($dbc, $abfrage);
        lmf_trace('zusammenfuehrenAusfuehren 1: ' . $abfrage);
        lmf_trace('mysqli_affected_rows 1: ' . mysqli_affected_rows($dbc));
        if (!$result) {
            lmf_trace('mysqli_rollback: 2');
            mysqli_rollback($dbc);
            lmf_trace('mysqli_rollback: 2');
            lmf_queryTrace($abfrage, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
            mysqli_close($dbc); //Close the DB Connection;
            exit;
        } else {
            lmf_queryTrace($abfrage, true, $dbc);
        }
    }
    // anmeldungen
    if ($complete) {
        $betroffeneJahre = (int) $request["jahre"];
        for ($i = 1; $i < $betroffeneJahre; $i++) {
            $jahr = $request["jahr_" . $i];
            $idZumLoeschen = ($request["id1"] == $request["auswahl_" . $i]) ? $request["id2"] : $request["id1"];
            $abfrage = "DELETE FROM anmeldung WHERE Schuljahr= " . $jahr . " AND SchuelerId=" . $idZumLoeschen . ";";
            $result = mysqli_query($dbc, $abfrage);
            lmf_trace('zusammenfuehrenAusfuehren 2-' . $i . ': ' . $abfrage);
            lmf_trace('mysqli_affected_rows 2-' . $i . ': ' . mysqli_affected_rows($dbc));
            if (!$result) {
                mysqli_rollback($dbc);
                lmf_trace('mysqli_rollback: 3');
                lmf_queryTrace($abfrage, false, $dbc);
                echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
                mysqli_close($dbc); //Close the DB Connection;
                exit;
            } else {
                lmf_queryTrace($abfrage, true, $dbc);
            }
            // die Anmeldung die ausgewaehlt wurde dem ausgewaehltem Schueler zuordnen
            $abfrage = "UPDATE anmeldung SET SchuelerId = " . $request["schuelerZiel"] .
                    " WHERE SchuelerId=" . $request["auswahl_" . $i] .
                    " AND Schuljahr= " . $jahr . ";";
            $result = mysqli_query($dbc, $abfrage);
            lmf_trace('zusammenfuehrenAusfuehren 3: ' . $abfrage);
            lmf_trace('mysqli_affected_rows 3: ' . mysqli_affected_rows($dbc));
            if (!$result) {
                mysqli_rollback();
                lmf_trace('mysqli_rollback: 4');
                lmf_queryTrace($abfrage, false, $dbc);
                echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
                mysqli_close($dbc); //Close the DB Connection;
                exit;
            } else {
                lmf_queryTrace($abfrage, true, $dbc);
            }
        }
    }
    // alle buecher an den Zielschueler
    $idZumLoeschen = ($request["id1"] == $request["schuelerZiel"]) ? $request["id2"] : $request["id1"];
    $abfrage = "UPDATE ausgeliehen SET schuelerId = " . $request["schuelerZiel"] .
            " WHERE schuelerId=" . $idZumLoeschen . ";";
    $result = mysqli_query($dbc, $abfrage);
    lmf_trace('zusammenfuehrenAusfuehren 4: ' . $abfrage);
    lmf_trace('mysqli_affected_rows 4: ' . mysqli_affected_rows($dbc));
    if (!$result) {
        mysqli_rollback($dbc);
        lmf_trace('mysqli_rollback: 5');
        lmf_queryTrace($abfrage, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        mysqli_close($dbc); //Close the DB Connection;
        exit;
    } else {
        lmf_queryTrace($abfrage, true, $dbc);
    }
    // Schueler löschen
    if ($complete) {
        $idZumLoeschen = ($request["id1"] == $request["schuelerZiel"]) ? $request["id2"] : $request["id1"];
        $abfrage = "DELETE FROM schueler WHERE schuelerId=" . $idZumLoeschen . ";";
        $result = mysqli_query($dbc, $abfrage);
        lmf_trace('zusammenfuehrenAusfuehren 5: ' . $abfrage);
        lmf_trace('mysqli_affected_rows 5: ' . mysqli_affected_rows($dbc));
        if (!$result) {
            mysqli_rollback($dbc);
            lmf_trace('mysqli_rollback: 6');
            lmf_queryTrace($abfrage, false, $dbc);
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
            mysqli_close($dbc); //Close the DB Connection;
            exit;
        } else {
            lmf_queryTrace($abfrage, true, $dbc);
        }
    }
    if (!mysqli_commit($dbc)) {
        lmf_trace('mysqli_commit fails');
        lmf_queryTrace("CommitError: " . $abfrage, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        mysqli_close($dbc); //Close the DB Connection;
        exit;
    } else {
        lmf_trace('mysqli_commit success');
        echo '{"loggedIn":true, "success":true}';
    }
}

function sucheEltervertreter($dbc) {
    $abfrage = "SELECT e.*, s.schuelerid, stufe, subklasse FROM 
                eltern e join schueler s on (e.familienid=s.familienid)
                left join `view_schueler_dieses_jahr` `vsj` ON (`s`.`schuelerId` = `vsj`.`schuelerId`)
                where s.istElternvertreter=1
                order by stufe, subklasse";
    $ergebnis = mysqli_query($dbc, $abfrage);
    if (!$ergebnis) {
        lmf_queryTrace($abfrage, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        $resultArray = array();
        while ($r = mysqli_fetch_assoc($ergebnis)) {
            $resultArray[] = $r;
        }
        echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
    }
}

if ($request["type"] == "elternVertreter") {
    sucheEltervertreter($dbc);
}
if ($request["type"] == "elternVertreterEntfernen") {
    $abfrage = "Update `schueler` SET istElternvertreter=0 WHERE SchuelerId=" . $request["SchuelerId"];
    $result = mysqli_query($dbc, $abfrage);
    if (!$result) {
        lmf_queryTrace($abfrage, false, $dbc);
        echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
    } else {
        lmf_queryTrace($abfrage, true, $dbc);
        sucheEltervertreter($dbc);
    }
}


mysqli_close($dbc); //Close the DB Connection;
exit;
?>