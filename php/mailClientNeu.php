<?php
// summary
//		Test file to handle image uploads (remove the image size check to upload non-images)
//		NOTE: This is obviously a PHP file, and thus you need PHP running for this to work
//		NOTE: Directories must have write permissions
//
require_once ('./lmf-session.php');
require_once("JSON.php");
require_once ('database_connection.php');
include ('constants.php');
require_once ('mailToDb.php');
require_once('lmf-sendMail.php');
$postdata = array();
$htmldata = array();
$ret = array();

if (!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['FamilienId'])) {
    $ret["loggedIn"] = false;
    $ret["success"] = false;
    $outHtml = '<textarea style="width:600px; height:150px;">logout</textarea>';
    echo $outHtml;
    exit;
} else {
    $ret["loggedIn"] = true;
}
lmf_trace("mailClientNeu start");
lmf_trace("---------------------------------------------------------");

// $data = "";
lmf_trace("POSTDATA: " . count($_FILES) . " FILES");
foreach ($_POST as $nm => $val) {
    $postdata[$nm] = $val;   // array for html
}
foreach ($postdata as $key => $val) {
    lmf_trace($key . "=" . $val);
}
foreach ($_FILES as $nm => $val) {
    lmf_trace("   file: " . $nm . "=" . $val);
}

if (isset($_FILES['uploadedfile0'])) {
    lmf_trace("HTML multiple POST:");
    lmf_trace($postdata, true);

    $htmldata = array();
    lmf_trace("HTML multiple POST done:");
    foreach ($htmldata as $key => $value) {
        lmf_trace($value, true);
    }
}

$json = new Services_JSON();

$mailCount = copyMailToDB($dbc, $postdata, $_FILES);
if ($mailCount > 0) {
    $ret["count"] = $mailCount;
    $ret["success"] = true;
} else {
    $ret["count"] = 0;
    $ret["success"] = false;
}
$ret["MAILS_PER_CALL"] = MAILS_PER_CALL;
$ret["CALL_INTERVAL_ON_CRON"] = CALL_INTERVAL_ON_CRON;
$ret["MAIL_TRIGGER_EXTERN"] = MAIL_TRIGGER_EXTERN;
$data = $json->encode($ret);
lmf_trace("Json Data Returned:");
lmf_trace($data);

function copyMailToDB($dbc, $_postdata, $attachments) {
    $jobId = createMailJob($dbc, "Rundmail EIS/LMF Admins ." . $_postdata["empfaenger"] . " / " . $_postdata["lmfMailBetreffInput"]);
    lmf_trace("jobId: " . $jobId);

    if ($jobId > 0) {
        lmf_trace("jobId>0");
        anlagenToDb($dbc, $jobId, $attachments);
        $cnt = 0;
        $jahr = $_postdata["lmfAuswahlJahr"];
        $mailToGruppe = $_postdata["empfaenger"];
        $mailToKlasse = $_postdata["lmfSelectKlasse"];
        $lmfKlasseText = $_postdata["lmfKlasseHidden"];
        $mailToKlassenStufe = $_postdata["lmfSelectKlassenstufe"];
        $mailText = $_postdata["lmfMailTextHidden"];
        $mailBetreff = $_postdata["lmfMailBetreffInput"];
        $filter = ($_postdata["filter"]);
        $nurEV = $filter == "nur_EV";
        $nur_LMF = $filter == "nur_LMF";
        $nur_Filter = $filter == "nur_Filter";
        $alle = $filter == "alle";

        $anrede = " Liebe Familie ";
        lmf_trace($mailToGruppe);
        $view = ($jahr == 0) ? "view_schueler_dieses_jahr s" : "view_schueler_naechstes_jahr s";

        $auswahl1 = (($mailToGruppe === "klasse") ? " AND s.KlassenId = " . $mailToKlasse :
                        (($mailToGruppe === "stufe") ? " AND s.Stufe = " . $mailToKlassenStufe : ""))
                . (($nurEV) ? " AND s.istElternvertreter = 1" : "")
                . (($nur_LMF) ? " AND s.bezahlt = 1" : "")
                . (($nur_Filter) ? " AND e.Freigabe_info = 1" : "")
                . (($alle) ? " AND e.stopMail != 1" : "");

        $auswahl2 = (($mailToGruppe === "klasse") ? " in Klasse " . $lmfKlasseText :
                        (($mailToGruppe === "stufe") ? " in Klassestufe " . $mailToKlassenStufe : ""))
                . (($nur_LMF) ? " im Lernmittelfonds (LMF) angemeldet ist" : "")
                . (($nurEV) ? " im ElternInfoSystem (EIS) registriert sind" : "")
                . (($nur_Filter) ? " im ElternInfoSystem (EIS) für allgemeine Informationen registriert ist" : "")
                . (($nur_Filter) ? " im ElternInfoSystem (EIS) eingetragen ist und dringende Informationen für alle vorliegen." : "");

        $prolog = " !<br><br> Sie erhalten diese Mail, weil Ihr Kind/eines Ihrer Kinder "
                . (($nurEV) ? " und Sie als Elternvertreter" : "")
                . (($jahr == 0) ? "in diesem Schuljahr " : "im kommenden Schuljahr")
                . $auswahl2 . "<br>";
        $query_familie = "SELECT DISTINCT e.*"
                . " FROM eltern e, " . $view
                . " WHERE s.FamilienId=e.FamilienId AND s.bezahlt<2 AND e.stopMail!=1 " . $auswahl1 . ";";
        $result_families = mysqli_query($dbc, $query_familie);
        lmf_trace("mailQuery: " . $query_familie);
        lmf_trace("Anzahl Ergebnisse: " . mysqli_num_rows($result_families));
        $_SESSION['MailsInsgesamt'] = mysqli_num_rows($result_families);
        $mailEmpfaengerAlle = "";
        $count = 0;

        while ($row = mysqli_fetch_array($result_families)) {
            $count++;
            // Angeben des Absenders und der Absenderemailadresse
            $mail = new sendmail();
            // Angeben des Absenders und der Absenderemailadresse
            if ($nur_LMF) {
                $mail->from("Lernmittelfonds", EMAIL_LMF);
            } else {
                $mail->from("ElternInfoSystem", EMAIL);
            }
            // Angeben der Empfängeremailadresse
            $mail->to($row['Email']);
            $mailEmpfaenger .= $row['Email'] . "; ";
            // Angeben des Betreff´s
            //$mail->subject("=?utf-8?b?" . base64_encode($mailBetreff) . "?=");
            $mail->subject($mailBetreff);

            $mail->text($anrede . $row['Nachname'] . $prolog . $mailText);
            mailToDb($dbc, $jobId, $mail);
        }
        return $count;
    } else {
        lmf_trace("jobId<=0");
        return -1;
    }
}
?>

<textarea style="width:600px; height:150px;"><?php print "$data"; ?></textarea>



