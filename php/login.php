<?php
header('P3P: CP="CAO PSA OUR"');
header('Cache-Control: no-cache');
header('Pragma: no-cache');
?>
<?php
require_once ('./lmf-session.php');
include ('database_connection.php');
require_once ('./lmf-logging.php');
require_once ('./mail.php');
if (!isset($_SESSION)) {
    lmf_trace("Session neu starten");
    session_start();
}
//lmf_trace("Pfad: " . realpath(dirname(__FILE__)));
$error = array(); //this array will store all error messages
$log = array(); //this array will store all log messages
$loginOk = false;
$initial = false;

if (isset($_POST['formsubmitted'])) {
    // Initialize a session:
    // Zuerst "neuer" Aktivierungscodedie
    if (isset($_POST['initCodeNeu']) && $_POST['initCodeNeu'] == "TRUE") {
        lmf_trace("sendMailRegister: " . $_SESSION["Email"] . "; " . $_SESSION["FamilienId_init"] . "; " . $_SESSION["Activation"]);
        $ret = sendMailRegister($_SESSION["Email"], $_SESSION["FamilienId_init"], $_SESSION["Activation"]);
        lmf_trace("Mail an register mail:  with success???: " . $ret);
        if (!ret) {
            echo ('{"errors":  ["Fehler beim Versenden der Aktivierungs Mail. Bitte versuchen sie es noch einmal oder wenden sich an unsere Mitarbeiter."]}');
        }else{
             echo ('{"message":  ["Ein neuer Aktivierungscode wurde per Mail verschickt!"],"errors":  []}');
        }
         exit;
         // dann  Aktivierungs-Variante
    } elseif (isset($_POST['aktivierungsCode'])) {
        lmf_trace('start aktivierung with #'.$_POST['aktivierungsCode'].'#');
        $key = $_POST['aktivierungsCode'];
        $id = $_SESSION["FamilienId_init"];
        lmf_trace('aktivierungsCode: ' . $_POST['aktivierungsCode']);
        lmf_trace($_POST);
        lmf_trace($_SESSION);
        $query_activate_account = "UPDATE eltern SET Activation=NULL WHERE FamilienId =$id AND Activation='$key' LIMIT 1";
        $result = mysqli_query($dbc, $query_activate_account);
        lmf_queryTrace($query_activate_account, $result, $dbc);
        $query_check_account = "Select FamilienId from  eltern WHERE Activation is NULL AND FamilienId =$id LIMIT 1";
        mysqli_query($dbc, $query_check_account);
        // Print a customized message:
        if (mysqli_affected_rows($dbc) == 1) {//if update query was successfull
            echo ('{"message":  ["Aktivierung erfolgreich!"],"errors":  []}');
            $_SESSION["FamilienId"] = $_SESSION["FamilienId_init"];
            $_SESSION["initial"] = false;
            lmf_queryTrace($query_check_account, true, $dbc);
            mysqli_close($dbc); //Close the DB Connection;
            exit;
        } else {
            echo ('{"errors":  ["Fehler bei der Aktivierung! Aktivierungscode neu anfordern?"]}');
            lmf_queryTrace($query_check_account, false, $dbc);
            mysqli_close($dbc); //Close the DB Connection;
            exit;
        }
    // dann die PW vergessen Variante
    } elseif ($_POST['PW-vergessen'] == "TRUE") {
        if (empty($_POST['e-mail'])) {//if the email supplied is empty
            echo ('{"errors":  ["Sie haben vergessen Ihre email-Adresse anzugeben!"]}');
            exit;
        }
        $Email = trim (strtolower($_POST['e-mail']));
        if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $Email)) {
            // check Mailadresse
            $query_verify_email = "SELECT * FROM eltern  WHERE LOWER(Email) ='$Email'";
            $result_verify_email = mysqli_query($dbc, $query_verify_email);
            if (!$result_verify_email) {//if the Query Failed ,similar to if($result_verify_email==false)
                echo ('{"errors":  ["Fehler bei der Datenbankabfrage!"]}');
                lmf_queryTrace($query_verify_email, false, $dbc);
                mysqli_close($dbc); //Close the DB Connection;
                exit;
            }

            if (mysqli_num_rows($result_verify_email) == 1) { // Adresse bekannt
                $row = mysqli_fetch_array($result_verify_email);
                // PW generieren
                $pool = "qwertzupasdfghkyxcvbnm23456789WERTZUPLKJHGFDSAYXCVBNM";
                srand((double) microtime() * 1000000);
                for ($index = 0; $index < 5; $index++) {
                    $Passwort .= substr($pool, (rand() % (strlen($pool))), 1);
                }
                // PW aendern
                $query_PW_neu = "Update `eltern`"
                        . " SET `Passwort`='" . md5($Passwort) . "'"
                        . " WHERE LOWER(`Email`)='" . $Email . "';";
                $result_PW_neu = mysqli_query($dbc, $query_PW_neu);
                if (!$result_PW_neu) {
                    lmf_queryTrace($query_PW_neu, false, $dbc);
                    echo ('{"errors": ["Fehler beim Ändern des Passworts!"]}');
                } else {
                    lmf_queryTrace($query_PW_neu, true, $dbc);
                    if (sendMailNeuesPasswort($Passwort, $Email, $row['Nachname'])) {
                        echo '{"PW_changed":true, "Mail":true}';
                    } else {
                        echo '{"PW_changed":true, "Mail":false}';
                    }
                }
                mysqli_close($dbc); //Close the DB Connection;
                exit;
            } else {
                lmf_trace("neues Passwort für ungültige Adresse angefordert: " . $_POST['e-mail']);
                echo ('{"PW_changed":"TRUE", "Mail":true}'); // fake um ungültige mail zu verschleiern!
                exit;
            }
        } else {
            lmf_trace('Die EMail-Adresse "' . $_POST['e-mail'] . '" ist ungültig');
            $error[] = 'Ihre EMail-Adresse "' . $_POST['e-mail'] . '" ist ungültig';
            exit;
        }
        echo ('{"logs":  "state 2"}');
        exit;
    }
    if (empty($_POST['e-mail'])) {//if the email supplied is empty
        $error[] = 'Sie haben vergessen Ihre email-Adresse anzugeben ';
    } else {
        $Email = trim (strtolower($_POST['e-mail']));
        if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $Email)) {
            
        } else {
            lmf_trace('Die EMail-Adresse "' . $_POST['e-mail'] . '" ist ungültig');
            $error[] = 'Ihre EMail-Adresse "' . $_POST['e-mail'] . '" ist ungültig';
        }
    }


    if (empty($_POST['Passwort'])) {
        $error[] = 'Bitte geben sie Ihr Passwort ein ';
    } else {
        $Passwort = $_POST['Passwort'];
    }

    array_push($log, md5($Passwort));
    if (empty($error)) {//if the array is empty , it means no error found
        $query_check_credentials = "SELECT * FROM eltern WHERE (LOWER(Email)='$Email' AND Passwort='" . md5($Passwort) . "')";
        array_push($log, $query_check_credentials);


        $result_check_credentials = mysqli_query($dbc, $query_check_credentials);
        if (!$result_check_credentials) {//If the QUery Failed
            lmf_queryTrace($query_check_credentials, false, $dbc);
            array_push($log, 'Query Failed ');
        }
        if (mysqli_num_rows($result_check_credentials) == 1) {
            $_SESSION = mysqli_fetch_array($result_check_credentials, MYSQLI_ASSOC);
            if ($_SESSION['Activation'] == null) {
                $_SESSION["initial"] = false;
                lmf_trace('login ' . $_SESSION['FamilienId']);
                $query_istEV = "SELECT count(*) as istEV FROM schueler where FamilienId= " . $_SESSION['FamilienId'] . " AND schueler.istElternvertreter>0 ";
                $result_istEV = mysqli_query($dbc, $query_istEV);
                if ($row = mysqli_fetch_assoc($result_istEV)) {
                    if ($row["istEV"] > 0) {
                        $_SESSION["istEV"] = true;
                    } else {
                        $_SESSION["istEV"] = false;
                    }
                } else {
                    $_SESSION["istEV"] = false;
                }
                $loginOk = true;
            } else {
                // initial !!! initialen Code abfragen
                $initial = true;
                $_SESSION["initial"] = true;
                $_SESSION["FamilienId_init"] = $_SESSION["FamilienId"];
                $_SESSION["FamilienId"] = "";
                $error[] = 'Ihr Zugang ist noch nicht aktiviert. Sie haben einen Aktivierungscode per mail erhalten. bitte geben sie diesen ein.';
                lmf_trace('FamilienId_init: ' . $_SESSION['FamilienId_init']);
            }
            $query_constants = "SELECT * FROM constants ";
            $result_constants = mysqli_query($dbc, $query_constants);
            if (isset($_SESSION['anmelden_str'])){
                lmf_trace($_SESSION['anmelden_str']);
            }
            while ($r = mysqli_fetch_assoc($result_constants)) {
                $_SESSION[$r["key"] . "_int"] = $r["intValue"];
                $_SESSION[$r["key"] . "_str"] = $r["strValue"];
            }
            //lmf_trace($_SESSION['thisYear_str']);
        } else {
            lmf_queryTrace($query_check_credentials, false, $dbc);
            lmf_trace('Loginfehler: -' . $_POST['e-mail']."###".$_POST['Passwort'])."-";
            $error[] = 'Entweder Ihr Zugang ist noch nicht aktiviert oder Email-Adresse bzw. Password sind nicht korrekt. Eventuell haben Sie sich auch noch nicht registriert.';
        }
    }

    /// var_dump($error);
    mysqli_close($dbc);
    // End of the main Submit conditional.
} else {
    // nur ein login check
    $loginOk = (isset($_SESSION['FamilienId']));
}
?>


{
"errors":  <?php echo json_encode($error); ?>,
"logs": <?php echo json_encode($log); ?>,
"loginOk":  <?php echo json_encode($loginOk); ?>,
"zuzahlungsbefreit":  0

<?php
if ($loginOk && $_SESSION['lmfAnmeldungenFreigeben_str']) {
    echo ', anmeldenFreigegeben: ' . $_SESSION['lmfAnmeldungenFreigeben_int'];
}
if ($loginOk && $_SESSION['lmfAnmeldungenStoppen_str']) {
    echo ', anmeldenGestoppt: ' . $_SESSION['lmfAnmeldungenStoppen_int'];
}
if ($loginOk && $_SESSION['lmfNeuAnmeldungenStoppen_str']) {
    echo ', neuAnmeldenGestoppt: ' . $_SESSION['lmfNeuAnmeldungenStoppen_int'];
}
if (isset( $_SESSION['initial']) && $_SESSION['initial']) {
    echo ', neueAnmeldung:true';
}
if (isset($_SESSION['startJahr_int']) && $_SESSION['startJahr_int'] > $_SESSION['thisYear_int']) {
    echo ', initial: true ';
} else {
    echo ', initial: false ';
}
if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin']) {
    echo ', isAdmin:true';
}
if (isset($_SESSION['istEV']) && $_SESSION['istEV']) {
    echo ', istEV:true';
} else {
    echo ', istEV:false';
}
if (isset($_SESSION['nurLmf_int']) && $_SESSION['nurLmf_int']) {
    echo ', nurLmf:true';
} else {
    echo ', nurLmf:false';
}
?>

}