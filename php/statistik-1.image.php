<?php

/* CAT:Stacked chart */

/* pChart library inclusions */
include("../pChart/class/pData.class.php");
include("../pChart/class/pDraw.class.php");
include("../pChart/class/pImage.class.php");
require_once ('./lmf-session.php');
require_once ('database_connection.php');
require_once ('constants.php');
if (!isset($_SESSION)) {
    session_start();
}
$abfrage = "SELECT `jahr` , sum(`teilnehmerBezahlt`) as teilnehmerBezahlt , sum(`teilnehmerZahlungsBefreit`) as teilnehmerZahlungsBefreit
            FROM `buecherlisten`
            GROUP BY `jahr`
            HAVING sum( `teilnehmerBezahlt` ) >0";
$ergebnis = mysqli_query($dbc, $abfrage);
if (!$ergebnis) {
    lmf_queryTrace($abfrage, false, $dbc);
    echo '';
} else {
    $jahr = array();
    $teilnehmerBezahlt = array();
    $teilnehmerZahlungsBefreit = array();

    while ($r = mysqli_fetch_assoc($ergebnis)) {
        $jahr[] = $r['jahr'];
        $teilnehmerBezahlt[] = $r['teilnehmerBezahlt'];
        $teilnehmerZahlungsBefreit[] = $r['teilnehmerZahlungsBefreit'];
    }
    /* Create and populate the pData object */
    $MyData = new pData();
    $MyData->addPoints($teilnehmerZahlungsBefreit, "lernmittelbefreit");
    $MyData->addPoints($teilnehmerBezahlt, "Teilnehmer");
    $MyData->setAxisName(0, "Teilnehmer");
    $MyData->addPoints($jahr, "Labels");
    $MyData->setSerieDescription("Labels", "Jahr");
    $MyData->setAbscissa("Labels");


    /* Create the pChart object */
    $myPicture = new pImage(1000, 380, $MyData);
    $myPicture->drawGradientArea(0, 0, 1000, 380, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
    $myPicture->drawGradientArea(0, 0, 1000, 380, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));

    /* Set the default font properties */
    $myPicture->setFontProperties(array("FontName" => "../fonts/verdana.ttf", "FontSize" => 10));

    /* Draw the scale and the chart */
    $myPicture->setGraphArea(60, 60, 950, 340);
    $myPicture->drawScale(array("DrawSubTicks" => TRUE, "Mode" => SCALE_MODE_ADDALL_START0));
    $myPicture->setShadow(FALSE);
    $myPicture->drawStackedBarChart(array("Surrounding" => -15, "InnerSurrounding" => 15));

    /* Write a label */
    for ($i = 0; $i < count($teilnehmerZahlungsBefreit); $i++) {
        $myPicture->writeLabel(array("lernmittelbefreit", "Teilnehmer"), $i, array("DrawVerticalLine" => False));
    }
    /* Write the chart legend */
    $myPicture->drawLegend(680, 360, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL));

    /* Render the picture (choose the best way) */
    $myPicture->autoOutput("pictures/example.drawStackedBarChart.shaded.png");
}
mysqli_close($dbc); //Close the DB Connection;
exit;
?>