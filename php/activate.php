<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Aktivierung Zugang Lernmittelfonds</title>
    
    
<style type="text/css">
body {
	font-family:"Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif;
	font-size:12px;
}



 .success {
	border: 1px solid;
	margin: 0 auto;
	padding:10px 5px 10px 60px;
	background-repeat: no-repeat;
	background-position: 10px center;
    
     width:450px;
     color: #4F8A10;
	background-color: #DFF2BF;
	background-image:url('images/success.png');
     
}



 .errormsgbox {
	border: 1px solid;
	margin: 0 auto;
	padding:10px 5px 10px 60px;
	background-repeat: no-repeat;
	background-position: 10px center;

     width:450px;
    	color: #D8000C;
	background-color: #FFBABA;
	background-image: url('images/error.png');
     
}

</style>

</head>
<body><?php
include ('./constants.php');
include ('database_connection.php');

if (isset($_GET['i']))
{
    $id = $_GET['i'];
}
if (isset($_GET['k']) && (strlen($_GET['k']) == 32))//The Activation key will always be 32 since it is MD5 Hash
{
    $key = $_GET['k'];
}


if (isset($id) && isset($key))
{
    // Update the database to set the "activation" field to null
    $query_activate_account = "UPDATE eltern SET Activation=NULL WHERE FamilienId =$id AND Activation='$key' LIMIT 1";
    $result=mysqli_query($dbc, $query_activate_account);
    lmf_queryTrace($query_activate_account, $result, $dbc);
    $query_check_account = "Select FamilienId from  eltern WHERE Activation is NULL AND FamilienId =$id LIMIT 1";
    mysqli_query($dbc, $query_check_account) ;
    // Print a customized message:
    if (mysqli_affected_rows($dbc) == 1)//if update query was successfull
    {
    echo '<div class="success">Ihr Zugang ist jetzt aktiv. Sie können sich jetzt <a href="' . LMF_URL . '">anmelden</a><br><br>';
    echo 'Nach der erneuten Anmeldung können Sie über die Schaltfläche "neues Kind" einen Datensatz für Ihr Kind anlegen. Korrigieren Sie den Vornamen und '
        .'falls notwendig den Namen, ändern Sie die Klasse entsprechend der Klasse die Ihr Kind im nächsten Jahr besuchen wird.<br>
            Wenn Sie sich am Lernmittelfonds anmelden wollen, wechseln Sie anschliessend über den Link links oben zum Lernmittelfonds und klicken Sie am Ende der Zeile mit dem Namen Ihres Kindes auf `anmelden`.<br><br>'
        .'Im Anschluss werden Sie eine weitere Mail mit den Kontodaten für die Überweisung erhalten.</div>';
    } else
    {
        echo '<div class="errormsgbox">Oops !Ihr Zugang konnte nicht aktiviert werden. Bitte überprüfen Sie noch einmal Ihren Link oder kontaktieren einen Verantwortlichen.</div>';
    }
    mysqli_close($dbc);

} else {
        lmf_trace("Fehler bei der Aktivierung.");
        echo '<div class="errormsgbox">Fehler bei der Aktivierung. Bitte machen Sie einen zweiten Versuch oder wenden sich an die Verantwortlichen des Lernmittelfonds.</div>';
}

?>
</body>
</html>