<?php

require_once ('./lmf-session.php');
include ('database_connection.php');
require_once ('constants.php');
require_once ('./lmf-logging.php');
if(!isset($_SESSION)){ 
    session_start(); 
} 

$error = array(); //this array will store all error messages
$loggedIn = true; //is logged in???
$request = json_decode(file_get_contents('php://input'), true);

if (!isset($_SESSION['FamilienId'])) {
    $error[] = 'Loginerror';
    echo '{"loggedIn":false, "errors":["Nicht korrekt angemeldet"], "success":false}';
    exit;
}

print '<html slick-uniqueid="3" xmlns="http://www.w3.org/1999/xhtml" xml:lang="de-de" lang="de-de">
    <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <style type="text/css">
    .lmf_pb { page-break-after:always ; }
    @media print { 
    body, table { 
        font-size: 12pt; 
    }
}
   </style>
  </head>
<body>';

$abfrage_klassen = "select * from  `view_klassen_altes_jahr`";
$result_klassen = mysqli_query($dbc, $abfrage_klassen);
while ($klasse = mysqli_fetch_array($result_klassen)) {
    if ($_REQUEST["type"] == "alle") {
        print '<h2 class="lmf_listenTitel"> Lernmittelfonds ' . $klasse['Jahr'] . '/' . (1 + $klasse['Jahr']) . ' - Bücherbestand Klasse ' . $klasse['Stufe'] . $klasse['SubKlasse'] . '-' . $klasse['Sprache'] . '</h2>';
        $abfrage_buecher = "SELECT al.buchId, count(al.buchId) , b.*
        FROM anmeldung a,  ausgeliehen al, buecher b
        WHERE a.Schuljahr=" . $klasse['Jahr'] . "
        AND a.schuelerId=al.schuelerId
        AND a.klassenId=" . $klasse['KlassenId'] . " 
        AND b.buchId=al.buchId
        GROUP BY al.buchId;";
    } else {
        print '<h2 class="lmf_listenTitel"> Lernmittelfonds ' . $klasse['Jahr'] . '/' . (1 + $klasse['Jahr']) . ' - Buchrückgabe Klasse ' . $klasse['Stufe'] . $klasse['SubKlasse'] . '-' . $klasse['Sprache'] . '</h2>';
        $abfrage_buecher = "SELECT al.buchId, count(al.buchId) , b.*
        FROM anmeldung a,  ausgeliehen al, buecher b
        WHERE a.Schuljahr=" . $klasse['Jahr'] . "
        AND a.schuelerId=al.schuelerId
        AND a.klassenId=" . $klasse['KlassenId'] . " 
        AND b.buchId=al.buchId
        AND b.bis >0
        AND b.bis <= " . $klasse['Stufe'] . " 
        GROUP BY al.buchId;";
    }
    $result_buecher = mysqli_query($dbc, $abfrage_buecher);
    print '<div>Folgende Lernmittel werden eingesammelt:</div><ol>';
    $anzahlBuecher = 0;
    $selectAbfrage = "SELECT s.*";
    while ($buch = mysqli_fetch_array($result_buecher)) {
        $anzahlBuecher++;
        print '<li> ' . $buch['Isbn'] . ': ' . $buch['Titel'] . ' (' . $buch['Neupreis'] . ' €)</li>';
        $selectAbfrage .= ",\n sum(if(al.buchid=" . $buch['BuchId'] . ",1 , 0)) as buch_" . $anzahlBuecher;
    }
    $abfrage_schueler = $selectAbfrage . "
        FROM   anmeldung a,  ausgeliehen al, schueler s
        WHERE a.klassenId = " . $klasse['KlassenId'] . " 
        AND a.Schuljahr= " . $klasse['Jahr'] . "
        AND a.schuelerId=al.schuelerId
        AND a.schuelerid=s.schuelerid
        AND ifNull(al.eingesammelt,0) = 0
        GROUP BY s.schuelerid
        ORDER BY s.nachname, s.vorname";
    lmf_trace($abfrage_schueler);
    $result_schueler = mysqli_query($dbc, $abfrage_schueler);
    $lfdNr = 1;

    print '</ol><table border=1 cellspacing="0" cellpadding="3" style="width:100%"><thead><tr><th style="width:30px">Nr</th><th >Schüler</th>';
    for ($i = 1; $i <= $anzahlBuecher; $i++) {
        print '<th style="width:50px">Buch ' . $i . '</th>';
    }
    if ($_REQUEST["type"] == "alle") {
        echo '<th>Bemerkungen</th></tr></thead>';
    } else {
        echo '<th>Rückgabe</th></tr></thead>';
    }
    while ($schueler = mysqli_fetch_array($result_schueler)) {
        print '<tr><td>' . $lfdNr . '</td><td> ' . $schueler['Nachname'] . ', ' . $schueler['Vorname'] . '</td>';
        for ($i = 1; $i <= $anzahlBuecher; $i++) {
            $rueckgabe = (intval($schueler['buch_' . $i]) > 0) ? 'X' : '&nbsp';
            echo '<td align="center"> ' . $rueckgabe . '</td>';
        }
        echo '<td> &nbsp </td></tr>';
        $lfdNr++;
    }
    echo '</table>';
    print '<div class = "lmf_pb"></div>';
}
print '</body></html>';

mysqli_close($dbc); //Close the DB Connection;
exit;
?>
