<?php
require_once ('./lmf-logging.php');
error_reporting(E_ALL);
/*Define constant to connect to database */
DEFINE('DATABASE_USER', 'user');
DEFINE('DATABASE_PASSWORD', 'password');
DEFINE('DATABASE_HOST', 'meinHost');
DEFINE('DATABASE_NAME', 'meineDB');


ini_set("display_errors",0);


// Make the connection:

mysqli_query("SET NAMES 'utf8'");
$dbc = @mysqli_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_NAME, 3306);
	mysqli_set_charset($dbc, 'utf8');

// test und Konstanten holen
$abfrage_constants = " SELECT *  FROM constants c;";
$result_constants = mysqli_query($dbc, $abfrage_constants);
while ($row = mysqli_fetch_array($result_constants)) {
    if ($row['key'] == "thisYear"){
        DEFINE('NEXT_YEAR', $row['intValue']+1);
        DEFINE('THIS_YEAR', $row['intValue']);
    }
}

if (!$dbc) {
     lmf_trace("Can't connect to database. Errorcode: " . mysqli_connect_errno());
    trigger_error('Could not connect to MySQL: ' . mysqli_connect_error());
}

?>
