<?php

class Logger {

    var $logfile;
    var $boolTimestamp;

    function Logger($filename, $boolTimestamp) {
        $this->boolTimestamp = $boolTimestamp;
        $this->logfile = $filename;
    }

    function write($txt) {
        $dt = date("d.m.y G:i:s");
        if ($this->boolTimestamp) {
            $txt = "[" . $dt . "]: " . $txt;
        }
        $fh = fopen($this->logfile, "a");
        if (is_array($txt)) {
            //$txt = "::::::::".$txt;
            $ar = $txt;
            $txt = "Array:::::\n";
            foreach ($ar as $key => $value) {
                $txt += $key . "=" . $value . "\n";
            }
        }
        fwrite($fh, $txt . "\n");
        fclose($fh);
    }

    function clear() {
        $fh = fopen($this->logfile, "w");
        fwrite($fh, "");
        fclose($fh);
    }

    function newline() {
        $fh = fopen($this->logfile, "a");
        fwrite($fh, "\n\n");
        fclose($fh);
    }

    function printr($ar) {
        $txt = "";
        foreach ($ar as $nm => $val) {
            $txt .= "    " . $nm . " = " . $val . "\n";
        }
        $this->write($txt);
    }

}

function lmf_trace($txt, $isArray = false) {
    require_once ('./lmf-session.php');
    date_default_timezone_set('Europe/Berlin');
    
    if (!$isArray && strlen($txt) == 0) {
        return;
    }
    if (isset($_SESSION) && isset($_SESSION['FamilienId'])) {
        $id = $_SESSION['FamilienId'];
    } else {
        $id = '---';
    }
    $date = date("Y-m-d-");
    $log = new Logger(realpath(dirname(__FILE__)) . "/../logs/" . $date . "lmf-log.txt", true);
    if (is_array($txt)) {
        $log->write($id . ": " . json_encode($txt));
    } else if (is_object(($txt))) {
        $log->write($id . ": " . json_encode($txt));
    } else {
        $log->write($id . ": " . $txt);
    }
}

function lmf_queryTrace($query, $success, $dbc) {
    $txt = ($success ? "Success: " : "Error:  ") . $query;
    date_default_timezone_set('Europe/Berlin');
    $date = date("Y-m-d-");
    $log = new Logger(realpath(dirname(__FILE__)) . "/../logs/" . $date . "lmfQueryLog.txt", true);
    if (isset($_SESSION) && isset($_SESSION['FamilienId'])) {
        $id = $_SESSION['FamilienId'];
    } else {
        $id = '---';
    }

    $log->write($id . " : " . $txt);
    if (!$success) {
        $log->write(" ErrorCode : " . mysqli_error($dbc) . " / " . mysqli_errno($dbc));
    }
}

?>