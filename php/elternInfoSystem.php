<?php

require_once ('./lmf-session.php');
require_once ('database_connection.php');
require_once ('constants.php');
require_once ('mail.php');
require_once ('mailToDb.php');
if (!isset($_SESSION)) {
    session_start();
}

function checkAbsenderEV($dbc, $klassenId, $alsEV) {
    $abfrageAbsender = "SELECT s.*, e.Email, concat (e.Vorname, ' ',e.Nachname) as EV    
                FROM view_schueler_dieses_jahr s, eltern e
                WHERE s.FamilienId=" . $_SESSION['FamilienId'] . "
                AND  s.FamilienId= e.FamilienId
                AND  klassenId=" . $klassenId;
    if ($alsEV) {
        $abfrageAbsender.= " AND  istElternvertreter=1 ";
    } else {
        $abfrageAbsender.= " AND  EisStatus=1 ";
    }
    $result = mysqli_query($dbc, $abfrageAbsender);
    if ($result) {
//        lmf_queryTrace($abfrageAbsender, true, $dbc);
        return $result;
    } else {
        lmf_queryTrace($abfrageAbsender, false, $dbc);
        return $result;
    }
}

function meineKlasse($dbc, $klassenId) {
    $abfrageKlasse = "SELECT *
                FROM view_schueler_dieses_jahr
                WHERE FamilienId=" . $_SESSION['FamilienId'] . "
                AND  klassenId=" . $klassenId;
    $result = mysqli_query($dbc, $abfrageKlasse);
    if (!$result) {
        lmf_queryTrace($abfrageKlasse, false, $dbc);
        return null;
    } else {
        $klasse = mysqli_fetch_array($result);
        if (!$klasse) {
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
            lmf_queryTrace($abfrageKlasse, false, $dbc);
            return null;
        } else {
            return $klasse;
        }
    }
}

function doMail($dbc, $absender, $abfrageMailEmpfaenger, $_postdata, $attachments) {
    lmf_trace("doMail");
    lmf_trace($attachments);
    $mailText = $_postdata["eisMailTextHidden"];
    $mailBetreff = $_postdata["eisMailBetreffInput"];
    $mailAbsender = $absender["Email"];
    $anrede = " Liebe Familie ";
    $Klasse = empty($absender["MetaKey"]) ? " Klasse " . $absender["Stufe"] . $absender["SubKlasse"] : $absender["MetaKey"] . " - Klassen ";
    $prolog = " !<br> Sie erhalten diese Mail von einem Elternverteter der " . $Klasse . ", weil Sie sich im ElternInfoSystem (EIS) unserer Schule für Rundmails Ihrer Elternvertreter registriert haben.<br><br>";
    $mailEmpfaengerListe = $Klasse . ": ";
    $count = 0;
    $resultMailEmpfaenger = mysqli_query($dbc, $abfrageMailEmpfaenger);
    $jobId = createMailJob($dbc,"Rundmail EV: ".$mailBetreff);
    anlagenToDb($dbc, $jobId, $attachments);
    if ($jobId != null) {
        while ($row = mysqli_fetch_array($resultMailEmpfaenger)) {
            $count++;
            $mail = new sendmail();
            // Angeben des Absenders und der Absenderemailadresse
            $mail->from($absender["EV"], $mailAbsender);
            // Angeben der Empfängeremailadresse
            $mail->to($row['Email']);
            $mailEmpfaengerListe .= $row['Email'] . "; ";
            // Angeben des Betreff´s
            //$mail->subject("=?utf-8?b?" . base64_encode($mailBetreff) . "?=");
            $mail->subject($mailBetreff);

            $mail->text($anrede . $row['Nachname'] . $prolog . $mailText);
            mailToDb($dbc, $jobId, $mail);
        }
    }else{
        
    }
    lmf_trace("Mail an: " . $mailEmpfaengerListe);
    lmf_trace("Betreff:" . $mailBetreff);
    lmf_trace("Inhalt:" . $anrede . " ... " . $prolog . $mailText);
    $filenames = "";
    $cnt = 0;
    while (isset($attachments['uploadedfile' . $cnt])) {
        $filenames.=" " . $attachments['uploadedfile' . $cnt]['name'] . "; ";
        $cnt++;
    }
    lmf_trace("Files:" . $filenames);
    lmf_trace("end do mail:");
    return $count;
}

$error = array(); //this array will store all error messages
$loggedIn = true; //is logged in???
if (file_get_contents('php://input')!=null) {
    $request = json_decode(file_get_contents('php://input'), true);
    if (!isset($_SESSION['FamilienId'])) {
        $error[] = 'Loginerror';
        $loggedIn = false;
        echo '{"loggedIn":false, "errors":["Nicht korrekt angemeldet' . $_SESSION['FamilienId'] . '|' . $request['_FamilienId'] . '"], "success":false}';
        exit;
    }

    /* liefert die daten der Klasse mit der übergebenen Id, wenn die angemeldete Persone dort Elternvertreters ist */
    if ($request["type"] == "EvKlasse") {
        $resultAbsender = checkAbsenderEV($dbc, $request['klassenId'], true);
        if (!$resultAbsender) {
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        } else {
            $resultArray = array();
            while ($r = mysqli_fetch_assoc($resultAbsender)) {
                $resultArray[] = $r;
            }
            echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
        }
    }

    /* ändert EisStatus eines Schuelers  mit der übergebenen Id, wenn die angemeldete Person in der Klasse Elternvertreters ist */
    if ($request["type"] == "EisStatusAendern") {
        $absenderResult = checkAbsenderEV($dbc, $request['klassenId'], true);
        if (!$absenderResult) {
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false" }';
        } else {
            $klasse = mysqli_fetch_array($absenderResult);
            if ($klasse) {
                if ($klasse['MetaKey'] != null && strlen($klasse['MetaKey']) > 0) {
                    $klassenAbfrage = " And  s.MetaKey='" . $klasse['MetaKey'] . "' ";
                } else {
                    $klassenAbfrage = " And  s.Stufe=" . $klasse['Stufe'] . " AND s.SubKlasse='" . $klasse['SubKlasse'] . "' ";
                }
                $abfrageEisStatusAendern = "Update schueler Set eisStatus=" . $request['EisStatus'] .
                        " WHERE SchuelerId= " . $request['schuelerId'];
                lmf_trace("EisStatusAendern: " . $abfrageEisStatusAendern);
                $resultEisStatusAendern = mysqli_query($dbc, $abfrageEisStatusAendern);
                if (!$resultEisStatusAendern) {
                    lmf_queryTrace($abfrageEisStatusAendern, false, $dbc);
                    echo ('{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}');
                } else {
                    lmf_queryTrace($abfrageEisStatusAendern, true, $dbc);
                    echo ('{"loggedIn":true, "success":true}');
                }
            } else {
                echo '{"loggedIn":true, "errors":["Sie sind nicht berechtigt die Zugänge der Klasse zu verwalten."]}';
            }
        }
    }

    /* liefert die Daten der Familien der Klasse mit der übergebenen Id, wenn die angemeldete Person dort Elternvertreters ist */
    if ($request["type"] == "EvKlasseFamilien") {
        $absenderResult = checkAbsenderEV($dbc, $request['klassenId'], true);
        if (!$absenderResult) {
            echo '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false}';
        } else {
            $klasse = mysqli_fetch_array($absenderResult);
            if ($klasse) {
                if ($klasse['MetaKey'] != null && strlen($klasse['MetaKey']) > 0) {
                    $klassenAbfrage = " And  s.MetaKey='" . $klasse['MetaKey'] . "' ";
                } else {
                    $klassenAbfrage = " And  s.Stufe=" . $klasse['Stufe'] . " AND s.SubKlasse='" . $klasse['SubKlasse'] . "' ";
                }
                $abfrageElternBerechtigung = "SELECT s.SchuelerId, e.Email, e.Telefon, e.Freigabe_ev,
                     concat (e.Vorname, ' ',e.Nachname) as Eltern, 
                     concat (s.Vorname, ' ',s.Nachname) as Schueler, 
                     s.istElternvertreter, s.eisStatus,
                     concat(s.Stufe,s.SubKlasse,' ', s.Sprache) as Klasse
                    FROM view_schueler_dieses_jahr s, eltern e
                    WHERE s.FamilienId= e.FamilienId "
                        . " AND e.zuzahlungsBefreit != 1 "
                        //." AND s.eisStatus > 0 "
                        . $klassenAbfrage;
                lmf_trace("abfrage_klassen: " . $abfrageElternBerechtigung);
                $resultElternBerechtigung = mysqli_query($dbc, $abfrageElternBerechtigung);
                $resultArray = array();
                while ($r = mysqli_fetch_assoc($resultElternBerechtigung)) {
                    $resultArray[] = $r;
                }
                echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
            } else {
                echo '{"loggedIn":true, "errors":["Sie sind nicht berechtigt die Zugänge der Klasse zu verwalten."]}';
            }
        }
    }

    /* liefert die Daten der Elternvertreter der Klasse mit der übergebenen Id, wenn die angemeldete Person dort Kinder hat */
    if ($request["type"] == "listeEV") {
        $klasse = meineKlasse($dbc, $request['klassenId']);
        if (!$klasse) {
            lmf_trace("0");
            echo '{"loggedIn":true, "errors":["Datenbankfehler"],"success":false}';
        } else {
            lmf_trace("1");
            if ($klasse['MetaKey'] != null && strlen($klasse['MetaKey']) > 0) {
                lmf_trace("2");
                $klassenAbfrage = " And  s.MetaKey='" . $klasse['MetaKey'] . "' ";
            } else {
                lmf_trace("3");
                $klassenAbfrage = " And  s.Stufe=" . $klasse['Stufe'] . " AND s.SubKlasse='" . $klasse['SubKlasse'] . "' ";
            }
            lmf_trace("4");
            $abfrageEV = "SELECT s.SchuelerId,
                     concat (e.Vorname, ' ',e.Nachname) as Eltern, 
                     concat (s.Vorname, ' ',s.Nachname) as Schueler, 
                     s.istElternvertreter,
                     concat(s.Stufe,s.SubKlasse,' ', s.Sprache) as Klasse
                    FROM view_schueler_dieses_jahr s, eltern e
                    WHERE s.FamilienId= e.FamilienId "
                    . " AND istElternvertreter=1 "
                    . $klassenAbfrage;
            lmf_trace("abfrage_klassen: " . $abfrageEV);
            $resultEV = mysqli_query($dbc, $abfrageEV);
            if (!$resultEV) {
                lmf_queryTrace($abfrageEV, false, $dbc);
                echo '{"loggedIn":true, "errors":["Datenbankfehler2"], "success":false}';
            } else {
                $resultArray = array();
                while ($r = mysqli_fetch_assoc($resultEV)) {
                    $resultArray[] = $r;
                }
                echo '{"loggedIn":true, "success":true, "data":' . json_encode($resultArray) . '}';
            }
        }
    }
}

/* liefert die Kontaktliste der Familien der Klasse mit der übergebenen Id, wenn die angemeldete Person dort berechtigt ist */
if (isset ($_GET["type"]) && $_GET["type"] == "kontaktListeAnzeigen") {
    echo '<html slick-uniqueid="3" xmlns="http://www.w3.org/1999/xhtml" xml:lang="de-de" lang="de-de">
        <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <style type="text/css">
          .lmf_pb { page-break-after:always ; }
          @media print { 
          body, table { 
              font-size: 12pt; 
          }
      }
         </style>
        </head>
      <body>';
    $absenderResult = checkAbsenderEV($dbc, $_GET['klassenId'], false);
    if (!$absenderResult) {
        echo 'Datenbankfehler: Bitte versuchen Sie es später noch einmal oder wenden sich an einen Administrator.';
    } else {
        $klasse = mysqli_fetch_array($absenderResult);
        if ($klasse) {
            if ($klasse['MetaKey'] != null && strlen($klasse['MetaKey']) > 0) {
                $klassenAbfrage = " And  s.MetaKey='" . $klasse['MetaKey'] . "' ";
                $klassenString = $klasse['MetaKey'] . " - Klassen";
                $istMetaKeyKlasse = true; // für diese Klasse ist die Kontaktliste nicht freigeschaltet
                echo "Für Ihre Klasse/Jahrgangsstufe wird die Kontaktliste leider nicht unterstützt.<br> Es ist zur Zeit keine zuverlässige Authentifizierung von Anmeldungen möglich.";
            } else {
                $klassenAbfrage = " And  s.Stufe=" . $klasse['Stufe'] . " AND s.SubKlasse='" . $klasse['SubKlasse'] . "' ";
                $klassenString = "Klasse " . $klasse['Stufe'] . $klasse['SubKlasse'];
                $abfrageElternBerechtigung = "SELECT s.SchuelerId, e.Email, e.Telefon, e.Anschrift,
                 concat (e.Vorname, ' ',e.Nachname) as Eltern, 
                 concat (s.Vorname, ' ',s.Nachname) as Schueler, 
                 s.istElternvertreter, s.eisStatus,
                 concat(s.Stufe,s.SubKlasse,' ', s.Sprache) as Klasse
                FROM view_schueler_dieses_jahr s, eltern e
                WHERE s.FamilienId= e.FamilienId "
                        . " AND e.zuzahlungsBefreit != 1 "
                        . " AND e.Freigabe_alle = 1 "
                        //." AND s.eisStatus > 0 "
                        . $klassenAbfrage . " Order by s.Vorname, s.Nachname";
                lmf_trace("abfrage_klassen: " . $abfrageElternBerechtigung);
                $resultElternBerechtigung = mysqli_query($dbc, $abfrageElternBerechtigung);
                $resultArray = array();
                echo '<h2 class="lmf_listenTitel"> Kontaktliste ' . $klassenString . ' ' . $_SESSION["thisYear_int"] . '/' . (1 + $_SESSION["thisYear_int"]) . ' </h2>';
                echo '<table border=1 cellspacing="0" cellpadding="3" style="width:95%"><thead><tr>
                <th>Schüler</th>
                <th><nobr>Mutter/</nobr>Vater</th>
                <th>Telefon</th>
                <th>Anschift</th>
                <th>Email</th>
                </tr></thead>';
                while ($row = mysqli_fetch_assoc($resultElternBerechtigung)) {
                    echo '<tr>
                <td>' . $row["Schueler"] . '</td>
                <td>' . $row["Eltern"] . '</td>
                <td>' . $row["Telefon"] . '</td>
                <td>' . $row["Anschrift"] . '</td>
                <td>' . $row["Email"] . '</td>
                </tr>';
                }
                echo '</table>';
            }
        } else {
            echo 'Fehler: Sie sind nicht berechtigt die Kontaktdaten dieser Klasse zu sehen.';
        }
    }
    echo '</body></html>';
}


if (isset ($_POST["type"]) && "rundMail" == $_POST["type"]) {
    // checkLogin
    if ($loggedIn == false) {
        lmf_trace("keine Session mehr");
        echo '<textarea style="width:600px; height:150px;">logout</textarea>';
        return;
    }

    $ret = array();
    lmf_trace("POSTDATA: " . count($_FILES) . " FILES");
    lmf_trace($_POST);

    // checkAbsender
    $absenderResult = checkAbsenderEV($dbc, $_POST['eisMailKlassenId'], true);
    $absender = mysqli_fetch_array($absenderResult);
    if ($absender) {
        if ($absender['MetaKey'] != null && strlen($absender['MetaKey']) > 0) {
            $klassenAbfrage = " And  s.MetaKey='" . $absender['MetaKey'] . "' ";
        } else {
            $klassenAbfrage = " And  s.Stufe=" . $absender['Stufe'] . " AND s.SubKlasse='" . $absender['SubKlasse'] . "' ";
        }
        $abfrageMailEmpfaenger = 'SELECT concat(s.Vorname," ",s.Nachname) as schueler, e.*
                 FROM view_schueler_dieses_jahr s, eltern e
                 WHERE s.FamilienId=e.FamilienId
                 AND  e.Freigabe_ev=1 ' . $klassenAbfrage;
        lmf_trace($abfrageMailEmpfaenger);
        $count = doMail($dbc, $absender, $abfrageMailEmpfaenger, $_POST, $_FILES);
        $ret["loggedIn"] = true;
        $ret["count"] = $count;
        $ret["success"] = true;
        $ret["MAILS_PER_CALL"] = MAILS_PER_CALL;
        $ret["CALL_INTERVAL_ON_CRON"] = CALL_INTERVAL_ON_CRON;
        $ret["MAIL_TRIGGER_EXTERN"] = MAIL_TRIGGER_EXTERN;
        require("JSON.php");
        $json = new Services_JSON();
        $data = $json->encode($ret);
        echo '<textarea style="width:600px; height:150px;">' . $data . '</textarea>';
    } else {
        lmf_trace("check nicht größer 0");
        // Klasse passt nicht zum Elternvertreter - da hat jemand gehackt
        echo '<textarea style="width:600px; height:150px;">logout</textarea>';
        return;
    }
}


mysqli_close($dbc); //Close the DB Connection;
exit;
?>