<?php

require_once ('lmf-session.php');
require_once ('database_connection.php');
require_once ('constants.php');
require_once('lmf-sendMail.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once 'PHPMailer/src/Exception.php';
require_once 'PHPMailer/src/PHPMailer.php';
require_once 'PHPMailer/src/SMTP.php';
ignore_user_abort(true);
$calledFromApp = true;
if (!isset($_SESSION['FamilienId'])) {
    if (isset($_GET[MAIL_TRIGGER_CODE]) && $_GET[MAIL_TRIGGER_CODE] == 'true'){
            $calledFromApp = false;
    }else{
        echo "das war nix";
        exit;
    }
} else {
    $ret["loggedIn"] = true;
}
//lmf_trace("mailHandler start");
$openJobs = 0;
$allJobs = array();
$anlagen = array();
$mailanzahl = 0;
$job = null;
$erfolgreichVersandt = 0;
$mailEmpfaenger = "";

if (defined('MAIL_SIMULATION_TEST_MODUS') && MAIL_SIMULATION_TEST_MODUS) {
    lmf_trace("MAIL_SIMULATION_TEST_MODUS ON");
//} else {
//    lmf_trace("MAIL_SIMULATION_TEST_MODUS OFF");
}
$post=file_get_contents('php://input');
if ($post!=null){
    $request = json_decode($post, true);
}
if (isset($request) && $request["type"] == "deleteMailJob" && isset($request["jobId"])){
    $ret["success"] = deleteJob($dbc, $request["jobId"]);
}

if ($calledFromApp) {
    $abfrage_jobs = "SELECT * FROM mailjobs where FamilienId=" . $_SESSION['FamilienId'] . " order by jobId;";
} else {
    $abfrage_jobs = "SELECT * FROM mailjobs order by jobId;";
}
$result_jobs = mysqli_query($dbc, $abfrage_jobs);
if (!$result_jobs) {
    lmf_queryTrace($abfrage_jobs, false, $dbc);
    echo errorResponse();
    exit;
} else {
    while ($r = mysqli_fetch_assoc($result_jobs)) {
        $allJobs[] = $r;
        if ($r['beendet']==0) {
            if ($openJobs == 0) {
                $job = $r;
                lmf_trace(json_encode($job));
            }
            $openJobs++;
        }
    }
//    lmf_trace("jobAnzahl: " . $openJobs);
    if ($openJobs > 0) {
        $abfrage_anlagen = "SELECT * FROM mailanlagen where jobId=" . $job['jobId'] . ";";
        $result_anlagen = mysqli_query($dbc, $abfrage_anlagen);
        if (!$result_anlagen) {
            lmf_queryTrace($abfrage_anlagen, false, $dbc);
            echo errorResponse();
            exit;
        } else {
            // wir haben Anlagen
            lmf_queryTrace($abfrage_anlagen, true, $dbc);
            while ($anlage = mysqli_fetch_array($result_anlagen)) {
                $anlagen[] = $anlage;
            }
        }
        if (!holeAnzahlOffeneMailsAusDB($dbc, "SELECT Count(*)as anzahl FROM mails where jobId=" . $job['jobId'] . " AND Fehler=0;")) {
            echo errorResponse();
            return;
        }
        if (sendeMailsAusDB($dbc, "SELECT * FROM mails where jobId=" . $job['jobId'] . " AND Fehler=0 limit " . MAILS_PER_CALL . ";")) {
            lmf_trace("mailHandler check Mails complete");
            if ($mailanzahl - $erfolgreichVersandt == 0) {
                lmf_trace("alle Mails ohne Fehler abgearbeitet -> jetzt noch mal die Fehler");
                // alle Mails ohne Fehler abgearbeitet -> jetzt noch mal die Fehler
                if (!holeAnzahlOffeneMailsAusDB($dbc, "SELECT Count(*)as anzahl FROM mails where jobId=" . $job['jobId'] . ";")) {
                    $mailanzahl = -1; // undefiniert - trotzdem abzuarbeiten und versuchen zu löschen
                }
                lmf_trace("mailanzahl: " . $mailanzahl);
                if (sendeMailsAusDB($dbc, "SELECT * FROM mails where jobId=" . $job['jobId'] . " AND Fehler>0;")) {
                    lmf_trace("sendeMailsAusDB  mails mit Fehlern versandt!");
                    // auch die zuvor fehlerhaften Mails wurden versandt
                    // jetzt job löschen und Anhänge
                } else {
                    lmf_trace("sendeMailsAusDB mit Fehlern OK");
                    // auch beim erneuten Versuch mit zuvor fehlerhaften Mails gab es mind. einen Fehler
                    // jetzt werden alle gelöscht und der job dazu
                }
                lmf_trace("Mailjob beendet: jobId=" . $job['jobId']);
                lmf_trace("Mails erfolgreich verschickt an: " . $mailEmpfaenger);
                $deleteOK = finishJob($dbc, $job);
                lmf_trace("deleteOK: " . $deleteOK);
                if ($deleteOK && $mailanzahl > -1 && $mailanzahl - $erfolgreichVersandt == 0) {
                    echo '{"loggedIn":true,  "success":true, "daten":' . mailDatenAlsJson() . '}';
                    exit;
                }
                lmf_trace("irgend was falsch: ");
                if ($mailanzahl - $erfolgreichVersandt > 0) {
                    echo '{"loggedIn":true, "errors":["Nicht alle Mails wurden erfolgreich versendet!"], "success":false, "daten":' . mailDatenAlsJson() . '}';
                    exit;
                }
                lmf_trace("alle Mails wurden erfolgreich versendet");
                if (!$deleteOK) {
                    echo '{"loggedIn":true, "errors":["Fehler beim Löschen des Mailjobs!"], "success":false, "daten":' . mailDatenAlsJson() . '}';
                    exit;
                }
                lmf_trace("Löschen erfolgreich");
                if ($mailanzahl == -1) {
                    echo '{"loggedIn":true, "errors":["undefinierter Zustand des aktuellen Mailjobs!"], "success":false, "daten":' . mailDatenAlsJson() . '}';
                    exit;
                }
                lmf_trace("mailanzahl auch OK");
            } else {
                // da ist noch was offen - erstmal zurück zum Client und status melden
                lmf_trace("Mail erfolgreich: jobId=" . $job['jobId']);
                lmf_trace("Mails erfolgreich verschickt an: " . $mailEmpfaenger);
                echo '{"loggedIn":true, "success":true, "daten":' . mailDatenAlsJson() . '}';
                exit;
            }
        } else {
            lmf_trace("sendeMailsAusDB mit Fehler!");
            echo '{"loggedIn":true, "errors":["sendeMailsAusDB mit Fehler!"], "success":false, "daten":' . mailDatenAlsJson() . '}';
            exit;
        }
    } else {
        echo '{"loggedIn":true, "success":true, "daten":' . mailDatenAlsJson() . '}';
        exit;
    }
}

function mailDatenAlsJson() {
    global $openJobs, $allJobs,  $mailanzahl, $job, $erfolgreichVersandt;
    if ($job != null) {
        $ret = '{"MAILS_PER_CALL":' . MAILS_PER_CALL 
                . ', "MAIL_TRIGGER_EXTERN":' . MAIL_TRIGGER_EXTERN 
                . ', "CALL_INTERVAL_ON_CRON":' . CALL_INTERVAL_ON_CRON 
//                . ', "allJobs":' .  json_encode($allJobs) 
                . ', "openJobCount":' . $openJobs 
                . ', "mailsGesamt":' . $job['anzahl'] 
                . ', "offen":' . ($mailanzahl - $erfolgreichVersandt) . '}';
        lmf_trace($ret);
        return $ret;
    } else {
        return $ret = '{"MAILS_PER_CALL":' . MAILS_PER_CALL 
                . ', "MAIL_TRIGGER_EXTERN":' . MAIL_TRIGGER_EXTERN 
                . ', "CALL_INTERVAL_ON_CRON":' . CALL_INTERVAL_ON_CRON 
  //              . ', "allJobs":' .  json_encode($allJobs) 
                . ', "openJobCount": 0' 
                . ', "mailsGesamt": 0'
                . ', "offen": 0}';
    }
}

function errorResponse() {
    return '{"loggedIn":true, "errors":["Datenbankfehler"], "success":false, "daten":' . mailDatenAlsJson() . '}';
}

function holeAnzahlOffeneMailsAusDB($dbc, $abfrage_mailanzahl) {
    lmf_trace("holeAnzahlOffeneMailsAusDB: " . $abfrage_mailanzahl);
    global $mailanzahl;
    $result_mailanzahl = mysqli_query($dbc, $abfrage_mailanzahl);
    if (!$result_mailanzahl) {
        lmf_queryTrace($abfrage_mailanzahl, false, $dbc);
        return false;
    } else {
        $r = mysqli_fetch_array($result_mailanzahl);
        $mailanzahl = $r['anzahl'];
        return true;
    }
}

function sendeMailsAusDB($dbc, $abfrage_mails) {
    global $anlagen, $job, $erfolgreichVersandt, $calledFromApp;
    if ($calledFromApp && MAIL_TRIGGER_EXTERN) {
        return true;
    }
    lmf_trace("sendeMailsAusDB: " . $abfrage_mails);
    $result_mails = mysqli_query($dbc, $abfrage_mails);
    if (!$result_mails) {
        lmf_trace("sendeMailsAusDB: fehler ");
        lmf_queryTrace($abfrage_mails, false, $dbc);
        echo errorResponse();
        return false;
    } else {
        // wir haben Mails
        $erfolgreichVersandt = 0;
        while ($mail = mysqli_fetch_array($result_mails)) {
            if (doMail($mail, $anlagen, $job)) {
                $erfolgreichVersandt++;
                if (!deleteMail($dbc, $mail)) {
                    lmf_trace('{"loggedIn":true, "errors":["Loeschfehler"],mail:' . json_encode($mail) . ' "success":false, "daten":' . mailDatenAlsJson() . '}');
                    return false;
                }
            } else {
                // fehler beim senden der mail - aufhören und Ergebniss zurück geben
                setzeFehler($dbc, $mail);
                lmf_trace('{"loggedIn":true, "errors":["Mailfehler"], "success":false, "daten":' . mailDatenAlsJson() . '}');
                return false;
            }
        }
        lmf_trace("sendeMailsAusDB: ende while schleife ");
        return true;
    }
}

function deleteJob($dbc, $jobId) {
    global $anlagen;
    foreach ($anlagen as $anlage) {
        unlink(realpath(dirname(__FILE__)) . "/../attachments/" . $anlage['jobId'] . "/" . $anlage['filename']);
    }
    rmdir(realpath(dirname(__FILE__)) . "/../attachments/" . $jobId);
    $abfrage = "DELETE from `mailanlagen` WHERE jobId=" . $jobId . ";";
    $ret = true;
    if (!mysqli_query($dbc, $abfrage)) {
        lmf_queryTrace($abfrage, false, $dbc);
        $ret = false;
    } else {
        lmf_queryTrace($abfrage, true, $dbc);
    }
    $abfrage = "DELETE from `mails` WHERE jobId=" . $jobId . ";";
    if (!mysqli_query($dbc, $abfrage)) {
        lmf_queryTrace($abfrage, false, $dbc);
        $ret = false;
    } else {
        lmf_queryTrace($abfrage, true, $dbc);
    }
    $abfrage = "DELETE from `mailjobs` WHERE jobId=" . $jobId . ";";
    if (!mysqli_query($dbc, $abfrage)) {
        lmf_queryTrace($abfrage, false, $dbc);
        $ret = false;
    } else {
        lmf_queryTrace($abfrage, true, $dbc);
    }
    return $ret;
}
function finishJob($dbc, $job) {
    $ret = true;
    $abfrage = "UPDATE `mailjobs` mj Set 
                    beendet=1,
                    fehler=(select count(*) from mails WHERE jobId=" . $job['jobId'] . ")
               WHERE jobId=" . $job['jobId'] . ";";
    if (!mysqli_query($dbc, $abfrage)) {
        lmf_queryTrace($abfrage, false, $dbc);
        $ret = false;
    } else {
        lmf_queryTrace($abfrage, true, $dbc);
    }
    return $ret;
}
function deleteMail($dbc, $mail) {
    $abfrage = "DELETE from `mails` WHERE mailId = " . $mail['mailId'] . ";";
    $result = mysqli_query($dbc, $abfrage);
    if (!$result) {
        lmf_queryTrace($abfrage, false, $dbc);
        return false;
    } else {
        lmf_queryTrace($abfrage, true, $dbc);
        return true;
    }
}

function setzeFehler($dbc, $mail) {
    $abfrage = "UPDATE mails SET fehler = " . (1 + $mail['fehler']) .
            " WHERE  mailId = " . $mail['mailId'] . ";";
    $result = mysqli_query($dbc, $abfrage);
    if (!$result) {
        lmf_queryTrace($abfrage, false, $dbc);
        return false;
    } else {
        lmf_queryTrace($abfrage, true, $dbc);
        return true;
    }
}

function createPhpMailer($mailFromDb) {
    $smtpType = "LMF";
    if ($mailFromDb["absenderAdresse"] != LMF_SMTP_USER) {
        $smtpType = "EIS";
    }
    $mailer = new PHPMailer;
    $mailer->CharSet = 'UTF-8';
    $mailer->isSMTP();                 // SMTP aktivieren
    $mailer->Host = constant($smtpType . '_SMTP_HOST');       //SMTP-Server
    $mailer->SMTPAuth = constant($smtpType . '_SMTP_AUTH');   // SMTP Authentifizierung aktivieren
    $mailer->Username = constant($smtpType . '_SMTP_USER');   // SMTP Benutzer
    $mailer->Password = constant($smtpType . '_SMTP_PW');     // SMTP Benutzer Passwort
    $mailer->SMTPSecure = constant($smtpType . '_SMTP_SEC');  // Verbindungssicherheit setzen ( SSL und TLS möglich )
    $mailer->Port = constant($smtpType . '_SMTP_PORT');       // Verbindungsport festlegen

    $mailer->IsHTML(true);
    if (EV_MAIL_FROM_EV) {
        $mailer->SetFrom($mailFromDb["absenderAdresse"], $mailFromDb["absenderName"]);
    } else {
        $mailer->SetFrom(constant($smtpType . '_SMTP_USER'), $mailFromDb["absenderName"]);
        $mailer->AddReplyTo($mailFromDb["absenderAdresse"], $mailFromDb["absenderName"]);
    }
    return $mailer;
}

function doMail($mailFromDb, $attachments, $job) {

    global $mailEmpfaenger;
    $oMailer = createPhpMailer($mailFromDb);
    $oMailer->AddAddress($mailFromDb['empfaengerAdresse']);
    $mailEmpfaenger .= $mailFromDb["empfaengerAdresse"] . "; ";
    $oMailer->Subject = $mailFromDb["betreff"];
    $oMailer->Body = "<html>" . $mailFromDb["text"] . "</html>";
    $oMailer->AltBody = strip_tags($oMailer->Body);
    if ($attachments != null) {
        foreach ($attachments as $attachment) {
            lmf_trace("anlage: " . realpath(dirname(__FILE__)) . "/../attachments/" . $attachment['jobId'] . "/" . $attachment['filename']);
            $oMailer->AddAttachment(realpath(dirname(__FILE__)) . "/../attachments/" . $attachment['jobId'] . "/" . $attachment['filename'], $attachment['filename']);
        }
    }
    if (defined('MAIL_SIMULATION_TEST_MODUS') && MAIL_SIMULATION_TEST_MODUS) {
        lmf_trace("sleep");
        usleep(1000000);
        lmf_trace("mail to " . $mailFromDb["empfaengeradresse"]);
        return true;
    } else {
        if (!$oMailer->Send()) {
            lmf_trace('Message could not be sent to : ' . $mailFromDb["empfaengerAdresse"]);
            lmf_trace('Mailer Error: ' . $oMailer->ErrorInfo);
            return false;
        }
        lmf_trace('Nachricht wurde versendet an: ' . $mailFromDb["empfaengerAdresse"]);
        return true;
    }
}

lmf_trace("mailClient end");
?>



