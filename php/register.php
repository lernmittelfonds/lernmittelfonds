<?php
require_once ('database_connection.php');
require_once ('mail.php');
require_once('lmf-logging.php');
error_reporting(E_ALL);
lmf_trace("start");

function boolNumber($bValue = false) {                      // returns integer
    return ($bValue ? 1 : 0);
}

if (isset($_POST['formsubmitted'])) {
    $error = array(); //Declare An Array to store any error message  
    if (empty($_POST['Nachname'])) {//kein Nachname eingetragen 
        $error[] = 'Bitte geben sie einen Nachnamen an '; //add to array "error"
    } else {
        $Nachname = $_POST['Nachname']; //else variable zuordnen
    }

    if (empty($_POST['Vorname'])) {//kein Vorname eingetragen 
        $error[] = 'Bitte geben Sie einen Vornamen an '; //add to array "error"
    } else {
        $Vorname = $_POST['Vorname'];
    }

//    if (empty($_POST['Telefon'])) {
//        $error[] = 'Bitte geben Sie eine gültige Telefonnummer an '; //add to array "error"
//    } else {
    $Telefon = $_POST['Telefon'];
//    }

    $Anschrift = $_POST['anschrift'];

    if (empty($_POST['e-mail'])) {
        $error[] = 'Bitte geben Sie eine gültige eMail-Adresse an ';
    } else {
        if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", trim (strtolower($_POST['e-mail'])))) {
            //regular expression for email validation
            $mail = trim (strtolower($_POST['e-mail']));
        } else {
            $error[] = 'Ihre eMail-Adresse ist ungültig  ';
        }
    }

    if (empty($_POST['Passwort'])) {
        $error[] = 'Bitte geben Sie ein Passwort ein ';
    } else {
        $Passwort = $_POST['Passwort'];
    }

//    if (empty($_POST['agree'])) {
//        $error[] = 'Bitte bestätigen Sie, dass Sie unsere Grundsätze anerkennen!'; //add to array "error"
//    } else {
//        $Telefon = $_POST['Telefon'];
//    }



    if (empty($error)) { //send to Database if there's no error ' // wenn alles OK...
        // check email address verfügbar:
        $query_verify_email = "SELECT * FROM eltern  WHERE LOWER(Email) ='" . $mail . "'";
        $result_verify_email = mysqli_query($dbc, $query_verify_email);
        if (!$result_verify_email) {//if the Query Failed ,similar to if($result_verify_email==false)
            $error[] = 'Fehler bei der Datenbankabfrage';
        }

        if (mysqli_num_rows($result_verify_email) == 0) { // noch niemand nutzt diese adresse.
            // Create a unique  activation code:
            $activation = substr (md5(uniqid(rand(), true)),0,6);
            $isAdmin = FALSE;
            if ($Vorname == "mdb-admin13") {
                $isAdmin = TRUE;
            }

            // echo $activation ;
            $query_insert_user = "INSERT INTO `eltern` 
                    ( `Nachname`, `Vorname`, `Telefon`, `Email`, `Passwort`, `Activation`, `isAdmin`,`Anschrift`,
                        `Freigabe_alle`,`Freigabe_ev`,`Freigabe_info`,`Freigabe_lmf`)"
                    . "VALUES ( '$Nachname', '$Vorname', '$Telefon', '$mail', '" . md5($Passwort) . "', '$activation', " . boolNumber($isAdmin) . ", '$Anschrift'," 
                        . boolNumber($_POST['lmf_Freigabe_alle']) . "," . boolNumber($_POST['lmf_Freigabe_ev']) . "," . boolNumber($_POST['lmf_Freigabe_info']) . "," . boolNumber($_POST['lmf_Freigabe_lmf']) . ")";
            $result_insert_user = mysqli_query($dbc, $query_insert_user);
            if (!$result_insert_user) {
                lmf_trace("Fehler beim Anlegen eines neues Nutzers(" . $mail . ")");
                $error[] = "Fehler beim Anlegen eines neues Nutzers(" . $isAdmin . ")";
                lmf_queryTrace($query_insert_user, false, $dbc);
            }else{
                lmf_queryTrace($query_insert_user, true, $dbc);
            }

            if (mysqli_affected_rows($dbc) == 1) { //Insert Query successfull.
                lmf_trace("Mail an register mail:  send: ");
                $ret=sendMailRegister($mail, mysqli_insert_id($dbc), $activation);
                 lmf_trace("Mail an register mail:  with success???: ");
                 lmf_trace($ret);
                 lmf_trace("Mail an register mail:  with success???: ". $ret);
                if (!ret) {
                    $error[] = "Fehler beim Versenden der Aktivierungs Mail. Bitte versuchen sie es noch einmal oder wenden sich an unsere Mitarbeiter.";
                    $query_delete_new_user = "DELETE FROM `eltern` WHERE `Email` = '$mail';";
                    if (mysqli_query($dbc, $query_delete_new_user)){
                                        lmf_queryTrace($query_delete_new_user, true, $dbc);
                    }else{
                                        lmf_queryTrace($query_delete_new_user, false, $dbc);
                    }
                }
            } else { // wenn es Fehler gab.
                $error[] = "Leider ist ein Fehler aufgetreten und Sie konnten nicht am System angemeldet werden";
            }
        } else { // email address ist nicht frei.
            $error[] = "Die email-Adresse, die Sie angegeben haben ist bereits registriert";
        }
    }
    mysqli_close($dbc); //Close the DB Connection
} else {
    $error[] = "Interner Fehler: Fehlerhafter Http-request.";
}
?>
{
"errors":  <?php echo json_encode($error); ?>
}
