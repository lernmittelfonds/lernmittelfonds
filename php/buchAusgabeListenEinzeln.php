<?php

require_once ('./lmf-session.php');
include ('database_connection.php');
require_once ('constants.php');
require_once ('./lmf-logging.php');
if(!isset($_SESSION)){ 
    session_start(); 
} 

$error = array(); //this array will store all error messages
$loggedIn = true; //is logged in???
$request = json_decode(file_get_contents('php://input'), true);

if (!isset($_SESSION['FamilienId'])) {
    $error[] = 'Loginerror';
    echo '{"loggedIn":false, "errors":["Nicht korrekt angemeldet"], "success":false}';
    exit;
}

print '<html slick-uniqueid="3" xmlns="http://www.w3.org/1999/xhtml" xml:lang="de-de" lang="de-de">
    <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <style type="text/css">
    .lmf_pb { page-break-after:always ; }
    @media print { 
    body, table { 
        font-size: 12pt; 
    }
}
   </style>
  </head>
<body>';

if ($_REQUEST["type"] == "leereListen" || 0==$_SESSION['lmfAnmeldungenFreigeben_int']) {
    $abfrage_klassen = "select * from  `view_klassen_altes_jahr`";
    $offSet = 0;
} else {
    $abfrage_klassen = "select * from  `view_klassen_neues_jahr`";
    $offSet = 1;
}
$result_klassen = mysqli_query($dbc, $abfrage_klassen);

while ($row = mysqli_fetch_array($result_klassen)) {
   
   $abfrage_buecher = "SELECT  BuchId , Titel, Isbn,  ROUND(Neupreis,2) as Preis
                    FROM view_analyse_buch_ausgabe
                    Where KlassenId=" . $row['KlassenId'] . " 
                    GROUP BY BuchId;";
    $result_buecher = mysqli_query($dbc, $abfrage_buecher);
    
    
    while ($row2 = mysqli_fetch_array($result_buecher)) {
        print '<h3 class="lmf_listenTitel"> Lernmittelfonds ' . $row['Jahr'] . '/' . (1 + $row['Jahr']) . ' - Ausgabe ' . $row2['Titel'] .' Klasse ' . $row['Stufe'] . $row['SubKlasse'] .'</h3>';
        print '<div>'.str_replace("\n", "<br>",$_POST['kommentar']).'</div><p></p>';
        
        print '<table border=1 cellspacing="0" cellpadding="3" style="width:100%"><thead><tr><th style="width:30px">Nr</th><th >Schüler</th>';
        print '<th>Lernmittel erhalten (Unterschrift)</th></tr></thead>';
        
        $selectAbfrage = "SELECT *";
        $selectAbfrage .= ",\n sum(if(BuchId=" . $row2['BuchId'] . ",auszugeben , 0)) as buch";
        $abfrage_schueler = $selectAbfrage . "
            FROM    view_analyse_buch_ausgabe   
            WHERE klassenId = " . $row['KlassenId'] . " 
            GROUP BY schuelerid
            ORDER BY sNachname, sVorname";
        //lmf_trace($abfrage_schueler);
        $result_schueler = mysqli_query($dbc, $abfrage_schueler);
        $lfdNr = 1;
        while ($row3 = mysqli_fetch_array($result_schueler)) {
            print '<tr><td>' . $lfdNr . '</td><td> ' . $row3['sNachname'] . ', ' . $row3['sVorname'] . '</td>';
            echo '<td> &nbsp </td></tr>';
            $lfdNr++;
        }
        echo '</table>';
        print '<div class = "lmf_pb"></div>';
        }

}
print '</body></html>';






mysqli_close($dbc); //Close the DB Connection;
exit;
?>
