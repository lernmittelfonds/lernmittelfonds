window.elternInfo = (function (elternInfo) {

    elternInfo.mailAnzeigen = function () {
        dojo.xhrGet({
            url: elternInfo.root + "pages/mails.htm?",
            preventCache: true,
            handleAs: "text",
            load: function (response) {
                dojo.byId("lmf_applikation").innerHTML = response;
                console.log("fertig callMailSeite");
                onLoadMail();
            },
            // Event handler on errors:
            error: function (response, ioArgs) {
                console.log(response);
                elternInfo.displayErrors(["pages/mails.htm konnte nicht geladen werden"]);
            }
        });
    };

    var lmfMailEditor = null;
    var lmfMailButton = null;
    var fileUploader = null;
    var selectKlassenStufe = null;
    var selectKlasse = null;
    function onLoadMail() {
        dojo.require("lmf.FileUploader");
        dojo.require("dijit.form.Button");
        dojo.require("dijit.ProgressBar");
        dojo.require("dijit.form.Select");
        dojo.require("dijit.Editor");
        dojo.require("dijit._editor.plugins.AlwaysShowToolbar");
        console.log("onLoadMail");
        var _options = [];
        if (dojo.byId("auswahlNaechstesJahr").checked) {

        }
        for (i = 7; i <= 12; i++) {
            _options.push({label: i, value: i});
        }

        selectKlassenStufe = new dijit.form.Select({
            name: 'lmfSelectKlassenstufe',
            options: _options
        }).placeAt(dojo.byId("lmfMailKlassenStufe"));

        selectKlasse = new dijit.form.Select({
            name: 'lmfSelectKlasse',
            options: klassenAuswahlTreffen()
        }).placeAt(dojo.byId("lmfMailKlassen"));
        if (lmfMailEditor) {
            lmfMailEditor.destroyRecursive();
        }
        lmfMailEditor = new dijit.Editor({
            value: "   ",
            extraPlugins: ['dijit._editor.plugins.AlwaysShowToolbar']},
                dojo.byId('lmfMailText'));
        lmfMailEditor.setValue("");
        if (lmfMailButton === null) {
            lmfMailButton = new dijit.form.Button({
                label: "Absenden",
                onClick: function (evt) {
                    dojo.byId("lmfMailTextHidden").value = lmfMailEditor.getValue();
                    if (lmfMailEditor.getValue().trim().length > 0) {
                        console.log("#" + lmfMailEditor.getValue().trim() + "#");
                        setzeKlasseHidden();
                        elternInfo.hideErrors();
                        console.log(dojo.byId("lmfMailTextHidden").value);
                        fileUploader.submit(dojo.byId("mailFormular"));
                    } else {
                        alert("Ohne Inhalt wird nichts gemailt!");
                    }
                    dojo.stopEvent(evt);
                }
            },
                    dojo.byId('lmfMailButtonDiv'));
        } else {
            lmfMailButton.placeAt("lmfMailButtonDiv");
        }
        if (dojo.byId("btnH")) {
            dojo.byId("hFiles").value = "";
            if (fileUploader) {
                fileUploader.destroyRecursive();
            }
            var props = {
                isDebug: true,
                devmode: true,
                hoverClass: "uploadHover",
                activeClass: "uploadPress",
                disabledClass: "uploadDisabled",
                uploadUrl: dojo.moduleUrl("dojox.form", window.location.origin + elternInfo.root + "php/mailClientNeu.php")
            };
            console.log(window.location.origin + elternInfo.root + "php/mailClientNeu.php");
            fileUploader = new lmf.FileUploader(dojo.mixin({
                force: "html",
                //showProgress: true,
                //progressWidgetId: "progressBarHtml",
                selectMultipleFiles: true,
                //uploadOnChange:true,
                fileListId: "hFiles",
                devMode: true
            }, props), "btnH");
            dojo.connect(fileUploader, "onComplete", function (dataArray) {
                console.log("onComplete");
                console.log(dataArray);
                if (dataArray && dataArray[0] && !dataArray[0].loggedIn) {
                    elternInfo.displayErrors(["Session ungültig. Bitte melden sie sich neu an"]);
                    elternInfo.loginSeiteLaden(["Session ungültig. Bitte melden sie sich neu an"], true);
                }
                if (dataArray && dataArray[0] && dataArray[0].success) {
                    var minutesToWait = 0;
                    if (dataArray[0].count) {
                        minutesToWait = Math.round(dataArray[0].CALL_INTERVAL_ON_CRON /60  * (0.5 + dataArray[0].count / dataArray[0].MAILS_PER_CALL));
                    }

                    elternInfo.displayInfo("Es wurden " + dataArray[0].count + " Mails erfolgreich vorbereitet. <br>Sie werden im Hintergrund verschickt solange Ihre Session läuft. Bitte beachten Sie, dass sich alle anderen Arbeitsprozesse in dieser Zeit etwas verlangsamen können. Es wird ca " + minutesToWait + " Minuten dauern.");
                    lmfMailEditor.setValue("");
                    dojo.byId("mail-icon-span").style.display = "block";
                    elternInfo.checkUndStartMailJobs();
                }
                console.warn("html onComplete", dataArray);
                dojo.forEach(dataArray, function (d) {
                    console.log(d);
                });
            });
            dojo.connect(fileUploader, "onError", function (err) {
                console.error("html error", err);
            });
        }
    }
    elternInfo.wechselAuswahlJahr = function () {
        selectKlasse.removeOption(selectKlasse.getOptions());
        selectKlasse.addOption(klassenAuswahlTreffen());
    };

    function setzeKlasseHidden() {
        var wert = "";
        if (dojo.byId("auswahlDiesesJahr").checked) {
            wert = elternInfo.formatterKlasseAlt(selectKlasse.value);
        } else {
            wert = elternInfo.formatterKlasse(selectKlasse.value);
        }
        dojo.byId("lmfKlasseHidden").value = wert;
    }
    function klassenAuswahlTreffen() {
        var _options2 = [];
        console.log(elternInfo.familyData);
        for (i = 0; i < elternInfo.familyData.KlassenKeys.length; i++) {
            if (dojo.byId("auswahlDiesesJahr").checked) {
                if (elternInfo.familyData.KlassenStringsAlt[i][0] >= "0") {
                    _options2.push({label: elternInfo.familyData.KlassenStringsAlt[i], value: elternInfo.familyData.KlassenKeysAlt[i]});
                }
            }
            if (dojo.byId("auswahlNaechstesJahr").checked) {
                if (elternInfo.familyData.KlassenStrings[i][0] >= "0") {
                    _options2.push({label: elternInfo.familyData.KlassenStrings[i], value: elternInfo.familyData.KlassenKeys[i]});
                }
            }
        }
        return _options2;
    }

    elternInfo.mailAnzeigen();

    return elternInfo;
})(window.elternInfo || {});
