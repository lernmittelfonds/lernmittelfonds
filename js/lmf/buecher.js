window.elternInfo = (function(elternInfo) {
    /*jslint nomen: true */
    /*jshint strict: true */
    var layoutBuecherListen = null;
    var buecherInListenData = null;
    var buecherInListenStore = null;
    var buecherListenGrids = {};
    var type = null;

    elternInfo.callBuecherListen = function(_type) {
        type = _type;
        var _jahr = elternInfo.familyData.thisYear;
        if (type !== 'alt') {
            _jahr += 1;
        }
        console.log("callBuecherListen");
        elternInfo.hideErrors();
        dojo.byId("lmf_applikation").innerHTML = '<div id="lmf_service"><div id="lmf_serviceTitle">&nbsp;</div><div id="lmf_serviceContent">&nbsp;</div></div>';
        var data = {
            type: "BuecherListen",
            _FamilienId: elternInfo.familyData.eltern.FamilienId,
            _jahr: _jahr
        };
        elternInfo.lmf_postService(
                data,
                function(response) {
                    console.log(response);
                    var style = elternInfo.style.tableBuecherListen;
                    if (layoutBuecherListen === null) {
                        layoutBuecherListen = [{
                                defaultCell: {
                                    editable: false,
                                    styles: style.default.styles
                                },
                                cells: [{
                                        field: 'checked',
                                        name: ' ',
                                        width: style.checked.width,
                                        styles: style.checked.styles,
                                        editable: true,
                                        type: dojox.grid.cells.Bool
                                    }, {
                                        field: 'ISBN',
                                        name: 'ISBN',
                                        width: style.ISBN.width
                                    }, {
                                        field: 'Titel',
                                        name: 'Titel',
                                        width: style.Titel.width
                                    }, {
                                        field: 'Neupreis',
                                        name: 'Neupreis',
                                        width: style.Neupreis.width,
                                        styles: style.Neupreis.styles,
                                        formatter: formatterRound2
                                    }, {
                                        field: '_item',
                                        name: "Aktion",
                                        width: style.Aktion.width,
                                        formatter: formatterBuchLoeschen
                                    }]
                            }];
                    }
                    var _items = [];
                    for (var i = 0; i < response.data.buecher.length; i++) {
                        _items[i] = dojo.clone(response.data.buecher[i]);
                        _items[i]['id'] = _items[i]['listenId'] + "-" + _items[i]['buchId']; // nur fuer den gridstore gebraucht 
                        _items[i].Neupreis = 1 * _items[i].Neupreis;
                    }
                    buecherInListenData = {
                        items: _items,
                        identifier: 'id'
                    };
                    buecherInListenStore = new dojo.data.ItemFileWriteStore({
                        data: buecherInListenData
                    });
                    buecherInListenStore._forceLoad();
                    var buecherListen = response.data.buecherListen;
                    var _anderesJahr = (type === 'alt') ? _jahr + 1 : _jahr - 1;
                    _link = (type === 'alt') ? "'neu'" : "'alt'";
                    dojo.byId("lmf_serviceContent").innerHTML = '<div class="lmf_ServiceLeft">Übersicht über alle Bücherlisten ' + _jahr + '/' + (1 + _jahr) + '</div><a  class="lmf_ServiceRight" href="javascript:elternInfo.callBuecherListen(' + _link + ');">' + (_anderesJahr) + '/' + (1 + _anderesJahr) + '</a>';
                    var buecherKlassenGrid, klassenStufe, sprache;
                    for (i = 0; i < buecherListen.length; i++) {
                        klassenStufe = buecherListen[i].klassenStufe;
                        sprache = buecherListen[i].sprache;
                        dojo.byId("lmf_serviceContent").innerHTML += '<br><hr><br><div class="lmf_ServiceLeft">Bücherliste für  <i>Klassenstufe ' + klassenStufe + '-' + sprache + '</i></div>';
                        dojo.byId("lmf_serviceContent").innerHTML += '<div class="lmf_buchButton" id="button_' + buecherListen[i].listenId + '"></div><div style="clear:left"></div>';
                        dojo.byId("lmf_serviceContent").innerHTML += '<div class="lmf_Beitragszeile">Teilnahmebeitrag: <i> ' + buecherListen[i].beitrag + ' €</i></div><div style="clear:left"></div>';
                        dojo.byId("lmf_serviceContent").innerHTML += '<div id="' + buecherListen[i].listenId + '"></div>';
                    }


                    for (var i = 0; i < buecherListen.length; i++) {

                        klassenStufe = buecherListen[i].klassenStufe;
                        sprache = buecherListen[i].sprache;
                        if (type !== 'alt') {
                            var listeBearbeitenButton = new dijit.form.Button({
                                label: "Liste bearbeiten "
                            }, document.createElement('div'));
							console.log ("create Button mit liste: ", buecherListen[i]);
                            listeBearbeitenButton._liste = buecherListen[i];
                            listeBearbeitenButton._listenId = buecherListen[i].listenId;
                            listeBearbeitenButton._listenTitle = 'Bücherliste für  <i>Klassenstufe ' + klassenStufe + '-' + sprache + '</i>';
                            dojo.byId('button_' + buecherListen[i].listenId).appendChild(listeBearbeitenButton.domNode);
                            // nicht globale Variable für connect, sonst wird immer auf die globale mit letzten schleifenwert referenziert
                            dojo.connect(listeBearbeitenButton, "onClick", function() {
                                listeBearbeiten(this);
                            });
                        }


                        if (buecherListenGrids[buecherListen[i].listenId]) {
                            buecherKlassenGrid = buecherListenGrids[buecherListen[i].listenId],
                                    buecherKlassenGrid.store = buecherInListenStore;
                            dojo.byId(buecherListen[i].listenId).appendChild(buecherKlassenGrid.domNode);
                            buecherKlassenGrid.render();
                        } else {
                            console.log("Query: " + klassenStufe + " / " + sprache + "(id: " + buecherListen[i].listenId + ")");
                            console.log(buecherInListenStore);
                            buecherKlassenGrid = new dojox.grid.DataGrid({
                                id: "buecherListe_" + buecherListen[i].listenId,
                                store: buecherInListenStore,
                                selectable: true,
                                query: {
                                    listenId: buecherListen[i].listenId
                                },
                                clientSort: true,
                                autoRender: true,
                                structure: layoutBuecherListen,
                                autoWidth: true,
                                autoHeight: true
                            },
                            document.createElement('div')
                                    );
                            dojo.byId(buecherListen[i].listenId).appendChild(buecherKlassenGrid.domNode);
                            buecherKlassenGrid.startup();
                            buecherListenGrids[buecherListen[i].listenId] = buecherKlassenGrid;
                        }
                        dojo.connect(buecherInListenStore, "onDelete", function() {
                            for (var _id in buecherListenGrids) {
                                buecherListenGrids[_id].render();
                            }
                        });
                    }
                }
        );
    };
    dojo.require("dijit.form.TextBox");
    dojo.require("dijit.form.NumberTextBox");

    function checkData() {
        var data = lmf_buchDialog.attr('value');
        console.log(data);
        return false;
    }

    function formatterRound2(value) {
        var tmp = value.toString();
        tmp += (tmp.indexOf('.') == -1) ? '.00' : '00';
        return tmp.substring(0, tmp.indexOf('.') + 3);
    }

    function formatterBuchLoeschen(_row) {
        var new_button = new dijit.form.Button({
            label: 'Löschen',
            showLabel: true,
            disabled: type === 'alt',
            onClick: function() {
                buchAusListeLoeschen(_row);
            },
            'class': 'gridButton'
        });
        new_button._destroyOnRemove = true;
        return new_button;
        //return "";
    }

    function formatterBuchHinzuFuegen(_row) {
        var new_button = new dijit.form.Button({
            label: '+',
            showLabel: true,
            onClick: function() {
                buchInListeEinfuegen(_row);
            },
            'class': 'gridButton'
        });
        new_button._destroyOnRemove = true;
        return new_button;
    }
    var listeZurBearbeitung = null;

    function buchInListeEinfuegen(_row) {
        console.log("buchInListeEinfuegen");
        var neuerListenEintrag = {
            "type": "buchInListeEinfuegen",
            _FamilienId: elternInfo.familyData.eltern.FamilienId,
            jahr: 1 + elternInfo.familyData.thisYear,
            listenId: listeZurBearbeitung.listenId,
            buchId: _row.BuchId[0],
            preisImJahr: _row.Neupreis[0]
        };
        elternInfo.lmf_postService(
                neuerListenEintrag,
                function(response) {
                    console.log("buchInListeEinfuegen" + response);
                    delete neuerListenEintrag.type;
                    delete neuerListenEintrag._FamilienId;
                    neuerListenEintrag.id = listeZurBearbeitung.listenId + "_" + _row.BuchId[0];
                    neuerListenEintrag.listenId = listeZurBearbeitung.listenId;
                    neuerListenEintrag.checked = "";
                    neuerListenEintrag.Titel = _row.Titel;
                    neuerListenEintrag.Neupreis = _row.Neupreis;
                    neuerListenEintrag.ISBN = _row.Isbn;
                    gridZurBearbeitung.store.newItem(neuerListenEintrag);
                    gridZurBearbeitung.render();
                }
        );
    }

    function buchAusListeLoeschen(_row) {
        console.log("buchAusListeLoeschen");
        if (_row.checked && !_row.checked[0]) { //das Feld wird als weiteres Tag missbraucht, weil es nicht benötigt wird
            alert("Die Auwahl wird nur bestätigt, wenn auch die Auswahlbox in der ersten Spalte aktiviert ist.");
            return;
        }
        elternInfo.hideErrors();
        console.log(_row);
        buecherInListenStore.fetchItemByIdentity({
            identity: _row.id,
            onItem: function(item) {
                if (buecherInListenStore.isItem(item)) {
                    buecherInListenStore.deleteItem(item);
                }
            }
        });
        elternInfo.hideErrors();
        var data = {
            type: "BuchAusListeLoeschen",
            _FamilienId: elternInfo.familyData.eltern.FamilienId,
            _listenId: _row.listenId[0],
            _buchId: _row.buchId[0]
        };
        elternInfo.lmf_postService(data, function(response) {
            console.log("BuecherListeLoeschen" + response);
        });
    }

    var myDialog = null;
    var buecherStore = null;
    var layoutBuecher = null;
    var gridZurBearbeitung = null;

    function lmf_beitrag_aendern(neuerBeitrag) {
        if (neuerBeitrag && listeZurBearbeitung.beitrag != neuerBeitrag) {
            if (isNaN(neuerBeitrag)) {
                elternInfo.displayErrors(["Fehler: Ihr eingegebener Wert '" + neuerBeitrag + "' ist keine gültige Zahl! Der Wert wurde zurückgesetzt"]);
                dijit.byId("lmf_beitragsFeld").setValue(1 * listeZurBearbeitung.beitrag);
            } else {
                elternInfo.hideErrors();
                var data = {
                    type: "BeitragAendern",
                    _listenId: listeZurBearbeitung.listenId,
                    _neuerBeitrag: neuerBeitrag
                };
                elternInfo.lmf_postService(data, function() {
                    elternInfo.displayInfo("Der Beitrag für die Bücherliste wurde erfolgreich geändert");
                });
            }
        } else {
            console.log("neuer Beitrag(" + neuerBeitrag + ") gleich alter Beitrag(" + listeZurBearbeitung.beitrag + ")");
        }
    }

    function listeBearbeiten(button) {
        listeZurBearbeitung = button._liste;
		dojo.byId("lmf_serviceContent").innerHTML = button._liste.listenId+": "+button._listenTitle + ' <i>(' + button._liste.jahr + '/' + (1 * button._liste.jahr + 1) + ')</i> bearbeiten<div style="clear:left"></div>';
        dojo.byId("lmf_serviceContent").innerHTML += '<div class="lmf_Beitragszeile">Teilnahmebeitrag:&nbsp;&nbsp;&nbsp; </div>' + '<div class="lmf_ServiceLeft" id="beitrag"> </div> €<div style="clear:left"></div>';
        dojo.byId("lmf_serviceContent").innerHTML += '<div id="gridZumBearbeiten"></div>';
        dojo.byId("lmf_serviceContent").innerHTML += '<br><hr>Übersicht über alle passenden Bücher: <br> ';
        dojo.byId("lmf_serviceContent").innerHTML += '<div id="gridAlleBuecher"></div>';

        try {
            if (dijit.byId("lmf_beitragsFeld")){
                dijit.byId("lmf_beitragsFeld").setAttribute("value",1 * button._liste.beitrag);
                dojo.byId("beitrag").appendChild(dijit.byId("lmf_beitragsFeld").domNode);
            }else{
                new dijit.form.NumberTextBox({
                id: "lmf_beitragsFeld",
                onChange: function(neuerWert) {
                    lmf_beitrag_aendern(neuerWert);
                },
                constraints: {
                    min: 0,
                    max: 200,
                    places: 2
                },
                "style": 'width: 40px; text-align: "right";',
                value: 1 * button._liste.beitrag
            }, dojo.byId('beitrag'));
            }
        } catch (error) {
            console.log(error);
        }

        var neuesBuchButton = new dijit.form.Button({
            label: "neues Buch",
            onClick: function() {
                var neuesBuch = {
                    Isbn: ["ISBN"],
                    Titel: ["TITEL"],
                    von: [7],
                    bis: [7],
                    Neupreis: [0],
                    BuchId: [-1]
                };
                buchSpeichern(neuesBuch);
            }
        },
        document.createElement('div')
                );
        dojo.byId("lmf_serviceContent").appendChild(neuesBuchButton.domNode);
        dojo.byId("gridZumBearbeiten").appendChild(buecherListenGrids[listeZurBearbeitung.listenId].domNode);
        buecherListenGrids[listeZurBearbeitung.listenId].startup();
        gridZurBearbeitung = buecherListenGrids[listeZurBearbeitung.listenId];
        var data = {
            _FamilienId: elternInfo.familyData.eltern.FamilienId,
            type: "buecher",
            klassenStufe: button._liste.klassenStufe
        };
        elternInfo.lmf_postService(
                data,
                function(response) {
                    console.log("Buecher:");
                    console.log(response.data);
                    var style = elternInfo.style.tableBuecherListen;
                    if (layoutBuecher === null) {
                        layoutBuecher = [{
                                defaultCell: {
                                    editable: true,
                                    styles: 'text-align: left;'
                                },
                                cells: [{
                                        field: 'Isbn',
                                        name: 'Isbn',
                                        width: style.ISBN.width
                                    }, {
                                        field: 'Titel',
                                        name: 'Titel',
                                        width: style.Titel.width
                                    }, {
                                        field: 'Neupreis',
                                        name: 'Neupreis',
                                        width: style.Neupreis.width,
                                        styles: style.Neupreis.styles,
                                        formatter: formatterRound2
                                    }, {
                                        field: 'von',
                                        name: 'von',
                                        width: style.vonBis.width,
                                        styles: style.vonBis.styles
                                    }, {
                                        field: 'bis',
                                        name: 'bis',
                                        width: style.vonBis.width,
                                        styles: style.vonBis.styles
                                    }, {
                                        field: '_item',
                                        name: " + ",
                                        width: style.checked.width,
                                        editable: false,
                                        formatter: formatterBuchHinzuFuegen,
                                        headerStyles: style.checked.styles
                                    }]
                            }];
                    }

                    var _items = [];
                    for (var i = 0; i < response.data.length; i++) {
                        _items[i] = dojo.clone(response.data[i]);
                        _items[i].Neupreis = 1 * _items[i].Neupreis;
                        _items[i].von = 1 * _items[i].von;
                        _items[i].bis = 1 * _items[i].bis;
                    }
                    var buecherData = {
                        items: _items,
                        identifier: 'BuchId'
                    };
                    buecherStore = new dojo.data.ItemFileWriteStore({
                        data: buecherData
                    });
                    buecherStore._forceLoad();
                    dojo.connect(buecherStore, "onSet", function(item, param, oldValue, newValue) {
                        if (oldValue !== newValue) {
                            buchSpeichern(item);
                        }
                    });
                    var myGrid5 = new dojox.grid.DataGrid({
                        store: buecherStore,
                        selectable: true,
                        clientSort: true,
                        autoRender: true,
                        structure: layoutBuecher,
                        autoWidth: true,
                        autoHeight: true
                    },
                    document.createElement('div')
                            );
                    dojo.byId("gridAlleBuecher").appendChild(myGrid5.domNode);
                    myGrid5.startup();
                    new dijit.Tooltip({
                        connectId: [myGrid5.getCell(5).getHeaderNode()],
                        label: 'Buch der Liste hinzufügen'
                    });
                    console.log(myGrid5);
                }
        );
    }

    function buchSpeichern(buch) {
        var data = {
            "type": "buchSpeichern",
            _FamilienId: elternInfo.familyData.eltern.FamilienId,
            Isbn: buch.Isbn[0],
            Titel: buch.Titel[0],
            von: buch.von[0],
            bis: buch.bis[0],
            Neupreis: buch.Neupreis[0],
            BuchId: buch.BuchId[0]
        };
        elternInfo.lmf_postService(
                data,
                function(response) {
                    console.log("buchSpeichern OK:");
                    console.log(response);
                    if (buch.BuchId[0] === -1) {
                        buch.BuchId = response.id;
                        buecherStore.newItem(buch);
                    }
                }
        );
    }

    elternInfo.rufeNaechsteFunktion();

    return elternInfo;
})(window.elternInfo || {});
