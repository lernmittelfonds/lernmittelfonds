dojo.provide("lmf.FileUploader");
dojo.require("dojox.form.FileUploader");
dojo.declare("lmf.FileUploader", [dojox.form.FileUploader], {
	upload: function(/*Object ? */data){
		// summary:
		// 	When called, begins file upload
		//	data: Object
		//	postData to be sent to server
		//
                /*
		if(!this.fileList.length){
			return false;
		}
                */ //upload only form
		if(!this.uploadUrl){
			console.warn("uploadUrl not provided. Aborting.");
			return false;
		}
		if(!this.showProgress){
			this.set("disabled", true);
		}

		if(this.progressWidgetId){

			var node = dijit.byId(this.progressWidgetId).domNode;
			if(dojo.style(node, "display") == "none"){
				this.restoreProgDisplay = "none";
				dojo.style(node, "display", "block");
			}
			if(dojo.style(node, "visibility") == "hidden"){
				this.restoreProgDisplay = "hidden";
				dojo.style(node, "visibility", "visible");
			}
		}

		if(data && !data.target){
			this.postData = data;
		}
		this.log("upload type:", this.uploaderType, " - postData:", this.postData);
                
                if(!this.fileList.length){
                    for (var i = 0; i < this.fileList.length; i++){
                            var f = this.fileList[i];
                            f.bytesLoaded = 0;
                            f.bytesTotal = f.size || 100000;
                            f.percent = 0;
                    }
                }
		if(this.uploaderType == "flash"){
			this.uploadFlash();
		}else{
			this.uploadHTML();
		}
		// prevent form submit
		return false;
	}
});
