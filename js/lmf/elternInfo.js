window.elternInfo = (function (elternInfo) {

    elternInfo.loadJs = function (filename) {
        var snode = document.createElement('script');
        snode.setAttribute('type', 'text/javascript');
        snode.setAttribute('src', elternInfo.root + "js/lmf/" + filename + ".js?" + new Date().getTime());
        document.getElementsByTagName('head')[0].appendChild(snode);
    };

    /*
     * macht Fehler div sichtbar und schreibt "error" hinein
     * @param: error : Array mit Fehlertexten
     */
    elternInfo.displayErrors = function (errors) {
        var errorString = "<ol>";
        for (var i = 0; i < errors.length; i++) {
            errorString += "<li>" + errors[i] + "</li>\n";
        }
        errorString += "</ol>";
        dojo.byId("lmf_error").innerHTML = errorString;
        dojo.byId("lmf_error").style.display = "block";
    }
    ;

    /*
     * macht Fehler div unsichtbar
     */
    elternInfo.hideErrors = function () {
        if (dojo.byId("lmf_error")) {
            dojo.byId("lmf_error").style.display = "none";
            dojo.byId("lmf_info").style.display = "none";
        }
    };

    /*
     * macht Info div sichtbar und schreibt "text" hinein
     * @param: text Infotext
     */
    elternInfo.displayInfo = function (text) {
        dojo.byId("lmf_info").innerHTML = text;
        dojo.byId("lmf_info").style.display = "block";
    };

    /*
     * macht Info div sichtbar und schreibt "text" hinein
     * @param: text Infotext
     */
    elternInfo.addInfo = function (text) {
        dojo.byId("lmf_info").innerHTML = dojo.byId("lmf_info").innerHTML+text;
        dojo.byId("lmf_info").style.display = "block";
    };

    /*
     * überprüft, ob der Nutzer bereits mit einer gültigen session am server angemeldet ist
     * ruft im OK Fall die seite mit den stammdaten des Nutzers (Home) auf
     * ruft im NOK-Fall die Login-Seite auf
     */
    elternInfo.checkLogin = function () {
        console.log("checkLogin start");
        dojo.xhrGet({
            url: elternInfo.root + "php/login.php?" + new Date().getTime(),
            preventCache: true,
            handleAs: "json",
            load: function (data) {
                console.log(data);
                if (data.loginOk) {
                    elternInfo.nurLmf = data.nurLmf;
                    elternInfo.istEV = data.istEV;
                    elternInfo.istAdmin = data.isAdmin || false;
                    elternInfo.altAnmeldenGestoppt = data.anmeldenGestoppt;
                    elternInfo.neuAnmeldenGestoppt = data.neuAnmeldenGestoppt;
                    elternInfo.anmeldenFreigegeben = data.anmeldenFreigegeben;
                    elternInfo.initial = data.initial;
                    if (elternInfo.appType == "eis") {
                        elternInfo.ladeFamilienseiteEis();
                    } else {
                        elternInfo.ladeStartSeite();
                    }
                } else {
                    elternInfo.loginSeiteLaden();
                }
            },
            error: function (data) {
                console.log(data);
                console.log("check login failed");
                elternInfo.displayErrors(["check login failed."]);
            }
        });
    };
    elternInfo.loginSeiteLaden = function (loginReason, isError) {
        console.log("loginSeiteLaden");
        dojo.xhrGet({
            url: elternInfo.root + "pages/login.htm?" + new Date().getTime(),
            preventCache: true,
            handleAs: "text",
            load: function (response) {
                dojo.byId("lmfContent").innerHTML = response;
                console.log("fertig callLoginSite");
                if (loginReason !== undefined) {
                    if (isError) {
                        elternInfo.displayErrors(loginReason);
                    } else {
                        elternInfo.displayInfo(loginReason);
                    }
                }
                elternInfo.onLoadLogin();
            },
            error: function (response, ioArgs) {
                console.log(response);
                console.log("Form submission failed.");
                elternInfo.displayErrors(["Form submission failed."]);
            }
        });
    };
    elternInfo.aktivierungsSeiteLaden = function (loginReason, isError) {
        console.log("aktivierungsSeiteLaden");
        dojo.xhrGet({
            url: elternInfo.root + "pages/aktivierung.htm?" + new Date().getTime(),
            preventCache: true,
            handleAs: "text",
            load: function (response) {
                dojo.byId("lmfContent").innerHTML = response;
                console.log("fertig aktivierungsSeiteLaden");
                if (loginReason !== undefined) {
                    if (isError) {
                        elternInfo.displayErrors(loginReason);
                    } else {
                        elternInfo.displayInfo(loginReason);
                    }
                }
                elternInfo.onLoadAktivierung();
            },
            error: function (response, ioArgs) {
                console.log(response);
                console.log("Form submission failed.");
                elternInfo.displayErrors(["Form submission failed."]);
            }
        });
    };

    elternInfo.passwort_aendern_laden = function () {
        console.log("passwort_aendern laden");
        dojo.xhrGet({
            url: elternInfo.root + "pages/changePW.htm?" + new Date().getTime(),
            handleAs: "text",
            load: function (response, ioArgs) {
                dojo.byId("lmfContent").innerHTML = response;
                console.log("fertig call passwort_aendern Site");
                var form = dojo.byId("changePwForm");
                dojo.connect(form, "onsubmit", function (event) {
                    dojo.stopEvent(event);
                    elternInfo.hideErrors();
                    passwort_aendern();
                });
            },
            error: function (response, ioArgs) {
                console.log(response);
                console.log("Form submission failed.");
                elternInfo.displayErrors(["Form submission failed."]);
            }
        });
    };

    function passwort_aendern() {
        if (dojo.byId("newPasswort").value === dojo.byId("newPasswort2").value) {
            elternInfo.lmf_save({
                type: "passwort_aendern",
                FamilienId: elternInfo.familyData.eltern.FamilienId,
                oldPasswort: dojo.byId("oldPasswort").value,
                newPasswort: dojo.byId("newPasswort").value
            },
                    function () {
                        elternInfo.ladeStartSeite();
                    }
            );
        } else {
            elternInfo.displayErrors(["Sie haben für das neue Passwort unterschiedliche Eingaben gemacht!"]);
        }
    }

    elternInfo.passwort_vergessen = function () {
        dojo.byId("lmf_PW").style.display = "none";
        dojo.byId("lmf_PW_hint").innerHTML = "<br>Bitte tragen Sie ihre registrierte eMail-Adresse ein und senden das Formular ab. "
                + "Sollte die Adresse bei uns registriert sein, erhalten sie in kürze eine Mail mit einem neuen Passwort ... oder Sie gehen direkt <a href='javascript:elternInfo.loginSeiteLaden();'>zurück zur Anmeldung</a>";
        dojo.byId("PW-vergessen").value = "TRUE";
        dojo.byId("lmfLoginButton").value = "Passwort anfordern";
    };

    function onLoadRegister() {
        var form = dojo.byId("registerForm");
        dojo.connect(form, "onsubmit", function (event) {
            dojo.stopEvent(event);
            var pw1 = dojo.byId("Passwort").value;
            var pw2 = dojo.byId("Passwort2").value;
            if (pw1 === pw2) {
                elternInfo.hideErrors();
                var xhrArgs = {
                    form: dojo.byId("registerForm"),
                    url: elternInfo.root + "php/register.php",
                    handleAs: "json",
                    load: function (data) {
                        console.log(data);
                        if (data.errors.length > 0) {
                            elternInfo.displayErrors(data.errors);
                        } else {
                            var successMessage = "Vielen Dank für Ihre Anmeldung! Eine Email zur Bestätigung wird in Kürze an Ihre Mailadresse versandt. Diese enthält einen Aktivierungscode."
                                    + " Bitte melden Sie sich anschliessend mit Ihrer Emailadresse und Ihrem selbstgewähltem Passwort an unserer Anwendung an. Sie werden dann einmalig zur Eingabe des Aktivierungscodes aufgefordert. Ergänzen Sie anschliessend die Daten Ihres Kindes / Ihrer Kinder.<br>"
                                    + "Sollten Sie innerhalb der nächsten 5 Minten keine Mail erhalten, "
                                    + "überprüfen Sie bitte noch einmal die eingegebene Email-Adresse und wenn möglich Ihren SPAM-Ordner.<br>"
                                    + "Wenn die Mail korrekt und der Spamordner leer ist, setzen Sie sich bitte mit uns per Mail in Verbindung.";
                            elternInfo.displayInfo(successMessage);
                        }
                    },
                    error: function (error) {
                        console.log(error);
                        elternInfo.displayErrors([error]);
                    }
                };
                dojo.xhrPost(xhrArgs);
            } else {
                console.log("Ihre beiden Passworte sind verschieden. Bitte korrigieren Sie die Werte.");
                elternInfo.displayErrors(["Ihre beiden Passworte sind verschieden. Bitte korrigieren Sie die Werte."]);
            }
        });
    }

    elternInfo.logout = function () {
        elternInfo.hideErrors();
        var xhrArgs = {
            url: elternInfo.root + "php/logout.php?" + new Date().getTime(),
            handleAs: "json",
            headers: {"Content-Type": "application/json; charset=utf-8"},
            load: function (data) {
                console.log("logout Success: " + data);
                elternInfo.loginSeiteLaden();
            },
            error: function (error) {
                console.log("logout Error :" + error);
                elternInfo.displayErrors([error]);
            }
        };
        dojo.xhrGet(xhrArgs);
    };

    elternInfo.registrieren = function () {
        console.log("registrieren");
        elternInfo.hideErrors();
        dojo.xhrGet({
            url: elternInfo.root + "pages/register.htm?" + new Date().getTime(),
            handleAs: "text",
            load: function (response) {
                dojo.byId("lmfContent").innerHTML = response;
                console.log("fertig call register site");
                onLoadRegister();
            },
            error: function (response) {
                console.log(response);
                console.log("call register site failed.");
                elternInfo.displayErrors(["call register site failed."]);
            }
        });
    };
    elternInfo.initCodeNeu = function () {
        elternInfo.hideErrors();
        dojo.byId("initCodeNeu").value = "TRUE";
        var xhrArgs = {
            form: dojo.byId("aktivierungsForm"),
            url: elternInfo.root + "php/login.php",
            handleAs: "json",
            load: function (data) {
                if (data.errors.length > 0) {
                    elternInfo.displayErrors(data.errors);
                } else {
                    console.log("neuer InitCode erfolgreich versendet!");
                    elternInfo.displayInfo("neuer Aktivierungscode erfolgreich versendet!");
                }
            },
            error: function (error) {
                console.log("initCodeNeu Error :" + error);
                elternInfo.displayErrors([error]);
            }
        };
        dojo.xhrPost(xhrArgs);
    }

    elternInfo.ladeFamilienseiteEis = function (FamilienId) {
        console.log("ladeFamilienseiteEis");
        var _FamilienId = FamilienId;
        elternInfo.hideErrors();
        dojo.xhrGet({
            url: elternInfo.root + "pages/rahmen.htm?" + new Date().getTime(),
            handleAs: "text",
            load: function (response) {
                dojo.byId("lmfContent").innerHTML = response;
                dojo.xhrGet({
                    url: elternInfo.root + "pages/familie.htm?",
                    preventCache: true,
                    handleAs: "text",
                    load: function (response) {
                        dojo.byId("lmf_applikation").innerHTML = response;
                        elternInfo.getFamilyData(_FamilienId, false, true);
                        dojo.byId("lmf_freigabeDaten").style.display = "block";
                        //dojo.byId("lmf_serviceMenue").style.display = "none";
                        dojo.byId("lmf_schuelerGrid").innerHTML = "Schüler und Klassen";
                        dojo.byId("lmf_WechselApplikation").innerHTML = "zum Lernmittelfonds";
                        dojo.byId("lmfTitel").innerHTML = "ElternInfoSystem (EIS)";
                        setTimeout(elternInfo.checkUndStartMailJobs(), 2000);
                        console.log ("trigger elternInfo.checkUndStartMailJobs");
                    },
                    error: function (response) {
                        console.log(response);
                        console.log("ladeFamilienseiteEis failed.");
                        elternInfo.displayErrors(["ladeFamilienseiteEis failed."]);
                    }
                });
            },
            error: function (response) {
                console.log(response);
                console.log("ladeFamilienseiteEis failed.");
                elternInfo.displayErrors(["ladeFamilienseiteEis failed."]);
            }
        });
    }
    elternInfo.lmf_save = function (data, onSuccess) {
        elternInfo.hideErrors();
        var xhrArgs = {
            url: elternInfo.root + "php/saveData.php",
            postData: dojo.toJson(data),
            handleAs: "json",
            headers: {"Content-Type": "application/json; charset=utf-8"},
            load: function (data) {
                console.log("lmf_save Succes" + data);
                if (!data.loggedIn) {
                    elternInfo.displayErrors(["Session ungültig. Bitte melden sie sich neu an"]);
                    elternInfo.loginSeiteLaden(["Session ungültig. Bitte melden sie sich neu an"], true);
                } else if (data.errors && data.errors !== "") {
                    elternInfo.displayErrors(data.errors);
                } else if (onSuccess) { // erfolgreich mit oder ohne onSuccess Callback
                    onSuccess(data);
                }
            },
            error: function (error) {
                console.log("lmf_save Error :" + error);
                elternInfo.displayErrors([error]);
            }
        };
        dojo.xhrPost(xhrArgs);
    };



    return elternInfo;
})(window.elternInfo || {});
