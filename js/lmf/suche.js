window.elternInfo = (function(elternInfo) {

    elternInfo.sucheAnzeigen = function() {
        dojo.xhrGet({
            url: elternInfo.root + "pages/suche.htm?",
            preventCache: true,
            handleAs: "text",
            load: function(response) {
                dojo.byId("lmf_applikation").innerHTML = response;
                console.log("fertig callSuchSeite");
            },
            error: function(response, ioArgs) {
                console.log(response);
                elternInfo.displayErrors(["pages/suche.htm konnte nicht geladen werden"]);
            }
        });
    };

    elternInfo.starteSuche = function(evt) {
        dojo.stopEvent(evt);
        var postData = {
            "type": "suchen",
            suchZiel: dojo.byId("lmf_SuchFeld").value
        };
        elternInfo.lmf_postService(postData,
                function(result) {
                    zeigeSuchergebnis(result.data);
                }
        );
        return false; // submit verhindern!
    };

    var suchErgebnisStore = null;
    var suchErgebnisGrid = null;
    var layoutSuchErgebnisGrid = null;
    function formatterEltern(row) {
        return '<a onClick="elternInfo.ladeStartSeiteAdmin(' + row.FamilienId + ')">' + row.Eltern + '</a>';
    }
    function formatterSchueler(row) {
        return '<a onClick="elternInfo.ladeStartSeiteAdmin(' + row.FamilienId + ')">' + row.Schueler + '</a>';
    }

    function zeigeSuchergebnis(schueler) {
        console.log(schueler);
        var data = {
            identifier: 'ID',
            items: dojo.clone(schueler)
        };
        suchErgebnisStore = new dojo.data.ItemFileWriteStore({data: data});
        suchErgebnisStore._forceLoad();
        if (true || layoutSuchErgebnisGrid === null) {
            var styles=elternInfo.style.tableSucheErgebnis;
            layoutSuchErgebnisGrid = [
                {defaultCell: {editable: false, styles: styles.default.styles},
                    cells: [
                        {field: 'check', name: '&nbsp;', width: styles.check.width, styles: styles.check.styles, editable: true, type: dojox.grid.cells.Bool},
                        {field: '_item', name: "Mutter / Vater", formatter: formatterEltern, width: styles.Eltern.width},
                        {field: '_item', name: "Schueler", formatter: formatterSchueler, width: styles.Schueler.width},
                        {field: 'KlasseAlt', name: 'Klasse ' + (elternInfo.familyData.thisYear) + '/' + (1 + elternInfo.familyData.thisYear - 2000), width: styles.KlasseAlt.width},
                        {field: 'KlasseNeu', name: 'Klasse ' + (1 + elternInfo.familyData.thisYear) + '/' + (2 + elternInfo.familyData.thisYear - 2000), width: styles.KlasseNeu.width}
                    ]}
            ];
            console.log(suchErgebnisStore);
            suchErgebnisGrid = new dojox.grid.DataGrid({
                store: suchErgebnisStore,
                selectable: true,
                singleClickEdit: true,
                clientSort: true,
                autoRender: true,
                structure: layoutSuchErgebnisGrid,
                autoWidth: true,
                autoHeight: true
            },
            document.createElement('div')
                    );
            // append the new grid to the div "lmfSuchergebnis":
            dojo.byId("lmfSuchergebnis").innerHTML = "";
            dojo.byId("lmfSuchergebnis").appendChild(suchErgebnisGrid.domNode);
            // Call startup, in order to render the grid:
            suchErgebnisGrid.startup();
            var buttonZusammenfuehren = new dijit.form.Button({
                label: "zusammenführen",
                title: "Öffnet eine Maske um zwei Einträge eines Schülers zusammen zuführen",
                onClick: function() {
                    maskeZusammenfuehren();
                }
            },
            document.createElement('div')
                    );
            dojo.byId("lmfSuchergebnis").appendChild(buttonZusammenfuehren.domNode);
        } else {
            suchErgebnisGrid.setStore(suchErgebnisStore);
        }
//suchErgebnisGrid.render();
        console.log(suchErgebnisGrid);
    }

    function maskeZusammenfuehren() {
        var gefunden1 = -1;
        var gefunden2 = -1;
        var mehrAls2Markiert = false;
        suchErgebnisStore.fetch({
            onComplete: function(items) {
                dojo.forEach(items, function(item) {
                    if (suchErgebnisStore.isItem(item)) {
                        if (suchErgebnisStore.getValue(item, "check")) {
                            if (gefunden1 == -1) {
                                gefunden1 = suchErgebnisStore.getValue(item, "SchuelerId");
                            } else if (gefunden2 == -1) {
                                gefunden2 = suchErgebnisStore.getValue(item, "SchuelerId");
                            } else {
                                mehrAls2Markiert = true;
                            }
                        }
                    }
                });
            }
        });
        if (mehrAls2Markiert) {
            alert("Sie können nur Daten von 2 Schülern zusammenführen. Bitte wählen Sie genau 2 Datensätze aus!");
        } else if (gefunden2 == -1) {
            alert("Bitte wählen Sie genau 2 Datensätze aus, um diese zusammenzuführen!");
        } else {
            var postData = {
                "type": "zusammenfuehrenVorbereiten",
                schuelerId1: gefunden1,
                schuelerId2: gefunden2
            };
            elternInfo.lmf_postService(postData,
                    function(result) {
                        maskeZusammenfuehrenAnzeigen(result.data);
                    }
            );
        }
    }

    function maskeZusammenfuehrenAnzeigen(daten) {
        console.log(daten);
        var id1 = daten.schueler[0].SchuelerId;
        var id2 = daten.schueler[1].SchuelerId;
        var jahre = [];
        var _html = "<br><form><div><b>2 Datensätze zusammenführen</b></div>";
        _html += '<table class="lmf_table1" width="100%">';
        _html += "<tr>";
        _html += '    <td class="lmfZusammenFuehren">' + elternText(1, daten.schueler[0]) + "</td>";
        _html += '    <td class="lmf_radio"> <input type="radio" name="lmf_elternZiel" value="' + id1 + '"> &nbsp;';
        _html += '    <input type="radio" name="lmf_elternZiel" value="' + id2 + '"></td>';
        _html += '    <td class="lmfZusammenFuehren">' + elternText(2, daten.schueler[1]) + "</td>";
        _html += "</tr>";
        _html += "<tr>";
        _html += '    <td class="lmfZusammenFuehren">' + schuelerText(daten.schueler[0]) + "</td>";
        _html += '    <td class="lmf_radio"> <input type="radio" name="lmf_schuelerZiel" value="' + id1 + '"> &nbsp;';
        _html += '    <input type="radio" name="lmf_schuelerZiel" value="' + id2 + '"></td>';
        _html += '    <td class="lmfZusammenFuehren">' + schuelerText(daten.schueler[1]) + "</td>";
        _html += "</tr>";
        _html += "<tr>";
        _html += '    <td class="lmfZusammenFuehren">' + buecherText(id1, daten.buecher) + "</td>";
        _html += '    <td class="lmf_radio"> &nbsp;</td>';
        _html += '    <td class="lmfZusammenFuehren">' + buecherText(id2, daten.buecher) + "</td>";
        _html += "</tr>";
        var a = daten.anmeldungen;
        for (var i = 0; i < a.length; i++) {
            jahre.push(a[i].Schuljahr);
            _html += "<tr>";
            if (a[i].SchuelerId == id1) {
                _html += '    <td>' + klasseUndTeilnahme("left", a[i]) + "</td>";
                _html += '    <td class="lmf_radio"> <input type="radio" name="lmf_anmeldungZiel' + a[i].Schuljahr + '" value="' + id1 + '"> ' + a[i].Schuljahr;
                if (i + 1 < a.length && a[i + 1].SchuelerId == id2 && a[i].Schuljahr == a[i + 1].Schuljahr) {
                    _html += '    <input type="radio" name="lmf_anmeldungZiel' + a[i + 1].Schuljahr + '" value="' + id2 + '"></td>';
                    _html += '    <td align="right">' + klasseUndTeilnahme("right", a[i + 1]) + "</td><tr>";
                    i++;
                } else {
                    _html += '    <input disabled type="radio" name="lmf_anmeldungZiel' + a[i].Schuljahr + '" value="' + id2 + '"></td>';
                    _html += '    <td>&nbsp;</td><tr>';
                }
            } else {
                _html += '    <td>&nbsp;</td>';
                _html += '    <td class="lmf_radio"> <input disabled type="radio" name="lmf_anmeldungZiel' + a[i].Schuljahr + '" value="-1"> ' + a[i].Schuljahr;
                _html += '    <input type="radio" name="lmf_anmeldungZiel' + a[i].Schuljahr + '" value="' + id2 + '"></td>';
                _html += '    <td align="right">' + klasseUndTeilnahme("right", a[i]) + "</td><tr>";
            }
        }
        _html += "</table>";
        _html += '<p><div id="lfm_buttonZusammenFuehren"></div>';
        _html += "</form>";
        dojo.byId("lmf_applikation").innerHTML = "";
        dojo.byId("lmf_applikation").innerHTML = _html;
        var buttonZusammenfuehren = new dijit.form.Button({
            label: "zusammenführen",
            title: "übernimmt die ausgewählten Anmeldungen und ale Bücher für den ausgewählten Schüler und löscht den anderen Schüler mit den restlichen Anmeldungen.",
            onClick: function() {
                zusammenfuehren(id1, id2, jahre, false);
            }
        },
        document.createElement('div')
                );
        dojo.byId("lfm_buttonZusammenFuehren").appendChild(buttonZusammenfuehren.domNode);
        var buttonBuecherUbertragen = new dijit.form.Button({
            label: "nur Buecher",
            title: "Überträgt alle Bücher auf den ausgewählten Schüler. Die anderen Datensätze bleiben bestehen",
            onClick: function() {
                zusammenfuehren(id1, id2, jahre, true);
            }
        },
        document.createElement('div')
                );
        dojo.byId("lfm_buttonZusammenFuehren").appendChild(buttonBuecherUbertragen.domNode);
    }

    function elternText(nr, datensatz) {
        return "<b>" + nr + ". Erziehungsberechtigte(r):</b><br>"
                + datensatz.Vorname + " " + datensatz.Nachname + "<br>"
                + datensatz.Telefon + "<br>"
                + datensatz.Email;
    }
    function schuelerText(datensatz) {
        return "Schüler(in):<br>"
                + datensatz.sVorname + " " + datensatz.sNachname;
    }
    function buecherText(id, buecher) {
        text = "";
        for (var i = 0; i < buecher.length; i++) {
            if (buecher[i].SchuelerId == id) {
                text += (1 == buecher[i].eingesammelt) ? elternInfo.bilder.eingesammelt : elternInfo.bilder.ausgeliehen + " " + buecher[i].Titel + "<br>";
            }
        }
        return text;
    }

    function klasseUndTeilnahme(align, datensatz) {
        var ret = "<div>";
        ret += (1 * datensatz.Schuljahr - 1 * datensatz.StartJahr + 1 * datensatz.StartKlassenStufe) + datensatz.SubKlasse + "-" + datensatz.Sprache;
        ret += '<hr class="lmf_gridLinkLine_' + align + '">';
        if (datensatz.bezahlt == "1" || datensatz.bezahlt == "2") {
            ret += "nimmt teil";
        } else if (datensatz.bezahlt == "0") {
            ret += "angemeldet/nicht bezahlt";
        } else {
            ret += "nicht angemeldet";
        }
        ret += "</div>";
        return ret;
    }

    function zusammenfuehren(id1, id2, jahre, nurBuecher) {

        var postData = {
            "type": "zusammenfuehrenAusfuehren",
            id1: id1,
            id2: id2,
            nurBuecher: nurBuecher,
            elternZiel: radioButtonWert("lmf_elternZiel"),
            schuelerZiel: radioButtonWert("lmf_schuelerZiel"),
            jahre: jahre.length
        };
        for (var i = 0; i < jahre.length; i++) {
            postData["jahr_" + i] = jahre[i];
            postData["auswahl_" + i] = radioButtonWert("lmf_anmeldungZiel" + jahre[i]);
        }
        elternInfo.lmf_postService(postData,
                function() {
                    if (nurBuecher){
                        elternInfo.displayInfo("Buecher erfolgreich verschoben.");
                    }else{
                        elternInfo.displayInfo("Datensätze erfolgreich vereinigt.");
                    }
                    dojo.byId("lmf_applikation").innerHTML = "";
                }
        );
    }

    function radioButtonWert(feldname) {
        var feld = document.getElementsByName(feldname);
        for (var i = 0; i < feld.length; i++) {
            if (feld[i].checked == true) {
                return feld[i].value;
            }
        }
        alert("Bitte Wählen Sie in jeder zeil aus, welchen Wert Sie benutzen wollen!");
        throw error("Kein Wert für " + feld + " ausgewählt");
    }

    elternInfo.sucheAnzeigen();

    return elternInfo;
})(window.elternInfo || {});


