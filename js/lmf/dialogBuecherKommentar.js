window.elternInfo = (function(elternInfo) {

    dojo.require("dijit.form.Button");
    dojo.require("dijit.Dialog");
    dojo.require("dijit.form.Form");
    dojo.require("dijit.form.Button");
    dojo.require("dijit.form.Textarea");

    elternInfo.dialogBuecherKommentarOeffnen = function(page) {
        elternInfo.dialogBuecherKommentar.page = page;
        elternInfo.dialogBuecherKommentar.show();        
    };
    elternInfo.dialogBuecherKommentarSchliessen = function() {
        elternInfo.dialogBuecherKommentar.hide();
    };

    

    elternInfo.dialogBuecherKommentar = new dijit.Dialog({
        title: "&nbsp;",
        href: elternInfo.root + "pages/buecherKommentarDialog.htm",
        style: elternInfo.style.dialogAusleihenUndKommentar.style
    });
    
    elternInfo.dialogOnLoadFunction();

    return elternInfo;
})(window.elternInfo || {});
