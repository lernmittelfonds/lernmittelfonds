window.elternInfo = (function(elternInfo) {

    dojo.require("dijit.form.Button");
    dojo.require("dijit.Dialog");
    dojo.require("dijit.form.Form");
    dojo.require("dijit.form.Button");
    dojo.require("dijit.form.Textarea");

    elternInfo.dialogKommentarOeffnen = function(id, initial) {
        console.log(elternInfo.getSchüler(id));
        dialogKommentar.schueler = elternInfo.getSchüler(id);
        dialogKommentar.show();
        if (!initial) {
            fuelleDialogFormular();
        }
    };
    elternInfo.dialogKommentarSchliessen = function() {
        dialogKommentar.hide();
    };

    function fuelleDialogFormular() {
        document.getElementById("lmfKommentarLabel").innerHTML = "Kommentar zu " + dialogKommentar.schueler.Vorname + " " + dialogKommentar.schueler.Nachname;
        document.getElementById("lmfSchuelerId").value = dialogKommentar.schueler.SchuelerId;
        document.getElementById("lmfKommentar").value = dialogKommentar.schueler.Kommentar[0].replace(/<br>/g, "\n");
    }

    elternInfo.speichernNOK = function(data) {
        console.log("ERROR Kommentar speichern");
        console.log(data);
        elternInfo.displayErrors(["unbekannter Fehler beim Speichern des Kommentars"]);
        elternInfo.dialogKommentarSchliessen();
    };
    elternInfo.speichernOK = function(data) {
        if (data.success) {
            console.log(data);
            dojo.byId("lmf_info").innerHTML = "Der Kommentar zum Schüler wurde erfolgreich gespeichert";
            dojo.byId("lmf_info").style.display = "block";
            dialogKommentar.schueler.Kommentar[0] = document.getElementById("lmfKommentar").value.replace(/\n/g, "<br>");
            elternInfo.schuelerGrid.render();
        } else {
            elternInfo.displayErrors(data.errors);
        }
        elternInfo.dialogKommentarSchliessen();
    };

    var dialogKommentar = new dijit.Dialog({
        title: "&nbsp;",
        href: elternInfo.root + "pages/kommentarDialog.htm",
        style: elternInfo.style.dialogAusleihenUndKommentar.style
    });
    dojo.connect(dialogKommentar,
            "onDownloadEnd",
            fuelleDialogFormular
            );

    elternInfo.dialogOnLoadFunction();

    return elternInfo;
})(window.elternInfo || {});
