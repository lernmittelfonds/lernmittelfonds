window.elternInfo = (function(elternInfo) {
    elternInfo.style = {};
    /* Dieses File enthält Styles für das dojo-Framework, die dirket in den Code eingebunden werden müssen */
    /* Faustregel für Tabellen: Breite (z.B. 470)-20px == Summe aller Spalten + Spaltenanzahl x8           */
    /* die x8 resultieren aus einem CellPadding von je 3px links und rechts und einem Rand von je 1px      */
    /* LMF-Hauptseite Start*/
    /* LMF Schülertabelle*/
    elternInfo.style.tableSchuelerLmf = {
        mehr: {
            /*          width: '23px', /* für 470px Breite */
            width: '35px', /* für 470px Breite */
            style: 'text-align: center'
        },
        Vorname: {
            /*            width: "82px" /* für 470px Breite */
            width: "105px" /* für 470px Breite */
        },
        Nachname: {
            /*            width: "82px" /* für 470px Breite */
            width: "105px" /* für 470px Breite */
        },
        alteKlassenId: {
            /*            width: "65px" /* für 470px Breite */
            width: "105px" /* für 470px Breite */
        },
        KlassenId: {
            /*            width: "65px" /* für 470px Breite */
            width: "105px" /* für 470px Breite */
        },
        Aktionen: {
            /*           width: "65px", /* für 470px Breite */
            width: "90px", /* für 470px Breite */
            styles: 'text-align: center;'
        },
        istEV: {
            /*            width: "28px", /* für 470px Breite */
            width: "40px", /* für 470px Breite */
            styles: 'text-align: center;'
        }
    };

    /* LMF Schülertabelle Dialoge für ausleihen und Kommentare*/
    elternInfo.style.dialogAusleihenUndKommentar = {
        /*        style: "width: 350px;" */
        style: "width: 450px;"
    };
    /* LMF-Hauptseite Ende*/

    /* EIS-Hauptseite Start*/
    /* EIS Schülertabelle*/
    elternInfo.style.tableSchuelerEis = {
        Vorname: {
            /*            width: "90px" /* für 470px Breite */
            width: "105px"
        },
        Nachname: {
            /*            width: "90px" /* für 470px Breite */
            width: "105px"
        },
        alteKlassenId: {
            /*            width: "65px" /* für 470px Breite */
            width: "105px"
        },
        KlassenId: {
            /*            width: "65px" /* für 470px Breite */
            width: "105px"
        },
        Aktionen: {
            /*            width: "75px" /* für 470px Breite */
            width: "90px",
            styles: 'text-align: center;'
        },
        istEV: {
            /*            width: "28px" /* für 470px Breite */
            width: "40px",
            styles: 'text-align: center;'
        }
    };
    /* Eis Klassenliste für EV bei Klick auf "freischalten"*/
    elternInfo.style.tableKlasseFuerEV = {
        Eltern: {
            width: "109px"
        },
        Schueler: {
            width: "109px"
        },
        Telefon: {
           width: "90px"
        },
        Email: {
           width: "183px"
        },
        Klasse: {
            width: "43px"
        },
        EisStatus: {
             width: "40px"
        }
    };
    /* EIS-Hauptseite Ende*/

    /* Menue->Nutzer: Start*/
    /* Menue->Nutzer->Suche: Suchergebnis*/
    elternInfo.style.tableSucheErgebnis = {
        default: {
            styles: 'text-align: left;'
        },
        check: {
            /*    width: "20px", /* für 470px Breite */
            width: "40px",
            styles: 'text-align: center;'
        },
        Eltern: {
            /*      width: "150px" /* für 470px Breite */
            width: "160px"
        },
        Schueler: {
            /*        width: "150px" /* für 470px Breite */
            width: "160px"
        },
        KlasseAlt: {
            /*          width: "55px" /* für 470px Breite */
            width: "130px"
        },
        KlasseNeu: {
            /*            width: "55px" /* für 470px Breite */
            width: "130px"
        }
    };
    
    /* Menue->Nutzer->Suche: Suchergebnis*/
    elternInfo.style.tableElternvertreter = {
        default: {
            styles: 'text-align: left;'
        },
        Klasse: {
            width: "100px"
        },
        Name: {
            width: "160px"
        },
        Telefon: {
            width: "120px"
        },
        Email: {
            width: "180px"
        },
        Loeschen: {
            width: "30px",
            styles: 'text-align: center;'
        }
    };
    /* Menue->Nutzer: Ende*/

    /* Menue->Verwaltung: Start*/
    
    /* Menue->Verwaltung->Klassen*/
    elternInfo.style.tableKlassenListe = {
        default: {
            styles: 'text-align: left;'
        },
        KlassenStufeAlt: {
            /* width: "60px" /* für 470px Breite */
            width: "80px"
        },
        KlassenStufeNeu: {
            /* width: "60px" /* für 470px Breite */
            width: "80px"
        },
        StartJahr: {
            /* width: "40px" /* für 470px Breite */
            width: "60px"
        },
        StartKlassenStufe: {
            /* width: "45px" /* für 470px Breite */
            width: "82px"
        },
        SubKlasse: {
            /* width: "30px" /* für 470px Breite */
            width: "50px"
        },
        Sprache: {
            /* width: "55px" /* für 470px Breite */
            width: "80px"
        },
        EndKlassenStufe: {
            /* width: "45px" /* für 470px Breite */
            width: "70px"
        },
        Kode: {
            /*          width: "25px" /* für 470px Breite */
            width: "40px"
        },
        Loeschen: {
            /*            width: "25px" /* für 470px Breite */
            width: "30px"
        }
    };


    /* Menue->Verwaltung: Ende*/

    /* Menue->Bücher: Start*/
    /* Menue->Bücher->Ausleihe verwalten*/
    elternInfo.style.tableAusleiheVerwalten = {
        table: {
            width: 527 /*angabe in px; Spalten werden berechnet 455-90 von unten - 8 für Rand und Abstand*/
        },
        schueler: {
            width: "120px"
        }
    };

    /* Menue->Bücher->20xx bearbeiten und weiter im Modus Liste bearbeiten*/
    elternInfo.style.tableBuecherListen = {
        default: {
            styles: 'text-align: left;'
        },
        checked: {
            /*            width: "20px", /* für 470px Breite */
            width: "30px",
            styles: 'text-align: center;'
        },
        ISBN: {
            /*            width: "90px" /* für 470px Breite */
            width: "110px"
        },
        Titel: {
            /*            width: "200px" /* für 470px Breite */
            width: "250px"
        },
        Neupreis: {
            /*            width: "40px", /* für 470px Breite */
            width: "50px",
            styles: 'text-align: right;'
        },
        Aktion: {
            /*            width: "56px" /* für 470px Breite */
            width: "70px"
        },
        vonBis: {
            /*            width: "22px", /* für 470px Breite */
            width: "30px",
            styles: 'text-align: right;'
        }
    };
    /* Menue->Bücher: Ende*/

    /* Menue->Anmeldungen: Start*/
    /* Menue->Anmeldungen->Anmeldungen Übersicht*/
    elternInfo.style.tableAnmeldungenUebersicht = {
        default: {
            styles: 'text-align: left;'
        },
        Klasse: {
            /*            width: "80px" /* für 470px Breite */
            width: "100px"
        },
        bezahlt: {
            /*            width: "70px" /* für 470px Breite */
            width: "90px"
        },
        unbezahlt: {
            /*            width: "70px" /* für 470px Breite */
            width: "90px"
        },
        gesamt: {
            /*            width: "70px" /* für 470px Breite */
            width: "90px"
        },
        Details: {
            /*            width: "70px", /* für 470px Breite */
            width: "90px",
            styles: 'text-align: center;'}
    };

    /* Menue->Anmeldungen->neue Anmeldungen*/
    elternInfo.style.tableNeueAnmeldungen = {
        default: {
            styles: 'text-align: left;'
        },
        Eltern: {
            /*            width: "85px" /* für 470px Breite */
            width: "130px"
        },
        Schueler: {
            /*            width: "85px" /* für 470px Breite */
            width: "130px"
        },
        Telefon: {
            /*            width: "70px" /* für 470px Breite */
            width: "90px"
        },
        Email: {
            /*            width: "125px" /* für 470px Breite */
            width: "190px"
        },
        Klasse: {
            /*            width: "50px" /* für 470px Breite */
            width: "75px"
        }
    };

    /* Menue->Anmeldungen->VergleichVorjahr*/
    elternInfo.style.tableVorjahresvergleich = {
        default: {
            styles: 'text-align: left;'
        },
        Klasse: {
            /*            width: "80px" /* für 470px Breite */
            width: "105px"
        },
        alt: {
            /*            width: "70px" /* für 470px Breite */
            width: "105px"
        },
        neu: {
            /*            width: "70px" /* für 470px Breite */
            width: "105px"
        },
        offen: {
            /*            width: "70px" /* für 470px Breite */
            width: "85px"
        },
        Details: {
            /*            width: "70px", /* für 470px Breite */
            width: "90px",
            styles: 'text-align: center;'}
    };


    elternInfo.style.tableKlassenDetails = {
        default: {
            styles: 'text-align: left;'
        },
        Eltern: {
            /*            width: "85px" /* für 470px Breite */
            width: "130px"
        },
        Schueler: {
            /*            width: "85px" /* für 470px Breite */
            width: "130px"
        },
        Telefon: {
            /*            width: "70px" /* für 470px Breite */
            width: "90px"
        },
        Email: {
            /*            width: "120px" /* für 470px Breite */
            width: "180px"
        },
        Anmerkungen: {
            /*            width: "50px", /* für 470px Breite */
            width: "600px"
        },
        alteKlassenId: {
            /*            width: "65px" /* für 470px Breite */
            width: "75px" /* für 470px Breite */
        },
        Spalte5: {
            /*            width: "32px", /* für 470px Breite */
            width: "45px",
            styles: 'text-align: center;'
        },
        Spalte6: {
            /*            width: "18px", /* für 470px Breite */
            width: "25px",
            styles: 'text-align: center;'
        }
    };

    elternInfo.style.tableElternOhneKinderOderAnmeldungen = {
        default: {
            styles: 'text-align: left;'
        },
        Name: {
            /*            width: "130px" /* für 470px Breite */
            width: "120px"
        },
        Telefon: {
            /*            width: "80px" /* für 470px Breite */
            width: "80px"
        },
        Email: {
            /*            width: "0px" /* für 470px Breite */
            width: "155px"
        },
        Aktiviert: {
            /*            width: "45px", /* für 470px Breite */
            width: "50px",
            styles: 'text-align: center;'
        },
        /* stopMail benutzt für Freigabe_lmf */
        stopMail: {
            /*            width: "30px", /* für 470px Breite */
            width: "50px",
            styles: 'text-align: center;'
        },
        registriertSeit: {
            /*            width: "105px" /* für 470px Breite */
            width: "120px"
        },
        Loeschen: {
            /*            width: "30px", /* für 470px Breite */
            width: "30px",
            styles: 'text-align: center;'
        }
    };

    /* Menue->Anmeldungen: Ende*/

    /* Menue->Einzahlungen: Start*/
    /* Menue->Einzahlungen->zuordnen*/
    elternInfo.style.tableEinzahlungenZuordnen = {
        default: {
            styles: 'text-align: left;'
        },
        check: {
/*            width: "20px", /* für 470px Breite */
            width: "25px",
            styles: 'text-align: center;'
        },
        Eltern: {
/*            width: "93px" /* für 470px Breite */
            width: "140px"
        },
        Schueler: {
/*            width: "93px" /* für 470px Breite */
            width: "140px"
        },
        Klasse: {
/*            width: "52px" /* für 470px Breite */
            width: "100px"
        },
        ZahlungsKey: {
/*            width: "88px" /* für 470px Breite */
            width: "100px"
        },
        Bestaetigen: {
/*            width: "58px" /* für 470px Breite */
            width: "80px"
        }
    };

    /* Menue->Einzahlungen->offene Einzahlungen*/
    elternInfo.style.tableOffeneEinzahlungen = {
        default: {
            styles: 'text-align: left;'
        },
        check: {
/*            width: "20px", /* für 470px Breite */
            width: "25px",
            styles: 'text-align: center;'
        },
        Valutatag: {
/*            width: "48px" /* für 470px Breite */
            width: "55px"
        },
        Betrag: {
/*            width: "40px" /* für 470px Breite */
            width: "60px",
            styles: 'text-align: right;'
        },
        Verwendungszweck: {
/*            width: "160px" /* für 470px Breite */
            width: "230px"
        },
        Absender: {
/*            width: "88px" /* für 470px Breite */
            width: "150px"
        },
        Verwerfen: {
/*            width: "55px" /* für 470px Breite */
            width: "70px"
        }
    };
    /* Menue->Einzahlungen: Ende*/
    
        elternInfo.style.tableMailJobs = {
        default: {
            styles: 'text-align: left;'
        },
        zeitpunkt: {
            /*            width: "130px" /* für 470px Breite */
            width: "150px"
        },
        kommentar: {
            /*            width: "80px" /* für 470px Breite */
            width: "250px"
        },
        anzahl: {
            /*            width: "0px" /* für 470px Breite */
            width: "50px",
            styles: 'text-align: center;'
        },
        fehler: {
            /*            width: "30px", /* für 470px Breite */
            width: "50px",
            styles: 'text-align: center;'
        },
        beendet: {
            /*            width: "45px", /* für 470px Breite */
            width: "50px",
            styles: 'text-align: center;'
        },
        Loeschen: {
            /*            width: "30px", /* für 470px Breite */
            width: "50px",
            styles: 'text-align: center;'
        }
    };
    
    console.log ("fertig mit styles");
    return elternInfo;
})(window.elternInfo || {});

