window.elternInfo = (function(elternInfo) {
    var lmfSelectKlasse = null;
    var lmfAusleiheAktualisieren = null;

    elternInfo.ausleiheVerwalten = function() {
        dojo.require("dijit.form.Select");
        elternInfo.hideErrors();
        dojo.byId("lmf_applikation").innerHTML
                = '<div id="lmf_service">\n\
                  <div id="lmf_serviceTitle">&nbsp;</div>\n\
                  <div id="lmf_serviceContent1">&nbsp;</div>\n\
                  <div id="lmf_serviceContent2">&nbsp;</div>\n\
              </div>';
        dojo.byId("lmf_serviceTitle").innerHTML = 'Ausleihe bearbeiten';
        dojo.byId("lmf_serviceContent1").innerHTML = '<div class="elements">'
                + '     <div class="label">Klasse auswählen:</div>'
                + '     <div id="lmf_KlasseFuerAusleihe"></div>'
                + '</div>';
        var postData = {
            "type": "db",
            table: "view_klassen_altes_jahr",
            _FamilienId: elternInfo.familyData.eltern.FamilienId
        };
        elternInfo.lmf_postService(postData,
                function(response, ioArgs) {
                    console.log(response);
                    var _options2 = [{label: "------------", value: -1}];
                    for (i = 0; i < response.data.length; i++) {
                        var k = response.data[i];
                        _options2.push({label: k.Stufe + k.SubKlasse + "-" + k.Sprache, value: k.KlassenId});
                    }
                    console.log(elternInfo.familyData);
                    if (lmfSelectKlasse == null) {
                        lmfSelectKlasse = new dijit.form.Select({
                            id: 'lmfSelectKlasse',
                            name: 'lmfSelectKlasse',
                            style: 'margin-top: -0.3em',
                            options: _options2
                        });
                    }
                    lmfSelectKlasse.placeAt(dojo.byId("lmf_KlasseFuerAusleihe"));
                    if (lmfAusleiheAktualisieren == null) {
                        lmfAusleiheAktualisieren = new dijit.form.Button({
                            id: 'lmfAusleiheAktualisieren',
                            label: "Aktualisieren",
                            style: 'margin: -0.3em 0 0 20px',
                            onClick: function(evt) {
                                ausleiheAnzeigeAktualisieren();
                                dojo.stopEvent(evt);
                            }
                        });
                    }
                    ;
                    lmfAusleiheAktualisieren.placeAt(dojo.byId("lmfSelectKlasse"), "after");
                });
    };

    var buecherZurAusleiheVerwaltung;
    var ausleiheVerwaltenStore = null;
    var ausleiheVerwaltenGrid = null;
    var layoutAusleiheverwaltenGrid = null;
    function ausleiheAnzeigeAktualisieren() {
        var klassenId = dijit.byId("lmfSelectKlasse").getValue();
        if (klassenId == -1) {
            return;
        }
        console.log("klassenid: " + klassenId);
        dojo.xhrGet({
            // The target URL on your webserver:
            url: elternInfo.root + "php/lmf-service.php?type=ListenVerlieheneBuecher&klassenId=" + klassenId
                    + "&_FamilienId=" + elternInfo.familyData.eltern.FamilienId,
            preventCache: true,
            handleAs: "json",
            // Event handler on successful call:
            load: function(response, ioArgs) {
                console.log(response);
                var anzahlBuecher = response.data.buecher.length;
                buecherZurAusleiheVerwaltung = response.data.buecher;
                layoutAusleiheverwaltenGrid = [
                    {defaultCell: {editable: false, styles: 'text-align: left;'},
                        cells: [
                            [
                                {field: 'name', name: "Schueler", width: elternInfo.style.tableAusleiheVerwalten.schueler.width }
                            ],
                            [
                                {headerClasses: "staticHeader", name: "alle einsammeln", width: elternInfo.style.tableAusleiheVerwalten.schueler.width }
                            ],
                            [
                                {headerClasses: "staticHeader", name: "alle ausgeben", width: elternInfo.style.tableAusleiheVerwalten.schueler.width }
                            ]
                        ]}
                ];
                //var maximaleBreite = 26; // wert ausprobiert
                var maximaleBreite=elternInfo.style.tableAusleiheVerwalten.table.width;
                var maximaleSpaltenbreite = (maximaleBreite / anzahlBuecher)-8; /*8 Px müssen für Rand und Abstand reserviert werden*/
                var spaltenbreite = (maximaleSpaltenbreite > 65) ? 65 : maximaleSpaltenbreite;
                var maximaleBuchstaben = 48; // wert ausprobiert
                var maximaleSpaltenBuchstaben = maximaleBuchstaben / anzahlBuecher;
                var spaltenBuchstaben = (maximaleSpaltenBuchstaben > 12) ? 12 : Math.floor(maximaleSpaltenBuchstaben);
                console.log("spaltenbreite: " + spaltenbreite + "  /  spaltenBuchstaben: " + spaltenBuchstaben);


                for (var j = 0; j < 3; j++) {
                    for (var i = 0; i < anzahlBuecher; i++) {
                        if (j == 0) {
                            layoutAusleiheverwaltenGrid[0].cells[j].push({
                                field: 'buch_' + response.data.buecher[i].BuchId,
                                /*field: "_item", */
                                name: '<span title="' + response.data.buecher[i].Titel + '">' + response.data.buecher[i].Titel.substr(0, spaltenBuchstaben) + '...</span>',
                                width: spaltenbreite+"px",
                                formatter: formatAusleiheButtons});
                        } else if (j == 1) {
                            layoutAusleiheverwaltenGrid[0].cells[j].push({
                                name: '<img src="' + elternInfo.root
                                        + 'images/einsammeln2.png" class="lmfAusleihenIcon"  \n\
                                       title="' + response.data.buecher[i].Titel + ' für alle Schüler einsammeln">',
                                headerClasses: "staticHeader"});
                        } else {
                            layoutAusleiheverwaltenGrid[0].cells[j].push({
                                name: '<img src="' + elternInfo.root
                                        + 'images/ausleihen2.png" class="lmfAusleihenIcon" \n\
                                        title="' + response.data.buecher[i].Titel + ' für alle Schüler zurücksetzen">',
                                headerClasses: "staticHeader"});
                        }
                    }
                }
                console.log(layoutAusleiheverwaltenGrid);
                var data = {
                    identifier: 'schuelerId',
                    items: response.data.schueler
                };
                console.log(data);
                ausleiheVerwaltenStore = new dojo.data.ItemFileWriteStore({data: data});
                ausleiheVerwaltenStore._forceLoad();
                console.log(ausleiheVerwaltenStore);
                ausleiheVerwaltenGrid = new dojox.grid.DataGrid({
                    store: ausleiheVerwaltenStore,
                    selectable: true,
                    singleClickEdit: true,
                    clientSort: true,
                    autoRender: true,
                    structure: layoutAusleiheverwaltenGrid,
                    width: "100%",
                    autoHeight: true,
                    onHeaderCellClick: function(e) {
                        var buchindex = (e.cell.index) % (anzahlBuecher + 1) - 1;
                        var einsammeln = (((e.cell.index) / (anzahlBuecher + 1)) > 2) ? 0 : 1;
                        if (e.cell.index > anzahlBuecher + 1 && buchindex >= 0) {
                            buchFuerKlasseEinsammelnUndAusgeben(response.data.buecher[buchindex].BuchId, einsammeln);
                        }
                        if (!dojo.hasClass(e.cell.id, "staticHeader")) {
                            e.grid.setSortIndex(e.cell.index);
                            e.grid.onHeaderClick(e);
                        }
                    },
                    onStyleRow: function(e) {
                        dojo.style(e.node.children[0].children[0].rows[1], 'display', 'none');
                        dojo.style(e.node.children[0].children[0].rows[2], 'display', 'none');
                    }
                },
                document.createElement('div')
                        );
                dojo.byId("lmf_serviceContent2").innerHTML = "";
                dojo.byId("lmf_serviceContent2").appendChild(ausleiheVerwaltenGrid.domNode);
                ausleiheVerwaltenGrid.startup();
                var showTooltip = function(e) {
                    var msg;
                    if (e.rowIndex < 0) { // header
                        msg = e.cell._titel;
                    } else {
                        var item = e.grid.getItem(e.rowIndex);
                        msg = e.grid.store.getValue(item, e.cell.field);
                    }
                    ;
                    if (msg) {
                        dijit.showTooltip(msg, e.cellNode);
                    }
                };
                var hideTooltip = function(e) {
                    dijit.hideTooltip(e.cellNode);
                };
                dojo.connect(ausleiheVerwaltenGrid, "onHeaderCellMouseOver", showTooltip);
                dojo.connect(ausleiheVerwaltenGrid, "onHeaderCellMouseOut", hideTooltip);
            },
            // Event handler on errors:
            error: function(response, ioArgs) {
                console.log(response);
                console.log("ausleiheAnzeigeAktualisieren failed.");
            }
        });
        function ausleihStatus(inRowIndex, row) {
            console.log("ausleihStatus:" + inRowIndex);
            console.log(row);
            return buecherZurAusleiheVerwaltung[inRowIndex];
        }

        function formatAusleiheButtons(value, inRowIndex, zelle) {
            var schuelerId = zelle.grid.getItem(inRowIndex).schuelerId;
            if (value != undefined) {
                if (value != "1") {
                    return  '<button type="button" class="lmf_buecher_button_ausgeliehen" onClick=" elternInfo.einsammelnUndAusgeben2(' + zelle.field.substr(5) + ', ' + schuelerId + ', 1)" title="einsammeln" onmouseout="elternInfo.bildAendern(this,\'ausgeliehen\')" onMouseOver="elternInfo.bildAendern(this,\'einsammeln\')"> \n\ '
                            + '</button>';
                } else {
                    return '<button type="button" class="lmf_buecher_button_eingesammelt" onClick=" elternInfo.einsammelnUndAusgeben2(' + zelle.field.substr(5) + ', ' + schuelerId + ', 0)"  title="wieder ausgeben" onmouseout="elternInfo.bildAendern(this,\'eingesammelt\')"  onMouseOver="elternInfo.bildAendern(this,\'ausleihen\')"> \n\ '
                            + '</button>';
                }
            }
        }
    }

    elternInfo.einsammelnUndAusgeben2 = function(buchId, schuelerId, einsammeln) {
        dojo.byId("lmf_info").innerHTML = "Bitte warten sie einen Moment - Ihre Daten werden geändert!";
        dojo.byId("lmf_info").style.display = "block";
        var updateDaten = {
            type: "einsammelnUndAusgeben",
            schuelerId: schuelerId,
            buchId: buchId,
            eingesammelt: einsammeln
        };
        elternInfo.lmf_save(updateDaten, function() {
            ausleiheVerwaltenStore.fetchItemByIdentity({
                identity: schuelerId,
                onItem: function(item) {
                    ausleiheVerwaltenStore.setValue(item, "buch_" + buchId, einsammeln);
                }

            });
            ausleiheVerwaltenGrid.render();
            dojo.byId("lmf_info").innerHTML = "Daten erfolgreich geändert";
            dojo.byId("lmf_info").style.display = "block";
        });
    };
    
    function buchFuerKlasseEinsammelnUndAusgeben(buchId, einsammeln) {
        var klassenId = dijit.byId("lmfSelectKlasse").getValue();
        dojo.byId("lmf_info").innerHTML = "Bitte warten sie einen Moment - Ihre Daten werden geändert!";
        dojo.byId("lmf_info").style.display = "block";
        var updateDaten = {
            type: "einsammelnUndAusgebenKlasse",
            klassenId: klassenId,
            buchId: buchId,
            eingesammelt: einsammeln
        };
        elternInfo.lmf_save(updateDaten, function() {
            ausleiheVerwaltenStore.fetch({
                onItem: function(item) {
                    if (ausleiheVerwaltenStore.isItem(item)) {
                        var oldValue = ausleiheVerwaltenStore.getValue(item, "buch_" + buchId);
                        console.log(oldValue);
                        if (oldValue != undefined) {
                            ausleiheVerwaltenStore.setValue(item, "buch_" + buchId, einsammeln);
                        }
                    }
                }
            });
            ausleiheVerwaltenGrid.render();
            dojo.byId("lmf_info").innerHTML = "Daten erfolgreich geändert";
            dojo.byId("lmf_info").style.display = "block";
        });
    }
    elternInfo.ausleiheVerwalten();

    return elternInfo;
})(window.elternInfo || {});
