window.elternInfo = (function(elternInfo) {

    function berechtigungAendern(schuelerDaten, param, alterWert, neuerWert) {
        console.log(schuelerDaten, param, alterWert, neuerWert);
        if (alterWert===neuerWert || param !='EisStatus'){
            return;
        }
         var data = {
            type: "EisStatusAendern",
            klassenId: elternInfo.KlassenIdFuerEV,
            schuelerId:schuelerDaten.SchuelerId[0],
            EisStatus:neuerWert?1:0
        };
        
        elternInfo.hideErrors();
        var xhrArgs = {
            url: elternInfo.root + "php/elternInfoSystem.php",
            preventCache: true,
            postData: dojo.toJson(data),
            handleAs: "json",
            headers: {"Content-Type": "application/json; charset=utf-8"},
            load: function(response) {
                console.log("lmf_post EvKlasse Succes" + response);
                if (!response.loggedIn) {
                    elternInfo.displayErrors(["Session ungültig. Bitte melden sie sich neu an"]);
                    elternInfo.loginSeiteLaden(["Session ungültig. Bitte melden sie sich neu an"], true);
                } else if (response.errors && response.errors !== "") {
                    elternInfo.displayErrors(response.errors);
                } else {
                    // Request war erfolgreich
                    console.log(response.data);
                    elternInfo.displayInfo("Daten erfolgreich geändert!"); 
                }
            },
            error: function(error) {
                console.log("lmf_post EisStatusAendern Error :" + error);
                elternInfo.displayErrors([error]);
            }
        };
        dojo.xhrPost(xhrArgs);
    }
    
    elternInfo.nichtAuthentifizierteElternOeffnen = function() {
        var data = {
            type: "EvKlasseFamilien",
            klassenId: elternInfo.KlassenIdFuerEV
        };
        
        elternInfo.hideErrors();
        var xhrArgs = {
            url: elternInfo.root + "php/elternInfoSystem.php",
            preventCache: true,
            postData: dojo.toJson(data),
            handleAs: "json",
            headers: {"Content-Type": "application/json; charset=utf-8"},
            load: function(response) {
                console.log("lmf_post EvKlasse Succes" + response);
                if (!response.loggedIn) {
                    elternInfo.displayErrors(["Session ungültig. Bitte melden sie sich neu an"]);
                    elternInfo.loginSeiteLaden(["Session ungültig. Bitte melden sie sich neu an"], true);
                } else if (response.errors && response.errors !== "") {
                    elternInfo.displayErrors(response.errors);
                } else {
                    // Request war erfolgreich
                    console.log(response.data);
                    if (response.data && response.data.length > 0) {
                        var styles= elternInfo.style.tableKlasseFuerEV;
                        var layoutKlasseFuerEV = [{
                                defaultCell: {editable: false, styles: 'text-align: left;'},
                                cells: [
                                    {field: 'Eltern', name: "Mutter / Vater", width: styles.Eltern.width},
                                    {field: 'Schueler', name: "Schueler", width: styles.Schueler.width},
                                    {field: 'Telefon', width: styles.Telefon.width},
                                    {field: 'Email', width: styles.Email.width, formatter: function(value) {
                                            return '<a href="mailto:' + value + '">' + value + '</a>';
                                        }},
                                    {field: "Freigabe_ev",name:'<img src="' + elternInfo.root + 'images/contact_email.png"  class="lmfRundmail" title="im Rundmailverteiler?">', 
                                        width: styles.EisStatus.width, 
                                        editable: false, formatter: function(isChecked) {
                                            if (isChecked=="1") {
                                                return '<img src="' + elternInfo.root + 'images/tick-icon.png"  class="lmfRundmail" title="Rundmail akzeptiert">';
                                            }else{
                                                 return '<img src="' + elternInfo.root + 'images/cross-icon.png"  class="lmfRundmail" title="Rundmail nicht freigegeben">';
                                            }
                                        }, styles: 'text-align: center;'},
                                    {field: "Klasse", width: styles.Klasse.width},
                                    {field: "EisStatus",name: "geprüft", width: styles.EisStatus.width, editable: true, type: dojox.grid.cells.Bool, styles: 'text-align: center;'}
                                ]
                            }];
                        
                        for (var i=0; i<response.data.length; i++){
                            response.data[i]["EisStatus"]=response.data[i]["EisStatus"]=="1"?true:false ;
                        }
                        var data = {items: response.data};
                        
                        
                        var storeKlasseFuerEV = new dojo.data.ItemFileWriteStore({
                            data: data
                        });
                        storeKlasseFuerEV.comparatorMap = {};
                        storeKlasseFuerEV._forceLoad();
                        dojo.connect(storeKlasseFuerEV, "onSet", berechtigungAendern);
                        var gridKlasseFuerEV = new dojox.grid.DataGrid(
                                {
                                    store: storeKlasseFuerEV,
                                    selectable: true,
                                    clientSort: true,
                                    autoRender: true,
                                    structure: layoutKlasseFuerEV,
                                    autoWidth: true,
                                    autoHeight: true
                                },
                        document.createElement('div')
                                );
                        dojo.byId("lmf_applikation").innerHTML = "";
                        tmp = document.createElement('div');
                        tmp.innerHTML="Sie können als Elternvertreter, Eltern Ihrer Klasse für den Zugriff auf die Kontaktliste berechtigen oder bestehende Berechtigungen wieder aufheben. Bitte gehen Sie verantwortungsbewusst mit diesem Recht um. Alle Aktionen werden auf Serverseite für eventuelle Streitfälle protokolliert.<br> Bitte helfen Sie mit, die Daten von Eltern Ihrer Klasse gegen unberechtigten Zufriff zu schützen und setzen Sie nur solche Daten auf geprüft, deren Eintrag Sie nachvollziehen können. Eltern, die für ein Kind den Beitrag für den Lernmittelfonds überwiesen haben, werden durch das System automatisch auf 'geprüft' gesetzt.<br><br>";
                        dojo.byId("lmf_applikation").appendChild(tmp);
                        dojo.byId("lmf_applikation").appendChild(gridKlasseFuerEV.domNode);
                        // Call startup, in order to  render the grid: 
                        gridKlasseFuerEV.startup();
                        gridKlasseFuerEV.render();
                        elternInfo.appType = "lmf";
                        dojo.byId("lmf_WechselApplikation").innerHTML = "zurück zur Elterninfo";
                    } else {
                        elternInfo.displayErrors(["Sie haben nicht die erforderlichen Rechte (oder es gibt keine weiteren eingetragenen Familien in dieser Klasse) - bitte wenden Sie sich an den Administrator des ElternInfoSystems"]);
                    }
                }
            },
            error: function(error) {
                console.log("lmf_post EvKlasse Error :" + error);
                elternInfo.displayErrors([error]);
            }
        };
        dojo.xhrPost(xhrArgs);
    };
    elternInfo.nichtAuthentifizierteElternOeffnen();
    return elternInfo;
})(window.elternInfo || {});
