window.elternInfo = (function(elternInfo) {

    dojo.require("dijit.form.Button");
    dojo.require("dijit.Dialog");
    dojo.require("dijit.form.Form");
    dojo.require("dijit.form.Button");
    dojo.require("dijit.form.Textarea");
    dojo.require("lmf.FileUploader");
    dojo.require("dijit.form.Button");
    dojo.require("dijit.ProgressBar");
    dojo.require("dijit.form.Select");
    dojo.require("dijit.Editor");
    dojo.require("dijit._editor.plugins.AlwaysShowToolbar");

    var eisMailEditor = null;
    var eisMailButton = null;
    var eisFileUploader = null;
    var elternRundmailDialog = null;
    var ajaxLoaded = false;
    var dialogLoaded = false;
    if (elternRundmailDialog) {
        // schon definiert
    } else {
        elternRundmailDialog = new dijit.Dialog({
            title: "Eltern Rundmail",
            class: "soria",
            id: "elternRundmailDialog",
            href: elternInfo.root + "pages/elternRundmail.htm"
        });
    }

    dojo.connect(elternRundmailDialog,
            "onDownloadEnd",
            onElternRundMailDialogLoad
            );

    elternInfo.elternRundmailDialogOeffnen = function() {
        console.log("elternRundmailDialogOeffnen: " + elternInfo.KlassenIdFuerEV);
        elternRundmailDialog.show();

        dojo.require("dijit.form.Select");
        var data = {
            type: "EvKlasse",
            klassenId: elternInfo.KlassenIdFuerEV
        };
        elternInfo.hideErrors();
        var xhrArgs = {
            url: window.location.origin+elternInfo.root + "php/elternInfoSystem.php",
            preventCache: true,
            postData: dojo.toJson(data),
            handleAs: "json",
            headers: {"Content-Type": "application/json; charset=utf-8"},
            load: function(response) {
                console.log("lmf_post EvKlasse Succes" + response);
                if (!response.loggedIn) {
                    elternRundmailDialogSchliessen();
                    elternInfo.displayErrors(["Session ungültig. Bitte melden sie sich neu an"]);
                    elternInfo.loginSeiteLaden(["Session ungültig. Bitte melden sie sich neu an"], true);
                } else if (response.errors && response.errors !== "") {
                    elternInfo.displayErrors(response.errors);
                } else {
                    // Request war erfolgreich
                    console.log(response.data);
                    if (response.data && response.data.length > 0) {
                        console.log("start Rundmail OK");
                        var klasse = response.data[0]; // alle Klassen sollten gleich sein, aber 2 Datensätze bei 2 Kindern in der Klasse möglich
                        elternInfo.rundMailKlasse = klasse;
                        onElternRundMailDialogLoad(true);
                    } else {
                        elternRundmailDialogSchliessen();
                        elternInfo.displayErrors(["Sie haben nicht die Rundmailrechte - bitte wenden Sie sich an den Administrator des ElternInfoSystems"]);
                    }
                }
            },
            error: function(error) {
                console.log("lmf_post EvKlasse Error :" + error);
                elternInfo.displayErrors([error]);
            }
        };
        dojo.xhrPost(xhrArgs);
    };

    function onElternRundMailDialogLoad(fromAjax) {
        if (fromAjax) {
            ajaxLoaded = true;
        } else {
            dialogLoaded = true;
        }
        if (ajaxLoaded && dialogLoaded) {
            dojo.byId("eisMailKlasseHidden").value = elternInfo.KlassenIdFuerEV;
            if (elternInfo.rundMailKlasse.MetaKey) {
                dojo.byId("RundMailEmpfaenger").innerHTML = "alle '" + elternInfo.rundMailKlasse.MetaKey + "'-Klassen";
            } else {
                dojo.byId("RundMailEmpfaenger").innerHTML = "Klasse " + elternInfo.rundMailKlasse.Stufe + elternInfo.rundMailKlasse.SubKlasse;
            }
            startMailEditor(elternInfo.KlassenIdFuerEV);
        }
    }

    function startMailEditor() {
        dojo.require("dijit._editor.plugins.LinkDialog");
        dojo.require("dijit._editor.plugins.TextColor");
        if (eisMailEditor) {
            //eisMailEditor.destroyRecursive();
            console.log(eisMailEditor);
            eisMailEditor.setValue("");
        } else {
            eisMailEditor = new dijit.Editor({
                class: "eisMailEditorClass",
                value: "   ",
                plugins: ['undo', 'redo', 'cut', 'copy', 'paste', '|',
                    'bold', 'italic', 'underline', '|',
                    'insertOrderedList', 'insertUnorderedList', '|',
                    'indent', 'outdent', 'createLink', 'insertImage', '|', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', '|', 'foreColor', 'hiliteColor']
                        // extraPlugins: ['dijit._editor.plugins.AlwaysShowToolbar']
            },
            dojo.byId('eisMailText'));
            console.log(eisMailEditor);
            eisMailEditor.setValue("");
        }
        if (eisMailButton === null) {
            eisMailButton = new dijit.form.Button({
                label: "Absenden",
                onClick: function(evt) {
                    dojo.byId("eisMailTextHidden").value = eisMailEditor.getValue();
                    if (eisMailEditor.getValue().trim().length > 0) {
                        console.log("#" + eisMailEditor.getValue().trim() + "#");
                        elternInfo.hideErrors();
                        console.log(dojo.byId("eisMailTextHidden").value);
                        eisFileUploader.submit(dojo.byId("elternRundMailFormular"));
                    } else {
                        alert("Ohne Inhalt wird nichts gemailt!");
                    }
                    dojo.stopEvent(evt);
                }
            },
            dojo.byId('eisMailButtonDiv'));
        }
        if (dojo.byId("eisMailDateiWahl")) {
            //dojo.byId("eisMailFiles").value = "";
            if (eisFileUploader) {
//            eisFileUploader.destroyRecursive();  der kann bleiben - mal testen
            } else {
                var props = {
                    isDebug: true,
                    devmode: true,
                    hoverClass: "uploadHover",
                    activeClass: "uploadPress",
                    disabledClass: "uploadDisabled",
                    uploadUrl: dojo.moduleUrl("dojox.form", window.location.origin+elternInfo.root + "php/elternInfoSystem.php")
                };
                eisFileUploader = new lmf.FileUploader(dojo.mixin({
                    force: "html",
                    //showProgress: true,
                    //progressWidgetId: "progressBarHtml",
                    selectMultipleFiles: true,
                    //uploadOnChange:true,
                    fileListId: "eisMailFiles",
                    devMode: true
                }, props), "eisMailDateiWahl");
                dojo.connect(eisFileUploader, "onComplete", function(dataArray) {
                    console.log("onComplete");
                    console.log(dataArray);
                    if (dataArray && dataArray[0] && !dataArray[0].loggedIn) {
                        alert("Ihre Sitzung ist leider nicht mehr gültig. \nBitte kopieren Sie den Inhalt Ihrer Mail und schliessen Sie den Maileditor. Dann melden Sie sich neu an. \nWenn Sie dann eine neue Rundmail erzeugen, können Sie Ihren Text wieder einfügen.");
                        elternInfo.loginSeiteLaden(["Session ungültig. Bitte melden sie sich neu an"], true);
                    }
                    if (dataArray && dataArray[0] && dataArray[0].success) {
                        elternRundmailDialog.hide();
                        var minutesToWait=0;
                        if (dataArray[0].count){
                           minutesToWait = Math.round(dataArray[0].CALL_INTERVAL_ON_CRON /60  * (0.5 + dataArray[0].count / dataArray[0].MAILS_PER_CALL));
                        }
                        elternInfo.displayInfo("Es wurden " + dataArray[0].count + " Mails erfolgreich vorbereitet. <br>Sie werden im Hintergrund verschickt solange Ihre Session läuft. Bitte beachten Sie, dass sich alle anderen Arbeitsprozesse in dieser Zeit etwas verlangsamen können. Es wird ca "+ minutesToWait+" Minuten dauern.");
                        eisMailEditor.setValue("");
                        dojo.byId("mail-icon-span").style.display = "block";
                        elternInfo.checkUndStartMailJobs ();
                    }
                    console.log("html onComplete", dataArray);
                    dojo.forEach(dataArray, function(d) {
                        console.log(d);
                    });
                });
                dojo.connect(eisFileUploader, "onError", function(err) {
                    console.error("html error", err);
                });
            }
        }
    }

    function elternRundmailDialogSchliessen() {
        elternRundmailDialog.hide();
    }

    elternInfo.elternRundmailDialogOeffnen();

    return elternInfo;
})(window.elternInfo || {});

