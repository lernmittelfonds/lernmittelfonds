window.elternInfo = (function(elternInfo) {

    var pMenuBar = null;
    elternInfo.serviceLaden = function() {
        if (pMenuBar == null) {
            pMenuBar = new dijit.MenuBar({});

            var myMenuBarItem = new dijit.MenuBarItem({
                label: "",
                _label: "Home",
                id: "lmf_Menue_Home",
                iconClass: "dijitEditorIcon dijitEditorIconSave",
                iconSrc: elternInfo.root + 'images/home-icon2.png',
                onClick: function() {
                    elternInfo.ladeStartSeite(true);
                }
            });
            var iconElem = document.createElement("span");
            iconElem.innerHTML = '<image src="' + myMenuBarItem.get("iconSrc") + '" title="' + myMenuBarItem.get("_label") + '">';
            myMenuBarItem.domNode.insertBefore(iconElem, myMenuBarItem.domNode.childNodes[0]);

            pMenuBar.addChild(myMenuBarItem);

            var pSubMenu1 = new dijit.Menu({});
            pSubMenu1.addChild(new dijit.MenuItem({
                label: "Suchen",
                onClick: function() {
                    suchFeldEinblenden();
                }
            }));
            pSubMenu1.addChild(new dijit.MenuItem({
                label: "Mail",
                onClick: function() {
                    oeffneMailSeite();
                }
            }));
            pSubMenu1.addChild(new dijit.MenuItem({
                label: "Elternvertreter",
                onClick: function() {
                    sucheElternvertreter();
                }
            }));
            pMenuBar.addChild(new dijit.PopupMenuBarItem({
                label: "Nutzer",
                id: "lmf_Menue_Nutzer",
                popup: pSubMenu1
            }));

            var pSubMenu2 = new dijit.Menu({});
            pSubMenu2.addChild(new dijit.MenuItem({
                label: "Jahresplanung",
                onClick: function() {
                    starteJahresPlanung();
                }
            }));
            pSubMenu2.addChild(new dijit.MenuItem({
                label: "Teilnehmerlisten",
                onClick: function() {
                    window.open(elternInfo.root + "php/buchAusgabeListen.php?type=leereListen&_FamilienId=" + elternInfo.familyData.eltern.FamilienId);
                }
            }));
            pSubMenu2.addChild(new dijit.MenuItem({
                label: "Klassen",
                onClick: function() {
                    elternInfo.callKlassenListe();
                }
            }));
            pSubMenu2.addChild(new dijit.MenuItem({
                label: "Klassen & Schüler",
                onClick: function() {
                    elternInfo.callKlassenSchuelerListe();
                }
            }));
            pSubMenu2.addChild(new dijit.MenuItem({
                label: "Texte",
                onClick: function() {
                    textVerwaltungEinblenden();
                }
            }));
            pSubMenu2.addChild(new dijit.MenuItem({
                label: "Statistik",
                onClick: function() {
                    window.open(elternInfo.root + "statistik.html");
                }
            }));
            pSubMenu2.addChild(new dijit.MenuItem({
                label: "Abrechnung lfd. Jahr",
                onClick: function() {
                    window.open(elternInfo.root + "php/kalkulation.php");
                }
            }));
            pMenuBar.addChild(new dijit.PopupMenuBarItem({
                label: "Verwaltung",
                id: "lmf_Menue_Verwaltung",
                popup: pSubMenu2
            }));

            var pSubMenu3 = new dijit.Menu({});
            pSubMenu3.addChild(new dijit.MenuItem({
                label: "Ausleihe verwalten",
                onClick: function() {
                    ausleiheVerwaltenOeffnen();
                }
            }));
            pSubMenu3.addChild(new dijit.MenuItem({
                label: "" + (1 + elternInfo.familyData.thisYear) + '/' + (2 + elternInfo.familyData.thisYear - 2000) + " bearbeiten",
                onClick: function() {
                    elternInfo.naechsteFunktion = "callBuecherListen";
                    ladeBuecher();
                }
            }));
            pSubMenu3.addChild(new dijit.MenuItem({
                label: "Ausgabelisten-1",
                onClick: function() {
                    if (typeof(elternInfo.dialogBuecherKommentarOeffnen) == "undefined") {
                        elternInfo.dialogOnLoadFunction = function() {
                            elternInfo.dialogBuecherKommentarOeffnen(elternInfo.root + "php/buchAusgabeListenEinzeln.php");
                        };
                        elternInfo.loadJs("dialogBuecherKommentar");
                    } else {
                        elternInfo.dialogBuecherKommentarOeffnen(elternInfo.root + "php/buchAusgabeListenEinzeln.php");
                    }
                }
            }));
            pSubMenu3.addChild(new dijit.MenuItem({
                label: "Ausgabelisten-2",
                onClick: function() {
                    window.open(elternInfo.root + "php/buchAusgabeListen.php   ");
                }
            }));
            pSubMenu3.addChild(new dijit.MenuItem({
                label: "Bestandslisten",
                onClick: function() {
                    window.open(elternInfo.root + "php/ListenVerlieheneBuecher.php?type=alle");
                }
            }));
            pSubMenu3.addChild(new dijit.MenuItem({
                label: "Rückgabelisten-1",
                onClick: function() {
                    // window.open(elternInfo.root + "php/ListenBuecherRueckgabeEinzeln.php");
                    if (typeof(elternInfo.dialogBuecherKommentarOeffnen) == "undefined") {
                        elternInfo.dialogOnLoadFunction = function() {
                            elternInfo.dialogBuecherKommentarOeffnen(elternInfo.root + "php/ListenBuecherRueckgabe.php");
                        };
                        elternInfo.loadJs("dialogBuecherKommentar");
                    } else {
                        elternInfo.dialogBuecherKommentarOeffnen(elternInfo.root + "php/ListenBuecherRueckgabe.php");
                    }
                }
            }));
             pSubMenu3.addChild(new dijit.MenuItem({
                label: "Rückgabelisten-2",
                onClick: function() {
                    // window.open(elternInfo.root + "php/ListenBuecherRueckgabeEinzeln.php");
                    if (typeof(elternInfo.dialogBuecherKommentarOeffnen) == "undefined") {
                        elternInfo.dialogOnLoadFunction = function() {
                            elternInfo.dialogBuecherKommentarOeffnen(elternInfo.root + "php/ListenBuecherRueckgabeEinzeln.php");
                        };
                        elternInfo.loadJs("dialogBuecherKommentar");
                    } else {
                        elternInfo.dialogBuecherKommentarOeffnen(elternInfo.root + "php/ListenBuecherRueckgabeEinzeln.php");
                    }
                }
            }));
            pSubMenu3.addChild(new dijit.MenuItem({
                label: "Bestellgrundlage",
                onClick: function() {
                    alert("noch nicht implementiert");
                }
            }));
            pMenuBar.addChild(new dijit.PopupMenuBarItem({
                label: "Bücher",
                id: "lmf_Menue_Buecher",
                popup: pSubMenu3
            }));



            var pSubMenu4 = new dijit.Menu({});
            pSubMenu4.addChild(new dijit.MenuItem({
                label: "Anmeldungen Übersicht",
                onClick: function() {
                    callUebersicht();
                }
            }));
            pSubMenu4.addChild(new dijit.MenuItem({
                label: "neue Anmeldungen",
                onClick: function() {
                    neueAnmeldungen();
                }
            }));
            pSubMenu4.addChild(new dijit.MenuItem({
                label: "Vergleich zum Vorjahr",
                onClick: function() {
                    AnmeldungenAltUndNeu();
                }
            }));
            pSubMenu4.addChild(new dijit.MenuItem({
                label: "nicht verlängert",
                onClick: function() {
                    offeneAnmeldungen();
                }
            }));
            pSubMenu4.addChild(new dijit.MenuItem({
                label: "Eltern ohne Schüler",
                onClick: function() {
                    elternOhneSchueler();
                }
            }));
            pSubMenu4.addChild(new dijit.MenuItem({
                label: "Eltern ohne Anmeldungen",
                onClick: function() {
                    elternOhneAnmeldungen();
                }
            }));
            pMenuBar.addChild(new dijit.PopupMenuBarItem({
                label: "Anmeldungen",
                id: "lmf_Menue_Anmeldungen",
                popup: pSubMenu4
            }));

            var pSubMenu5 = new dijit.Menu({});
            pSubMenu5.addChild(new dijit.MenuItem({
                label: "Upload",
                onClick: function() {
                    elternInfo.naechsteFunktion = "zeigeUploadEinzahlungen";
                    ladeEinzahlungen();
                }
            }));
            pSubMenu5.addChild(new dijit.MenuItem({
                label: "Zuordnen",
                onClick: function() {
                    elternInfo.naechsteFunktion = "zeigeCheckEinzahlungen";
                    ladeEinzahlungen();
                }
            }));
            pSubMenu5.addChild(new dijit.MenuItem({
                label: "Offene Zahlungen",
                onClick: function() {
                    elternInfo.naechsteFunktion = "zeigeOffeneEinzahlungen";
                    ladeEinzahlungen();
                }
            }));
            pMenuBar.addChild(new dijit.PopupMenuBarItem({
                label: "Einzahlungen",
                id: "lmf_Menue_Einzahlungen",
                popup: pSubMenu5
            }));

            var pSubMenu6 = new dijit.Menu({});
            pSubMenu6.addChild(new dijit.MenuItem({
                label: "Hilfe",
                onClick: function() {
                    alert("noch nicht implementiert");
                }
            }));
            pSubMenu6.addChild(new dijit.MenuItem({
                label: "Info",
                onClick: function() {
                    alert("Verwaltungssoftware für Lernmittelfonds\nCopyright Heiko Kendziorra - 2013-2014");
                }
            }));
            pMenuBar.addChild(new dijit.PopupMenuBarItem({
                label: "&nbsp;&nbsp;?&nbsp;&nbsp;",
                id: "lmf_Menue_Hilfe",
                popup: pSubMenu6
            }));

            var pSubMenu7 = new dijit.Menu({});
            pMenuBar.addChild(new dijit.PopupMenuBarItem({
                label: "&nbsp;&nbsp;&nbsp;&nbsp;",
                id: "lmf_Menue_Ende",
                popup: pSubMenu7
            }));
        }
        pMenuBar.placeAt("lmf_serviceMenue");
        pMenuBar.startup();
    };

    function starteJahresPlanung() {
        if (typeof(elternInfo.planungAnzeigen) == "undefined") {
            elternInfo.loadJs("jahresPlanung");
        } else {
            elternInfo.planungAnzeigen();
        }
    }

    function ladeEinzahlungen() {
        if (typeof(elternInfo.zeigeUploadEinzahlungen) == "undefined") {
            elternInfo.loadJs("einzahlungen");
        } else {
            elternInfo.rufeNaechsteFunktion();
        }
    }
    function ladeBuecher() {
        if (typeof(elternInfo.callBuecherListen) == "undefined") {
            elternInfo.loadJs("buecher");
        } else {
            elternInfo.rufeNaechsteFunktion();
        }
    }

    elternInfo.rufeNaechsteFunktion = function() {
        switch (elternInfo.naechsteFunktion) {
            case "zeigeUploadEinzahlungen":
                elternInfo.zeigeUploadEinzahlungen();
                break;
            case "zeigeCheckEinzahlungen":
                elternInfo.zeigeCheckEinzahlungen();
                break;
            case "zeigeOffeneEinzahlungen":
                elternInfo.zeigeOffeneEinzahlungen();
                break;
            case "callBuecherListen":
                elternInfo.callBuecherListen('neu');
                break;
            default:
                break;
        }
    };

    function suchFeldEinblenden() {
        if (typeof(elternInfo.sucheAnzeigen) == "undefined") {
            elternInfo.loadJs("suche");
        } else {
            elternInfo.sucheAnzeigen();
        }
    }

    function oeffneMailSeite() {
        if (typeof(elternInfo.mailAnzeigen) == "undefined") {
            elternInfo.loadJs("mail");
        } else {
            elternInfo.mailAnzeigen();
        }
    }
    
    function sucheElternvertreter() {
        elternInfo.hideErrors();
        var data = {type: "elternVertreter"};
        elternInfo.lmf_postService(data, elternInfo.elternVertreterAnzeigen);
    }
    
     elternInfo.elternVertreterAnzeigen = function(response) {
            console.log(response);
            var styles = elternInfo.style.tableElternvertreter;
            var layoutElternVertreter = [{
                    defaultCell: {editable: false, styles: styles.default.styles},
                    cells: [
                        {field: "_item", name: "Klasse", width: styles.Klasse.width, formatter: function(row) {
                                return row["Stufe"] +row["SubKlasse"];
                            }},
                        {field: '_item', name: "Name", width: styles.Name.width, formatter: function(row) {
                                return '<a onClick="elternInfo.ladeStartSeiteAdmin(' + row.FamilienId + ')">' + row.Nachname + ", " + row.Vorname + '</a>';
                            }},
                        {field: 'Telefon', width: styles.Telefon.width},
                        {field: 'Email', width: styles.Email.width, formatter: function(value) {
                                        return '<a href="mailto:' + value + '">' + value + '</a>';
                                    }},
                        {field: '_item', name: "&nbsp;", width: styles.Loeschen.width, styles: styles.Loeschen.styles, formatter: function(row) {
                                return '<button type="button" class="lmf_klassen_loeschen_button" \n\
                            onClick="elternInfo.elternVertreterLoeschen(' + row["schuelerid"] + ',\''+row.Nachname + ', ' + row.Vorname +'\')" title="löschen"></button>';
                            }}
                    ]
                }];
            var elternVertreterStore = new dojo.data.ItemFileWriteStore ({
                data: {items: dojo.clone(response.data)}
            });
            elternVertreterStore._forceLoad();

            var elternVertreterGrid = new dojox.grid.DataGrid(
                    {
                        store: elternVertreterStore,
                        selectable: true,
                        clientSort: true,
                        autoRender: true,
                        structure: layoutElternVertreter,
                        autoWidth: true,
                        autoHeight: true
                    },
            document.createElement('div')
                    );
            dojo.byId("lmf_applikation").innerHTML = "<br><div>Elternvertreter</div><br>";
            dojo.byId("lmf_applikation").appendChild(elternVertreterGrid.domNode);
            // Call startup, in order to  render the grid: 
            elternVertreterGrid.startup();
            elternVertreterGrid.render();
        };
    
    elternInfo.elternVertreterLoeschen = function(id, Name) {
        elternInfo.hideErrors();
        if (confirm("Wollen Sie " + Name + " die Eigenschaft 'Elternvertreter' entziehen?")) {
            var postData = {
                "type": "elternVertreterEntfernen",
                "SchuelerId": id
            };
            elternInfo.lmf_postService(postData, function(response) {
                elternInfo.displayInfo ("Elternvertreter erfolgreich entfernt.");
                elternInfo.elternVertreterAnzeigen(response);
            });
        }
    };
     

    function textVerwaltungEinblenden() {
        if (typeof(elternInfo.texteAnzeigen) == "undefined") {
            elternInfo.loadJs("textVerwaltung");
        } else {
            elternInfo.texteAnzeigen();
        }
    }

    function ausleiheVerwaltenOeffnen() {
        if (typeof(elternInfo.ausleiheVerwalten) == "undefined") {
            elternInfo.loadJs("ausleiheVerwalten");
        } else {
            elternInfo.ausleiheVerwalten();
        }
    }

    elternInfo.lmf_postService = function(data, onSuccess) {
        elternInfo.hideErrors();
        var xhrArgs = {
            url: elternInfo.root + "php/lmf-service.php",
            preventCache: true,
            postData: dojo.toJson(data),
            handleAs: "json",
            headers: {"Content-Type": "application/json; charset=utf-8"},
            load: function(data) {
                console.log("lmf_postService Succes" + data);
                if (!data.loggedIn) {
                    elternInfo.displayErrors(["Session ungültig. Bitte melden sie sich neu an"]);
                    elternInfo.loginSeiteLaden(["Session ungültig. Bitte melden sie sich neu an"], true);
                } else if (data.errors && data.errors !== "") {
                    elternInfo.displayErrors(data.errors);
                } else if (onSuccess) { // erfolgreich mit oder ohne onSuccess Callback
                    onSuccess(data);
                }
            },
            error: function(error) {
                console.log("lmf_postService Error :" + error);
                elternInfo.displayErrors([error]);
            }
        };
        dojo.xhrPost(xhrArgs);
    };

///////////////////////////////////
// Menue Verwaltung -> Anmeldungen
///////////////////////////////////

    function formatterLinkKlasse(_row) {
        return '<a href="javascript:elternInfo.callKlassenUebersicht(' + _row.KlassenId + ')">Details</a>';
    }
    function formatterLinkKlasseAltNeu(_row) {
        return '<a href="javascript:elternInfo.UebersichtKlasseAltNeu(' + _row.KlassenId + ')">Details</a>';
    }
    /*ruft Liste von Schülern, die in diesem, aber nicht im letzten Jahr angemeldet waren*/
    /*potentielle Doppeleingaben !!!!*/
    function neueAnmeldungen() {
        console.log("neueAnmeldungen");
        elternInfo.hideErrors();
        dojo.byId("lmf_applikation").innerHTML = '<div id="lmf_service"><div id="lmf_serviceTitle">&nbsp;</div><div id="lmf_serviceContent">&nbsp;</div></div>';
        var data = {type: "NeuAnmeldungen"};
        elternInfo.lmf_postService(data,
                function(response) {
                    console.log(response);
                    var styles = elternInfo.style.tableNeueAnmeldungen;
                    var layoutNeuAnmeldungen = [{
                            defaultCell: {editable: false, styles: styles.default.styles},
                            cells: [
                                {field: '_item', name: "Mutter / Vater", formatter: function(row) {
                                        return formatterFamilienLink(row, true);
                                    }, width: styles.Eltern.width},
                                {field: '_item', name: "Schueler", formatter: function(row) {
                                        return formatterFamilienLink(row, false);
                                    }, width: styles.Schueler.width},
                                {field: 'Telefon', width: styles.Telefon.width},
                                {field: 'email', width: styles.Email.width, formatter: function(value) {
                                        return '<a href="mailto:' + value + '">' + value + '</a>';
                                    }},
                                {field: "Klasse", width: styles.Klasse.width}
                            ]
                        }];
                    var data = {items: dojo.clone(response.data)};
                    var neuAnmeldungenStore = new dojo.data.ItemFileWriteStore({
                        data: data
                    });
                    neuAnmeldungenStore.comparatorMap = {};
                    neuAnmeldungenStore._forceLoad();
                    var neuAnmeldungenGrid = new dojox.grid.DataGrid(
                            {
                                store: neuAnmeldungenStore,
                                selectable: true,
                                clientSort: true,
                                autoRender: true,
                                structure: layoutNeuAnmeldungen,
                                autoWidth: true,
                                autoHeight: true
                            },
                    document.createElement('div')
                            );
                    dojo.byId("lmf_serviceContent").innerHTML = "";
                    dojo.byId("lmf_serviceContent").appendChild(neuAnmeldungenGrid.domNode);
                    // Call startup, in order to  render the grid: 
                    neuAnmeldungenGrid.startup();
                    neuAnmeldungenGrid.render();
                });
    }

    function textZuZahlComperator(a, b) {
        if ((1 * a) > (1 * b))
            return 1;
        if ((1 * a) < (1 * b))
            return -1;
        return 0;
    }

    /* vergleicht Anmeldungen Vorjahr, aktuelles Jahr - Klassenweise */
    function AnmeldungenAltUndNeu() {
        console.log("neueAnmeldungen");
        elternInfo.hideErrors();
        dojo.byId("lmf_applikation").innerHTML = '<div id="lmf_service"><div id="lmf_serviceTitle">&nbsp;</div><div id="lmf_serviceContent">&nbsp;</div></div>';
        var data = {type: "AnmeldungenAltUndNeu"};
        elternInfo.lmf_postService(data,
                function(response) {
                    console.log(response);
                    var styles = elternInfo.style.tableVorjahresvergleich;
                    var layoutAnmeldungenaltUndNeu = [{
                            defaultCell: {editable: false, styles: styles.default.styles},
                            cells: [
                                {field: 'KlassenId', name: "Klasse " + elternInfo.familyData.thisYear + '/' + (1 + elternInfo.familyData.thisYear), width: styles.Klasse.width, formatter: elternInfo.formatterKlasseAlt},
                                {field: 'alt_gesamt', name: "Anmeldungen dieses Jahr: " + response.summe[0].alt_gesamt, width: styles.alt.width},
                                {field: 'neu_angemeldet', name: "Anmeldungen nächstes Jahr: " + response.summe[0].neu_angemeldet, width: styles.neu.width},
                                {field: 'neu_abgemeldet', name: "Abmeldungen nächstes Jahr: " + response.summe[0].neu_abgemeldet, width: styles.neu.width},
                                {field: 'offen', name: "offen: " + response.summe[0].offen, width: styles.offen.width},
                                {field: '_item', name: '<img class="lmf_refresh_icon" src="' + elternInfo.root + 'images/refresh1.png" height="16" width="16" onclick="AnmeldungenAltUndNeu()">', width: styles.Details.width, styles: styles.Details.styles, formatter: formatterLinkKlasseAltNeu}
                            ]
                        }];
                    var data = {items: dojo.clone(response.data)};
                    var anmeldungenAltNeuStore = new dojo.data.ItemFileWriteStore({
                        data: data
                    });
                    anmeldungenAltNeuStore.comparatorMap = {};
                    anmeldungenAltNeuStore.comparatorMap["alt_gesamt"] = textZuZahlComperator;
                    anmeldungenAltNeuStore.comparatorMap["neu_angemeldet"] = textZuZahlComperator;
                    anmeldungenAltNeuStore.comparatorMap["offen"] = textZuZahlComperator;
                    anmeldungenAltNeuStore._forceLoad();
                    var anmeldungenAltNeuGrid = new dojox.grid.DataGrid(
                            {
                                store: anmeldungenAltNeuStore,
                                selectable: true,
                                clientSort: true,
                                autoRender: true,
                                structure: layoutAnmeldungenaltUndNeu,
                                autoWidth: true,
                                autoHeight: true
                            },
                    document.createElement('div')
                            );
                    dojo.byId("lmf_serviceContent").innerHTML = "";
                    dojo.byId("lmf_serviceContent").appendChild(anmeldungenAltNeuGrid.domNode);
                    // Call startup, in order to  render the grid: 
                    anmeldungenAltNeuGrid.startup();
                    anmeldungenAltNeuGrid.render();
                }
        );
    }

    function callUebersicht() {
        console.log("callUebersicht");
        elternInfo.hideErrors();
        dojo.byId("lmf_applikation").innerHTML = '<div id="lmf_service"><div id="lmf_serviceTitle">&nbsp;</div><div id="lmf_serviceContent">&nbsp;</div></div>';
        var data = {type: "Uebersicht"};
        elternInfo.lmf_postService(data,
                function(response) {
                    console.log(response);
                    var styles = elternInfo.style.tableAnmeldungenUebersicht;
                    var layoutUebersicht = [{
                            defaultCell: {editable: false, styles: styles.default.styles},
                            cells: [
                                {field: 'KlassenId', name: "Klasse", width: styles.Klasse.width, formatter: elternInfo.formatterKlasse},
                                {field: 'bezahlt', name: "Anmeldungen bezahlt: " + response.summe[0].bezahlt, width: styles.bezahlt.width},
                                {field: 'unbezahlt', name: "Anmeldungen unbezahlt: " + response.summe[0].unbezahlt, width: styles.unbezahlt.width},
                                {field: 'gesamt', name: "Anmeldungen gesamt: " + response.summe[0].gesamt, width: styles.gesamt.width},
                                {field: '_item', name: '<img class="lmf_refresh_icon" src="' + elternInfo.root + 'images/refresh1.png" height="16" width="16" onclick="callUebersicht()">', width: styles.Details.width, styles: styles.Details.styles, formatter: formatterLinkKlasse}
                            ]
                        }];
                    var data = {items: dojo.clone(response.data)};
                    var uebersichtStore = new dojo.data.ItemFileWriteStore({
                        data: data
                    });
                    uebersichtStore.comparatorMap = {};
                    uebersichtStore.comparatorMap["gesamt"] = textZuZahlComperator;
                    uebersichtStore.comparatorMap["unbezahlt"] = textZuZahlComperator;
                    uebersichtStore.comparatorMap["bezahlt"] = textZuZahlComperator;
                    uebersichtStore._forceLoad();
                    var uebersichtGrid = new dojox.grid.DataGrid(
                            {
                                store: uebersichtStore,
                                selectable: true,
                                clientSort: true,
                                autoRender: true,
                                structure: layoutUebersicht,
                                autoWidth: true,
                                autoHeight: true
                            },
                    document.createElement('div')
                            );
                    dojo.byId("lmf_serviceContent").innerHTML = "";
                    dojo.byId("lmf_serviceContent").appendChild(uebersichtGrid.domNode);
                    // Call startup, in order to  render the grid: 
                    uebersichtGrid.startup();
                    uebersichtGrid.render();
                }
        );
    }

    function formatterJaNein(value) {
        if (value && value != 0 && value != '0') {
            return '<img src="' + elternInfo.root + 'images/tick-icon.png" class="lmfSmallTicIcon">';
        } else {
            return "";
        }
    }

    function formatterJaNeinInvers(value) {
        if (value) {
            return "";
        } else {
            return '<img src="' + elternInfo.root + 'images/tick-icon.png" class="lmfSmallTicIcon">';
        }
    }

    function alteTeilnahmeFormatter(value) {
        return formatterJaNein(value);
    }

    function formatterFamilienLink(row, eltern) {
        if (eltern) {
            return '<a onClick="elternInfo.ladeStartSeiteAdmin(' + row.FamilienId + ')">' + row.elternName + '</a>';
        } else {
            return '<a onClick="elternInfo.ladeStartSeiteAdmin(' + row.FamilienId + ')">' + row.schuelerName + '</a>';
        }
    }

    var layoutKlassenUebersicht = null;


    elternInfo.UebersichtKlasseAltNeu = function(klassenId) {
        var selectedIndex = dojo.indexOf(elternInfo.familyData.KlassenKeysAlt, klassenId);
        abfrageKlassenUebersicht(klassenId, "AnmeldungenAltUndNeuKlasse",
                "Vorjahresvergleich alte Klasse " + elternInfo.familyData.KlassenStringsAlt[selectedIndex],
                'bezahltNeu', 'bezahlt neu', 'bezahlt', 'alt');
    };

    elternInfo.callKlassenUebersicht = function(klassenId) {
        var selectedIndex = dojo.indexOf(elternInfo.familyData.KlassenKeys, klassenId);
        abfrageKlassenUebersicht(klassenId, "KlassenUebersicht",
                "Einzahlungen von Klasse " + elternInfo.familyData.KlassenStrings[selectedIndex],
                'bezahlt', 'bezahlt', 'altAngemeldet', 'alt');
    };

    function offeneAnmeldungen() {
        console.log("offeneAnmeldungen");
        elternInfo.hideErrors();
        dojo.byId("lmf_applikation").innerHTML = '<div id="lmf_service"><div id="lmf_serviceTitle">&nbsp;</div><div id="lmf_serviceContent">&nbsp;</div></div>';
        dojo.byId("lmf_serviceContent").innerHTML = "";
        var data = {type: "AnmeldungenAltUndNeuKlasse",
            _klassenId: 0
        };
        elternInfo.lmf_postService(data,
                function(response) {
                    console.log(response);
                    console.log("******************" + elternInfo.initial);
                    var styles = elternInfo.style.tableKlassenDetails;
                    var layoutOffeneAnmeldungen = [{
                            defaultCell: {editable: false, styles: styles.default.styles},
                            cells: [[
                                {field: '_item', name: "Mutter / Vater", formatter: function(row) {
                                        return formatterFamilienLink(row, true);
                                    }, width: styles.Eltern.width, rowSpan:2},
                                {field: '_item', name: "Schueler", formatter: function(row) {
                                        return formatterFamilienLink(row, false);
                                    }, width: styles.Schueler.width},
                                {field: 'Telefon', width: styles.Telefon.width},
                                {field: 'email', width: styles.Email.width, formatter: function(value) {
                                        return '<a href="mailto:' + value + '">' + value + '</a>';
                                    }},
                                {field: 'KlassenId', name: 'Klasse', width: styles.alteKlassenId.width, formatter: elternInfo.formatterKlasseAlt}
                            ],[{field: "Anmerkungen", name: "Anmerkungen", width: styles.Anmerkungen.width, colSpan: 4 }]]
                        }];
                    var data = {items: dojo.clone(response.data)};
                    var offeneAnmeldungenStore = new dojo.data.ItemFileWriteStore({
                        data: data
                    });
                    console.log (data)
                    offeneAnmeldungenStore._forceLoad();
                    console.log (offeneAnmeldungenStore)
                    var offeneAnmeldungenGrid = new dojox.grid.DataGrid(
                            {
                                store: offeneAnmeldungenStore,
                                selectable: true,
                                clientSort: true,
                                autoRender: true,
                                structure: layoutOffeneAnmeldungen,
                                autoWidth: true,
                                autoHeight: true
                            },
                    document.createElement('div')
                            );
                    dojo.byId("lmf_serviceContent").innerHTML = "<div>nicht verlängerte Anmeldungen</div>";
                    dojo.byId("lmf_serviceContent").appendChild(offeneAnmeldungenGrid.domNode);
                    // Call startup, in order to  render the grid: 
                    offeneAnmeldungenGrid.startup();
                    offeneAnmeldungenGrid.render();
                    console.log ("fertig", offeneAnmeldungenGrid);
                }
        );
    }
    ;


    function abfrageKlassenUebersicht(klassenId, abfrage, titel, feld5, name5, feld6, name6) {
        console.log("callKlassenUebersicht");
        elternInfo.hideErrors();
        var data = {type: abfrage,
            _klassenId: klassenId
        };
        elternInfo.lmf_postService(data,
                function(response) {
                    console.log(response);
                    console.log("******************" + elternInfo.initial);
                    var styles = elternInfo.style.tableKlassenDetails;
                    layoutKlassenUebersicht = [{
                            defaultCell: {editable: false, styles: styles.default.styles},
                            cells: [
                                {field: '_item', name: "Mutter / Vater", formatter: function(row) {
                                        return formatterFamilienLink(row, true);
                                    }, width: styles.Eltern.width},
                                {field: '_item', name: "Schueler", formatter: function(row) {
                                        return formatterFamilienLink(row, false);
                                    }, width: styles.Schueler.width},
                                {field: 'Telefon', width: styles.Telefon.width},
                                {field: 'email', width: styles.Email.width, formatter: function(value) {
                                        return '<a href="mailto:' + value + '">' + value + '</a>';
                                    }},
                                {field: feld5, name: name5, width: styles.Spalte5.width, styles: styles.Spalte5.styles, type: dojox.grid.cells.Bool},
                                {field: feld6, name: name6, width: styles.Spalte6.width, editable: elternInfo.initial, styles: styles.Spalte6.styles, type: dojox.grid.cells.Bool, formatter: alteTeilnahmeFormatter}
                            ]
                        }];
                    var data = {items: dojo.clone(response.data)};
                    var uebersichtKlassenStore = new dojo.data.ItemFileWriteStore({
                        data: data
                    });
                    uebersichtKlassenStore._forceLoad();
                    dojo.connect(uebersichtKlassenStore, "onSet", function(item, param, oldValue, newValue) {
                        if (param === "altAngemeldet" && oldValue !== newValue) {
                            if (newValue) {
                                anAbMeldenAlt("altAnmelden", item.klassenId[0], item.schuelerId[0]);
                            } else {
                                anAbMeldenAlt("altAbmelden", item.klassenId[0], item.schuelerId[0]);
                            }
                        }
                    });
                    var uebersichtKlassenGrid = new dojox.grid.DataGrid(
                            {
                                store: uebersichtKlassenStore,
                                selectable: true,
                                clientSort: true,
                                autoRender: true,
                                structure: layoutKlassenUebersicht,
                                autoWidth: true,
                                autoHeight: true
                            },
                    document.createElement('div')
                            );
                    dojo.byId("lmf_serviceContent").innerHTML = "<div>" + titel + "</div>";
                    dojo.byId("lmf_serviceContent").appendChild(uebersichtKlassenGrid.domNode);
                    // Call startup, in order to  render the grid: 
                    uebersichtKlassenGrid.startup();
                    uebersichtKlassenGrid.render();
                }
        );
    }

    function anAbMeldenAlt(_type, _klassenid, _schuelerId) {
        console.log("anAbMeldenAlt");
        elternInfo.hideErrors();
        var data = {type: _type,
            _FamilienId: elternInfo.familyData.eltern.FamilienId,
            KlassenId: _klassenid,
            SchuelerId: _schuelerId
        };
        elternInfo.lmf_postService(data);
    }

    function elternOhneSchueler() {
        elternOhne("Schueler");
    }

    function elternOhneAnmeldungen() {
        elternOhne("Anmeldungen");
    }

    function elternOhne(ohneWas) {
        elternInfo.hideErrors();
        var data = {type: "elternOhne" + ohneWas};
        elternInfo.lmf_postService(data, function(response) {
            console.log(response);
            var styles = elternInfo.style.tableElternOhneKinderOderAnmeldungen;
            var layoutElternOhne = [{
                    defaultCell: {editable: false, styles: styles.default.styles},
                    cells: [
                        {field: '_item', name: "Name", width: styles.Name.width, formatter: function(row) {
                                return '<a onClick="elternInfo.ladeStartSeiteAdmin(' + row.FamilienId + ')">' + row.Nachname + ", " + row.Vorname + '</a>';
                            }},
                        {field: 'Telefon', width: styles.Telefon.width},
                        {field: 'Email', width: styles.Email.width, styles: styles.Email.styles},
                        {field: 'Activation', name: "aktiviert", width: styles.Aktiviert.width, styles: styles.Aktiviert.styles, formatter: formatterJaNeinInvers},
                        {field: 'Freigabe_lmf', name: "LMF-Info", width: styles.stopMail.width, styles: styles.stopMail.styles, formatter: formatterJaNein},
                        {field: 'registriertSeit', name: "angemeldet seit", width: styles.registriertSeit.width}
                    ]
                }];
            if (ohneWas == "Schueler") {
                layoutElternOhne[0].cells.push(
                        {field: '_item', name: "&nbsp;", width: styles.Loeschen.width, styles: styles.Loeschen.styles, formatter: function(row) {
                                return '<button type="button" class="lmf_klassen_loeschen_button" \n\
                            onClick="elternInfo.elternLoeschen(' + row["FamilienId"] + ',\'' + row["Vorname"] + '\',\'' + row["Nachname"] + '\')" title="löschen"></button>';
                            }}
                );
            }
            var elternOhneStore = new dojo.data.ItemFileWriteStore({
                data: {items: dojo.clone(response.data)}
            });
            elternOhneStore._forceLoad();

            var elternOhneGrid = new dojox.grid.DataGrid(
                    {
                        store: elternOhneStore,
                        selectable: true,
                        clientSort: true,
                        autoRender: true,
                        structure: layoutElternOhne,
                        autoWidth: true,
                        autoHeight: true
                    },
            document.createElement('div')
                    );
            dojo.byId("lmf_applikation").innerHTML = "<br><div>Eltern ohne " + ohneWas + "</div><br>";
            dojo.byId("lmf_applikation").appendChild(elternOhneGrid.domNode);
            // Call startup, in order to  render the grid: 
            elternOhneGrid.startup();
            elternOhneGrid.render();
        });

    }

    elternInfo.elternLoeschen = function(id, vorname, name) {
        if (confirm("Wollen Sie den Datensatz für " + vorname + " " + name + " wirklich löschen?")) {
            var data = {type: "elternLoeschen", FamilienId: id};
            elternInfo.lmf_postService(data, function(response) {
                elternOhneSchueler();
            });
        }
    };
///////////////////////////////////
// Menue Verwaltung -> Klassen
///////////////////////////////////

    elternInfo.callKlassenListe = function() {
        console.log("callKlassenListe");
        elternInfo.hideErrors();
        dojo.byId("lmf_applikation").innerHTML = '<div id="lmf_service"><div id="lmf_serviceTitle">&nbsp;</div><div id="lmf_serviceContent">&nbsp;</div></div>';
        var postData = {
            "type": "KlassenListe"
        };
        elternInfo.lmf_postService(postData,
                function(result) {
                    zeigeKlassenListe(result);
                }
        );
    };
    elternInfo.callKlassenSchuelerListe = function() {
        console.log("callKlassenSchuelerListe");
        elternInfo.hideErrors();
        dojo.byId("lmf_applikation").innerHTML = '<div id="lmf_service"><div id="lmf_serviceTitle">&nbsp;</div><div id="lmf_serviceContent">&nbsp;</div></div>';
        var postData = {
            "type": "KlassenListe"
        };
        elternInfo.lmf_postService(postData,
                function(result) {
                    zeigeKlassenListe(result);
                }
        );
    };

    var lmfLayoutKlassenListe = null;
    var lmfAlleKlassenGrid = null;
    var lmfAlleKlassenStore = null;

    function KlassenAktualisieren(row, attribute, alt, neu) {
        console.log(alt + " / " + neu);
        console.log(row);
        if (alt != neu) {
            var postData = {
                "type": "KlassenAktualisieren",
                "KlassenId": row.KlassenId[0],
                "StartJahr": row.StartJahr[0],
                "StartKlassenStufe": row.StartKlassenStufe[0],
                "SubKlasse": row.SubKlasse[0],
                "Sprache": row.Sprache[0],
                "EndKlassenStufe": row.EndKlassenStufe[0],
                "MetaKey": row.MetaKey[0]
            };
            if (0 == row.KlassenId[0]) {
                console.log("doit");
                elternInfo.lmf_postService(postData, function(data) {
                    console.log("onSuccess", data);
                    if (data.id != null) {
                        row.KlassenId = data.id;
                        lmfAlleKlassenStore.newItem(row);
                        lmfAlleKlassenGrid.render();
                    }
                });
            } else {
                elternInfo.lmf_postService(postData);
            }
        }
    }

    elternInfo.KlasseLoeschen = function(id, altName, neuName, rowIndex) {
        if (confirm("Wollen Sie die Klasse  '" + altName + "' (" + (1 + elternInfo.familyData.thisYear) + '/' + (2 + elternInfo.familyData.thisYear - 2000) + ": '" + neuName + "')  wirklich löschen?")) {
            var postData = {
                "type": "KlassenLoeschen",
                "KlassenId": id
            };
            elternInfo.lmf_postService(postData, function() {
                var rowdata = lmfAlleKlassenGrid.getItem(rowIndex);
                lmfAlleKlassenStore.deleteItem(rowdata);
            });
        }
    };

    function KlasseLoeschenFormatter(row, rowIndex) {
        return '<button type="button" class="lmf_klassen_loeschen_button" \n\
        onClick="elternInfo.KlasseLoeschen(' + row["KlassenId"] + ',\'' + row["KlassenStufeAlt"] + '\',\'' + row["KlassenStufeNeu"] + '\',' + rowIndex + ')" title="löschen"></button>';
    }
    function zeigeKlassenListe(response) {
        console.log("zeigeKlassenListe");
        if (lmfLayoutKlassenListe == null) {
            var styles = elternInfo.style.tableKlassenListe;
            lmfLayoutKlassenListe = [{
                    defaultCell: {editable: true, styles: styles.default.styles},
                    cells: [
                        {field: 'KlassenStufeAlt', editable: false, name: "Name " + elternInfo.familyData.thisYear + '/' + (1 + elternInfo.familyData.thisYear - 2000), width: styles.KlassenStufeAlt.width},
                        {field: 'KlassenStufeNeu', editable: false, name: "Name " + (1 + elternInfo.familyData.thisYear) + '/' + (2 + elternInfo.familyData.thisYear - 2000), width: styles.KlassenStufeNeu.width},
                        {field: 'StartJahr', name: "Start&shy;jahr", width: styles.StartJahr.width},
                        {field: 'StartKlassenStufe', name: "Start&shy;klassen&shy;stufe", width: styles.StartKlassenStufe.width},
                        {field: 'SubKlasse', name: "Teil&shy;klasse", width: styles.SubKlasse.width},
                        {field: 'Sprache', name: "Sprache/&shy;Kurs", width: styles.Sprache.width},
                        {field: 'EndKlassenStufe', name: "End&shy;klassen&shy;Stufe", width: styles.EndKlassenStufe.width},
                        {field: 'MetaKey', name: "Kode", width: styles.Kode.width},
                        {field: '_item', name: "&nbsp;", width: styles.Loeschen.width, formatter: KlasseLoeschenFormatter}
                    ]
                }];
        }
        var data = {items: dojo.clone(response.data), identifier: 'KlassenId'};
        lmfAlleKlassenStore = new dojo.data.ItemFileWriteStore({
            data: data
        });
        lmfAlleKlassenStore.comparatorMap = {};
        lmfAlleKlassenStore.comparatorMap["StartKlassenStufe"] = textZuZahlComperator;
        lmfAlleKlassenStore.comparatorMap["StartJahr"] = textZuZahlComperator;
        lmfAlleKlassenStore.comparatorMap["KlassenStufeAlt"] = textZuZahlComperator;
        lmfAlleKlassenStore.comparatorMap["KlassenStufeNeu"] = textZuZahlComperator;
        lmfAlleKlassenStore.comparatorMap["EndKlassenStufe"] = textZuZahlComperator;

        lmfAlleKlassenStore._forceLoad();
        dojo.connect(lmfAlleKlassenStore, "onSet", KlassenAktualisieren);

        if (lmfAlleKlassenGrid == null) {
            lmfAlleKlassenGrid = new dojox.grid.DataGrid(
                    {
                        store: lmfAlleKlassenStore,
                        selectable: true,
                        clientSort: true,
                        autoRender: true,
                        structure: lmfLayoutKlassenListe,
                        autoWidth: true,
                        autoHeight: true
                    },
            document.createElement('div')
                    );
            dojo.byId("lmf_serviceContent").innerHTML = "";
            dojo.byId("lmf_serviceContent").appendChild(lmfAlleKlassenGrid.domNode);
            // Call startup, in order to  render the grid: 
            lmfAlleKlassenGrid.startup();
            lmfAlleKlassenGrid.render();
            var f = lmfAlleKlassenGrid.getSortProps;/*grid*/
            lmfAlleKlassenGrid.getSortProps = function() {
                var info = dojo.hitch(lmfAlleKlassenGrid, f)();
                if (info != null && info[0] && info[0]['attribute'] === 'KlassenStufeAlt') {
                    info[0]['attribute'] = 'OrderAlt'; //set to reall item field for single sorting
                }
                if (info != null && info[0] && info[0]['attribute'] === 'KlassenStufeNeu') {
                    info[0]['attribute'] = 'OrderNeu'; //set to reall item field for single sorting
                }
                return info;
            };
        } else {
            lmfAlleKlassenGrid.setStore(lmfAlleKlassenStore);
            lmfAlleKlassenGrid.render();
            dojo.byId("lmf_serviceContent").innerHTML = "";
            dojo.byId("lmf_serviceContent").appendChild(lmfAlleKlassenGrid.domNode);

        }
        var neuesKlasseButton = new dijit.form.Button({
            label: "neue Klasse",
            onClick: function() {
                var neuesKlasse = {
                    KlassenStufeAlt: ["-neu angelegt-"],
                    KlassenStufeNeu: ["-neu angelegt-"],
                    StartJahr: [elternInfo.familyData.thisYear + 1],
                    StartKlassenStufe: [7],
                    EndKlassenStufe: [10],
                    SubKlasse: ["Neu"],
                    Sprache: ["Neu"],
                    OrderAlt: ["___"],
                    OrderNeu: ["___"],
                    MetaKey: [""],
                    KlassenId: "0"
                };
                KlassenAktualisieren(neuesKlasse, "KlassenId", null, "0");
            }
        },
        document.createElement('div')
                );
        dojo.byId("lmf_serviceContent").appendChild(neuesKlasseButton.domNode);
    }

    elternInfo.ladeStartSeiteAdmin = function(FamilienId) {
        console.log("ladeStartSeiteAdmin: " + FamilienId);
        var _FamilienId = FamilienId;
        elternInfo.hideErrors();
        dojo.xhrGet({
            url: elternInfo.root + "pages/familie.htm?",
            preventCache: true,
            handleAs: "text",
            load: function(response) {
                dojo.byId("lmf_applikation").innerHTML = response;
                console.log("fertig ladeStartSeiteAdmin");
                dojo.byId("lmf_div_PW_aendern").style.display = "none";
                dojo.byId("lmf_adminDaten").style.display = "block";
                dojo.byId("lmf_freigabeDaten").style.display = "block";
                elternInfo.alsAdmin = true;
                elternInfo.getFamilyData(_FamilienId, false, false);
            },
            error: function(response) {
                console.log(response);
                console.log("ladeStartSeiteadmin failed.");
                elternInfo.displayErrors(["ladeStartSeiteAdmin failed."]);
            }
        });
    };

    elternInfo.serviceLaden();

    return elternInfo;
})(window.elternInfo || {});
