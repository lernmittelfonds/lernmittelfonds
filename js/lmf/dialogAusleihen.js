window.elternInfo = (function(elternInfo) {

    dojo.require("dijit.form.Button");
    dojo.require("dijit.Dialog");
    dojo.require("dijit.form.Form");
    dojo.require("dijit.form.Button");
    dojo.require("dijit.form.Textarea");

    elternInfo.dialogAusleihenOeffnen = function(id, klassenStufe, initial) {
        console.log("dialogAusleihenOeffnen: " + klassenStufe);
        console.log(elternInfo.getSchüler(id));
        dialogAusleihen.schueler = elternInfo.getSchüler(id);
        dialogAusleihen.klassenStufe = klassenStufe;
        dialogAusleihen.show();
        if (!initial) {
            fuelleDialogFormular();
        }

        dojo.require("dijit.form.Select");
        var data = {
            type: "buecher",
            klassenStufe: dialogAusleihen.klassenStufe
        };
        elternInfo.lmf_postService(
                data,
                function(response) {
                    console.log("Buecher:");
                    console.log(response.data);
                    var _items = [];
                    _items[0] = {BuchId: -1, Titel: "--------------"};
                    for (var i = 0; i < response.data.length; i++) {
                        _items[i + 1] = dojo.clone(response.data[i]);
                    }
                    var buecherDatenQuelle = {items: _items, identifier: 'BuchId', label: 'Titel'};
                    var buecherStoreZumAusleihen = new dojo.data.ItemFileWriteStore({
                        data: buecherDatenQuelle
                    });
                    buecherStoreZumAusleihen._forceLoad();

                    var buecherSelector = dijit.byId("buecherSelector");
                    if (buecherSelector) {
                        buecherSelector.store = buecherStoreZumAusleihen;
                    } else {
                        var buecherSelector = new dijit.form.Select({
                            id: "buecherSelector",
                            name: "buchId",
                            value: "-1",
                            store: buecherStoreZumAusleihen,
                            maxHeight: -1 // tells _HasDropDown to fit menu within viewport
                        }, "lmfSelectBuch");

                    }
                    buecherSelector.startup();
                });
    };

    elternInfo.dialogAusleihenSchliessen=function() {
        dialogAusleihen.hide();
    };

    function fuelleDialogFormular() {
        document.getElementById("lmfDialogAusleihenTitel").innerHTML = "Buch ausleihen für  " + dialogAusleihen.schueler.Vorname + " " + dialogAusleihen.schueler.Nachname;
        document.getElementById("lmfSchuelerId").value = dialogAusleihen.schueler.SchuelerId;
    }

    elternInfo.buchAusleihenSpeichernNOK = function(data) {
        console.log("ERROR Kommentar speichern");
        console.log(data);
        elternInfo.displayErrors(["unbekannter Fehler beim Speichern des Kommentars"]);
        elternInfo.dialogAusleihenSchliessen();
    };
    elternInfo.buchAusleihenSpeichernOK = function(data) {
        if (data.success) {
            dojo.byId("lmf_info").innerHTML = "Der Kommentar zum Schüler wurde erfolgreich gespeichert";
            dojo.byId("lmf_info").style.display = "block";
            console.log(dialogAusleihen.schueler);
            elternInfo.getAusgeliehenes(dialogAusleihen.schueler.FamilienId);
            elternInfo.schuelerGrid.render();
        } else {
            elternInfo.displayErrors(data.errors);
        }
        elternInfo.dialogAusleihenSchliessen();
    };

    var dialogAusleihen = new dijit.Dialog({
        title: "&nbsp;",
        href: elternInfo.root + "pages/ausleihenDialog.htm",
        style: elternInfo.style.dialogAusleihenUndKommentar.style
    });
    dojo.connect(dialogAusleihen,
            "onDownloadEnd",
            fuelleDialogFormular
            );

    elternInfo.dialogOnLoadFunction();

    return elternInfo;
})(window.elternInfo || {});
