window.elternInfo = (function(elternInfo) {

    elternInfo.texteAnzeigen = function() {
        dojo.xhrGet({
            url: elternInfo.root + "pages/texte.htm?",
            preventCache: true,
            handleAs: "text",
            load: function(response) {
                dojo.byId("lmf_applikation").innerHTML = response;
                console.log("fertig texteAnzeigen");
            },
            error: function(response, ioArgs) {
                console.log(response);
                elternInfo.displayErrors(["pages/suche.htm konnte nicht geladen werden"]);
            }
        });
    };

    elternInfo.textLaden=function() {
        console.log("textLaden");
        console.log(this);
        document.getElementById('lmf_textInhalt').value = "";
        if (document.getElementById("lmf_textCode").value != "-----") {
            var postData = {
                "type": "texte",
                code: dojo.byId("lmf_textCode").value
            };
            elternInfo.lmf_postService(postData,
                    function(result) {
                        document.getElementById('lmf_textInhalt').value = result.data;
                        document.getElementById('lmfTextSpeichernButton').disabled = true;
                    }
            );
        }
    };

    elternInfo.textSpeichern=function(evt) {
        dojo.stopEvent(evt);
        document.getElementById('lmfTextSpeichernButton').disabled = true;
        var postData = {
            "type": "texte",
            code: dojo.byId("lmf_textCode").value,
            text: dojo.byId("lmf_textInhalt").value
        };
        elternInfo.lmf_postService(postData,
                function(result) {
                    if (result.success) {
                        document.getElementById('lmfTextSpeichernButton').disabled = true;
                    } else {
                        document.getElementById('lmfTextSpeichernButton').disabled = false;
                    }
                }
        );
        return false; // submit verhindern!
    };

    elternInfo.texteAnzeigen();

    return elternInfo;
})(window.elternInfo || {});
