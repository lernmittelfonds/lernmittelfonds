window.elternInfo = (function (elternInfo) {
    elternInfo.root = "/lmf/";
    elternInfo.bildAendern = function (_this, bildname) {
        _this.className = "lmf_buecher_button_" + bildname;
    };
    elternInfo.bilder = {
        ausgeliehen: '<img src="' + elternInfo.root + 'images/ausgeliehen2.png" class="lmfBuchIcon">',
        einsammeln: '<img src="' + elternInfo.root + 'images/einsammeln2.png" class="lmfBuchIcon" title="Einsammeln">',
        eingesammelt: '<img src="' + elternInfo.root + 'images/eingesammelt2.png" class="lmfBuchIcon" >',
        ausleihen: '<img src="' + elternInfo.root + 'images/ausleihen2.png" class="lmfBuchIcon" title="Ausgeben">'
    };



    dojo.require("dijit.dijit");
    dojo.require("dijit.form.Button");
    dojo.require("dijit.InlineEditBox");
    dojo.require("dijit.form.Textarea");
    dojo.require("dojo.data.ItemFileWriteStore");
    dojo.require("dojox.grid.DataGrid");
    dojo.require("dojox.grid.cells.dijit");
    dojo.require("dojox.grid.cells._base");
    dojo.require("dojo.data.ItemFileWriteStore");
    dojo.require("dojox.grid.DataGrid");
    dojo.require("dojox.grid.cells.dijit");
    dojo.require("dojox.grid.cells._base");
    dojo.require("dijit.MenuBar");
    dojo.require("dijit.MenuBarItem");
    dojo.require("dijit.PopupMenuBarItem");
    dojo.require("dijit.Menu");
    dojo.require("dijit.MenuItem");
    dojo.require("dijit.PopupMenuItem");

    dojo.require("dijit.form.Button");
    dojo.require("dijit.Dialog");
    dojo.require("dijit.form.Form");
    dojo.require("dijit.form.Button");
    dojo.require("dijit.form.Textarea");
    dojo.require("dijit.form.Button");
    dojo.require("dijit.ProgressBar");
    dojo.require("dijit.form.Select");
    dojo.require("dijit.Editor");
    dojo.require("dijit._editor.plugins.AlwaysShowToolbar");
    dojo.require("lmf.FileUploader");


    dojo.addOnLoad(
            function () {
                elternInfo.loadJs("styles");
                elternInfo.checkLogin();
            }
    );

    function callServiceSite() {
        if (typeof (elternInfo.serviceLaden) == "undefined") {
            elternInfo.loadJs("service");
        } else {
            elternInfo.serviceLaden();
        }
    }
    elternInfo.onLoadLogin = function () {
        var form = dojo.byId("loginForm");
        if (elternInfo.appType == "eis") {
            document.getElementById("lmfAnmeldeLegende").innerHTML = "Login ElternInfoSystem";
            document.getElementById("lmfAnmeldeText").innerHTML = "Anmeldung zum ElternInfoSystem";
        }
        dojo.connect(form, "onsubmit", function (event) {
            // Stopt das Submitevent - wir übertragen es selbst asynchron
            dojo.stopEvent(event);
            elternInfo.hideErrors();
            // das Formular definiert Parameter und URL!
            var xhrArgs = {
                form: dojo.byId("loginForm"),
                url: elternInfo.root + "php/login.php",
                handleAs: "json",
                load: function (data) {
                    console.log(data);
                    if (data.PW_changed) {
                        elternInfo.loginSeiteLaden("Falls Ihre Mailadresse bei uns registriert ist, so wurde Ihr Passwort geändert und an die Adresse verschickt.", false);
                    }
                    if (data.neueAnmeldung) {
                        elternInfo.aktivierungsSeiteLaden(data.errors[0], false);
                        return;
                    }
                    if (data.errors.length > 0) {
                        elternInfo.displayErrors(data.errors);
                    } else {
                        elternInfo.nurLmf = data.nurLmf;
                        elternInfo.istEV = data.istEV;
                        elternInfo.istAdmin = data.isAdmin || false;
                        elternInfo.altAnmeldenGestoppt = data.anmeldenGestoppt;
                        elternInfo.neuAnmeldenGestoppt = data.neuAnmeldenGestoppt;
                        elternInfo.anmeldenFreigegeben = data.anmeldenFreigegeben;
                        elternInfo.initial = data.initial;
                        if (elternInfo.appType == "eis") {
                            elternInfo.ladeFamilienseiteEis();
                        } else {
                            elternInfo.ladeStartSeite();
                        }
                    }
                },
                error: function (error) {
                    console.log(error);
                    elternInfo.displayErrors([error]);
                }
            };
            dojo.xhrPost(xhrArgs);
        });
    };
    elternInfo.onLoadAktivierung = function () {
        var form = dojo.byId("aktivierungsForm");
        document.getElementById("lmfAnmeldeLegende").innerHTML = "Aktivierung Lernmittelfonds";
        dojo.connect(form, "onsubmit", function (event) {
            // Stopt das Submitevent - wir übertragen es selbst asynchron
            dojo.stopEvent(event);
            elternInfo.hideErrors();
            // das Formular definiert Parameter und URL!
            var xhrArgs = {
                form: dojo.byId("aktivierungsForm"),
                url: elternInfo.root + "php/login.php",
                handleAs: "json",
                load: function (data) {
                    console.log(data);
                    if (data.errors.length > 0) {
                        elternInfo.displayErrors(data.errors);
                    } else {
                        elternInfo.ladeStartSeite();
                    }
                },
                error: function (error) {
                    console.log(error);
                    elternInfo.displayErrors([error]);
                }
            };
            dojo.xhrPost(xhrArgs);
        });
    };

    elternInfo.ladeStartSeite = function (andResetAlsAdmin) {
        if (andResetAlsAdmin)
            elternInfo.alsAdmin = false;
        console.log("ladeStartSeite");
        elternInfo.hideErrors();
        dojo.xhrGet({
            url: elternInfo.root + "pages/rahmen.htm?" + new Date().getTime(),
            handleAs: "text",
            load: function (response) {
                dojo.byId("lmfContent").innerHTML = response;
                dojo.xhrGet({
                    url: elternInfo.root + "pages/familie.htm?" + new Date().getTime(),
                    handleAs: "text",
                    load: function (response) {
                        dojo.byId("lmf_familie").innerHTML = response;
                        if (elternInfo.nurLmf) {
                            dojo.byId("lmf_WechselApplikation").style.display = "none";
                        }
                        console.log("fertig ladeStartSeite");
                        elternInfo.getFamilyData();
                        setTimeout(elternInfo.checkUndStartMailJobs(), 2000);
                    },
                    error: function (response) {
                        console.log(response);
                        console.log("ladeStartSeite failed.");
                        elternInfo.displayErrors(["ladeStartSeite failed."]);
                    }
                });
            },
            error: function (response) {
                console.log(response);
                console.log("ladeStartSeite failed.");
                elternInfo.displayErrors(["ladeStartSeite failed."]);
            }
        });
    };

    function formatterJaNein(value) {
        if (value && value != 0 && value != '0') {
            return '<img src="' + elternInfo.root + 'images/tick-icon.png" class="lmfSmallTicIcon">';
        } else {
            return "...";
        }
    }
    elternInfo.showMailJobs = function () {
        if (!elternInfo.mailJobs) {
            elternInfo.mailJobs = [];
        }
        var styles = elternInfo.style.tableMailJobs;
        var layoutMailJobs = [{
                defaultCell: {editable: false, styles: styles.default.styles},
                cells: [
                    {field: 'zeitpunkt', name: "Zeitpunkt", width: styles.zeitpunkt.width},
                    {field: 'kommentar', name: "Kommentar", width: styles.kommentar.width},
                    {field: 'anzahl', name: "Gesamt", width: styles.anzahl.width, styles: styles.anzahl.styles},
                    {field: 'fehler', name: "Fehler", width: styles.fehler.width, styles: styles.fehler.styles},
                    {field: 'beendet', name: "beendet", width: styles.beendet.width, styles: styles.beendet.styles, formatter: formatterJaNein},
                    {field: '_item', name: "löschen", width: styles.Loeschen.width, styles: styles.Loeschen.styles, formatter: function (row) {
                            return '<button type="button" class="lmf_klassen_loeschen_button" \n\
                            onClick="elternInfo.mailLoeschen(' + row["jobId"] + ')" title="löschen"></button>';
                        }}
                ]
            }];
        var mailJobStore = new dojo.data.ItemFileWriteStore({
            data: {items: dojo.clone(elternInfo.mailJobs)}
        });
        mailJobStore._forceLoad();

        var mailJobGrid = new dojox.grid.DataGrid(
                {
                    store: mailJobStore,
                    selectable: true,
                    clientSort: true,
                    autoRender: true,
                    structure: layoutMailJobs,
                    autoWidth: true,
                    autoHeight: true
                },
                document.createElement('div')
                );
        dojo.byId("lmf_applikation").innerHTML = "<br><div>Beauftragte Mailjobs:</div><br>";
        dojo.byId("lmf_applikation").appendChild(mailJobGrid.domNode);
        // Call startup, in order to  render the grid: 
        mailJobGrid.startup();
        mailJobGrid.render();
    }

    elternInfo.mailLoeschen = function (jobId) {
        var data = {
            type: "deleteMailJob",
            jobId: jobId
        };

        elternInfo.hideErrors();
        var xhrArgs = {
            url: elternInfo.root + "php/mailHandler.php",
            preventCache: true,
            postData: dojo.toJson(data),
            handleAs: "json",
            headers: {"Content-Type": "application/json; charset=utf-8"},
            load: function (response) {
                console.log("lmf_post EvKlasse Succes" + response);
                if (response["success"]) {
                    elternInfo.mailJobs = [];
                    if (response["daten"]) {
                        if (response["daten"]["allJobs"]) {
                            elternInfo.mailJobs = response["daten"]["allJobs"];
                        }
                    } else {
                        elternInfo.displayInfo("Der letzte vorhandene Mail Job wurde gelöscht!");
                        dojo.byId("mail-icon-span").style.display = "none";
                    }
                    elternInfo.showMailJobs();
                } else {
                    elternInfo.displayErrors(["Fehler beim Löschen des Mail Jobs!"]);
                }
            },
            error: function (error) {
                console.log("deleteMailJob " + jobId + " Error :" + error);
                elternInfo.displayErrors([error]);
            }
        };
        dojo.xhrPost(xhrArgs);
    };


    elternInfo.checkUndStartMailJobs = function () {
        if (elternInfo.mailJobs && elternInfo.mailJobs.length > 0) {
            dojo.byId("mail-icon-span").style.display = "block";
        } else {
            dojo.byId("mail-icon-span").style.display = "none";
        }

        if (elternInfo.checkUndStartMailJobsRunning) {
            return; // da läuft schon ein Mail job
        }
        elternInfo.checkUndStartMailJobsRunning = true;
        dojo.xhrGet({
            url: elternInfo.root + "php/mailHandler.php",
            handleAs: "json",
            load: function (response) {
                var intervall = 30000;
                if (response["daten"] && response["daten"]["allJobs"] && response["daten"]["allJobs"].length>0) {
                    elternInfo.mailJobs = response["daten"]["allJobs"];
                    dojo.byId("mail-icon-span").style.display = "block";
                } else {
                    elternInfo.mailJobs = [];
                    dojo.byId("mail-icon-span").style.display = "none";
                }
                if (response["success"] && response["daten"]) {
                    if (response["daten"]["openJobCount"] > 0) {
                        if (response["daten"]["openJobCount"] > 1 || response["daten"]["offen"] > 0) {
                            dojo.byId("mail-icon").title = "Mailprozess 1/" + response["daten"]["openJobCount"] + " läuft. Mail: " + (response["daten"]["mailsGesamt"] - response["daten"]["offen"]) + "/" + response["daten"]["mailsGesamt"];
                            var triggerExtern = response["daten"]["MAIL_TRIGGER_EXTERN"];
                            intervall = triggerExtern ? 30000 : 1000 * response["daten"]["CALL_INTERVAL_ON_CRON"];
                        } else {
                            elternInfo.checkUndStartMailJobsRunning = false;
                            // nur ein job und der ist fertig
                            elternInfo.displayInfo("Alle Mails wurde abgearbeitet.")
                            dojo.byId("mail-icon").title = "keine offenen Mailjobs gefunden!";
                        }
                    }
                }
                setTimeout(function () {
                    elternInfo.checkUndStartMailJobsRunning = false;
                    elternInfo.checkUndStartMailJobs();
                }, intervall);
            },
            error: function (response) {
                console.log("checkUndStartMailJobs failed.", response);
                elternInfo.displayErrors(["checkUndStartMailJobs failed."]);
                elternInfo.checkUndStartMailJobsRunning = false;
                setTimeout(function () {
                    elternInfo.checkUndStartMailJobs();
                }, 30000);
            }
        });
    };

    elternInfo.kommentarBearbeiten = function (id) {
        var _id = id;
        if (typeof (elternInfo.dialogKommentarOeffnen) == "undefined") {
            elternInfo.dialogOnLoadFunction = function () {
                elternInfo.dialogKommentarOeffnen(_id, true);
            };
            elternInfo.loadJs("dialogKommentar");
        } else {
            elternInfo.dialogKommentarOeffnen(id, false);
        }
    };

    elternInfo.ausleihen = function (schuelerId, klassenStufe) {
        var _id = schuelerId;
        console.log("ausleihen: " + schuelerId + " / " + klassenStufe);
        if (typeof (elternInfo.dialogAusleihenOeffnen) == "undefined") {
            elternInfo.dialogOnLoadFunction = function () {
                elternInfo.dialogAusleihenOeffnen(_id, klassenStufe, true);
            };
            elternInfo.loadJs("dialogAusleihen");
        } else {
            elternInfo.dialogAusleihenOeffnen(_id, klassenStufe, false);
        }
    };

    elternInfo.dialogOnLoadFunction = function () {
    };

    var schuelerStore = null;
    elternInfo.schuelerGrid = null;
    var neuerSchuelerSchaltflaeche = null;
    var layoutSchuelerGrid = null;
    var layoutSchuelerGridEis = null;
    elternInfo.istAdmin = false;
    elternInfo.appType = elternInfo.appType || "lmf";
    elternInfo.anmeldenFreigegeben = false;
    elternInfo.altAnmeldenGestoppt = false;

    elternInfo.wechselApplikation = function () {
        if (elternInfo.appType == "lmf") {
            elternInfo.appType = "eis";
            dojo.byId("lmf_WechselApplikation").innerHTML = "zum Lernmittelfonds";
            dojo.byId("lmfTitel").innerHTML = "ElternInfoSystem (EIS)";
            elternInfo.ladeFamilienseiteEis(elternInfo.familyData.eltern.FamilienId);
        } else {
            elternInfo.appType = "lmf";
            elternInfo.ladeStartSeite();
        }
    };
    elternInfo.rufeStammdatenSeite = function () {
        if (elternInfo.appType == "lmf") {
            elternInfo.ladeStartSeite();
        } else {
            elternInfo.ladeFamilienseiteEis(elternInfo.familyData.eltern.FamilienId);
        }
    };

    function anmeldenErlaubt(letztesJahrAngemeldet) {
        console.log(elternInfo.anmeldenFreigegeben, elternInfo.altAnmeldenGestoppt, 1 * letztesJahrAngemeldet > 0, letztesJahrAngemeldet)
        console.log(elternInfo.anmeldenFreigegeben, elternInfo.neuAnmeldenGestoppt, 1 * letztesJahrAngemeldet < 0, letztesJahrAngemeldet)
        return elternInfo.anmeldenFreigegeben
                // die Freigabe zur anmeldung ist erteilt
                && ((!elternInfo.altAnmeldenGestoppt && 1 * letztesJahrAngemeldet > 0)
                        // und (Altanmeldungen wurden noch nicht gestoppt und es ist eine Altanmeldung
                        || (!elternInfo.neuAnmeldenGestoppt && 1 * letztesJahrAngemeldet < 1));
        // oder  Neuanmeldungen wurden noch nicht gestoppt und es ist eine Neuanmeldung
    }

    elternInfo.formatterKlasse = function (klassenId) {
        var selectedIndex = dojo.indexOf(elternInfo.familyData.KlassenKeys, klassenId);
        var ret = "";
        if (elternInfo.familyData.KlassenStrings[selectedIndex]) {
            ret += elternInfo.familyData.KlassenStrings[selectedIndex];
        } else {
            ret += "";
        }
        return ret;
    };

    function formatterTerminKlasseNeu(klassenId, rowIndex) {
        var ret = elternInfo.formatterKlasse(klassenId);
        if (this.grid && this.grid.getItem(rowIndex)) {
            var row = this.grid.getItem(rowIndex);

            ret += "<hr class=\"lmf_gridLinkLine\">";
            if (row.nextYear == "1") {
                ret += "bezahlt";
            } else if (row.nextYear == "0") {
                ret += "angemeldet";
            } else if (row.nextYear == "2") {
                ret += "befreit";
            } else if (row.nextYear == "-2") {
                ret += "abgemeldet";
            } else {
                ret += "?????";
            }
        }
        return ret;
    }

    elternInfo.formatterKlasseAlt = function (klassenId) {
        var selectedIndex = dojo.indexOf(elternInfo.familyData.KlassenKeysAlt, klassenId);
        var ret = "";
        if (elternInfo.familyData.KlassenStringsAlt[selectedIndex]) {
            ret += elternInfo.familyData.KlassenStringsAlt[selectedIndex];
        } else {
            ret += "";
        }
        return ret;
    };

    function formatterTerminKlasseAlt(klassenId, rowIndex) {
        var ret = elternInfo.formatterKlasseAlt(klassenId);
        if (this.grid && this.grid.getItem(rowIndex)) {
            var row = this.grid.getItem(rowIndex);
            ret += "<hr class=\"lmf_gridLinkLine\">";
            if (row.thisYear == "1" || row.thisYear == "2") {
                ret += "nimmt teil";
            } else if (row.thisYear == "0") {
                ret += "nicht bezahlt";
            } else {
                ret += "<div onClick='alert(\"Die Teilnahme im Vorjahr wird durch uns manuell geprüft und gegebenenfalls nachgetragen\")'>keine Teilnahme<div>";
            }
        }
        return ret;
    }

    function formatterChange(row) {
        var klassenStufe = elternInfo.formatterKlasseAlt(row.alteKlassenId);
        var ret = "";
        klassenStufe = klassenStufe.substr(0, klassenStufe.indexOf("-"));
        klassenStufe = isNaN(klassenStufe) ? null : klassenStufe;
        if (elternInfo.anmeldenFreigegeben) {
            if (row.nextYear == "1") {
                if (elternInfo.istAdmin) {
                    ret += "<a title=\"Zahlung lösen.\" href='javascript: elternInfo.zahlungLoesen(" + row.SchuelerId + ")'>Zahlung lösen</a>";
                    if (klassenStufe) {
                        ret += "<hr class=\"lmf_gridLinkLine\"><a title=\"neues Buch ausleihen.\" href='javascript: elternInfo.ausleihen(" + row.SchuelerId + "," + klassenStufe + ")'>ausleihen</a>";
                    }
                }
                return ret;
            } else {
                if (row.nextYear == "0" || row.nextYear == "2") {
                    ret = "<a title=\"Die derzeitige Anmeldung für das nächste Jahr zurücknehmen.\" href='javascript: elternInfo.abmelden(" + row.SchuelerId + "," + row.FamilienId + ")'>abmelden</a>";
                } else {
                    ret = "<a title=\"Das Kind für das nächste Jahr für den Lernmittelfonds anmelden.\" href='javascript: elternInfo.anmelden(" + row.SchuelerId + "," + row.thisYear + ")'>anmelden</a>";
                }
                // offene Anmeldungen können sich auch abmelden
                if (row.nextYear == "-1") {
                    ret = ret + "<hr class=\"lmf_gridLinkLine\"><a title=\"Die derzeitige Anmeldung für das nächste Jahr zurücknehmen.\" href='javascript: elternInfo.abmelden(" + row.SchuelerId + "," + row.FamilienId + ")'>abmelden</a>";
                }
                if (row.fixed != "1") {
                    ret = ret + "<hr class=\"lmf_gridLinkLine\"><a title=\"Den Datensatz für das Kind vollständig löschen.\" href='javascript: elternInfo.loeschen(" + row.SchuelerId + "," + row.FamilienId + ")'>löschen</a>";
                }
                if (elternInfo.istAdmin && klassenStufe) {
                    ret = ret + "<hr class=\"lmf_gridLinkLine\"><a title=\"neues Buch ausleihen.\" href='javascript: elternInfo.ausleihen(" + row.SchuelerId + "," + klassenStufe + ")'>ausleihen</a>";
                }
                return ret;
            }
        } else {
            // zu Beginn des Schuljahres noch Anmeldungen für das laufende Schuljahr
            if (row.thisYear == "0" || row.thisYear == "2") {
                //ret = "";
                ret = "<a title=\"Die derzeitige Anmeldung für das nächste Jahr zurücknehmen.\" href='javascript: elternInfo.abmelden(" + row.SchuelerId + "," + row.FamilienId + ",\"thisYear\")'>abmelden</a>";

            } else if (row.thisYear == "-1" || row.thisYear == "-2") {
                if (elternInfo.istAdmin) {
                    ret = "<a title=\"Das Kind für dieses Schuljahr für den Lernmittelfonds anmelden.\" href='javascript: elternInfo.anmelden(" + row.SchuelerId + "," + row.thisYear + ")'>anmelden</a>";
                }
            }
            if (row.fixed != "1") {
                ret = ret + "<hr class=\"lmf_gridLinkLine\"><a title=\"Den Datensatz für das Kind vollständig löschen.\" href='javascript: elternInfo.loeschen(" + row.SchuelerId + "," + row.FamilienId + ")'>löschen</a>";
            }
            if (elternInfo.istAdmin && klassenStufe) {
                ret = ret + "<hr class=\"lmf_gridLinkLine\"><a title=\"neues Buch ausleihen.\" href='javascript: elternInfo.ausleihen(" + row.SchuelerId + "," + klassenStufe + ")'>ausleihen</a>";
            }
            return ret;
        }
    }

    function formatterKlassenliste(row) {
        console.log("********");
        console.log(row);
        if (row["EisStatus"]) {
            var eisStatus = row["EisStatus"][0];
        } else {
            var eisStatus = 0;
        }
        var kontaktlistenLink = "<a title=\"Kontaktdatenliste anzeigen.\" href='javascript: elternInfo.kontaktListeAnzeigen(" + row.alteKlassenId + "," + eisStatus + ")'>Kontaktliste</a><br>";
        if (row.fixed[0] != "1") {
            kontaktlistenLink += "<hr class=\"lmf_gridLinkLine\"><a title=\"Den Datensatz für das Kind vollständig löschen.\" href='javascript: elternInfo.loeschen(" + row.SchuelerId + "," + row.FamilienId + ")'>löschen</a>";
        }
        if (row["istEV"] && row["istEV"][0]) {
            return kontaktlistenLink
                    + "<hr class=\"lmf_gridLinkLine\"><a title=\"Rundmail versenden.\" href='javascript: elternInfo.starteRundmail(" + row.alteKlassenId + ")'>Rundmail</a><br>"
                    + "<hr class=\"lmf_gridLinkLine\"><a title=\"Berechtigungen für neue Nutzer der Klasse vergeben.\" href='javascript: elternInfo.freiSchalten(" + row.alteKlassenId + ")'>Frei schalten</a>";
        } else {
            return kontaktlistenLink;
        }
    }
    function formatterCheckboxReadOnly(isChecked) {
        if (isChecked) {
            return '<img src="' + elternInfo.root + 'images/checkedTrue.png"  class="lmfBuchIcon" title="Einsammeln">';
        } else {
            return '<img src="' + elternInfo.root + 'images/checkedFalse.png"  class="lmfBuchIcon" title="Einsammeln">';
        }
    }

    function schuelerDatenAendern(SchuelerId, parameter, newValue) {
        schuelerStore.fetchItemByIdentity({
            identity: SchuelerId,
            onItem: function (item) {
                if (schuelerStore.isItem(item)) {
                    schuelerStore.setValue(item, parameter, newValue);
                }
            }
        });
    }

    elternInfo.anmelden = function (SchuelerId, letztesJahrAngemeldet) {
        var _si, _ks;
        var valideKlasse = false;
        var jahrstring = "dieses";
        var klassenId = -1;
        if (elternInfo.schuelerGrid.edit.isEditing()) {
            elternInfo.schuelerGrid.edit.apply();
        }
        schuelerStore.fetchItemByIdentity({
            identity: SchuelerId,
            onItem: function (item) {
                if (schuelerStore.isItem(item)) {
                    if (elternInfo.anmeldenFreigegeben) {
                        klassenId = schuelerStore.getValue(item, "KlassenId");
                    } else {
                        klassenId = schuelerStore.getValue(item, "alteKlassenId");
                    }
                }
            }
        });

        var jahr = anmeldenErlaubt(letztesJahrAngemeldet) ? 1 : (elternInfo.altAnmeldenGestoppt ? 1 : 0);
        if (jahr === 0) {
            _si = dojo.indexOf(elternInfo.familyData.KlassenKeysAlt, klassenId);
            _ks = elternInfo.familyData.KlassenStringsAlt[_si];
        } else {
            _si = dojo.indexOf(elternInfo.familyData.KlassenKeys, klassenId);
            _ks = elternInfo.familyData.KlassenStrings[_si];
            jahrstring = "das nächste";

        }
        console.log(_ks, anmeldenErlaubt(letztesJahrAngemeldet), elternInfo.altAnmeldenGestoppt)
        if (klassenId !== null && _ks && _ks.substring(0, 3) !== "---" && _ks.substring(0, 3) !== "+++") {
            valideKlasse = true;
        }
        console.log("valideKlasse", valideKlasse)
        if (!valideKlasse) {
            elternInfo.displayErrors(["Fehler: Bitte wählen sie zuerst eine gültige Klasse für " + jahrstring + " Schuljahr aus."]);
        } else if (elternInfo.familyData.eltern.zuzahlungsBefreit == "1" || anmeldenErlaubt(letztesJahrAngemeldet)) {
            if (elternInfo.familyData.eltern.zuzahlungsBefreit == "1" || confirm('Mit dieser Anmeldung stimme ich den Grundsätzen des Lernmittelfonds zu!\n\nSie finden diese am unteren Ende dieser Seite.')) {
                elternInfo.lmf_save({
                    type: "anmelden",
                    jahr: elternInfo.familyData.thisYear + jahr,
                    SchuelerId: SchuelerId
                }, function (data) {
                    if (elternInfo.anmeldenFreigegeben) {
                        anmeldenErfolgreich(data, "nextYear");
                    } else {
                        anmeldenErfolgreich(data, "thisYear");
                    }
                });
            } else {
                console.log("Grundsätzen des Lernmittelfonds abgelehnt");
            }
        } else {
            if (elternInfo.altAnmeldenGestoppt) {
                if (elternInfo.istAdmin) {
                    if (confirm("Achtung! Sie melden den Schüler als Administrator für " + jahrstring + " Schuljahr an. \n"
                            + "Die Eltern werden eine entsprechende Mail mit einer Zahlungsaufforderung erhalten. \n"
                            + "Wollen Sie das wirklich?")) {
                        elternInfo.lmf_save({
                            type: "anmelden",
                            jahr: elternInfo.familyData.thisYear + jahr,
                            SchuelerId: SchuelerId
                        }, function (data) {
                            anmeldenErfolgreich(data, "nextYear");
                        });

                    }
                } else {
                    alert("Leider ist für dieses Jahr keine Anmeldung mehr möglich.\nBitte beachten sie im nächsten Jahr die veröffentlichten Anmeldezeiten.");
                }
            } else {
                if (elternInfo.istAdmin) {
                    if (confirm("Achtung! Sie melden den Schüler als Administrator für das laufende Schuljahr an. \n"
                            + "Die Eltern werden eine entsprechende Mail mit einer Zahlungsaufforderung erhalten. \n"
                            + "Wollen Sie das wirklich?")) {
                        elternInfo.lmf_save({
                            type: "anmelden",
                            jahr: elternInfo.familyData.thisYear,
                            SchuelerId: SchuelerId
                        }, function (data) {
                            anmeldenErfolgreich(data, "thisYear");
                        });
                    }
                } else {
                    alert("Für das nächste Schuljahr sind noch keine Anmeldungen möglich.\nWir werden sie rechtzeitig per Mail informieren.");
                }
            }
        }
    };

    function anmeldenErfolgreich(data, jahr) {
        console.log(data);
        if (jahr == "thisYear" || data.beitrag == 0) {
            schuelerDatenAendern(data.schuelerId, jahr, "1");
        } else {
            schuelerDatenAendern(data.schuelerId, jahr, "0");
        }
        elternInfo.displayInfo(data.content);
    }

    elternInfo.abmelden = function (SchuelerId, FamilienId, jahr) {
        console.log("elternInfo.abmelden");
        var _jahr = (jahr == null ? "nextYear" : jahr);
        var offset = (_jahr == "nextYear" ? 1 : 0);
        elternInfo.lmf_save({
            type: "abmelden",
            FamilienId: FamilienId,
            SchuelerId: SchuelerId,
            offset: offset
        },
                function () {
                    schuelerDatenAendern(SchuelerId, _jahr, "-2");
                }
        );
    };

    elternInfo.zahlungLoesen = function (schuelerId) {
        elternInfo.lmf_save({
            type: "zahlungLoesen",
            SchuelerId: schuelerId
        },
                function () {
                    schuelerDatenAendern(schuelerId, "nextYear", "0");
                }
        );
    };


    elternInfo.loeschen = function (SchuelerId, FamilienId) {
        elternInfo.lmf_save({
            type: "schuelerLoeschen",
            FamilienId: FamilienId,
            SchuelerId: SchuelerId
        },
                function () {
                    schuelerStore.fetchItemByIdentity({
                        identity: SchuelerId,
                        onItem: function (item) {
                            if (schuelerStore.isItem(item)) {
                                schuelerStore.deleteItem(item);
                                elternInfo.schuelerGrid.render();
                            }
                        }
                    });
                }
        );
    };

    function getCheckboxInhalt(checkbox) {
        if (formHalter[checkbox] !== undefined) {
            if (formHalter[checkbox].checked) {
                return 1;
            }
        }
        return 0;
    }


    function lmf_save_eltern() {
        console.log("lmf_save_eltern");
        console.log(this);
        var zzb, istAdmin, stopMail = 0;
        if (formHalter.lmf_zuzahlungsBefreit_CheckBox !== undefined) {
            if (formHalter.lmf_zuzahlungsBefreit_CheckBox.checked) {
                zzb = 1;
            } else {
                zzb = 0;
            }
        }
        if (formHalter.lmf_ist_Admin_CheckBox !== undefined) {
            if (formHalter.lmf_ist_Admin_CheckBox.checked) {
                istAdmin = 1;
            } else {
                istAdmin = 0;
            }
        }
        if (formHalter.lmf_stopMail_CheckBox !== undefined) {
            if (formHalter.lmf_stopMail_CheckBox.checked) {
                stopMail = 1;
            } else {
                stopMail = 0;
            }
        }

        var elternData = {
            type: "eltern_save",
            FamilienId: elternInfo.familyData.eltern.FamilienId,
            Vorname: formHalter.lmf_Vorname.getValue(),
            Nachname: formHalter.lmf_Nachname.getValue(),
            Telefon: formHalter.lmf_Telefon.getValue(),
            Email: formHalter.lmf_Email.getValue(),
            Anschrift: formHalter.lmf_Anschrift.getValue(),
            Freigabe_ev: getCheckboxInhalt("lmf_freigabe_ev_CheckBox"),
            Freigabe_alle: getCheckboxInhalt("lmf_freigabe_alle_CheckBox"),
            Freigabe_info: getCheckboxInhalt("lmf_freigabe_info_CheckBox"),
            Freigabe_lmf: getCheckboxInhalt("lmf_freigabe_lmf_CheckBox"),
            stopMail: stopMail,
            zuzahlungsBefreit: zzb,
            istAdmin: istAdmin
        };
        elternInfo.lmf_save(elternData);
    }

    function lmf_schuelerSpeichern(schuelerDaten, param, alterWert, neuerWert) {
        console.log("start lmf_schuelerSpeichern: " + param + "=" + neuerWert);
        console.log(schuelerDaten);
        if (param !== 'nextYear' && param !== 'thisYear' && alterWert !== neuerWert) {
            transferData = elternInfo.itemToJS(schuelerStore, schuelerDaten);
            transferData.type = "schuelerSpeichern";
            if (param === "KlassenId") {
                transferData.subType = "anmeldungNaechstesJahr";
            }
            if (param === "alteKlassenId") {
                transferData.subType = "anmeldungDiesesJahr";
                transferData.KlassenId = transferData.alteKlassenId;
            }
            elternInfo.lmf_save(transferData, function () {
                elternInfo.getFamilyData(transferData.FamilienId, true);
                elternInfo.displayInfo("Sie haben die Daten Ihres Kindes erfolgreich geändert. "
                        + "<b>Bitte kontrollieren Sie noch einmal</b>, ob die Klassen für die beiden Schuljahre korrekt sind.");
            });
        }
    }

    function schuelerHinzufuegen() {
        console.log(schuelerHinzufuegen);
        console.log(elternInfo.familyData.eltern.FamilienId);
        console.log(formHalter.lmf_Nachname.getValue());
        console.log(elternInfo.familyData);
        console.log(elternInfo.familyData.KlassenKeys);
        var neuesKind = {
            "type": "neuerSchueler",
            FamilienId: elternInfo.familyData.eltern.FamilienId,
            Vorname: "Vorname",
            EisStatus: 0,
            Nachname: formHalter.lmf_Nachname.getValue(),
            alteKlassenId: 0,
            thisYear: -1,
            nextYear: -1,
            fixed: 0,
            KlassenId: 0
        };
        elternInfo.lmf_save(neuesKind,
                function (data) {
                    console.log("neues Kind");
                    delete neuesKind.type;
                    neuesKind.SchuelerId = data.newId;
                    schuelerStore.newItem(neuesKind);
                    var message = "Sie haben ein neues Kind erfolgreich hinzugefügt. Bitte ändern Sie jetzt den Namen, und tragen";
                    if (elternInfo.appType == "lmf") {
                        message += "im Normalfall die Klasse ein, die Ihr Kind im <b>NÄCHSTEN</b> Jahr besuchen wird.<br>"
                                + "Im Anschluss klicken Sie auf 'anmelden' um die Anmeldung abzuschließen. Sie erhalten dann eine Mail mit der Kontoverbindung.";
                    } else {
                        message += "in der linken Klassenspalte die Klasse ein, die Ihr Kind in diesem Schuljahr besuchet";
                    }
                    elternInfo.displayInfo(message);
                    elternInfo.schuelerGrid.render();
                }
        );
    }
    var formHalter = {
        lmf_Vorname: null,
        lmf_Nachname: null,
        lmf_Telefon: null,
        lmf_Email: null,
        lmf_Anschrift: null,
        lmf_Freigabe_ev: 0,
        lmf_Freigabe_alle: 0,
        lmf_Freigabe_info: 0,
        lmf_Freigabe_lmf: 0,
        lmf_zuzahlungsBefreit: 0,
        lmf_stopMail: 0,
        lmf_ist_Admin: 0
    };
    function getCheck(inRowIndex, row) {
        if (!this.grid.expandedRows)
            this.grid.expandedRows = [];
        return {image: (this.grid.expandedRows[inRowIndex] ? 'open.gif' : 'closed.gif'),
            show: (this.grid.expandedRows[inRowIndex] ? 'false' : 'true'),
            row: row};
    }

    elternInfo.toggle = function (inIndex, inShow) {
        elternInfo.schuelerGrid.expandedRows[inIndex] = inShow;
        elternInfo.schuelerGrid.updateRow(inIndex);
    };

    function formatCheck(value, inRowIndex) {
        if (value.row) {
            var kommentarBild = "comment-add-icon.png";
            if (value.row["Kommentar"] && value.row["Kommentar"][0].length > 0) {
                console.log("Kommentarlänge: " + value.row["Kommentar"][0].length + "    " + value.row["Kommentar"]);
                kommentarBild = "comment-add-icon-red.png";
            }
            var kommentarButton = elternInfo.istAdmin ? '<div class="lmf_kommentar_icon" onClick="elternInfo.kommentarBearbeiten(' + value.row["SchuelerId"] + ')"><img src="' + elternInfo.root + 'images/' + kommentarBild + '"  class="lmfKommentarIcon"></div>' : '';
            return '<img src="' + elternInfo.root + 'images/' + value.image + '" onclick="elternInfo.toggle(' + inRowIndex + ', ' + value.show + ')"  class="lmfToggleIcon">'
                    + kommentarButton;
        } else {
            return "";
        }

    }

    elternInfo.getSchüler = function (schuelerId) {
        console.log("start getSchüler: " + schuelerId);
        for (var i = 0; i < elternInfo.familyData.Schueler.length; i++) {
            if (elternInfo.familyData.Schueler[i]["SchuelerId"] == schuelerId) {
                console.log("return getSchüler: " + elternInfo.familyData.Schueler[i]);
                return elternInfo.familyData.Schueler[i];
            }
        }
        console.log("return getSchüler: null");
        return null;
    };

    function formatDetail(value, inRowIndex) {
        if (value) {
            var buecher = elternInfo.familyData.ausGelieheneBuecher["schueler_" + value];
            var ret = '';
            var kommentar = elternInfo.getSchüler(value)["Kommentar"][0];
            if (elternInfo.istAdmin && kommentar != null && kommentar.length > 0) {
                ret += '<div class="lmf_kommentar"><div><b>Kommentar:</b></div>';
                ret += "<div>" + kommentar + "</div>";
                ret += "</div>";
            }
            if (buecher) {
                ret += '<div class="ausgeliehenes"><b>ausgeliehene Bücher:</b><br>';
                for (var ii = 0; ii < buecher.length; ii++) {
                    if (!buecher[ii].eingesammelt) {
                        ret += '<div>';
                        ret += "<div class=lmf_buecher_in_liste>" + buecher[ii]["Isbn"] + ": " + buecher[ii]["Titel"] + " (" + buecher[ii]["Neupreis"] + " €)</div>";
                        ret += '<div class="lmf_left">';
                        if (elternInfo.istAdmin) {
                            ret += '<button type="button" class="lmf_buecher_button_ausgeliehen" onClick="elternInfo.buchEinsammeln(' + ii + ',' + value + ')" title="einsammeln" \n\
                            onmouseout="elternInfo.bildAendern(this,\'ausgeliehen\')" onMouseOver="elternInfo.bildAendern(this,\'einsammeln\')"><span> \n\ '
                                    + '</span>   </button>';
                        }
                        ret += '</div><div class="lmfClear"></div></div>';
                    }
                }
                ret += "</div>";
                ret += '<div class="ausgeliehenes"><b>zurückgegebene Bücher:</b><br>';
                for (var ii = 0; ii < buecher.length; ii++) {
                    if (buecher[ii].eingesammelt) {
                        ret += '<div>';
                        ret += "<div class=lmf_buecher_in_liste>" + buecher[ii]["Isbn"] + ": " + buecher[ii]["Titel"] + " (" + buecher[ii]["Neupreis"] + " €)</div>";
                        ret += '<div class="lmf_left">';
                        if (elternInfo.istAdmin) {
                            ret += '<button type="button" class="lmf_buecher_button_eingesammelt" onClick="elternInfo.buchAusgeben(' + ii + ',' + value + ')" title="wieder ausgeben" \n\
                            onmouseout="elternInfo.bildAendern(this,\'eingesammelt\')" onMouseOver="elternInfo.bildAendern(this,\'ausleihen\')"> \n\ '
                                    + '</button>';
                        }
                        ret += '</div><div class="lmfClear"></div></div>';
                    }
                }
                ret += "</div>";
                return ret;
            } else {
                return ret + '<div class="ausgeliehenes">keine ausgeliehenen Bücher';
            }
        }
        return value;
    }
    elternInfo.buchAusgeben = function (buchIndex, schuelerId) {
        einsammelnUndAusgeben(buchIndex, schuelerId, false);
    };
    elternInfo.buchEinsammeln = function (buchIndex, schuelerId) {
        einsammelnUndAusgeben(buchIndex, schuelerId, true);
    };

    function einsammelnUndAusgeben(buchIndex, schuelerId, einsammeln) {
        var buecher = elternInfo.familyData.ausGelieheneBuecher["schueler_" + schuelerId];
        var buchZumEinsammeln = buecher[buchIndex];
        var updateDaten = {
            type: "einsammelnUndAusgeben",
            schuelerId: schuelerId,
            buchId: buchZumEinsammeln.BuchId,
            eingesammelt: (einsammeln ? 1 : 0)
        };
        elternInfo.lmf_save(updateDaten, function () {
            buchZumEinsammeln.eingesammelt = einsammeln;
            elternInfo.schuelerGrid.render();
        });
    }
    function onBeforeRow(inDataIndex, inRow) {
        inRow[1].hidden = (!this.grid || !this.grid.expandedRows || !this.grid.expandedRows[inDataIndex]);
    }

    function setInlineEditBox(formElementId, jsObject) {
        var tmp;
        tmp = document.createElement('div');
        dojo.byId(formElementId).appendChild(tmp);
        try {
            formHalter[jsObject] = new dijit.InlineEditBox({
                /* editor: "dijit.form.Textarea", */
                autoSave: true,
                onChange: lmf_save_eltern,
                class: ""
            },
                    tmp
                    );
        } catch (error) {
            console.log(error);
        }
    }
    function setInlineCheckBox(formElementId, jsObject, value) {
        var tmp;
        tmp = document.createElement('div');
        dojo.byId(formElementId).appendChild(tmp);
        try {
            formHalter[jsObject] = new dijit.form.CheckBox({
                onChange: lmf_save_eltern,
                value: value,
                checked: value == 1 ? true : false
            },
                    tmp
                    );
        } catch (error) {
            console.log(error);
        }
    }

    elternInfo["schuelerFilter"] = {
        Vorname: "",
        Nachname: "",
        KlasseAlt: "",
        KlasseNeu: ""
    };
    elternInfo.schuelerFiltern = function () {
        if (elternInfo.schuelerFilter.Vorname != dojo.byId("lmf_filterVorname").value
                || elternInfo.schuelerFilter.Nachname != dojo.byId("lmf_filterNachname").value
                || elternInfo.schuelerFilter.KlasseAlt != dojo.byId("lmf_filterKlasseAlt").value
                || elternInfo.schuelerFilter.KlasseNeu != dojo.byId("lmf_filterKlasseNeu").value) {
            elternInfo.schuelerFilter = {
                Vorname: dojo.byId("lmf_filterVorname").value,
                Nachname: dojo.byId("lmf_filterNachname").value,
                KlasseAlt: dojo.byId("lmf_filterKlasseAlt").value,
                KlasseNeu: dojo.byId("lmf_filterKlasseNeu").value
            };
            elternInfo.schuelerGrid.filter({
                Vorname: "*" + dojo.byId("lmf_filterVorname").value + "*",
                Nachname: "*" + dojo.byId("lmf_filterNachname").value + "*",
                KlasseAlt: "*" + dojo.byId("lmf_filterKlasseAlt").value + "*",
                KlasseNeu: "*" + dojo.byId("lmf_filterKlasseNeu").value + "*"
            });
            console.log(elternInfo.schuelerGrid);
        }
    };
    elternInfo.familyData = {};
    console.log(elternInfo["schuelerFilter"]);
    elternInfo.getFamilyData = function (FamilienId, nurSchuelerRefresh, eis) {
        console.log(" elternInfo.getFamilyData: " + FamilienId);
        var allesNeu = true;
        if (nurSchuelerRefresh) {
            allesNeu = false;
        }
        var istLmf = eis ? false : true;
        if (istLmf) {
            elternInfo.appType = "lmf"; // initialisierung nach erneutem Login
        }
        console.log("getFamilyData");
        var _FamilienId = (FamilienId) ? FamilienId : "";
        dojo.xhrGet({
            url: elternInfo.root + "php/getFamilyData.php?type=stammdaten&FamilienId=" + _FamilienId + "&" + new Date().getTime(),
            handleAs: "json",
            load: function (response, ioArgs) {
                if (response.loggedIn != null && response.loggedIn == false) {
                    elternInfo.displayErrors(["Session ungültig. Bitte melden sie sich neu an"]);
                    elternInfo.loginSeiteLaden(["Session ungültig. Bitte melden sie sich neu an"], true);
                    return;
                }
                elternInfo.familyData = response;
                elternInfo.familyData.KlassenStringsAlt.unshift("-------");
                elternInfo.familyData.KlassenStrings.unshift("-------");
                elternInfo.familyData.KlassenKeysAlt.unshift(0);
                elternInfo.familyData.KlassenKeys.unshift(0);
                console.log(elternInfo.familyData);
                var styles = null;
                if (elternInfo.istAdmin) {
                    callServiceSite();
                }
                var SpalteEVanzeigen = elternInfo.istAdmin && elternInfo.alsAdmin;
                console.log(elternInfo);
                styles = elternInfo.style.tableSchuelerLmf;
                if (allesNeu) {
                    if (istLmf) {
                        elternInfo.getAusgeliehenes(_FamilienId);
                        layoutSchuelerGrid =
                                {defaultCell: {editable: false, styles: 'text-align: left;'},
                                    onBeforeRow: onBeforeRow,
                                    cells: [
                                        [
                                            {name: 'mehr', width: styles.mehr.width, get: getCheck, formatter: formatCheck, styles: styles.mehr.styles},
                                            {field: 'Vorname', name: 'Vorname', width: styles.Vorname.width, editable: true},
                                            {field: 'Nachname', name: 'Nachname', width: styles.Nachname.width, editable: true},
                                            {field: 'alteKlassenId', name: 'Klasse ' + (elternInfo.familyData.thisYear) + '/' + (1 + elternInfo.familyData.thisYear - 2000), width: styles.alteKlassenId.width, formatter: formatterTerminKlasseAlt, editable: true, type: dojox.grid.cells.Select, options: elternInfo.familyData.KlassenStringsAlt, values: elternInfo.familyData.KlassenKeysAlt},
                                            {field: 'KlassenId', name: 'Klasse ' + (1 + elternInfo.familyData.thisYear) + '/' + (2 + elternInfo.familyData.thisYear - 2000), width: styles.KlassenId.width, formatter: formatterTerminKlasseNeu, editable: true, type: dojox.grid.cells.Select, options: elternInfo.familyData.KlassenStrings, values: elternInfo.familyData.KlassenKeys},
                                            {field: '_item', name: "Aktionen", width: styles.Aktionen.width, styles: styles.Aktionen.styles, formatter: formatterChange},
                                            {field: 'istEV', name: 'istEV ', hidden: !SpalteEVanzeigen, width: styles.istEV.width, styles: styles.istEV.styles, editable: elternInfo.istAdmin, type: dojox.grid.cells.Bool}
                                        ],
                                        [{field: 'SchuelerId', name: 'Detail', colSpan: 8, formatter: formatDetail}]
                                    ]};
                    } else {
                        styles = elternInfo.style.tableSchuelerEis;
                        layoutSchuelerGridEis =
                                {defaultCell: {editable: false, styles: 'text-align: left;'},
                                    cells: [
                                        {field: 'Vorname', name: 'Vorname', width: styles.Vorname.width, editable: true},
                                        {field: 'Nachname', name: 'Nachname', width: styles.Nachname.width, editable: true},
                                        {field: 'alteKlassenId', name: 'Klasse ' + (elternInfo.familyData.thisYear) + '/' + (1 + elternInfo.familyData.thisYear - 2000), width: styles.alteKlassenId.width, formatter: elternInfo.formatterKlasseAlt, editable: true, type: dojox.grid.cells.Select, options: elternInfo.familyData.KlassenStringsAlt, values: elternInfo.familyData.KlassenKeysAlt},
                                        {field: 'KlassenId', name: 'Klasse ' + (1 + elternInfo.familyData.thisYear) + '/' + (2 + elternInfo.familyData.thisYear - 2000), width: styles.KlassenId.width, formatter: elternInfo.formatterKlasse, editable: true, type: dojox.grid.cells.Select, options: elternInfo.familyData.KlassenStrings, values: elternInfo.familyData.KlassenKeys},
                                        {field: '_item', name: "Aktionen", width: styles.Aktionen.width, styles: styles.Aktionen.styles, formatter: formatterKlassenliste},
                                        {field: 'istEV', name: 'istEV ', hidden: !SpalteEVanzeigen, width: styles.istEV.width, styles: styles.istEV.styles, editable: elternInfo.istAdmin, type: dojox.grid.cells.Bool}
                                    ]};
                    }
                    setInlineEditBox("lmfVorname", "lmf_Vorname");
                    setInlineEditBox("lmfNachname", "lmf_Nachname");
                    setInlineEditBox("lmfTelefon", "lmf_Telefon");
                    setInlineEditBox("lmfEmail", "lmf_Email");
                    setInlineEditBox("lmfAnschrift", "lmf_Anschrift");
                    setInlineCheckBox("lmf_Freigabe_ev", "lmf_freigabe_ev_CheckBox", 1 * response.eltern.Freigabe_ev);
                    setInlineCheckBox("lmf_Freigabe_alle", "lmf_freigabe_alle_CheckBox", 1 * response.eltern.Freigabe_alle);
                    setInlineCheckBox("lmf_Freigabe_info", "lmf_freigabe_info_CheckBox", 1 * response.eltern.Freigabe_info);
                    setInlineCheckBox("lmf_Freigabe_lmf", "lmf_freigabe_lmf_CheckBox", 1 * response.eltern.Freigabe_lmf);
                    setInlineCheckBox("lmf_stopMail", "lmf_stopMail_CheckBox", 1 * response.eltern.stopMail);
                    setInlineCheckBox("lmf_zuzahlungsBefreit", "lmf_zuzahlungsBefreit_CheckBox", 1 * response.eltern.zuzahlungsBefreit);
                    setInlineCheckBox("lmf_ist_Admin", "lmf_ist_Admin_CheckBox", 1 * response.eltern.isAdmin);
                    formHalter.lmf_Vorname.setValue(response.eltern.Vorname);
                    formHalter.lmf_Nachname.setValue(response.eltern.Nachname);
                    formHalter.lmf_Telefon.setValue(response.eltern.Telefon);
                    formHalter.lmf_Email.setValue(response.eltern.Email);
                    formHalter.lmf_Anschrift.setValue(response.eltern.Anschrift);
                }

                if (response.Schueler.length > 5) {
                    dojo.byId('lmf_schuelerFilter').style.display = 'block';
                    for (var i = 0; i < response.Schueler.length; i++) {
                        response.Schueler[i]["KlasseAlt"] = elternInfo.formatterKlasseAlt(response.Schueler[i]["KlassenId"]);
                        response.Schueler[i]["KlasseNeu"] = elternInfo.formatterKlasse(response.Schueler[i]["KlassenId"]);
                    }
                } else {
                    dojo.byId('lmf_schuelerFilter').style.display = 'none';
                }

                var data = {
                    identifier: 'SchuelerId',
                    items: response.Schueler
                };

                schuelerStore = new dojo.data.ItemFileWriteStore({data: data});
                dojo.connect(schuelerStore, "onSet", lmf_schuelerSpeichern);

                if (allesNeu) {
                    schuelerStore.comparatorMap = {};
                    schuelerStore.comparatorMap["KlassenId"] = function (a, b) {
                        var stringA = elternInfo.formatterKlasse(a);
                        var stringB = elternInfo.formatterKlasse(b);
                        if (a == b || stringA == stringB)
                            return 0;
                        var intA = 1 * stringA.substr(0, stringA.indexOf("-"));
                        var intB = 1 * stringB.substr(0, stringB.indexOf("-"));
                        if (intA > intB)
                            return 1;
                        if (intA < intB)
                            return -1;
                        if (elternInfo.formatterKlasse(a) > elternInfo.formatterKlasse(b))
                            return  1;
                        if (elternInfo.formatterKlasse(a) < elternInfo.formatterKlasse(b))
                            return -1;
                        return 0;
                    };
                    schuelerStore._forceLoad();
                    console.log(schuelerStore);
                    var _layout = layoutSchuelerGrid;
                    if (!istLmf) {
                        _layout = layoutSchuelerGridEis;
                    }
                    elternInfo.schuelerGrid = new dojox.grid.DataGrid({
                        store: schuelerStore,
                        selectable: true,
                        canEdit: function (cellColumn, rowIndex) {
                            if (!cellColumn.editable) {
                                return false;
                            } else {
                                if (cellColumn.field != 'alteKlassenId') {
                                    return true;
                                } else {
                                    if (elternInfo.alsAdmin || (cellColumn.field + "_" + rowIndex) === elternInfo.letzteErlaubteAenderung) {
                                        return true;
                                    }
                                    // Get the clicked cell value from the grid -- doesn't seem accessible with a property of the object passed in
                                    var grid = cellColumn.grid;
                                    var item = grid.getItem(rowIndex);
                                    var lmfAngemeldet = 1 * item["thisYear"][0] > 0;

                                    // Determine the condition for which it's not editable and return false
                                    if (lmfAngemeldet) {
                                        return false;
                                    } else {
                                        if (item["EisStatus"][0]) {
                                            if (confirm("Wenn sie die Klasse für dieses Jahr ändern, verlieren Sie Ihre Berechtigung, \n"
                                                    + "um im ElternInfoSystem die Kontaktliste der Klasse aufzurufen. \n"
                                                    + "Sie müssen dies beim neuen Elternvertreter neu beantragen.")) {
                                                elternInfo.letzteErlaubteAenderung = (cellColumn.field + "_" + rowIndex);
                                                return true;
                                            } else {
                                                return false;
                                            }
                                        } else {
                                            return true;
                                        }
                                    }
                                }
                            }
                        },
                        singleClickEdit: true,
                        clientSort: true,
                        autoRender: true,
                        structure: _layout,
                        autoWidth: true,
                        autoHeight: true
                    },
                            document.createElement('div')
                            );

                    dojo.byId("lmf_schueler").appendChild(elternInfo.schuelerGrid.domNode);
                    elternInfo.schuelerGrid.startup();
                    console.log(elternInfo.schuelerGrid);
                    neuerSchuelerSchaltflaeche = new dijit.form.Button({
                        label: "Kind hinzufügen",
                        onClick: function () {
                            schuelerHinzufuegen();
                        }
                    },
                            document.createElement('div')
                            );
                    dojo.byId("lmf_neuerSchuelerSchaltflaeche").appendChild(neuerSchuelerSchaltflaeche.domNode);
                } else {
                    elternInfo.schuelerGrid.store = schuelerStore;
                    elternInfo.schuelerGrid.render();
                }
            },
            error: function (response, ioArgs) {
                console.log(response);
                console.log("getFamilyData failed.");
            }
        });
    };

    elternInfo.getAusgeliehenes = function (FamilienId) {
        var _FamilienId = (FamilienId) ? FamilienId : "";
        console.log("getAusgeliehenes");
        dojo.xhrGet({
            url: elternInfo.root + "php/getFamilyData.php?type=buecher&FamilienId=" + _FamilienId + "&" + new Date().getTime(),
            handleAs: "json",
            sync: true,
            load: function (response, ioArgs) {
                console.log("onLoad ausgeliehen");
                console.log(response);
                elternInfo.familyData.ausGelieheneBuecher = response.ausGelieheneBuecher;
            },
            error: function (response, ioArgs) {
                console.log(response);
                console.log("getAusgeliehenes failed.");
            }
        });
    };

    elternInfo.starteRundmail = function (klassenId) {
        elternInfo.KlassenIdFuerEV = klassenId;
        if (typeof (elternInfo.elternRundmailDialogOeffnen) == "undefined") {
            elternInfo.loadJs("elternRundMail");
        } else {
            elternInfo.elternRundmailDialogOeffnen();
        }
    };

    elternInfo.kontaktListeAnzeigen = function (klassenId, berechtigt) {
        console.log("elternInfo.kontaktListeAnzeigen" + klassenId);
        if (berechtigt) {
            window.open(elternInfo.root + "php/elternInfoSystem.php?type=kontaktListeAnzeigen&klassenId=" + klassenId);
        } else {
            var data = {
                type: "listeEV",
                klassenId: klassenId
            };

            elternInfo.hideErrors();
            var xhrArgs = {
                url: elternInfo.root + "php/elternInfoSystem.php",
                preventCache: true,
                postData: dojo.toJson(data),
                handleAs: "json",
                headers: {"Content-Type": "application/json; charset=utf-8"},
                load: function (response) {
                    console.log("lmf_post EvKlasse Succes" + response);
                    if (!response.loggedIn) {
                        elternInfo.displayErrors(["Session ungültig. Bitte melden sie sich neu an"]);
                        elternInfo.loginSeiteLaden(["Session ungültig. Bitte melden sie sich neu an"], true);
                    } else if (response.errors && response.errors !== "") {
                        elternInfo.displayErrors(response.errors);
                    } else {
                        // Request war erfolgreich
                        console.log(response.data);
                        var infoText = "Sie wurden noch nicht für die Ansicht der Kontaktliste berechtigt. Das ist nur durch einen Elternvertreter der Klasse möglich.<br>";
                        if (response.data.length == 0) {
                            infoText += "Leider ist uns zur Zeit kein Elternvertreter für Ihre Klasse bekannt. Bitte wenden Sie sich auf anderem Weg an Ihre Elternsprecher um dieses Problem zu lösen.";
                        } else {
                            infoText += "Bitte geben Sie Ihrem Kind einen formlosen Zettel mit der Bitte um Freischaltung im ElternInfoSystem mit. ";
                            infoText += "Diesen Zettel soll es über deren Kinder an einen der Elternvertreter Ihrer Klasse weiterleiten.<br>";
                            infoText += "Zur Zeit sind uns folgende Elternvertreter für Ihre Klasse bekannt: <br><br>";
                            for (var i = 0; i < response.data.length; i++) {
                                infoText += "<i>&nbsp;&nbsp;&nbsp; " + response.data[i].Eltern + " (Schüler/in: " + response.data[i].Schueler + " )</i><br>";
                            }
                            infoText += "<br>Bitte haben Sie Verständnis für dieses etwas komplizierte Verfahren, aber nur so können wir sicher stellen, dass Ihre Daten auch wirklich nur für andere Eltern Ihrer Klasse sichtbar sind.<br>";
                        }
                        infoText += "";
                        elternInfo.displayErrors([infoText, "Teilnehmer am Lernmittelfonds werden zeitnah automatisch freigeschaltet."]);
                    }
                },
                error: function (error) {
                    console.log("lmf_post EvKlasse Error :" + error);
                    elternInfo.displayErrors([error]);
                }
            };
            dojo.xhrPost(xhrArgs);
        }
    };

    elternInfo.freiSchalten = function (klassenId) {
        elternInfo.KlassenIdFuerEV = klassenId;
        if (typeof (elternInfo.nichtAuthentifizierteElternOeffnen) == "undefined") {
            elternInfo.loadJs("elternInfoSystem");
        } else {
            elternInfo.nichtAuthentifizierteElternOeffnen();
        }
    };

    elternInfo.itemToJS = function (store, item) {
// summary: Function konvertiert ein dojo item in ein einfaches JS-Objekt.
// store: Der dojo datastore aus dem das item kommt
// item:  Das dojo item das konvertiert werden soll.
        var js = {};
        if (store !== null && store !== undefined && store.isItem(item)) {
            var attributes = store.getAttributes(item);
            if (attributes && attributes.length > 0) {
                var i;
                for (i = 0; i < attributes.length; i++) {
                    var values = store.getValues(item, attributes[i]);
                    if (values) {
                        //Bearbeitet multi-Value und Single-Value Attributes.
                        if (values.length > 1) {
                            var j;
                            js[attributes[i]] = [];
                            for (j = 0; j < values.length; j++) {
                                var value = values[j];
                                //Checkt dass der  Value kein anders item ist. Wenn alles OK, dann bearbeiten
                                if (store.isItem(value)) {
                                    js[attributes[i]].push(elternInfo.itemToJS(store, value));
                                } else {
                                    js[attributes[i]].push(value);
                                }
                            }
                        } else {
                            if (store.isItem(values[0])) {
                                js[attributes[i]] = elternInfo.itemToJS(store, values[0]);
                            } else {
                                js[attributes[i]] = values[0];
                            }
                        }
                    }
                }
            }
        }
        return js;
    };

    return elternInfo;
})(window.elternInfo || {});
