window.elternInfo = (function (elternInfo) {


    elternInfo.planungAnzeigen = function () {
        dojo.xhrGet({
            // The target URL on your webserver:
            url: elternInfo.root + "pages/planung.htm?" + new Date().getTime(),
            preventCache: true,
            handleAs: "text",
            // Event handler on successful call:
            load: function (response) {
                dojo.byId("lmf_applikation").innerHTML = response;
                console.log("fertig callPlanungSeite");
                onLoadPlanung();
            },
            // Event handler on errors:
            error: function (response, ioArgs) {
                console.log(response);
                elternInfo.displayErrors(["pages/planung.htm konnte nicht geladen werden"]);
            }
        });
    };


    var strings = {
        lmfBuchlistenErzeugen: {
            label: "Neue Bücherlisten erzeugen",
            onStart: "jetzt erzeugen",
            onDone: "zurück setzen"
        },
        lmfKlassenErzeugen: {
            label: "Neue Klassen erzeugen",
            onStart: "jetzt erzeugen",
            onDone: "zurück setzen"
        },
        lmfAnmeldungenFreigeben: {
            label: "Anmeldungen freigeben",
            onStart: "jetzt freigeben",
            onDone: "wieder sperren"
        },
        lmfSchlussTerminFestlegen: {
            label: "Zahlungsende festlegen",
            onStart: "jetzt festlegen",
            onDone: "später neu setzen"
        },
        lmfMailAnmelden: {
            label: "Mail zur Anmeldung versenden",
            onStart: "jetzt mailen",
            onDone: "zurück setzen"
        },
        lmfMailErinnern: {
            label: "Mail zur Teilnahmeerinnerung",
            onStart: "jetzt mailen",
            onDone: "zurück setzen"
        },
        lmfMailBezahlen: {
            label: "Mail zur Zahlungserinnerung",
            onStart: "jetzt mailen",
            onDone: "zurück setzen"
        },
        lmfMailBuchRueckgabe: {
            label: "Mahnungs-Mail zur Buchrückgabe",
            onStart: "jetzt mailen",
            onDone: "zurück setzen"
        },
        lmfMailBuchRueckgabeInfo: {
            label: "Info-Mail zur Buchrückgabe",
            onStart: "jetzt mailen",
            onDone: "zurück setzen"
        },
        lmfAnmeldungenStoppen: {
            label: "Anmeldungen (alt) stoppen",
            onStart: "jetzt stoppen",
            onDone: "wieder freigeben"
        },
        lmfNeuAnmeldungenStoppen: {
            label: "Anmeldungen (neu) stoppen",
            onStart: "jetzt stoppen",
            onDone: "wieder freigeben"
        },
        lmfCheckBuecherlisten: {
            label: "Bücherbestand prüfen",
            onStart: "jetzt prüfen",
            onDone: "zurück setzen"
        },
        lmfCheckKlassenwechsel: {
            label: "Klassenwechsel prüfen",
            onStart: "jetzt prüfen",
            onDone: "zurück setzen"
        },
        lmfAusgabelistenDrucken: {
            label: "Ausgabelisten drucken",
            onStart: "jetzt drucken",
            onDone: "zurück setzen"
        },
        lmfBuecherZuordnen: {
            label: "Bücher zuordnen",
            onStart: "jetzt zuordnen",
            onDone: "erneut zuordnen"
        }
    };

    function onLoadPlanung() {
        var postData = {
            "type": "ladePlanungsStand",
            _FamilienId: elternInfo.familyData.eltern.FamilienId
        };
        elternInfo.lmf_postService(postData,
                function (result) {
                    zeigePlanungsDaten(result.data);
                }
        );
    }

    function zeigePlanungsDaten(planungsStand) {
        if (dojo.byId(planungsStand[0].key)) { // wenn nicht wurde bereits zu anderer Ansicht gewechselt
            for (var i = 0; i < planungsStand.length; i++) {
                if (planungsStand[i].key != "lmfZahlungsTermin") {
                    var key = planungsStand[i].key;
                    dojo.byId(key).checked = 1 * planungsStand[i].intValue;
                    //console.log (key, strings[key].onDone,strings[key].onStart)
                    if (1 * planungsStand[i].intValue) {
                        dojo.byId(key + "Button").value = strings[key].onDone;
                    } else {
                        dojo.byId(key + "Button").value = strings[key].onStart;
                    }
                } else {
                    elternInfo.lmfZahlungsTermin = planungsStand[i].strValue;
                }
            }
            dojo.byId("lmfLegendPlanung").innerHTML = "Jahresplanung " + elternInfo.familyData.thisYear + "/" + (elternInfo.familyData.thisYear + 1);
        }
    }
    function setPlanungStand(planungsTyp, value) {
        var _value = null;
        if (value) {
            _value = value;
        }
        var postData = {
            "type": "setPlanungsStand",
            "value": _value,
            "planungsTyp": planungsTyp,
            _FamilienId: elternInfo.familyData.eltern.FamilienId
        };
        elternInfo.lmf_postService(postData,
                function (result) {
                    elternInfo.displayInfo("Planungsschritt erfolgreich durchgeführt");
                    zeigePlanungsDaten(result.data);
                    elternInfo.checkUndStartMailJobs();
                }
        );
    }

    function sendeStartAnmeldungMail() {
        sendeMail("anmeldeMail");
    }

    function sendeErinnerungsMail() {
        sendeMail("erinnerungsMail");
    }

    function sendeZahlungsErinnerungsMail() {
        sendeMail("zahlungErinnerungsMail");
    }

    function sendeBuchRueckgabeMail() {
        sendeMail("buchRueckgabeMail");
    }
    function sendeBuchRueckgabeInfoMail() {
        sendeMail("buchRueckgabeInfoMail");
    }

    function sendeMail(type) {
        elternInfo.addInfo("<br>Es werden jetzt die entsprechenden Mails zum Versand vorbereitet und später im Hintergrund verschickt. Bitte haben sie einen Moment Geduld!");
        var postData = {
            "type": type
        };
        elternInfo.lmf_postService(postData,
                function (result) {
                    console.log(result);
                    var minutesToWait = 0;
                    if (result.data) {
                        minutesToWait = Math.round(result.CALL_INTERVAL_ON_CRON /60 * (0.5 + result.data / result.MAILS_PER_CALL));
                    }
                    elternInfo.displayInfo(result.data + "Mails wurden erfolgreich vorbereitet. <br>Sie werden im Hintergrund verschickt solange Ihre Session läuft. Bitte beachten Sie, dass sich alle anderen Arbeitsprozesse in dieser Zeit etwas verlangsamen können. Es wird ca " + minutesToWait + " Minuten dauern.");
                    elternInfo.checkUndStartMailJobs();
                }
        );
    }

    function buecherZuSchuelernZuordnen() {
        var postData = {
            "type": "buecherZuSchuelernZuordnen",
            _FamilienId: elternInfo.familyData.eltern.FamilienId
        };
        elternInfo.lmf_postService(postData,
                function () {
                    dojo.byId("lmf_info").innerHTML = "Die Zuordnung der Bücher war erfolgreich. Bitte prüfen Sie dennoch exemplarisch.";
                    dojo.byId("lmf_info").style.display = "block";
                }
        );
    }


    function buchListenKopieren() {
        var postData = {
            "type": "buchListenKopieren",
            _FamilienId: elternInfo.familyData.eltern.FamilienId
        };
        elternInfo.lmf_postService(postData,
                function () {
                    dojo.byId("lmf_info").innerHTML = "Die Buchlisten des Vorjahres wurden erfolgreich kopiert. Sie können sie jetzt weiter bearbeiten.";
                    dojo.byId("lmf_info").style.display = "block";
                }
        );
    }

    function buchListenLoeschen() {
        var postData = {
            "type": "buchListenLoeschen",
            _FamilienId: elternInfo.familyData.eltern.FamilienId
        };
        elternInfo.lmf_postService(postData,
                function () {
                    dojo.byId("lmf_info").innerHTML = "Die Buchlisten des nächsten Jahres wurden vollständig gelöscht.";
                    dojo.byId("lmf_info").style.display = "block";
                }
        );
    }

    elternInfo.planung = function (planungsTyp) {
        if (!dojo.byId(planungsTyp + "Radio").checked) {
            console.log(dojo.byId("lmf" + planungsTyp));
            alert("Bitte aktivieren sie die Funktion in der ersten Spalte!");
            return;
        } else {
            dojo.byId(planungsTyp + "Radio").checked = false;
            dojo.byId("lmf_info").innerHTML = "Einen Moment bitte - ihr Auftrag wird bearbeitet.";
            dojo.byId("lmf_info").style.display = "block";
        }

        var valueBefore = dojo.byId(planungsTyp).checked;
        setPlanungStand(planungsTyp);
        switch (planungsTyp) {
            case "lmfAltesJahr":
                elternInfo.familyData.thisYear--;
                elternInfo.anmeldenFreigegeben = true;
                elternInfo.altAnmeldenGestoppt = true;
                elternInfo.neuAnmeldenGestoppt = true;
                break;
            case "lmfBuecherZuordnen":
                if (!valueBefore)
                    buecherZuSchuelernZuordnen();
                break;
            case "lmfKlassenErzeugen":
                elternInfo.callKlassenListe();
                break;
            case "lmfBuchlistenErzeugen":
                if (!valueBefore) {
                    if (confirm("Sollen die Bücherlisten des aktuellen Schuljahres als erster Schritt kopiert werden?")) {
                        buchListenKopieren();
                    }
                } else {
                    if (confirm("Sollen die bereits angelegten Bücherlisten des nächsten Jahres wieder gelöscht werden?")) {
                        buchListenLoeschen();
                    }
                }
                break;
            case "lmfSchlussTerminFestlegen":
                if (!valueBefore) {
                    var neuerTermin = prompt("Bitte bestätigen/korrigieren Sie den Termin,\nder den Eltern als spätest möglicher Termin für den Geldeingang genannt wird:\n"
                            , elternInfo.lmfZahlungsTermin);
                    if (neuerTermin != null && neuerTermin != elternInfo.lmfZahlungsTermin) {
                        // termin in DB aendern 
                        setPlanungStand("lmfZahlungsTermin", neuerTermin);
                    } else {
                        // nichts tun
                    }
                }
                break;
            case "lmfAnmeldungenFreigeben":
                elternInfo.anmeldenFreigegeben = !elternInfo.anmeldenFreigegeben;
                alert("Mit diesem Feld erlauben und sperren Sie Anmeldungen durch die Eltern. Es wirkt gemeinsam mit dem Feld 'Anmeldungen stoppen'");
                break;
            case "lmfMailAnmelden":
                if (!valueBefore)
                    if (confirm("Soll wirklich eine Mail versandt werden (OK), oder der Punkt nur abgehakt werden (Abbrechen)")) {
                        sendeStartAnmeldungMail();
                    }
                ;
                break;
            case "lmfMailErinnern":
                if (!valueBefore)
                    if (confirm("Soll wirklich eine Mail versandt werden (OK), oder der Punkt nur abgehakt werden (Abbrechen)")) {
                        sendeErinnerungsMail();
                    }
                break;
            case "lmfMailBezahlen":
                if (!valueBefore)
                    if (confirm("Soll wirklich eine Mail versandt werden (OK), oder der Punkt nur abgehakt werden (Abbrechen)")) {
                        sendeZahlungsErinnerungsMail();
                    }
                break;
            case "lmfAnmeldungenStoppen":
                elternInfo.altAnmeldenGestoppt = !elternInfo.altAnmeldenGestoppt;
                alert("Mit diesem Feld erlauben und sperren Sie Anmeldungen durch die Eltern, die letztes Jahr bereits angemeldet waren. Es wirkt gemeinsam mit dem Feld 'Anmeldungen freigeben'");
                break;
            case "lmfNeuAnmeldungenStoppen":
                elternInfo.neuAnmeldenGestoppt = !elternInfo.neuAnmeldenGestoppt;
                alert("Mit diesem Feld erlauben und sperren Sie neue Anmeldungen durch die Eltern. Es wirkt gemeinsam mit dem Feld 'Anmeldungen freigeben'");
                break;
            case "lmfMailBuchRueckgabe":
                if (!valueBefore)
                    if (confirm("Soll wirklich eine Mail versandt werden (OK), oder der Punkt nur abgehakt werden (Abbrechen)")) {
                        sendeBuchRueckgabeMail();
                    }
                break;
            case "lmfMailBuchRueckgabeInfo":
                if (!valueBefore)
                    if (confirm("Soll wirklich eine Mail versandt werden (OK), oder der Punkt nur abgehakt werden (Abbrechen)")) {
                        sendeBuchRueckgabeInfoMail();
                    }
                break;
            case "lmfCheckBuecherlisten":
                if (!valueBefore)
                    alert("Bitte überprüfen Sie die Bücherlisten und den Bestand und organisieren die Buchbestellung!");
                break;
            case "lmfCheckKlassenwechsel":
                if (!valueBefore)
                    alert("Bitte überprüfen Sie, ob während der Ferien noch Klassenwechsel vollzogen wurden!");
                break;
            case "lmfAusgabelistenDrucken":
                if (!valueBefore)
                    window.open(elternInfo.root + "php/buchAusgabeListen.php?_FamilienId=" + elternInfo.familyData.eltern.FamilienId);
                break;
            case "lmfNeuesJahr":
                elternInfo.familyData.thisYear++;
                elternInfo.anmeldenFreigegeben = false;
                elternInfo.altAnmeldenGestoppt = false;
                elternInfo.neuAnmeldenGestoppt = false;
                break;
            default:
                alert("Das was jetzt passiert ist, hat der Entwickler nicht vorgesehen. Wenn sie ihm das erzählen, wird er sich sehr wundern;-) !");
                break;
        }
    };

    elternInfo.planungAnzeigen();

    return elternInfo;
})(window.elternInfo || {});
