window.elternInfo = (function(elternInfo) {

    var indexEinzahlungToCheck = 0;
    var einzahlungenToCheck = null;
    var letzteEinzahlungsId = -1;
    elternInfo.zeigeCheckEinzahlungen = function() {
        console.log("callCheckEinzahlungen");
        elternInfo.hideErrors();
        dojo.byId("lmf_applikation").innerHTML = '<div id="lmf_service"><div id="lmf_serviceTitle">&nbsp;</div><div id="lmf_serviceContent">&nbsp;</div></div>';
        var data = {type: "CheckEinzahlungen", _FamilienId: elternInfo.familyData.eltern.FamilienId};
        elternInfo.lmf_postService(data,
                function(response) {
                    console.log(response);
                    einzahlungenToCheck = response.data;
                    indexEinzahlungToCheck = 0;
                    try {
                        einzahlungenAbgleichen();
                    } catch (error) {
                        console.log(error);
                    }
                });
    };

    function einzahlungenAbgleichen() {
        dojo.byId("lmf_serviceTitle").innerHTML = "Einzahlungen manuell abgleichen";
        if (einzahlungenToCheck.length > indexEinzahlungToCheck) {
            var einzahlung = einzahlungenToCheck[indexEinzahlungToCheck];
            letzteEinzahlungsId = einzahlung.einzahlungsId;
            var content = "";
            dojo.byId("lmf_serviceContent").innerHTML = "";
            content = '<div id="lmf_for_back_buttons"></div>';
            content += '<div class="lmf_innerTitle">gefundene Einzahlung: ' + einzahlung.Betrag + '</div>';
            content += '<div><div class="label">Buchung/Valuta: </div>' + einzahlung.Buchungstag + ' / ' + einzahlung.Valutatag + '</div>';
            content += '<div><div class="label">BLZ/Konto: </div>' + einzahlung.BLZ + ' / ' + einzahlung.Konto + '</div>';
            content += '<div><div class="label">Einzahler: </div>' + einzahlung.Absender + '</div>';
            content += '<div><div class="label">Verwendungzweck: </div>' + einzahlung.Verwendungszweck + '</div><br>';
            content += '<hr>';
            content += '<div class="lmf_innerTitle">mögliche Anmeldungen: </div>';
            content += '<div id="lmf_gridEinzahlungen"></div>';
            dojo.byId("lmf_serviceContent").innerHTML = content;
            var backButton = new dijit.form.Button({
                label: "vorherige",
                onClick: function() {
                    vorherigeEinzahlung();
                }
            },
            document.createElement('div')
                    );
            dojo.byId("lmf_for_back_buttons").appendChild(backButton.domNode);
            var cloneButton = new dijit.form.Button({
                label: "Klonen",
                onClick: function() {
                    EinzahlungKlonen();
                }
            },
            document.createElement('div')
                    );
            dojo.byId("lmf_for_back_buttons").appendChild(cloneButton.domNode);
            var upButton = new dijit.form.Button({
                label: "nächste",
                onClick: function() {
                    naechsteEinzahlung();
                }
            },
            document.createElement('div')
                    );
            dojo.byId("lmf_for_back_buttons").appendChild(upButton.domNode);
            zeigePassendeAnmeldungen();
        } else {
            dojo.byId("lmf_serviceContent").innerHTML = "keine Einzahlungen gefunden, die zugeordnet werden können.";
        }
    }

    function naechsteEinzahlung() {
        elternInfo.hideErrors();
        for (var i = indexEinzahlungToCheck; einzahlungenToCheck.length > i; i++) {
            var einzahlung = einzahlungenToCheck[i];
            var einzahlungsId = einzahlung.einzahlungsId;
            if (letzteEinzahlungsId != einzahlungsId) {
                indexEinzahlungToCheck = i;
                einzahlungenAbgleichen();
                return true;
            }
        }
        alert("das ist die letzte Einzahlung");
        return false;
    }

    function vorherigeEinzahlung() {
        elternInfo.hideErrors();
        if (indexEinzahlungToCheck >= einzahlungenToCheck.length) {
            indexEinzahlungToCheck = einzahlungenToCheck.length - 1;
        }
        for (var i = indexEinzahlungToCheck; i >= 0; i--) {
            var einzahlung = einzahlungenToCheck[i];
            var einzahlungsId = einzahlung.einzahlungsId;
            if (letzteEinzahlungsId != einzahlungsId) {
                indexEinzahlungToCheck = i;
                einzahlungenAbgleichen();
                return true;
            }
        }
        alert("das ist die erste Einzahlung");
        return false;
    }
    function EinzahlungKlonen() {
        elternInfo.hideErrors();
        var data = {
            type: "EinzahlungKlonen",
            einzahlungsId: einzahlungenToCheck[indexEinzahlungToCheck].einzahlungsId
        };
        elternInfo.lmf_postService(data, function() {
            alert("Klonen war erfolgreich. Bitte den Menüpunkt 'Einzahlungen zuordnen' jetzt neu aufrufen, um die neue Einzahlung zu sehen");
        });
        return false;
    }

    function einzahlung_ausgewaehlt(_row, _rowIndex, _grid, _button) {
        console.log("einzahlung_ausgewaehlt: ");
        if (_row.anmeldungsId && !_row.anmeldungsId[0]) { //das Feld wird als weiteres Tag missbraucht, weil es nicht benötigt wird
            alert("Die Auswahl wird nur bestätigt, wenn auch die Auswahlbox in der ersten Spalte aktiviert ist.");
            return;
        }
        elternInfo.hideErrors();
        console.log(_row);
        var data = {
            type: "EinzahlungZuordnen",
            _FamilienId: elternInfo.familyData.eltern.FamilienId,
            einzahlungsId: _row.einzahlungsId[0],
            anmeldungsId: _row.anMeldId[0],
            absender: _row.Absender[0],
            Absender: _row.Absender[0],
            BLZ: _row.BLZ[0],
            Konto: _row.Konto[0],
            Betrag: _row.Betrag[0],
            VWZ: _row.Verwendungszweck[0],
            Buchungstag: _row.Buchungstag[0]
        };
        elternInfo.lmf_postService(data, function(response) {
            console.log(response);
            zugeordneteEinzahlungEntfernen(_row.einzahlungsId[0], _row.anMeldId[0]);
            var successMessage = "Einzahlung erfolgreich zugeordnet";
            elternInfo.displayInfo(successMessage);
        });
    }

    function zugeordneteEinzahlungEntfernen(_einzahlungsId, _anmeldeId) {
        for (var i = einzahlungenToCheck.length - 1; i >= 0; i--) {
            if (einzahlungenToCheck[i].einzahlungsId == _einzahlungsId
                    || einzahlungenToCheck[i].anMeldId == _anmeldeId) {
                einzahlungenToCheck.splice(i, 1);
            }
        }
        checkStore.fetch({
            onComplete: function(items) {
                dojo.forEach(items, function(item, index) {
                    if (checkStore.isItem(item)) {
                        if (checkStore.getValue(item, "einzahlungsId") == _einzahlungsId
                                || checkStore.getValue(item, "anMeldId") == _anmeldeId) {
                            checkStore.deleteItem(item);
                        }
                    }
                });
            }
        });
        checkGrid.render();
    }

    var buttonFormatter = function(_row, _rowIndex, _grid) {
        var new_button = new dijit.form.Button({
            label: 'Auswahl',
            showLabel: true,
            onClick: function() {
                einzahlung_ausgewaehlt(_row, _rowIndex, _grid, this);
            },
            'class': 'gridButton'});
        new_button._destroyOnRemove = true;
        return new_button;
    };


    var layoutCheckGrid = null;
    var checkStore = null;
    var checkGrid = null;
    function zeigePassendeAnmeldungen() {
        if (layoutCheckGrid === null) {
            var styles = elternInfo.style.tableEinzahlungenZuordnen;
            layoutCheckGrid = [{
                    defaultCell: {editable: false, styles: styles.default.styles},
                    cells: [
                        {field: 'anmeldungsId', name: " ", width: styles.check.width, styles: styles.check.styles, editable: true, type: dojox.grid.cells.Bool},
                        {field: 'elternName', name: "Mutter / Vater", width: styles.Eltern.width},
                        {field: 'schuelerName', name: "Schüler", width: styles.Schueler.width},
                        {field: 'KlassenId', name: 'Klasse ' + (1 + elternInfo.familyData.thisYear) + '/' + (2 + elternInfo.familyData.thisYear - 2000),
                            width: styles.Klasse.width, formatter: elternInfo.formatterKlasse, type: dojox.grid.cells.Select, options: elternInfo.familyData.KlassenStrings, values: elternInfo.familyData.KlassenKeys},
                        {field: 'ZahlungsKey', name: "Schlüssel", width: styles.ZahlungsKey.width},
                        {field: '_item', name: "Bestätigen", width: styles.Bestaetigen.width, formatter: buttonFormatter}
                    ]
                }];
        }
        var data = {items: dojo.clone(einzahlungenToCheck)};
        checkStore = new dojo.data.ItemFileWriteStore({
            data: data
        });
        checkStore._forceLoad();
        if (checkGrid === null) {
            checkGrid = new dojox.grid.DataGrid(
                    {
                        query: {einzahlungsId: letzteEinzahlungsId},
                        store: checkStore,
                        selectable: true,
                        clientSort: true,
                        autoRender: true,
                        structure: layoutCheckGrid,
                        autoWidth: true,
                        autoHeight: true
                    },
            document.createElement('div')
                    );
        } else {
            checkGrid.setStore(checkStore);
            checkGrid.setQuery({"einzahlungsId": letzteEinzahlungsId});
        }
        dojo.byId("lmf_gridEinzahlungen").appendChild(checkGrid.domNode);
        // Call startup, in order to  render the grid: 
        checkGrid.startup();
        checkGrid.render();
    }

    dojo.require("dojo.io.iframe");
    var uploadErrors = [];
    elternInfo.zeigeUploadEinzahlungen = function() {
        uploadErrors = [];
        dojo.byId("lmf_applikation").innerHTML = '<div id="lmf_service"><div id="lmf_serviceTitle">&nbsp;</div><div id="lmf_serviceContent">&nbsp;</div></div>';
        var content = '<form action="' + elternInfo.root + 'php/upload_zahlungen.php" method="post" enctype="multipart/form-data" name="uploadEinzahlungenForm" id="uploadEinzahlungenForm">'
                + 'Bitte CSV-File mit Einzahlungen zum Upload auswählen: <br />'
                + '<input name="csv" type="file" id="csv" />'
                + '  <input type="submit" name="Submit" value="Submit" />'
                + '</form>';
        dojo.byId("lmf_serviceContent").innerHTML = content;
        var form = dojo.byId("uploadEinzahlungenForm");
        dojo.connect(form, "onsubmit", function(event) {
            // Stop the submit event since we want to control form submission.
            dojo.stopEvent(event);
            elternInfo.hideErrors();
            // The parameters to pass to xhrPost, the form, how to handle it, and the callbacks.
            // Note that there isn't a url passed.  xhrPost will extract the url to call from the form's
            //'action' attribute.  You could also leave off the action attribute and set the url of the xhrPost object
            // either should work.

            dojo.io.iframe.send({
                form: 'uploadEinzahlungenForm',
                handleAs: "json",
                load: function(data) {
                    console.log(data);
                    if (data.errors.length > 0) {
                        uploadErrors = data.errors;
                        elternInfo.displayErrors(["Es wurden " + data.count + " Datensätze gefunden<br>. "
                                    + "Davon wurden " + data.successLines + " Datensätze in die Datenbank übernommen<br>. "
                                    + "Es wurden " + data.successMatch + " Einzahlungen eindeutig Anmeldungen zugeordnet<br>. "
                                    + data.errors.length + " Datensätze wurde nicht in die Datenbank übertragen. Eventuell wurden sie schon früher gespeichert? "
                                    + "Klicken Sie <a onclick='uploadFehler();' href='#'>hier</a>, wenn Sie wissen wollen, welche Datensätze nicht gespeichert wurden."]
                                );
                    } else {
                        elternInfo.displayInfo("Es konnten alle " + data.successLines + " Datensätze erfolgreich gespeichert werden.<br>"
                                + "Es wurden " + data.successMatch + " Einzahlungen eindeutig Anmeldungen zugeordnet. ");
                    }
                },
                error: function(error) {
                    console.log(error);
                    console.log("upload Einzahlungen failed.");
                    elternInfo.displayErrors(["Der Upload der Einzahlung ist gescheitert. Bitte laden Sie die Seite neu, melden sich neu an und versuchen es erneut. Bei wiederholtem Scheitern informieren Sie bitte den Administrator."]);
                }
            });
        });
    };

    function uploadFehler() {
        var text = "Nicht gespeicherte Datensätze: \n";
        for (var i = 0; i < uploadErrors.length; i++) {
            text += uploadErrors[i] + "\n";
        }
        alert(text);
    }

    elternInfo.zeigeOffeneEinzahlungen = function() {
        console.log("zeigeOffeneEinzahlungen");
        elternInfo.hideErrors();
        dojo.byId("lmf_applikation").innerHTML = '<div id="lmf_service"><div id="lmf_serviceTitle">&nbsp;</div><div id="lmf_serviceContent">&nbsp;</div></div>';
        var data = {type: "offeneEinzahlungen"};
        elternInfo.lmf_postService(data,
                function(response) {
                    console.log(response);
                    var styles = elternInfo.style.tableOffeneEinzahlungen;
                    var layoutOffeneEinzahlungen = [{
                            defaultCell: {editable: false, styles: styles.default.styles},
                            cells: [
                                {field: 'check', name: " ", width: styles.check.width, styles: styles.check.styles, editable: true, type: dojox.grid.cells.Bool},
                                {field: 'Valutatag', width: styles.Valutatag.width},
                                {field: 'Betrag', width: styles.Betrag.width, styles: styles.Betrag.styles},
                                {field: 'Verwendungszweck', width: styles.Verwendungszweck.width},
                                {field: 'Absender', width: styles.Absender.width},
                                {field: '_item', name: "Verwerfen", width: styles.Verwerfen.width, formatter: buttonVerwerfen}
                            ]
                        }];
                    var data = {items: dojo.clone(response.data)};
                    var offeneEinzahlungenStore = new dojo.data.ItemFileWriteStore({
                        data: data
                    });
                    offeneEinzahlungenStore._forceLoad();
                    var offeneEinzahlungenGrid = new dojox.grid.DataGrid(
                            {
                                store: offeneEinzahlungenStore,
                                selectable: true,
                                clientSort: true,
                                autoRender: true,
                                structure: layoutOffeneEinzahlungen,
                                autoWidth: true,
                                autoHeight: true
                            },
                    document.createElement('div')
                            );
                    dojo.byId("lmf_serviceContent").innerHTML = "";
                    dojo.byId("lmf_serviceContent").appendChild(offeneEinzahlungenGrid.domNode);
                    // Call startup, in order to  render the grid: 
                    offeneEinzahlungenGrid.startup();
                    offeneEinzahlungenGrid.render();
                });
    };

    var buttonVerwerfen = function(_row, _rowIndex, _grid) {
        var new_button = new dijit.form.Button({
            label: 'und weg',
            showLabel: true,
            onClick: function() {
                einzahlungVerwerfen(_row, _rowIndex, _grid, this);
            },
            'class': 'gridButton'});
        new_button._destroyOnRemove = true;
        return new_button;
    };

    function einzahlungVerwerfen(_row, _rowIndex, _grid, button) {
        console.log("einzahlungVerwerfen: ");
        if (_row.check == undefined || _row.check == null || !_row.check[0]) {
            alert("Die Auswahl wird nur bestätigt, wenn auch die Auswahlbox in der ersten Spalte aktiviert ist.");
            return;
        }
        elternInfo.hideErrors();
        console.log(_row);
        var data = {
            type: "einzahlungVerwerfen",
            einzahlungsId: _row.einzahlungsId[0]
        };
        elternInfo.lmf_postService(data, elternInfo.zeigeOffeneEinzahlungen);
    }

    elternInfo.rufeNaechsteFunktion();

    return elternInfo;
})(window.elternInfo || {});
