window.elternInfo = (function(elternInfo) {
    var lmfSelectKlasse = null;
    var lmfAusleiheAktualisieren = null;

    elternInfo.klassenUndSchuelerVerwalten = function() {
        dojo.require("dijit.form.Select");
        elternInfo.hideErrors();
        dojo.byId("lmf_applikation").innerHTML
                = '<div id="lmf_service">\n\
                  <div id="lmf_serviceTitle">&nbsp;</div>\n\
                  <div id="lmf_serviceContent1">&nbsp;</div>\n\
                  <div id="lmf_serviceContent2">&nbsp;</div>\n\
              </div>';
        dojo.byId("lmf_serviceTitle").innerHTML = 'Klassen und Schüeler';
        dojo.byId("lmf_serviceContent1").innerHTML = '<div class="elements">'
                + '     <div class="label">Klasse auswählen:</div>'
                + '     <div id="lmf_KlasseFuerSchueler"></div>'
                + '</div>';
        var postData = {
            "type": "db",
            table: "view_klassen_altes_jahr",
            _FamilienId: elternInfo.familyData.eltern.FamilienId
        };
        elternInfo.lmf_postService(postData,
                function(response, ioArgs) {
                    console.log(response);
                    var _options2 = [{label: "------------", value: -1}];
                    for (i = 0; i < response.data.length; i++) {
                        var k = response.data[i];
                        _options2.push({label: k.Stufe + k.SubKlasse + "-" + k.Sprache, value: k.KlassenId});
                    }
                    console.log(elternInfo.familyData);
                    if (lmfSelectKlasse == null) {
                        lmfSelectKlasse = new dijit.form.Select({
                            id: 'lmfSelectKlasse',
                            name: 'lmfSelectKlasse',
                            style: 'margin-top: -0.3em',
                            options: _options2
                        });
                    }
                    lmfSelectKlasse.placeAt(dojo.byId("lmf_KlasseFuerSchueler"));
                    if (lmfKlasseAktualisieren == null) {
                        lmfKlasseAktualisieren = new dijit.form.Button({
                            id: 'lmfKlasseAktualisieren',
                            label: "Aktualisieren",
                            style: 'margin: -0.3em 0 0 20px',
                            onClick: function(evt) {
                                klassenAnzeigeAktualisieren();
                                dojo.stopEvent(evt);
                            }
                        });
                    }
                    ;
                    lmfAusleiheAktualisieren.placeAt(dojo.byId("lmfSelectKlasse"), "after");
                });
    };

    var schueler;
    var klassenVerwaltenStore = null;
    var klassenVerwaltenGrid = null;
    var layoutKlassenVerwaltenGrid = null;
    function klassenAnzeigeAktualisieren() {
        var klassenId = dijit.byId("lmfSelectKlasse").getValue();
        if (klassenId == -1) {
            return;
        }
        console.log("klassenid: " + klassenId);
        dojo.xhrGet({
            // The target URL on your webserver:
            url: elternInfo.root + "php/lmf-service.php?type=SchuelerInKlasse&klassenId=" + klassenId
                    + "&_FamilienId=" + elternInfo.familyData.eltern.FamilienId,
            preventCache: true,
            handleAs: "json",
            // Event handler on successful call:
            load: function(response, ioArgs) {
                console.log(response);
                schueler = response.data.schueler;
                layoutKlassenVerwaltenGrid = [
                    {defaultCell: {editable: false, styles: 'text-align: left;'},
                        cells: [
                            [
                                {field: 'name', name: "Schueler", width: elternInfo.style.tableAusleiheVerwalten.schueler.width,editable: false },
                                {field: 'alteKlassenId', name: 'Klasse ' + (elternInfo.familyData.thisYear) + '/' + (1 + elternInfo.familyData.thisYear - 2000), width: styles.alteKlassenId.width, formatter: elternInfo.formatterKlasseAlt, editable: true, type: dojox.grid.cells.Select, options: elternInfo.familyData.KlassenStringsAlt, values: elternInfo.familyData.KlassenKeysAlt},
                                {field: 'KlassenId', name: 'Klasse ' + (1 + elternInfo.familyData.thisYear) + '/' + (2 + elternInfo.familyData.thisYear - 2000), width: styles.KlassenId.width, formatter: elternInfo.formatterKlasse, editable: true, type: dojox.grid.cells.Select, options: elternInfo.familyData.KlassenStrings, values: elternInfo.familyData.KlassenKeys}
                            ]
                        ]}
                ];

                console.log(layoutKlassenVerwaltenGrid);
                var data = {
                    identifier: 'schuelerId',
                    items: response.data.schueler
                };
                console.log(data);
                klassenVerwaltenStore = new dojo.data.ItemFileWriteStore({data: data});
                dojo.connect(klassenVerwaltenStore, "onSet", lmf_schuelerSpeichern);
                klassenVerwaltenStore._forceLoad();
                console.log(klassenVerwaltenStore);
                klassenVerwaltenGrid = new dojox.grid.DataGrid({
                    store: klassenVerwaltenStore,
                    selectable: true,
                    singleClickEdit: true,
                    clientSort: true,
                    autoRender: true,
                    structure: layoutKlassenVerwaltenGrid,
                    width: "100%",
                    autoHeight: true
                },
                document.createElement('div')
                        );
                dojo.byId("lmf_serviceContent2").innerHTML = "";
                dojo.byId("lmf_serviceContent2").appendChild(klassenVerwaltenGrid.domNode);
                klassenVerwaltenGrid.startup();
                var showTooltip = function(e) {
                    var msg;
                    if (e.rowIndex < 0) { // header
                        msg = e.cell._titel;
                    } else {
                        var item = e.grid.getItem(e.rowIndex);
                        msg = e.grid.store.getValue(item, e.cell.field);
                    }
                    ;
                    if (msg) {
                        dijit.showTooltip(msg, e.cellNode);
                    }
                };
                var hideTooltip = function(e) {
                    dijit.hideTooltip(e.cellNode);
                };
                dojo.connect(klassenVerwaltenGrid, "onHeaderCellMouseOver", showTooltip);
                dojo.connect(klassenVerwaltenGrid, "onHeaderCellMouseOut", hideTooltip);
            },
            // Event handler on errors:
            error: function(response, ioArgs) {
                console.log(response);
                console.log("ausleiheAnzeigeAktualisieren failed.");
            }
        });

    }
    function lmf_schuelerSpeichern(schuelerDaten, param, alterWert, neuerWert) {
        console.log("start lmf_schuelerSpeichern: " + param + "=" + neuerWert);
        console.log(schuelerDaten);
        if (alterWert !== neuerWert) {
            transferData = elternInfo.itemToJS(schuelerStore, schuelerDaten);
            transferData.type = "schuelerSpeichern";
            if (param === "KlassenId") {
                transferData.subType = "anmeldungNaechstesJahr";
            }
            if (param === "alteKlassenId") {
                transferData.subType = "anmeldungDiesesJahr";
                transferData.KlassenId = transferData.alteKlassenId;
            }
            elternInfo.lmf_save(transferData, function() {
                elternInfo.getFamilyData(transferData.FamilienId, true);
                elternInfo.displayInfo("Klasse erfolgreich geändert. ");
            });
        }
    }
    
    elternInfo.klassenUndSchuelerVerwalten();

    return elternInfo;
})(window.elternInfo || {});
